import { useState } from "react";
import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { TabList, TabPanel, TabContext } from "@mui/lab";
import {
  Accordion,
  Box,
  Grid,
  IconButton,
  Tab,
  Typography,
} from "@mui/material";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));

import axiosConfig, { localeHeaderConfig } from "../config/axios";

export default function FAQ({ faqData }) {
  const { t } = useTranslation("faq_page");

  const [value, setValue] = useState("1");
  const [expanded, setExpanded] = useState("1");

  const accordionHandler = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const tabsHandler = (event, newValue) => {
    setExpanded(null);
    setValue(newValue);
  };

  return (
    <>
      <Head>
        <title>{t("page_title")}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t("page_title")} />
        <meta property="og:title" content={t("page_title")} />
        <meta name="twitter:title" content={t("page_title")} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar
          sx={{ borderBottom: "1px solid #eaeaea", boxShadow: "none" }}
        />
        <SectionContainer>
          <Grid item p="30px 0" xs={12}>
            <Typography
              component="h1"
              sx={{
                fontSize: { xs: "6vw", sm: "4vw", md: "3vw" },
                fontWeight: "bold",
                color: "text.medium",
              }}
            >
              {t("big_title")}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <TabContext value={value}>
              <Box>
                <TabList
                  onChange={tabsHandler}
                  variant="scrollable"
                  scrollButtons="auto"
                >
                  {faqData.map(({ groupTitle, idGroup }) => (
                    <Tab
                      key={idGroup}
                      label={groupTitle}
                      value={idGroup.toString()}
                    />
                  ))}
                </TabList>
              </Box>

              {faqData.map(({ faqs, idGroup }) => (
                <TabPanel
                  sx={{ padding: "24px 0" }}
                  key={idGroup}
                  value={idGroup.toString()}
                >
                  <Box width="100%">
                    {faqs.map(({ id, headTitle, title }) => (
                      <Box key={id}>
                        <Accordion
                          sx={{
                            boxShadow: "none",
                          }}
                          expanded={expanded === id.toString()}
                          onChange={accordionHandler(id.toString())}
                        >
                          <AccordionSummary
                            id={id.toString()}
                            sx={{ borderBottom: "1px solid #c0c2c2" }}
                          >
                            <Typography
                              variant="h6"
                              sx={{
                                flex: "1 1 100%",
                                whiteSpace: "break-spaces",
                                fontSize: 20,
                                color: "#707070",
                                fontWeight: "normal",
                              }}
                            >
                              {headTitle}
                            </Typography>

                            <IconButton
                              sx={{
                                backgroundColor: "rgba(192,194,194,.26)",
                                borderRadius: "5px",
                                maxHeight: 33,
                              }}
                              size="small"
                            >
                              <ArrowDropDownIcon
                                sx={{
                                  transform: "scale(1.5)",
                                  color: "#ABABAB",
                                }}
                              />
                            </IconButton>
                          </AccordionSummary>
                          <MuiAccordionDetails>
                            <Typography
                              variant="h6"
                              color="common.black"
                              fontSize={16}
                            >
                              {title}
                            </Typography>
                          </MuiAccordionDetails>
                        </Accordion>
                      </Box>
                    ))}
                  </Box>
                </TabPanel>
              ))}
            </TabContext>
          </Grid>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  const faqData = await axiosConfig.get("/Faqs", localeHeaderConfig(locale));
  return {
    props: {
      faqData: faqData.data,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "faq_page",
      ])),
    },
  };
}
