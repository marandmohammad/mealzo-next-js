import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { Typography, Grid, Button } from "@mui/material";

const PageContainer = dynamic(() => import("../src/containers/Page.containers"))
const MainFooter = dynamic(() => import("../src/footers/Main.footers"))
const GetToKnowUsAppBar = dynamic(() => import("../src/appbars/StaticPage.appbars"))
const StaticPageHeader = dynamic(() => import("../src/headers/StaticPage.headers"))
const SectionContainer = dynamic(() => import("../src/containers/Section.containers"))
const Link = dynamic(() => import("../src/links/Link"))
const ZohoSalesIQ = dynamic(() => import("../src/ZohoSalesIQ"))

import SupportHeaderImage from "../public/Images/support.png";

export default function help() {
  const { t } = useTranslation('help_page');

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <GetToKnowUsAppBar />
        <StaticPageHeader
          title={t('page_header')}
          subtitle={t('page_header_subtitle')}
          imageUrl={SupportHeaderImage}
        />
        <SectionContainer textAlign="center">
          <Grid item mb={2} xs={12}>
            <Typography variant="h6" component="h3">
              {t('we_love_help')}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle1" component="div" fontSize={20}>
              {t('text_1')}
            </Typography>
            <Typography variant="subtitle1" component="div" fontSize={20}>
              {t('text_2')}
            </Typography>
            <Typography
              variant="subtitle1"
              component="div"
              fontSize={20}
              color="primary.main"
            >
              info@mealzo.co.uk
            </Typography>
            <Link href="/faq">
              <Button variant="contained" sx={{ mt: 4 }}>
                {t('customer_faq_btn')}
              </Button>
            </Link>
          </Grid>
        </SectionContainer>
        <ZohoSalesIQ />
      </PageContainer>
      <MainFooter />
    </>
  );
}


export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "help_page",
      ])),
    },
  };
}