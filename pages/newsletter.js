import dynamic from "next/dynamic";
import Head from "next/head";
import { Suspense } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Button, Grid, Typography } from "@mui/material";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));

const NewsletterCard = dynamic(() => import("../src/cards/Newsletter.cards"), {
  suspense: true,
});

import Link from "../src/links/Link";

export default function NewsLetter() {
  const pageTitle = "Newsletter - Mealzo";

  return (
    <>
      <Head>
        <title>{pageTitle}</title>
        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={pageTitle} />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={pageTitle} />
        <meta name="twitter:title" content={pageTitle} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
        <meta itemProp="image" content="Images/header.jpg" />
        <meta property="og:image" content="Images/header.jpg" />
        <meta name="twitter:card" content="Images/header.jpg" />
        <meta name="twitter:image" content="Images/header.jpg" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <Grid container position="relative" justifyContent="stretch">
          <img
            src={"/Images/newsletter/newsletterheader.jpg"}
            alt="mealzo newsletter header"
            width="100%"
          />
        </Grid>

        <SectionContainer
          sx={{
            maxWidth: 1200,
            mx: "auto",
            flexDirection: "column",
            rowGap: 10,
          }}
        >
          <Suspense>
            {/* first typography */}
            <Typography color="black" fontSize={26}>
              Dear partners,
              <br />
              <br />
              We’re thrilled to unveil our refreshed brand identity, our company
              WeeTech is being rebranded onto the Mealzo brand. This is an
              exciting step towards a brighter future for our company, and we
              are looking forward to building on the success we have had so far.
              <br />
              <br />
              The updates show the evolution of our company since its founding
              in 2008. While this is a significant change, our core beliefs
              remain the same.
              <br />
              <br />
              We are looking forward to continuing to work with our partners and
              to building an even stronger relationship for the Mealzo family in
              the coming year.
              <br />
              <br />
              Kind Regards,
              <br />
              Pedro Peroz
            </Typography>

            {/* mealzo new year image */}
            <img
              src="/Images/newsletter/newyear.jpg"
              alt="mealzo new year"
              width="100%"
            />

            <NewsletterCard
              imageUrl="/Images/newsletter/mealzodelivery.jpg"
              title="Mealzo Delivery Fleet!"
              text="Mealzo Delivery Fleet!
            Yes, you heard it here first…
            Mealzo Riders will be on the road in 2023, offering our partners new opportunities to save on the cost of delivering delicious food around their local area. Keep an eye out on the road for our new Mealzo Delivery Fleet coming soon!"
            />

            <NewsletterCard
              imageUrl="/Images/newsletter/localhero.jpg"
              title="Local Hero"
              subtitle="Join the Local Hero Initiative and Give Back to Your Community in 2023"
              text="One of our proudest achievements has been the success of our Local Hero of the Month Initiative, where we have partnered with our takeaway and restaurant partners to give back to the local community through charity events. Several partners from all across Scotland have participated in this initiative helping thousands of people in need."
              ButtonComponent={
                <Grid container justifyContent="center" mt={5}>
                  <Button
                    variant="contained"
                    LinkComponent={Link}
                    href="/local-heroes"
                    sx={{
                      fontSize: 26,
                      minWidth: 90,
                      borderRadius: "11px",
                      p: "37px 63px",
                    }}
                  >
                    Find out more
                  </Button>
                </Grid>
              }
              reverse
            />

            <NewsletterCard
              imageUrl="/Images/newsletter/nationwide.jpg"
              title="Nationwide advertising"
              subtitle="Participating in various sponsorships and looking forward to even more in the New Year"
              text="We have also had the pleasure of participating in various sponsorships throughout the year, such as the Edinburgh Restaurant Awards and Glasgow Pride and many more. Looking forward to the New Year we cannot wait to get involved with even more great charities and exciting events."
            />

            <NewsletterCard
              imageUrl="/Images/newsletter/cardtermial.jpg"
              title="Introducing the MealzoCardTerminal"
              subtitle="Unbeatable Transaction Rates and Enhanced Payment Processing"
              text="We are thrilled to announce that we will be launching the Mealzo Card Terminal with unbeatable transaction rates later this year, which will make it even easier and safer for our partners to process payments."
              reverse
            />

            <NewsletterCard
              imageUrl="/Images/newsletter/offergrocery.jpg"
              title="Mealzo to offer grocery shopping in 2023"
              text="Another exciting development is that we are looking to expand our services into the grocery shopping sector in 2023. Soon, you will be able to shop for groceries on our portal, making Mealzo a one-stop shop for all your food needs."
            />
          </Suspense>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "about_us_page",
      ])),
    },
  };
}
