import { useState } from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getCookie } from "cookies-next";
import { useAuth } from "../../hooks/authentication";

import { Grid, NoSsr } from "@mui/material";

const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const RegularAppBar = dynamic(() =>
  import("../../src/appbars/Regular.appbars")
);
const CheckOutForm = dynamic(() => import("../../src/forms/CheckOut.forms"));
const ShopMenuBasketSidebars = dynamic(() =>
  import("../../src/sidebars/ShopMenuBasket.sidebars")
);
const SignUpAndLoginForm = dynamic(() =>
  import("../../src/forms/SignUpAndLogin.forms")
);

import axiosConfig, { localeHeaderConfig } from "../../config/axios";
import { cookiesNameSpaces } from "../../utilities/appWideHelperFunctions";
import { saleMethods } from "../../utilities/areaPathHandlers";

const { ORDER_ID, LOCATION_DETAIL, AUTHENTICATION } = cookiesNameSpaces;

const CheckoutPage = ({ shopDetail, basketData, saleMethod }) => {
  const { isLogin } = useAuth();
  const [basket, setBasket] = useState(basketData);

  const updateBasket = (newBasket) => {
    setBasket(newBasket);
  };

  return (
    <>
      <Head></Head>
      <PageContainer flexDirection="column">
        <RegularAppBar />
        <Grid
          container
          my={{ xs: 0, md: 3 }}
          px={{ xs: 0, md: 2 }}
          mx="auto"
          maxWidth={1200}
        >
          <NoSsr>
            <Grid item flexGrow={1} md={8} xs={12}>
              {isLogin ? (
                <CheckOutForm
                  shopDetail={shopDetail}
                  saleMethod={saleMethod}
                  setBasket={setBasket}
                  isHaveCouponCode={basket.copuncode}
                />
              ) : (
                <SignUpAndLoginForm />
              )}
            </Grid>
          </NoSsr>
          <Grid item md={4} pl={3} display={{ xs: "none", md: "block" }}>
            <ShopMenuBasketSidebars
              saleMethod={saleMethod}
              basketData={basket}
              shopDetail={shopDetail}
              basketUpdateHandler={updateBasket}
              gotoCheckOutButton={false}
              sx={{ borderRadius: "8px" }}
            />
          </Grid>
        </Grid>
      </PageContainer>
    </>
  );
};

export default CheckoutPage;

export async function getServerSideProps({ req, res, query, locale }) {
  //? Get Shop Identity
  const { shopId } = query;
  if (!shopId || parseInt(shopId) <= 0)
    return {
      notFound: true,
    };

  // Get SaleMethod
  const saleMethodKey = getCookie("saleMethod", { req, res }) || "delivery";
  const saleMethod = saleMethods[saleMethodKey].code;

  // Get PostCode
  let [postCode, lat, lon] = ["all", 0, 0];
  const rawPostCode = getCookie(LOCATION_DETAIL, { req, res }) || "all";
  if (rawPostCode !== "all") {
    const { DeliveryPostCode, Lat, Lon } = JSON.parse(rawPostCode);
    postCode = DeliveryPostCode;
    lat = Lat;
    lon = Lon;
  }

  // Fetching Shop Detail
  const shopDetail = await (
    await axiosConfig.get(
      `/ShopDetails/${shopId}/${postCode}/${saleMethod}/${lat}/${lon}`, localeHeaderConfig(locale)
    )
  ).data;

  // Basket data
  const rawOrderId = getCookie(ORDER_ID, { req, res }) || 0;
  const orderId = rawOrderId !== 0 ? JSON.parse(rawOrderId) : 0;
  const basketData =
    typeof orderId === "object"
      ? (
          await axiosConfig.get(
            `/Basket/${shopDetail.address.shopId}/${orderId.orderId}`, localeHeaderConfig(locale)
          )
        ).data
      : null;

  if (!basketData || basketData.length <= 0)
    return {
      notFound: true,
    };

  return {
    props: {
      // pageData: finalProducts,
      shopDetail: shopDetail,
      // initialPostCode: postCode,
      // initialSaleMethod: saleMethodKey,
      basketData: basketData,
      saleMethod: saleMethodKey,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "basket",
        "addresses",
        "checkout_page",
      ])),
    },
  };
}
