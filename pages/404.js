import Image from "next/image";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { Button, Grid } from "@mui/material";

import ErrorPagesTypography from "../src/typographies/ErrorPages.typographies";

import Error404Image from "../public/Images/error-images/Error404Image.png";
import MealzoLogo from "../public/Images/mealzo.png";

export default function Custom404() {
  const { t } = useTranslation('error_pages');

  return (
    <Grid
      height="100%"
      flexDirection="column"
      py={{ xs: 3, md: 5 }}
      px={{ xs: 1, md: 10 }}
      sx={{
        backgroundImage: "url(/Images/error-images/ErrorPageBackground.jpg)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <style jsx global>{`
        body,
        #__next {
          overflow: hidden;
          height: 100vh;
        }
      `}</style>
      <Grid item justifySelf="flex-start" alignSelf="flex-start">
        <Image src={MealzoLogo} height={40} width={180} />
      </Grid>
      <Grid
        item
        container
        flexGrow={1}
        alignItems="center"
        flexDirection="column"
      >
        <Grid
          item
          position="relative"
          width={{ xs: "100%", md: 700 }}
          height={{ xs: "50%", md: 300, lg: 500 }}
          maxHeight={{ xs: "50%", md: "25%" }}
        >
          <Image src={Error404Image} layout="fill" objectFit="contain" />
        </Grid>
        <Grid item mb={5}>
          <ErrorPagesTypography fontSize={{ xs: 34, md: 64 }}>
            {t('404_main')}
          </ErrorPagesTypography>
          <ErrorPagesTypography fontSize={{ xs: 18, md: 24 }} maxWidth={600}>
            {t('404_text')}
          </ErrorPagesTypography>
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            LinkComponent="a"
            href="/"
            sx={{
              borderRadius: "8px",
              fontWeight: 700,
              fontSize: { xs: 22, md: 32 },
            }}
          >
            {t('404_go_home_btn')}
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["error_pages"])),
    },
  };
}
