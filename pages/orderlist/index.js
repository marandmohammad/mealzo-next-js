import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

const LoggedNeedPagesLayout = dynamic(() =>
  import("../../src/layouts/LoggedNeedPages.layouts")
);
const OrdersView = dynamic(() => import("../../src/views/Orders.views"));

const OrderListPage = () => {
  return (
    <>
      <Head>{/* Some Header Properties */}</Head>
      <LoggedNeedPagesLayout>
        <OrdersView />
      </LoggedNeedPagesLayout>
    </>
  );
};

export default OrderListPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "basket",
        "previous_orders_page",
      ])),
    },
  };
}
