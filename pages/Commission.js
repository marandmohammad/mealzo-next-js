import Head from "next/head";
import dynamic from "next/dynamic";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const AboutMealzoCard = dynamic(() => import("../src/cards/AboutMealzo.cards"))
const StaticPageAppBar = dynamic(() => import("../src/appbars/StaticPage.appbars"))
const StaticPageHeader = dynamic(() => import("../src/headers/StaticPage.headers"))
const PageContainer = dynamic(() => import("../src/containers/Page.containers"))
const MainFooter = dynamic(() => import("../src/footers/Main.footers"))
const SectionContainer = dynamic(() => import("../src/containers/Section.containers"))

import NoCommissionImage from "../public/Images/NOCOMMISSION.png";
import Group8608Image from "../public/Images/Group8608.svg";
import Group8609Image from "../public/Images/Group8609.svg";
import MoneyImage from "../public/Images/money.svg";
import Group8758Image from "../public/Images/Group8758.svg";

export default function Commission() {
  const { t } = useTranslation('commission_page');

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader title={t('page_header')} imageUrl={NoCommissionImage} />
        <SectionContainer>
          <AboutMealzoCard
            title={t('your_cost_title')}
            description={t('your_cost_text')}
            imageUrl={Group8608Image}
          />
          <AboutMealzoCard
            title={t('why_mealzo_title')}
            description={t('why_mealzo_text')}
            imageUrl={Group8609Image}
            flexDirection="row-reverse"
          />
          <AboutMealzoCard
            title={t('zero_commission_title')}
            description={t('zero_commission_text')}
            imageUrl={MoneyImage}
          />
          <AboutMealzoCard
            title={t('we_care_title')}
            description={t('we_care_text')}
            imageUrl={Group8758Image}
            flexDirection="row-reverse"
          />
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "commission_page",
      ])),
    },
  };
}