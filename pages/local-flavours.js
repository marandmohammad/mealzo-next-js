import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const StaticPageAppBar = dynamic(() => import("../src/appbars/StaticPage.appbars"))
const PageContainer = dynamic(() => import("../src/containers/Page.containers"))
const StaticPageHeader = dynamic(() => import("../src/headers/StaticPage.headers"))
const MainFooter = dynamic(() => import("../src/footers/Main.footers"))
const LandingSectionContainer = dynamic(() => import("../src/containers/LandingSection.containers"))
const AboutMealzoCard = dynamic(() => import("../src/cards/AboutMealzo.cards"))

import HeaderImage from "../public/Images/findyourlocal.png";
import ClassicsImage from "../public/Images/Classics.svg";
import ExclusiveToMealzoImage from "../public/Images/exclusivetomealzo.svg";
import DessertsSweetsImage from "../public/Images/DessertsSweets.svg";
import HealthyFoodImage from "../public/Images/HealthyFood.svg";
import OurPicksImage from "../public/Images/OurPicks.svg";
import AsianImage from "../public/Images/Asian.svg";

export default function LocalFlavours() {
  const { t } = useTranslation('local_flavours');

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader
          title={t('header_page')}
          imageUrl={HeaderImage}
        />
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('exclusive_title')}
            description={t('exclusive_text')}
            imageUrl={ClassicsImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }} bgcolor="common.white">
          <AboutMealzoCard
            flexDirection="row-reverse"
            title={t('classic_title')}
            description={t('classic_text')}
            imageUrl={ExclusiveToMealzoImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('dessert_title')}
            description={t('dessert_text')}
            imageUrl={DessertsSweetsImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }} bgcolor="common.white">
          <AboutMealzoCard
            flexDirection="row-reverse"
            title={t('healthy_title')}
            description={t('healthy_text')}
            imageUrl={HealthyFoodImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('our_pick_title')}
            description={t('our_pick_text')}
            imageUrl={OurPicksImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }} bgcolor="common.white">
          <AboutMealzoCard
            flexDirection="row-reverse"
            title={t('oriental_title')}
            description={t('oriental_text')}
            imageUrl={AsianImage}
          />
        </LandingSectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "local_flavours",
      ])),
    },
  };
}