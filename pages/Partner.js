import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { Grid, Typography } from "@mui/material";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const GetToKnowUsAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const BecomePartnerForm = dynamic(() =>
  import("../src/forms/BecomePartner.forms")
);
const ExclusiveShopsCarousel = dynamic(() =>
  import("../src/carousels/ExclusiveShops.carousels")
);
const FeedBackCarousel = dynamic(() =>
  import("../src/carousels/FeedBack.carousels")
);
const BecomeAPartnerCard = dynamic(() =>
  import("../src/cards/BecomeAPartner.cards")
);

import axiosConfig, { localeHeaderConfig } from "../config/axios";

import BecamePartnerData from "../utilities/BecamePartnerData";

export default function Partner({ exclusiveShops, partnerFeedBack }) {
  const { t } = useTranslation("become_partner_page");

  return (
    <>
      <Head>
        <title>{t("page_title")}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t("page_title")} />
        <meta property="og:title" content={t("page_title")} />
        <meta name="twitter:title" content={t("page_title")} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <GetToKnowUsAppBar />

        {/* Become Partner Part */}
        <SectionContainer
          sx={{
            backgroundImage: `url(/Images/becomePartnerHeader.jpg)`,
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "center",
            my: 0,
            py: 10,
            px: { lg: 15, md: 10, sm: 5, xs: 2 },
          }}
        >
          <Grid item display="flex" alignItems="center" md={6} xs={12}>
            <Grid
              sx={{
                width: { lg: "80%", md: "95%", xs: "100%" },
                mb: { md: 0, xs: 3 },
                py: 2,
                px: 3,
                bgcolor: "primary.main",
              }}
            >
              <Typography
                variant="h6"
                component="h3"
                fontSize={20}
                color="common.white"
              >
                {t("header_description")}
              </Typography>
            </Grid>
          </Grid>
          <Grid item md={6} xs={12}>
            <BecomePartnerForm />
          </Grid>
        </SectionContainer>

        {/* Exclusive Shops */}
        <SectionContainer>
          <ExclusiveShopsCarousel shops={exclusiveShops} />
        </SectionContainer>

        <SectionContainer
          justifyContent="space-between"
          rowGap={3}
          maxWidth={1200}
          margin="0 auto"
        >
          {BecamePartnerData.map(({ iconUrl, title, text }, idx) => (
            <Grid
              item
              key={idx}
              xs={12}
              sm={idx === BecamePartnerData.length - 1 ? 12 : 5.7}
              md={3.7}
            >
              <BecomeAPartnerCard
                iconUrl={iconUrl}
                title={title}
                text={text}
                height="100%"
                item
              />
            </Grid>
          ))}
        </SectionContainer>

        {/* FeedBacks */}
        <SectionContainer sx={{ bgcolor: "#EAEAEA", my: 0, py: 10 }}>
          <FeedBackCarousel feedBack={partnerFeedBack} />
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  let exclusiveShops = null;
  let partnerFeedBack = null;
  try {
    exclusiveShops = await (
      await axiosConfig.get(`/Exclusive`, localeHeaderConfig(locale))
    ).data;

    partnerFeedBack = await (
      await axiosConfig.get(`/PartnerFeedBack`, localeHeaderConfig(locale))
    ).data;
  } catch (error) {}

  return {
    props: {
      exclusiveShops,
      partnerFeedBack,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "become_partner_page",
      ])),
    },
    revalidate: 60,
  };
}
