import dynamic from "next/dynamic";
import Head from "next/head";
import Image from "next/image";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { Grid, Typography } from "@mui/material";

const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const StaticPageHeader = dynamic(() =>
  import("../src/headers/StaticPage.headers")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const Link = dynamic(() => import("../src/links/Link"));

import MealzoAppsImage from "../public/Images/MealzoApps.png";
import DownloadForAndroidImage from "../public/Images/android.svg";
import DownloadForIOSImage from "../public/Images/ios.svg";
import MealzoLogoImage from "../public/Images/selectedCuisineW.svg";

export default function Apps() {
  const { t } = useTranslation("apps_page");

  return (
    <>
      <Head>
        <title>{t("page_title")}</title>

        <meta name="description" content={t("page_description")} />
        <meta name="keywords" content={t("page_keywords")} />
        <meta itemProp="name" content={t("page_title")} />
        <meta property="og:title" content={t("page_title")} />
        <meta name="twitter:title" content={t("page_title")} />
        <meta itemProp="description" content={t("page_description")} />
        <meta property="og:description" content={t("page_description")} />
        <meta name="twitter:description" content={t("page_description")} />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader title={t('page_header')} imageUrl={MealzoAppsImage} />
        <SectionContainer alignItems="center">
          <Grid
            item
            container
            xs={12}
            sm={6}
            mb={{ xs: 5, sm: 0 }}
            flexDirection="row"
            flexWrap="nowrap"
            alignItems="center"
            justifyContent="center"
          >
            <Grid
              sx={{
                width: 70,
                height: 70,
                bgcolor: "primary.main",
                mr: 2,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image src={MealzoLogoImage} alt="Mealzo Logo" />
            </Grid>
            <Typography variant="h6">{t('download_the_app')}</Typography>
          </Grid>

          <Grid item container xs={12} sm={6} textAlign="center">
            <Grid item xs={6} pr={1}>
              <Link
                href="https://apps.apple.com/us/app/mealzo/id1558698286"
                target="_blank"
              >
                <Image src={DownloadForIOSImage} loading="lazy" />
              </Link>
            </Grid>
            <Grid item xs={6} pr={1}>
              <Link
                href="https://play.google.com/store/apps/details?id=com.mealzowee"
                target="_blank"
              >
                <Image src={DownloadForAndroidImage} loading="lazy" />
              </Link>
            </Grid>
          </Grid>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getServerSideProps({ req, locale }) {
  const userAgent = req.headers["user-agent"];

  if (userAgent !== null && userAgent.toLowerCase().includes("android"))
    return {
      redirect: {
        permanent: false,
        destination:
          "https://play.google.com/store/apps/details?id=com.mealzowee",
      },
    };
  else if (
    userAgent !== null &&
    (userAgent.toLowerCase().includes("iphone") ||
      userAgent.toLowerCase().includes("ipad"))
  )
    return {
      redirect: {
        permanent: false,
        destination: "https://apps.apple.com/us/app/mealzo/id1558698286",
      },
    };

  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "apps_page",
      ])),
    },
  };
}
