import { useState, useRef } from "react";
import dynamic from "next/dynamic";
import Head from "next/head";
import Image from "next/image";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import Grid from "@mui/material/Grid";

const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));
const LandingFocusBackdrops = dynamic(() =>
  import("../src/backdrops/LandingFocus.backdrops")
);
const HeaderSearchBox = dynamic(() => import("../src/cards/HeaderSearchBox"));
const LandingView = dynamic(() => import("../src/views/Landing.views"));

import MealzoHeaderImage from "../public/Images/header.jpg";
import MealzoHeaderSmallSizeImage from "../public/Images/header360.jpg";
import MealzoLogoImage from "../public/Images/mealzo.png";

import axiosConfig, { localeHeaderConfig } from "../config/axios";

export default function Index({ topPicks, topCuisines, error }) {
  const { t } = useTranslation("home_page");
  const pageTitle = `${t("page_title")} - Mealzo`;
  const pageDescription = t("page_description");

  const [open, setOpen] = useState(false);
  const searchRef = useRef(null);

  const chipClickHandler = () => {
    setOpen(true);
    searchRef.current.scrollIntoView({
      block: "center",
    });
    setTimeout(() => {
      searchRef.current.focus();
    }, 1000);
  };

  const handleClickAway = () => {
    setOpen(false);
  };

  return (
    <>
      <Head>
        <title>{pageTitle}</title>

        <meta name="description" content={pageDescription} />
        <meta name="keywords" content={t("page_keywords")} />

        <meta itemProp="name" content={pageTitle} />
        <meta property="og:title" content={pageTitle} />
        <meta name="twitter:title" content={pageTitle} />
        <meta itemProp="description" content={pageDescription} />
        <meta property="og:description" content={pageDescription} />
        <meta name="twitter:description" content={pageDescription} />
      </Head>
      {open && <LandingFocusBackdrops onClick={() => setOpen(false)} />}
      <PageContainer>
        {/* Header */}
        <Grid container position="relative">
          <Grid flexGrow={1} display={{ xs: "none", sm: "block" }}>
            <Image
              src={MealzoHeaderImage}
              alt="Mealzo home page banner large"
              placeholder="blur"
              loading="eager"
              priority={true}
              // unoptimized
            />
          </Grid>
          <Grid
            component="div"
            flexGrow={1}
            display={{ xs: "block", sm: "none" }}
            height={648}
          >
            <Image
              src={MealzoHeaderSmallSizeImage}
              alt="Mealzo home page banner small"
              loading="eager"
              placeholder="blur"
              priority={true}
              layout="fill"
              // unoptimized
            />
          </Grid>
          <StaticPageAppBar
            imageUrl={MealzoLogoImage}
            sx={{ bgcolor: "transparent", boxShadow: "none", maxWidth: "100%" }}
            position="absolute"
            iconColor="primary.main"
            showButtons
          />
          <HeaderSearchBox
            handleClickAway={handleClickAway}
            open={open}
            searchRef={searchRef}
          />
        </Grid>
        <LandingView
          topPicks={topPicks}
          topCuisines={topCuisines}
          chipClickHandler={chipClickHandler}
        />
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  let [topPicks, topCuisines, errorMessage] = [null, null, null];
  try {
    topPicks = await (
      await axiosConfig.get("/TopPicks", localeHeaderConfig(locale))
    ).data;
    topCuisines = await (
      await axiosConfig.get("/TopCuisines", localeHeaderConfig(locale))
    ).data;
  } catch (error) {
    errorMessage = error.message;
  }

  return {
    props: {
      // locationDetail: cookies.LocationDetail,
      topPicks,
      topCuisines,
      error: errorMessage,
      ...(await serverSideTranslations(locale, [
        "common",
        "home_page",
        "footer",
        "login_form",
        "sign_up_form",
        "drawer",
      ])),
    },
    revalidate: 60,
  };
}
