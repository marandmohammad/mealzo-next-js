import { useState, Suspense, useMemo, useEffect } from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { setCookies } from "cookies-next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import useLocation from "../../hooks/useLocation";

import { Grid, NoSsr, Skeleton } from "@mui/material";

//! direct Imported Components
import AppBarLoading from "../../src/loadings/AppBar.loadings";
import AreaPageLoading from "../../src/loadings/AreaPage.loadings";
import MainFooterLoading from "../../src/loadings/MainFooter.loadings";

//! without suspense
const AreaDeliveryModal = dynamic(() =>
  import("../../src/modals/AreaDelivery.modals")
);
const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const AreaFiltersModal = dynamic(() =>
  import("../../src/modals/AreaFilters.modals")
);

//! with suspense
const AreaFiltersSideBarComponent = dynamic(
  () => import("../../src/sidebars/AreaFilters.sidebars"),
  {
    suspense: true,
  }
);
const AreaCategoriesCarouselComponent = dynamic(
  () => import("../../src/carousels/AreaCategories.carousels"),
  {
    suspense: true,
  }
);
const AreaOptionsCarouselComponent = dynamic(
  () => import("../../src/carousels/AreaOptions.carousels"),
  {
    suspense: true,
  }
);
const AreaTopPicksCarouselComponent = dynamic(
  () => import("../../src/carousels/AreaTopPicks.carousels"),
  {
    suspense: true,
  }
);
const AreaFoodsListComponent = dynamic(
  () => import("../../src/lists/AreaFoods.lists"),
  {
    suspense: true,
  }
);
const AppBarComponent = dynamic(
  () => import("../../src/appbars/Area.appbars"),
  {
    suspense: true,
  }
);
const MainFooterComponent = dynamic(
  () => import("../../src/footers/Main.footers"),
  {
    suspense: true,
  }
);

// import AreaFiltersModal from "../../src/modals/AreaFilters.modals";

import { AreaPageContext } from "../../context/AreaPageContext";
import {
  getCategoriesInitialState,
  queryParamDecode,
  organizeAreaInitialData,
  getFilterInitialState,
  saleMethods,
  queryKeyWords,
  getPrevQueryParamsInString,
  objectKeyValueToUrlQueryParam,
  areaHeadPropertiesHandler,
  saleMethodAndSearchChangeHandler,
} from "../../utilities/areaPathHandlers";
import parseCookies from "../../utilities/parseCookies";
import axiosConfig, { localeHeaderConfig } from "../../config/axios";
import postCodeToLatLong from "../../utilities/postCodeToLatLong";
import {
  cookiesNameSpaces,
  validateSaleMethod,
} from "../../utilities/appWideHelperFunctions";

const { LOCATION_DETAIL } = cookiesNameSpaces;

export default function Area({
  queryParams,
  categoriesData,
  sortData,
  offersData,
  promotionsData,
  shopsData,
  saleMethodId,
  searchText,
  headData,
}) {
  // Get header data
  const { title, description, pageKeywords } = headData;
  const { postCode, updateLocationDetail } = useLocation();

  const hasInitialPostCode = useMemo(
    () =>
      queryParams[queryKeyWords.postCodeQueryKeyWord] === "all" ? false : true,
    [postCode]
  );

  useEffect(() => {
    updateLocationDetail();
  }, []);

  // states
  const [deliveryModal, setDeliveryModal] = useState(!hasInitialPostCode);
  const [filterOpen, setFilterOpen] = useState(false);
  const [shopLoading, setShopLoading] = useState(false);

  // Define Query Keywords
  const {
    categoryQueryKeyWord,
    sortQueryKeyWord,
    offerQueryKeyWord,
    saleMethodQueryKeyWord,
    searchQueryKeyWord,
  } = queryKeyWords;

  //* Create Initial States
  const categoryInitialState = useMemo(
    () =>
      getCategoriesInitialState(
        queryParams,
        categoriesData,
        categoryQueryKeyWord
      ),
    []
  );
  const sortInitialState = useMemo(
    () =>
      getFilterInitialState(
        queryParams,
        sortQueryKeyWord,
        sortData,
        sortQueryKeyWord
      ),
    []
  );
  const offerInitialState = useMemo(
    () =>
      getFilterInitialState(
        queryParams,
        offerQueryKeyWord,
        offersData,
        offerQueryKeyWord
      ),
    []
  );

  // Define States
  const [saleMethod, setSaleMethod] = useState(saleMethodId);
  //   const [postCode, setPostCode] = useState(queryParamDecode(queryParams.area));
  const [sort, setSort] = useState(sortInitialState);
  const [offer, setOffer] = useState(offerInitialState);
  const [categories, setCategories] = useState(categoryInitialState);
  const [shops, setShops] = useState(shopsData);
  const [searchInputValue, setSearchInputValue] = useState(searchText);

  const router = useRouter();
  const deliveryHandler = async (value) => {
    await saleMethodAndSearchChangeHandler(
      router,
      saleMethodQueryKeyWord,
      value,
      setSaleMethod,
      setShops,
      shopLoading,
      setShopLoading
    );
  };

  return (
    <>
      <Head>
        <title>{title}</title>

        <meta name="description" content={description} />
        <meta name="keywords" content={pageKeywords} />
        <meta itemProp="name" content={title} />
        <meta property="og:title" content={title} />
        <meta name="twitter:title" content={title} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      {/*<Suspense fallback={<Skeleton height="100vh" width="100vw"/>}>*/}
      <AreaPageContext.Provider
        value={{
          deliveryModal,
          setDeliveryModal,
          filterOpen,
          setFilterOpen,

          // Category Group
          categoriesData,
          categories,
          setCategories,
          categoryQueryKeyWord,

          // Sort Group
          sortData,
          sort,
          setSort,
          sortQueryKeyWord,

          // Offers Group
          offersData,
          offer,
          setOffer,
          offerQueryKeyWord,

          // PostCode Group
          //   postCode,

          // SaleMethod Group
          saleMethod,
          setSaleMethod,
          saleMethodQueryKeyWord,

          // Promotion Group
          promotionsData,

          // Shop Group
          shops,
          setShops,
          shopLoading,
          setShopLoading,

          // Search Group
          searchInputValue,
          setSearchInputValue,
          searchQueryKeyWord,
        }}
      >
        <PageContainer
          sx={{
            height: `${filterOpen ? "100vh" : "unset"}`,
            overflowY: `${filterOpen ? "hidden" : "unset"}`,
            mb: 3,
          }}
        >
          <Suspense fallback={<AppBarLoading />}>
            <NoSsr>
              <AppBarComponent />
            </NoSsr>
          </Suspense>
          <Grid
            container
            mt={{ md: 2, xs: 5 }}
            px={{ sm: 4, xs: 2 }}
            flexDirection="row"
            flexWrap="nowrap"
          >
            <Grid
              item
              xl={2.2}
              lg={3}
              md={4}
              xs={0}
              sx={{ display: { xs: "none", md: "block" }, pr: 5 }}
            >
              <Suspense
                fallback={
                  <Skeleton
                    variant="rectangular"
                    animation="wave"
                    width="100%"
                    height="100%"
                  />
                }
              >
                <AreaFiltersSideBarComponent />
              </Suspense>
            </Grid>
            <Grid
              item
              container
              xl={9.8}
              lg={9}
              md={8}
              xs={12}
              pt={{ xs: 5, md: 0 }}
            >
              <Suspense fallback={<AreaPageLoading />}>
                <AreaCategoriesCarouselComponent />
                <AreaOptionsCarouselComponent />
                <AreaTopPicksCarouselComponent />
                <AreaFoodsListComponent />
              </Suspense>
            </Grid>
          </Grid>
        </PageContainer>

        <NoSsr>
          <AreaDeliveryModal
            deliveryModal={deliveryModal}
            setDeliveryModal={setDeliveryModal}
            hasInitialPostCode={hasInitialPostCode}
            saleMethod={saleMethod}
            deliveryHandler={deliveryHandler}
            postCode={postCode}
          />
        </NoSsr>

        <AreaFiltersModal
          open={filterOpen}
          handleClose={() => setFilterOpen(false)}
        />
      </AreaPageContext.Provider>
      <Suspense fallback={<MainFooterLoading />}>
        <MainFooterComponent />
      </Suspense>
    </>
  );
}

export async function getServerSideProps(context) {
  const { req, res, query, locale } = context;

  // Get header cookies
  const cookies = parseCookies(req);

  // Get queryKeyWords
  const {
    postCodeQueryKeyWord,
    categoryQueryKeyWord,
    sortQueryKeyWord,
    offerQueryKeyWord,
    saleMethodQueryKeyWord,
    searchQueryKeyWord,
  } = queryKeyWords;

  /*
   * All validated and available data will store in this object
   * At the end, object will map into a string witch it will be api query params
   */
  let initialRequestParameters = {};

  // Validate sale method
  const saleMethod = validateSaleMethod(query[saleMethodQueryKeyWord]);

  if (!saleMethod) {
    const prevQueryParamsString = getPrevQueryParamsInString(
      query,
      postCodeQueryKeyWord
    );
    const area = query[postCodeQueryKeyWord];
    return {
      redirect: {
        permanent: false,
        destination: `/area/${area}?${saleMethodQueryKeyWord}=delivery${prevQueryParamsString}`,
      },
    };
  }
  // Set sale method
  initialRequestParameters["SaleMethod"] = saleMethods[saleMethod]?.code;

  // Validate sort
  if (typeof query[sortQueryKeyWord] === "string")
    initialRequestParameters["Sort"] = queryParamDecode(
      query[sortQueryKeyWord]
    );

  // Validate offer
  if (typeof query[offerQueryKeyWord] === "string")
    initialRequestParameters["Offer"] = queryParamDecode(
      query[offerQueryKeyWord]
    );

  // Get Decoded Post Code
  const postCode = queryParamDecode(
    query[postCodeQueryKeyWord],
    postCodeQueryKeyWord
  );

  // Set post code
  initialRequestParameters["PostCode"] = postCode;

  // Validate and set location details
  if (typeof cookies[LOCATION_DETAIL] !== "undefined") {
    if (postCode !== "all") {
      const { Lat, Lon } = JSON.parse(cookies[LOCATION_DETAIL]);

      initialRequestParameters["La"] = Lat;
      initialRequestParameters["Lo"] = Lon;
    }
  } else {
    if (postCode !== "all") {
      const [Lat, Lon] = await postCodeToLatLong(postCode);
      const locationDetail = {
        DeliveryPostCode: postCode,
        Lat: Lat,
        Lon: Lon,
      };

      setCookies(LOCATION_DETAIL, JSON.stringify(locationDetail), {
        req,
        res,
      });
      initialRequestParameters["La"] = Lat;
      initialRequestParameters["Lo"] = Lon;
    }
  }

  let cuisineCount = 0;
  // Validate and set category list ( Cuisines )
  if (typeof query[categoryQueryKeyWord] === "object") {
    const cuisines = [...query[categoryQueryKeyWord]];
    initialRequestParameters["SubjectList"] = cuisines
      .map((cuisine) => queryParamDecode(cuisine, categoryQueryKeyWord))
      .join(";");
    cuisineCount = cuisines.length;
  } else if (typeof query[categoryQueryKeyWord] === "string") {
    initialRequestParameters["SubjectList"] = queryParamDecode(
      query[categoryQueryKeyWord],
      categoryQueryKeyWord
    );
    cuisineCount = 1;
  }

  // Validate and set searchText
  if (
    typeof query[searchQueryKeyWord] === "string" &&
    query[searchQueryKeyWord].trim() !== ""
  )
    initialRequestParameters["SearchText"] = query[searchQueryKeyWord];

  // Map Object To String
  const requestQueryParameter = objectKeyValueToUrlQueryParam(
    initialRequestParameters
  );

  // Validate count of keys
  if (requestQueryParameter === "") {
    const prevQueryParamsString = getPrevQueryParamsInString(
      query,
      postCodeQueryKeyWord
    );
    const area = query[postCodeQueryKeyWord];
    return {
      redirect: {
        permanent: false,
        destination: `/area/${area}?${saleMethodQueryKeyWord}=delivery${prevQueryParamsString}`,
      },
    };
  }

  // Request
  const pageData = await (
    await axiosConfig.get(
      `/AreaShops?${requestQueryParameter}`,
      localeHeaderConfig(locale)
    )
  ).data;

  const [categoriesData, sortData, offersData, promotionsData, shopsData] =
    organizeAreaInitialData(pageData);

  // Calculate Page Title & Description
  const [title, description, pageKeywords] = areaHeadPropertiesHandler(
    postCode,
    cuisineCount,
    pageData,
    initialRequestParameters
  );

  return {
    props: {
      queryParams: query,
      categoriesData,
      sortData,
      offersData,
      promotionsData,
      shopsData,
      saleMethodId: saleMethod,
      searchText:
        typeof initialRequestParameters["SearchText"] === "string"
          ? queryParamDecode(initialRequestParameters["SearchText"])
          : "",
      headData: {
        title,
        description,
        pageKeywords,
      },
      ...(await serverSideTranslations(locale, [
        "common",
        "area_page",
        "footer",
        "login_form",
        "sign_up_form",
        "drawer",
        "addresses",
      ])),
    },
  };
}
