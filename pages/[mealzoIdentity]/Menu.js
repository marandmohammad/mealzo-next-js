import { useState, Suspense, useMemo, useEffect } from "react";
import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { getCookie, setCookies } from "cookies-next";

import useOrder from "../../hooks/orders";
import useLocation from "../../hooks/useLocation";

import Grid from "@mui/material/Grid";

//! with suspense
const MenuAppBar = dynamic(() => import("../../src/appbars/Menu.appbars"), {
  suspense: true,
});
const ShopMenuHeaderCard = dynamic(
  () => import("../../src/cards/ShopMenuHeader.cards"),
  {
    suspense: true,
  }
);
const ShopMenuCategoryCarousel = dynamic(
  () => import("../../src/carousels/ShopMenuCategory.carousels"),
  {
    suspense: true,
  }
);
const ShopMenuCategoryList = dynamic(
  () => import("../../src/lists/ShopMenuCategory.lists"),
  {
    suspense: true,
  }
);
const ShopMenuBasketSidebars = dynamic(
  () => import("../../src/sidebars/ShopMenuBasket.sidebars"),
  {
    suspense: true,
  }
);
const ViewBasketSmallSizesButton = dynamic(
  () => import("../../src/buttons/ViewBasketSmallSizes.buttons"),
  {
    suspense: true,
  }
);

//! without suspense
const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const MenuDeliveryModal = dynamic(() =>
  import("../../src/modals/MenuDelivery.modals")
);
const ChangeSaleMethodErrorModal = dynamic(() =>
  import("../../src/modals/ChangeSaleMethodError.modals")
);
const BasketModal = dynamic(() => import("../../src/modals/Basket.modals"));

import MenuPageLoading from "../../src/loadings/MenuPage.loadings";

import axiosConfig, { localeHeaderConfig } from "../../config/axios";
import { saleMethods } from "../../utilities/areaPathHandlers";
import { MenuPageContext } from "../../context/MenuPageContext";
import {
  bundleGetMenuWithPopularItemsCategory,
  changeSaleMethodStatusHandler,
  detectInBasketProducts,
} from "../../utilities/menuPageHelperFunctions";
import {
  cookiesNameSpaces,
  saleMethodChangeStatusCodes,
} from "../../utilities/appWideHelperFunctions";

const { LOCATION_DETAIL, ORDER_ID, SHOP_IDENTITY, SHOP_ID, SALE_METHOD } =
  cookiesNameSpaces;

export default function ShopMenu({
  pageData,
  shopDetail,
  initialPostCode,
  initialSaleMethod,
  basketData,
  shopOpenStatus,
}) {
  const router = useRouter();
  const { t } = useTranslation("common");
  const { getOrderId } = useOrder();
  const { postCode } = useLocation();

  const hasInitialPostCode = useMemo(
    () => (postCode === "all" ? false : true),
    [postCode]
  );

  //! State Definitions
  const [saleMethod, setSaleMethod] = useState(initialSaleMethod);
  const [basket, setBasket] = useState(basketData);
  const [products, setProducts] = useState(pageData);
  const [deliveryModal, setDeliveryModal] = useState(!hasInitialPostCode);
  const [changeSaleMethodAlertOpen, setChangeSaleMethodAlertOpen] = useState(
    hasInitialPostCode ? shopOpenStatus : { ...shopOpenStatus, open: false }
  );
  const [basketModal, setBasketModal] = useState(false);
  const handleBasketModal = () => setBasketModal(!basketModal);

  const handleSaleMethodAlertClose = () =>
    setChangeSaleMethodAlertOpen({
      open: false,
      message: changeSaleMethodAlertOpen.message,
      statusCode: null,
      continueButtonCallback: null,
    });

  const updateProduct = (products, currentBasket) => {
    const newProducts = detectInBasketProducts(products, currentBasket);
    setProducts(newProducts);
  };

  const updateBasket = (newBasket) => {
    setBasket(newBasket);
    updateProduct(pageData, newBasket);
  };

  //! On SaleMethod Change
  const saleMethodChangeHandler = async (key, preferredPostCode) => {
    const { mealzoIdentity } = router.query;
    const { OK_SHOP, CLOSE_SHOP, NO_DELIVERY_SHOP, PRE_ORDER_SHOP } =
      saleMethodChangeStatusCodes;
    const orderId = getOrderId() || 0;

    //? CallBack Object
    const callbacks = {
      [OK_SHOP]: async (data) => {
        await setSaleMethod(key);
        setCookies(SALE_METHOD, key);
        if (data.data.orderId !== 0) setBasket(data.data);
        // const newPageData = await bundleGetMenuWithPopularItemsCategory(
        //   mealzoIdentity,
        //   saleMethods[key].code
        // );
        // await updateProduct(newPageData, basket);
      },
      [PRE_ORDER_SHOP]: (data) => {
        setChangeSaleMethodAlertOpen({
          open: true,
          message: data.status.message[0],
          statusCode: PRE_ORDER_SHOP,
          continueButtonCallback: () => {
            setSaleMethod(key);
            if (data.data.orderId !== 0) setBasket(data.data);
          },
        });
      },
      [CLOSE_SHOP]: (data) => {
        setChangeSaleMethodAlertOpen({
          open: true,
          message: data.status.message[0],
          statusCode: CLOSE_SHOP,
          continueButtonCallback: null,
        });
      },
      [NO_DELIVERY_SHOP]: (data) => {
        setChangeSaleMethodAlertOpen({
          open: true,
          message: data.status.message[0],
          statusCode: NO_DELIVERY_SHOP,
          continueButtonCallback: null,
        });
      },
    };

    await changeSaleMethodStatusHandler(
      callbacks,
      shopDetail.address.shopId,
      saleMethods[key].code,
      preferredPostCode ? preferredPostCode : postCode,
      typeof orderId === "object" ? orderId.orderId : orderId
    );
  };

  useEffect(() => {
    if (hasInitialPostCode) saleMethodChangeHandler(saleMethod);
  }, [postCode]);

  const contextValue = {
    // postCode,
    // setPostCode,
    saleMethod,
    setSaleMethod,
    setDeliveryModal,

    // Products
    products,
    setProducts,

    // Shop Detail
    shopDetail,
    setBasket,
    updateBasket,
  };

  return (
    <>
      <Head>
        <title>{`${shopDetail.address.shopTitle} Menu`}</title>

        <meta itemProp="image" content={shopDetail.address.shopLogo} />
        <meta property="og:image" content={shopDetail.address.shopLogo} />
        <meta name="twitter:card" content={shopDetail.address.shopLogo} />
        <meta name="twitter:image" content={shopDetail.address.shopLogo} />
      </Head>
      <PageContainer flexDirection="column">
        <Suspense fallback={<MenuPageLoading />}>
          <MenuPageContext.Provider value={contextValue}>
            <MenuAppBar
              shopDetail={shopDetail}
              handleBasketModal={handleBasketModal}
            />
            <Grid
              container
              my={{ xs: 0, md: 2 }}
              px={{ xs: 0, md: 2 }}
              mx="auto"
              maxWidth={1200}
            >
              <Grid container item flexGrow={1} md={8} xs={12}>
                <ShopMenuHeaderCard data={shopDetail} saleMethod={saleMethod} />
                <ShopMenuCategoryCarousel pageData={products} />

                {products.map((category, index) => (
                  <ShopMenuCategoryList
                    category={category}
                    shopDetail={shopDetail}
                    key={index}
                    sx={{
                      scrollMarginTop: 60,
                      mb: index === products.length - 1 ? 10 : 8,
                      mt: index === 0 ? 5 : "unset",
                    }}
                  />
                ))}
              </Grid>
              <Grid item md={4} pl={3} display={{ xs: "none", md: "block" }}>
                <ShopMenuBasketSidebars
                  basketData={basket}
                  shopDetail={shopDetail}
                  saleMethod={saleMethod}
                  postCode={postCode}
                  basketUpdateHandler={updateBasket}
                  sx={{ borderRadius: "4px" }}
                />
              </Grid>
              <Grid
                container
                sx={{
                  position: "fixed",
                  bottom: 0,
                  p: 2,
                  // bgcolor: "white",
                  bgcolor: "transparent",
                  backdropFilter: "blur(10px)",
                  WebkitBackdropFilter: "blur(10px)",
                  boxShadow: "0px -3px 6px #00000029",
                  display: { xs: "flex", md: "none" },
                  zIndex: 1101,
                  // boxShadow: '0 -1px 4px rgb(0 0 0 / 8%)'
                }}
              >
                <ViewBasketSmallSizesButton
                  buttonText={t("menu_view_basket")}
                  basket={basket}
                  currencyId={shopDetail.address.currencyId}
                  onClick={handleBasketModal}
                />
              </Grid>
            </Grid>

            <MenuDeliveryModal
              deliveryModal={deliveryModal}
              setDeliveryModal={setDeliveryModal}
              hasInitialPostCode={hasInitialPostCode}
              saleMethod={saleMethod}
              deliveryHandler={saleMethodChangeHandler}
              postCode={postCode}
              shopDetail={shopDetail}
              setBasket={setBasket}
            />

            <BasketModal
              open={basketModal}
              handleClose={handleBasketModal}
              basketData={basket}
              shopDetail={shopDetail}
              saleMethod={saleMethod}
              postCode={postCode}
              basketUpdateHandler={updateBasket}
            />

            <ChangeSaleMethodErrorModal
              open={changeSaleMethodAlertOpen.open}
              handleClose={handleSaleMethodAlertClose}
              message={changeSaleMethodAlertOpen.message}
              statusCode={changeSaleMethodAlertOpen.statusCode}
              continueButtonCallback={
                changeSaleMethodAlertOpen.continueButtonCallback
              }
              shopDetail={shopDetail}
            />
          </MenuPageContext.Provider>
        </Suspense>
      </PageContainer>
    </>
  );
}

export async function getServerSideProps({ req, res, query, locale }) {
  // Get Shop Identity
  const { mealzoIdentity } = query;

  if (mealzoIdentity && mealzoIdentity.trim() !== "")
    setCookies(SHOP_IDENTITY, mealzoIdentity, { req, res });

  // Get SaleMethod
  const saleMethodKey = getCookie("saleMethod", { req, res }) || "delivery";
  const saleMethod = saleMethods[saleMethodKey].code;

  // Get PostCode
  let [postCode, lat, lon] = ["all", 0, 0];
  const rawPostCode = getCookie(LOCATION_DETAIL, { req, res }) || "all";
  if (rawPostCode !== "all") {
    const { DeliveryPostCode, Lat, Lon } = JSON.parse(rawPostCode);
    postCode = DeliveryPostCode;
    lat = Lat;
    lon = Lon;
  }

  // Fetching Shop Detail
  let shopDetail = null;
  try {
    shopDetail = await (
      await axiosConfig.get(
        `/ShopDetails/GetByIdentity/${mealzoIdentity}/${postCode}/${saleMethod}/${lat}/${lon}`,
        localeHeaderConfig(locale)
      )
    ).data;


    if (!shopDetail.address) {
      return {
        notFound: true,
      };
    }
  } catch (error) {
    return {
      notFound: true,
    };
  }

  setCookies(SHOP_ID, shopDetail?.address?.shopId, { req, res });

  const newPageData = await bundleGetMenuWithPopularItemsCategory(
    mealzoIdentity,
    saleMethod,
    locale
  );

  // Basket data
  const rawOrderId = getCookie(ORDER_ID, { req, res }) || 0;
  const orderId = rawOrderId !== 0 ? JSON.parse(rawOrderId) : 0;
  const basketData =
    typeof orderId === "object" &&
    shopDetail.address.shopId === parseInt(orderId.shopId)
      ? (
          await axiosConfig.get(
            `/Basket/${shopDetail.address.shopId}/${orderId.orderId}`,
            localeHeaderConfig(locale)
          )
        ).data
      : null;

  const finalProducts = detectInBasketProducts(newPageData, basketData);

  // Check Shop Open Status
  const { OK_SHOP, CLOSE_SHOP, NO_DELIVERY_SHOP, PRE_ORDER_SHOP } =
    saleMethodChangeStatusCodes;
  let shopOpenStatus = {
    open: false,
    message: "",
    statusCode: null,
  };

  try {
    const result = await axiosConfig.patch(
      "/Basket/ChangeSaleMethod",
      {
        shopId: shopDetail.address.shopId,
        orderId: typeof orderId === "object" ? orderId.orderId : 0,
        saleMethod,
        postCode,
      },
      localeHeaderConfig(locale)
    );

    if (
      typeof result.data.status.statusCode !== "undefined" &&
      result.data.status.statusCode === PRE_ORDER_SHOP
    ) {
      shopOpenStatus.open = true;
      shopOpenStatus.message = result.data.status.message[0];
      shopOpenStatus.statusCode = PRE_ORDER_SHOP;
    }
  } catch (e) {
    const { response, message } = e;
    switch (response.status) {
      case CLOSE_SHOP:
        shopOpenStatus.open = true;
        shopOpenStatus.message = response.data.status.message[0];
        shopOpenStatus.statusCode = CLOSE_SHOP;
        break;
      case NO_DELIVERY_SHOP:
        shopOpenStatus.open = true;
        shopOpenStatus.message = response.data.status.message[0];
        shopOpenStatus.statusCode = NO_DELIVERY_SHOP;
        break;
      default:
        shopOpenStatus.open = false;
        shopOpenStatus.message = "Unsupported response status";
        shopOpenStatus.statusCode = response.status;
    }
  }

  return {
    props: {
      pageData: finalProducts,
      shopDetail: shopDetail,
      initialPostCode: postCode,
      initialSaleMethod: saleMethodKey,
      basketData: basketData,
      shopOpenStatus,
      ...(await serverSideTranslations(locale, [
        "common",
        "footer",
        "login_form",
        "sign_up_form",
        "drawer",
        "shop_info_modal",
        "basket",
        "food_modal",
        "addresses",
      ])),
    },
  };
}
