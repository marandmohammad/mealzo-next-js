import { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { useRouter } from "next/router";
import useNotification from "../../hooks/useNotification";

import { Grid, Skeleton } from "@mui/material";

const ShopMenuBasketSidebar = dynamic(() =>
  import("../../src/sidebars/ShopMenuBasket.sidebars")
);
const ReviewsView = dynamic(() => import("../../src/views/Reviews.views"));
const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const RegularAppBar = dynamic(() => import("../../src/appbars/Regular.appbars"))

import axiosConfig from "../../config/axios";

const OrderStepsViewLoading = () => (
  <Grid container flexDirection="column">
    <Skeleton
      variant="rectangular"
      component="div"
      sx={{ height: { xs: "50vh", md: 400 } }}
    />
    <Skeleton variant="text" component="div" sx={{ mt: 5 }} />
    <Skeleton
      variant="rectangular"
      component="div"
      sx={{ height: { xs: 40, md: 40 }, width: 200 }}
    />
  </Grid>
);

const ReviewPage = () => {
  const { showNotification } = useNotification();

  const [basket, setBasket] = useState(null);
  const [orderAddress, setOrderAddress] = useState(null);
  const [reviews, setReviews] = useState(null);
  const [shopDetail, setShopDetail] = useState(null);

  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const { reviewIdentity } = router.query;

  //! Get initial data
  useEffect(() => {
    const paymentStatusCheck = async () => {
      setLoading(true);
      try {
        const { basket, orderAddress, reviews, shopDetail } = await (
          await axiosConfig.get(`/Review/${reviewIdentity}`)
        ).data;
        setBasket(basket);
        setOrderAddress(orderAddress);
        setReviews(reviews);
        setShopDetail(shopDetail);
      } catch (error) {
        showNotification(error.message, "error", 3000);
      }
      setLoading(false);
    };

    reviewIdentity && paymentStatusCheck();
  }, [reviewIdentity]);

  return (
    <>
      <Head>{/* Some Header Property */}</Head>
      <PageContainer flexDirection="column">
        <Grid container>
          <RegularAppBar />
        </Grid>
        <Grid
          container
          item
          my={{ xs: 0, md: 3 }}
          px={{ xs: 0, md: 2 }}
          mx="auto"
          maxWidth={1200}
        >
          <Grid item flexGrow={1} md={8} xs={12}>
            {loading ? (
              <OrderStepsViewLoading />
            ) : (
              <ReviewsView
                shopDetail={shopDetail}
                saleMethod={basket.saleMethod}
                orderNumber={basket.orderId}
                postCode={shopDetail.postCode}
                reviewQuestions={reviews}
                reviewIdentity={reviewIdentity}
              />
            )}
          </Grid>
          <Grid item md={4} pl={3} display={{ xs: "none", md: "block" }}>
            {loading ? (
              <Skeleton
                variant="rectangular"
                sx={{ display: { xs: "none", md: "block" }, height: "100%" }}
              />
            ) : (
              <ShopMenuBasketSidebar
                basketData={basket}
                orderDetailShopIdCurrencyId={shopDetail}
                gotoCheckOutButton={false}
              />
            )}
          </Grid>
        </Grid>
      </PageContainer>
    </>
  );
};

export default ReviewPage;

export async function getServerSideProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "basket",
        "order_steps_page", //keep_shopping_btn
      ])),
    },
  };
}
