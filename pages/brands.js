import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));
const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const StaticPageHeader = dynamic(() =>
  import("../src/headers/StaticPage.headers")
);
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const LocalBrandsCard = dynamic(() => import("../src/cards/LocalBrands.cards"));

import BrandsHeaderImage from "../public/Images/toplocalfavories.png";

import axiosConfig, { localeHeaderConfig } from "../config/axios";

export default function brands({ pageData }) {
  const { t } = useTranslation('brands_page');

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader
          title={t('page_header')}
          subtitle={t('page_header_subtitle')}
          imageUrl={BrandsHeaderImage}
        />
        <SectionContainer justifyContent="flex-start">
          {pageData.map(({ topLocalNote, mealzoLogo }, index) => (
            <LocalBrandsCard
              title={topLocalNote}
              imageUrl={mealzoLogo}
              key={index}
            />
          ))}
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  const topBrands = await axiosConfig.get("/TopLocals", localeHeaderConfig(locale));

  return {
    props: {
      pageData: topBrands.data,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "brands_page",
      ])),
    },
    revalidate: 60,
  };
}
