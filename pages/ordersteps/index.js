import { useEffect, useMemo, useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import dynamic from "next/dynamic";
import Head from "next/head";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";

import useNotification from "../../hooks/useNotification";
import useOrder from "../../hooks/orders";
import useLocation from "../../hooks/useLocation";

import { Grid, Skeleton } from "@mui/material";

const LoggedNeedPagesLayout = dynamic(() =>
  import("../../src/layouts/LoggedNeedPages.layouts")
);
const ShopMenuBasketSidebar = dynamic(() =>
  import("../../src/sidebars/ShopMenuBasket.sidebars")
);
const OrderStepsView = dynamic(() =>
  import("../../src/views/OrderSteps.views")
);

import axiosConfig, { localeHeaderConfig } from "../../config/axios";
import { saleMethods } from "../../utilities/areaPathHandlers";
import { cookiesNameSpaces } from "../../utilities/appWideHelperFunctions";

const { SALE_METHOD, SHOP_IDENTITY, LOCATION_DETAIL, ORDER_ID } =
  cookiesNameSpaces;

const OrderStepsViewLoading = () => (
  <Grid container flexDirection="column">
    <Skeleton
      variant="rectangular"
      component="div"
      sx={{ height: { xs: "50vh", md: 400 } }}
    />
    <Skeleton variant="text" component="div" sx={{ mt: 5 }} />
    <Skeleton
      variant="rectangular"
      component="div"
      sx={{ height: { xs: 40, md: 40 }, width: 200 }}
    />
  </Grid>
);

const OrderSteps = ({ shopDetail, saleMethod, basketData }) => {
  const router = useRouter();
  const { t } = useTranslation("order_steps_page");
  const { showNotification } = useNotification();
  const { getOrderId, removeOrderId } = useOrder();
  const { postCode } = useLocation();

  const [loading, setLoading] = useState(true);

  const orderId = useMemo(() => {
    const rawOrderId = getOrderId();
    return rawOrderId ? rawOrderId.orderId : rawOrderId;
  }, []);

  //! Check payment
  useEffect(() => {
    const paymentStatusCheck = async () => {
      if (router && orderId) {
        setLoading(true);
        const { redirect_status } = router.query;

        if (redirect_status !== "failed") {
          try {
            await axiosConfig.patch(`/Checkout/Complete/${orderId}`);
          } catch (error) {}
          removeOrderId();
          showNotification(t('successful_payment_message'), "success");
          setLoading(false);
        } else {
          showNotification(
            t('failed_payment_message'),
            "error",
            3000
          );
        }
      }
      // setLoading(false);
    };

    paymentStatusCheck();
  }, []);

  return (
    <>
      <Head>{/* Some Header Property */}</Head>
      <LoggedNeedPagesLayout>
        <Grid item flexGrow={1} md={8} xs={12}>
          {loading ? (
            <OrderStepsViewLoading />
          ) : (
            <OrderStepsView
              shopDetail={shopDetail}
              saleMethod={saleMethod}
              orderNumber={orderId}
            />
          )}
        </Grid>
        <Grid item md={4} pl={3} display={{ xs: "none", md: "block" }}>
          {loading ? (
            <Skeleton
              variant="rectangular"
              sx={{ display: { xs: "none", md: "block" }, height: "100%" }}
            />
          ) : (
            <ShopMenuBasketSidebar
              basketData={basketData}
              shopDetail={shopDetail}
              saleMethod={saleMethod}
              postCode={postCode}
              basketUpdateHandler={() => "temp"}
              gotoCheckOutButton={false}
            />
          )}
        </Grid>
      </LoggedNeedPagesLayout>
    </>
  );
};

export default OrderSteps;

export async function getServerSideProps({ req, res, locale }) {
  const mealzoIdentity = getCookie(SHOP_IDENTITY, { req, res }) || null;

  // Get SaleMethod
  const saleMethodKey = getCookie(SALE_METHOD, { req, res }) || "delivery";
  const saleMethod = saleMethods[saleMethodKey].code;

  // Get PostCode
  let [postCode, lat, lon] = ["all", 0, 0];
  const rawPostCode = getCookie(LOCATION_DETAIL, { req, res }) || "all";
  if (rawPostCode !== "all") {
    const { DeliveryPostCode, Lat, Lon } = JSON.parse(rawPostCode);
    postCode = DeliveryPostCode;
    lat = Lat;
    lon = Lon;
  }

  // Fetching Shop Detail
  const shopDetail = await (
    await axiosConfig.get(
      `/ShopDetails/GetByIdentity/${mealzoIdentity}/${postCode}/${saleMethod}/${lat}/${lon}`, localeHeaderConfig(locale)
    )
  ).data;

  // Basket data
  const rawOrderId = getCookie(ORDER_ID, { req, res }) || 0;
  const orderId = rawOrderId !== 0 ? JSON.parse(rawOrderId) : 0;
  const basketData =
    typeof orderId === "object" &&
    shopDetail.address.shopId === parseInt(orderId.shopId)
      ? (
          await axiosConfig.get(
            `/Basket/${shopDetail.address.shopId}/${orderId.orderId}`, localeHeaderConfig(locale)
          )
        ).data
      : null;

  return {
    props: {
      shopDetail: shopDetail,
      saleMethod: saleMethodKey,
      basketData: basketData,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "basket",
        "shop_info_modal",
        "order_steps_page",
      ])),
    },
  };
}
