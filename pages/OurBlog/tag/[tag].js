import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import Grid from "@mui/material/Grid";

const PageContainer = dynamic(() => import("../../../src/containers/Page.containers"))
const MainFooter = dynamic(() => import("../../../src/footers/Main.footers"))
const GetToKnowUsAppBar = dynamic(() => import("../../../src/appbars/StaticPage.appbars"))
const StaticPageHeader = dynamic(() => import("../../../src/headers/StaticPage.headers"))
const SectionContainer = dynamic(() => import("../../../src/containers/Section.containers"))
const BlogsSidebar = dynamic(() => import("../../../src/sidebars/Blog.sidebars"))
const BlogInfiniteScroll = dynamic(() => import("../../../src/infinitescrolls/Blog.infinitescrolls"))

import OurBlogHeaderImage from "../../../public/Images/OurBlog.png";

import axiosConfig, { localeHeaderConfig } from "../../../config/axios";

export default function BlogTag({ pageData }) {
  const { t } = useTranslation('blog_pages');

  const initialPosts =
    pageData.blogListViewModel.length > 0 ? pageData.blogListViewModel : null;

  // Tag Name
  const { tag } = pageData;

  // For Side Bar
  const tags = pageData.tagListViewModel;
  const recentPosts = pageData.recentBlogListViewModel;
  const archive = pageData.blogArchiveListViewModel;

  const title = `${t('tagged')}: ${tag} - Mealzo`;

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={title} />
        <meta property="og:title" content={title} />
        <meta name="twitter:title" content={title} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <GetToKnowUsAppBar />
        <StaticPageHeader title={t('page_header')} imageUrl={OurBlogHeaderImage} />
        <SectionContainer
          sx={{ maxWidth: 1300, mx: "auto", px: { xs: 2, sm: 4, lg: 0 } }}
        >
          <Grid container item md={8} xs={12} position="relative">
            <BlogInfiniteScroll
              initialPosts={initialPosts}
              pageData={pageData}
            />
          </Grid>

          <Grid container item md={4} xs={12} pl={{ md: 2, xs: 0 }}>
            <BlogsSidebar
              tags={tags}
              recentPosts={recentPosts}
              archive={archive}
            />
          </Grid>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getServerSideProps({ store, res, query, locale }) {
  const { tag } = query;

  const pageData = await axiosConfig.get(`/Blogs?tag=${tag}&pageNumber=1`, localeHeaderConfig(locale));

  return {
    props: {
      pageData: pageData.data,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "blog_pages",
      ])),
    },
  };
}

// BlogTag.getInitialProps = async ({ store, res }) => {
//   if (res) {
//     // res available only at server
//     // no-store disable bfCache for any browser. So your HTML will not be cached
//     res.setHeader("Cache-Control", "no-store");
//   }
// };
