import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import Grid from "@mui/material/Grid";

const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const MainFooter = dynamic(() => import("../../src/footers/Main.footers"));
const GetToKnowUsAppBar = dynamic(() =>
  import("../../src/appbars/StaticPage.appbars")
);
const StaticPageHeader = dynamic(() =>
  import("../../src/headers/StaticPage.headers")
);
const BlogsSidebar = dynamic(() => import("../../src/sidebars/Blog.sidebars"));
const SingleBlogCard = dynamic(() =>
  import("../../src/cards/SingleBlog.Cards")
);
const DataError = dynamic(() => import("../../src/errors/Data.errors"));
const SectionContainer = dynamic(() =>
  import("../../src/containers/Section.containers")
);

import OurBlogHeaderImage from "../../public/Images/OurBlog.png";

import axiosConfig, { localeHeaderConfig } from "../../config/axios";
import { queryParamDecode } from "../../utilities/areaPathHandlers";

export default function SingleBlog({ pageData }) {
  const { t } = useTranslation("blog_pages");

  const post =
    pageData.blogListViewModel.length > 0
      ? pageData.blogListViewModel[0]
      : null;

  // For Title
  const identity = queryParamDecode(pageData.identity);

  // For Side Bar
  const tags = pageData.tagListViewModel;
  const recentPosts = pageData.recentBlogListViewModel;
  const archive = pageData.blogArchiveListViewModel;

  const title = `${identity} - Mealzo`;

  return (
    <>
      <Head>
        <title>{title}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={title} />
        <meta property="og:title" content={title} />
        <meta name="twitter:title" content={title} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <GetToKnowUsAppBar />
        <StaticPageHeader title={t('page_header')} imageUrl={OurBlogHeaderImage} />
        <SectionContainer>
          <Grid container spacing={2}>
            <Grid item md={8} sm={12}>
              {/* <HighCommissionersCards cardData={highComData} /> */}
              {post ? <SingleBlogCard cardData={post} /> : <DataError />}
            </Grid>
            <Grid item md={4} sm={12}>
              <BlogsSidebar
                tags={tags}
                recentPosts={recentPosts}
                archive={archive}
              />
            </Grid>
          </Grid>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getServerSideProps({ query, locale }) {
  const [category, postIdentity] = query.blogDetail;

  const pageData = await axiosConfig.get(
    `/Blogs?identity=${postIdentity}&pageNumber=1`, localeHeaderConfig(locale)
  );

  return {
    props: {
      pageData: pageData.data,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "blog_pages",
      ])),
    },
  };
}
