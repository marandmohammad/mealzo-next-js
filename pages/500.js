import Image from "next/image";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { Button, Grid } from "@mui/material";

import ErrorPagesTypography from "../src/typographies/ErrorPages.typographies";

import Error500Image from "../public/Images/error-images/Error500Image.png";
import MealzoLogo from "../public/Images/mealzo.png";

export default function Custom500() {
  const { t } = useTranslation('error_pages');

  return (
    <Grid
      height="100%"
      display="flex"
      flexDirection="column"
      py={{ xs: 3, md: 5 }}
      px={{ xs: 1, md: 10 }}
      sx={{
        backgroundImage: "url(/Images/error-images/ErrorPageBackground.jpg)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <style jsx global>{`
        body,
        #__next {
          overflow: hidden;
          height: 100vh;
        }
      `}</style>
      <Grid item container>
        <Image src={MealzoLogo} height={40} width={180} />
      </Grid>
      <Grid
        item
        container
        flexGrow={1}
        alignItems="center"
        justifyContent={{ lg: "center" }}
        flexDirection="column"
      >
        <Grid
          item
          width={{ xs: "100%", md: 700 }}
          height={{ xs: "50%", md: 500 }}
          maxHeight={{ xs: "50%", md: "35%" }}
          position="relative"
        >
          <Image src={Error500Image} layout="fill" objectFit="contain" />
        </Grid>
        <Grid item mb={5}>
          <ErrorPagesTypography fontSize={{ xs: 34, md: 64 }}>
            {t('500_main')}
          </ErrorPagesTypography>
          <ErrorPagesTypography fontSize={{ xs: 18, md: 24 }} maxWidth={600}>
            {t('500_text')}
          </ErrorPagesTypography>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            LinkComponent="a"
            href="/"
            sx={{
              borderRadius: "8px",
              fontWeight: 700,
              fontSize: { xs: 22, md: 32 },
            }}
          >
            {t('404_go_home_btn')}
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["error_pages"])),
    },
  };
}
