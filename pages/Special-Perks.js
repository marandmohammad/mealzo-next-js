import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const StaticPageHeader = dynamic(() => import("../src/headers/StaticPage.headers"))
const StaticPageAppBar = dynamic(() => import("../src/appbars/StaticPage.appbars"))
const PageContainer = dynamic(() => import("../src/containers/Page.containers"))
const MainFooter = dynamic(() => import("../src/footers/Main.footers"))
const LandingSectionContainer = dynamic(() => import("../src/containers/LandingSection.containers"))
const AboutMealzoCard = dynamic(() => import("../src/cards/AboutMealzo.cards"))

import SpecialPerksHeaderImage from "../public/Images/SpecialPerks.png";
import ExclusiveDiscountsImage from "../public/Images/exclusivediscounts.svg";
import PriceGuaranteeImage from "../public/Images/Priceguarantee.svg";
import LatestTechnologyImage from "../public/Images/Latesttechnology.svg";
import ThePerksImage from "../public/Images/theperks.svg";

export default function SpecialPerks() {
  const { t } = useTranslation("special_perks_page");

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader
          title={t('header_title')}
          imageUrl={SpecialPerksHeaderImage}
        />
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('exclusive_discounts_title')}
            description={t('exclusive_discounts_text')}
            imageUrl={ExclusiveDiscountsImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('price_guarantee_title')}
            description={t('price_guarantee_text')}
            imageUrl={PriceGuaranteeImage}
            flexDirection="row-reverse"
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('latest_tech_title')}
            description={t('latest_tech_text')}
            imageUrl={LatestTechnologyImage}
          />
        </LandingSectionContainer>
        <LandingSectionContainer sx={{ my: 5 }}>
          <AboutMealzoCard
            title={t('the_perks_title')}
            description={t('the_perks_text')}
            imageUrl={ThePerksImage}
            flexDirection="row-reverse"
          />
        </LandingSectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "special_perks_page",
      ])),
    },
  };
}