import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const StaticPageHeader = dynamic(() =>
  import("../src/headers/StaticPage.headers")
);
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));

import MealzoNotifyImage from "../public/Images/MelazoNotify.png";


export default function PrivacyPolicy() {
  const { t } = useTranslation("privacy_policy_page");

  return (
    <>
      <Head>
        <title>{t("page_title")}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t("page_title")} />
        <meta property="og:title" content={t("page_title")} />
        <meta name="twitter:title" content={t("page_title")} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader
          title={t("page_header")}
          imageUrl={MealzoNotifyImage}
          bgColor="secondary"
        />
        <SectionContainer
          flexDirection="column"
          dangerouslySetInnerHTML={{
            __html: t("body"),
          }}
        />
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "privacy_policy_page",
      ])),
    },
  };
}
