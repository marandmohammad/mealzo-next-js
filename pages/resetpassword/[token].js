import { Fragment, useState, useEffect } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";

import { CircularProgress, Grid, Box, Typography } from "@mui/material";

const PageContainer = dynamic(() =>
  import("../../src/containers/Page.containers")
);
const StaticPageAppBar = dynamic(() =>
  import("../../src/appbars/StaticPage.appbars")
);
const PasswordInput = dynamic(() => import("../../src/inputs/Password.inputs"));
const LoadingPrimaryButton = dynamic(() =>
  import("../../src/buttons/LoadingPrimary.buttons")
);
const Link = dynamic(() => import("../../src/links/Link"));

import { recoveryPasswordFormikSetup } from "../../utilities/formikSetups";
import axiosConfig from "../../config/axios";

const WithToken = ({
  isTokenValid,
  validationErrorMessage,
  overAllLoading,
  t,
  children,
}) =>
  !overAllLoading ? (
    isTokenValid ? (
      <Fragment>{children}</Fragment>
    ) : (
      <Fragment>
        <Typography fontSize={32} fontWeight={700} textAlign="center">
          {t("set_new_pass")}
        </Typography>

        <Typography fontSize={20} fontWeight={500} textAlign="center" mb={5}>
          {validationErrorMessage}
        </Typography>

        <LoadingPrimaryButton variant="contained" LinkComponent={Link} href="/">
          {t('back_to_login')}
        </LoadingPrimaryButton>
      </Fragment>
    )
  ) : (
    <CircularProgress color="primary" />
  );

function ChangePasswordPage() {
  const { t } = useTranslation("reset_password_page");
  const { showNotification } = useNotification();
  const router = useRouter();
  const { token } = router.query;

  const formik = recoveryPasswordFormikSetup();

  const [loading, setLoading] = useState(false);
  const [overAllLoading, setOverAllLoading] = useState(true);
  const [isTokenValid, setIsTokenValid] = useState(false);
  const [tokenValidationErrorMessage, setTokenValidationErrorMessage] =
    useState(null);
  const [isSucceeded, setIsSucceeded] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);

  //! Validate token
  useEffect(() => {
    const validateToken = async () => {
      if (!token) {
        setIsTokenValid(false);
        return;
      }
      setOverAllLoading(true);
      try {
        await axiosConfig.post("/Account/ForgetPasswordValidation", {
          identity: token,
        });
        setIsTokenValid(true);
      } catch (error) {
        const errorMessage =
          typeof error.response !== "undefined"
            ? error.response.data.message[0]
            : error.message;

        setIsTokenValid(false);
        setTokenValidationErrorMessage(errorMessage);
      }
      setOverAllLoading(false);
    };

    validateToken();
  }, [token]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!formik.isValid) {
      const errorKeys = Object.keys(formik.errors);
      showNotification(formik.errors[errorKeys[0]], "error", 3000);
      return;
    }
    await setLoading(true);
    const { password, confirmPassword } = formik.values;
    try {
      const result = await (
        await axiosConfig.post("/Account/ResetPassword", {
          identity: token,
          password: password,
          ConfirmPassword: confirmPassword,
        })
      ).data;

      await setIsSucceeded(true);
      await setSuccessMessage(result.message[0]);
    } catch (error) {
      const errorMessage =
        typeof error.response !== "undefined"
          ? error?.response?.data?.message[0]
          : error.message;

      showNotification(errorMessage, "error", 3000);
    }
    await setLoading(false);
  };

  return (
    <Fragment>
      <PageContainer flexDirection="column">
        <StaticPageAppBar
          sx={{
            boxShadow: "none",
            bgcolor: "white",
            borderBottomWidth: 1,
            borderBottomStyle: "solid",
            borderBottomColor: "divider",
          }}
        />
        <Grid
          container
          item
          sx={{
            px: { lg: 20, md: 10, sm: 5, xs: 2 },
            pt: 3,
            justifyContent: "center",
          }}
        >
          <Box
            component={!isSucceeded ? "form" : "div"}
            display="flex"
            flexDirection="column"
            maxWidth={400}
            gap={5}
            onSubmit={handleSubmit}
          >
            <WithToken
              isTokenValid={isTokenValid}
              validationErrorMessage={tokenValidationErrorMessage}
              overAllLoading={overAllLoading}
              t={t}
            >
              <Typography fontSize={32} fontWeight={700} textAlign="center">
                {t("set_new_pass")}
              </Typography>

              <Typography
                fontSize={20}
                fontWeight={500}
                textAlign="center"
                mb={5}
              >
                {isSucceeded
                  ? successMessage
                  : t("repetitive_password_message")}
              </Typography>

              {!isSucceeded ? (
                <Fragment>
                  <PasswordInput
                    name="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.errors.password}
                  >
                    {t('new_password_input_label')}
                  </PasswordInput>
                  <PasswordInput
                    name="confirmPassword"
                    value={formik.values.confirmPassword}
                    onChange={formik.handleChange}
                    error={formik.errors.confirmPassword}
                  >
                    {t('confirm_password_input_label')}
                  </PasswordInput>

                  <LoadingPrimaryButton
                    variant="contained"
                    type="submit"
                    disabled={!formik.isValid}
                    loading={loading}
                  >
                    {t('reset_password_btn')}
                  </LoadingPrimaryButton>
                </Fragment>
              ) : (
                <LoadingPrimaryButton
                  variant="contained"
                  LinkComponent={Link}
                  href="/"
                >
                  {t('back_to_login')}
                </LoadingPrimaryButton>
              )}
            </WithToken>
          </Box>
        </Grid>
      </PageContainer>
    </Fragment>
  );
}

export default ChangePasswordPage;

export async function getServerSideProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "reset_password_page",
      ])),
    },
  };
}
