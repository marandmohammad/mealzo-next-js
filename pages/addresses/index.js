import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import dynamic from "next/dynamic";
import Head from "next/head";

const LoggedNeedPagesLayout = dynamic(() => import("../../src/layouts/LoggedNeedPages.layouts"));
const AddressesView = dynamic(() => import("../../src/views/Addresses.views"));

const AddressesPage = () => {
  return (
    <>
      <Head>{/* Some Header Properties */}</Head>
      <LoggedNeedPagesLayout>
        <AddressesView />
      </LoggedNeedPagesLayout>
    </>
  );
};

export default AddressesPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "basket",
        "addresses",
        // "previous_orders_page",
      ])),
    },
  };
}