import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const PageContainer = dynamic(() => import("../src/containers/Page.containers"));
const StaticPageAppBar = dynamic(() => import("../src/appbars/StaticPage.appbars"));
const StaticPageHeader = dynamic(() => import("../src/headers/StaticPage.headers"));

const SectionContainer = dynamic(() => import("../src/containers/Section.containers"));
const AboutMealzoCard = dynamic(() => import("../src/cards/AboutMealzo.cards"));
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));

import AboutUsHeaderImage from "../public/Images/MelazoNotify.png";
import OurMissionImage from "../public/Images/ourMission.png";
import OurPartnersImage from "../public/Images/OurPartners.svg";
import OurCustomersImage from "../public/Images/OurCustomers.png";
import OurtechnologyImage from "../public/Images/Ourtechnology.png";

export default function AboutUs() {
  const { t } = useTranslation('about_us_page');
  
  return (
    <>
      <Head>
        <title>{t('page_title')}</title>
        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t('page_title')} />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={t('page_title')} />
        <meta name="twitter:title" content={t('page_title')} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
        <meta itemProp="image" content="Images/header.jpg" />
        <meta property="og:image" content="Images/header.jpg" />
        <meta name="twitter:card" content="Images/header.jpg" />
        <meta name="twitter:image" content="Images/header.jpg" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader title={t('page_header')} imageUrl={AboutUsHeaderImage} />
        <SectionContainer>
          <AboutMealzoCard
            title={t('our_mission_title')}
            description={t('our_mission_text')}
            imageUrl={OurMissionImage}
          />
          <AboutMealzoCard
            title={t('our_partner_title')}
            description={t('our_partner_text')}
            imageUrl={OurPartnersImage}
            flexDirection="row-reverse"
          />
          <AboutMealzoCard
            title={t('our_customers_title')}
            description={t('our_customers_text')}
            imageUrl={OurCustomersImage}
          />
          <AboutMealzoCard
            title={t('our_technology_title')}
            description={t('our_technology_text')}
            imageUrl={OurtechnologyImage}
            flexDirection="row-reverse"
          />
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "about_us_page",
      ])),
    },
  };
}
