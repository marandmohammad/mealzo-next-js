import dynamic from "next/dynamic";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import Box from "@mui/material/Box";

const PageContainer = dynamic(() =>
  import("../src/containers/Page.containers")
);
const StaticPageAppBar = dynamic(() =>
  import("../src/appbars/StaticPage.appbars")
);
const MainFooter = dynamic(() => import("../src/footers/Main.footers"));
const StaticPageHeader = dynamic(() =>
  import("../src/headers/StaticPage.headers")
);
const LocalHeroInfiniteScroll = dynamic(() =>
  import("../src/infinitescrolls/LocalHero.infinitescrolls")
);
const SectionContainer = dynamic(() =>
  import("../src/containers/Section.containers")
);

import localHeroesImage from "../public/Images/localheroes.png";

import axiosConfig, { localeHeaderConfig } from "../config/axios";

const LocalHeros = ({ pageData }) => {
  const { t } = useTranslation("local_hero_page");

  return (
    <>
      <Head>
        <title>{t("page_title")}</title>

        <meta name="description" />
        <meta name="keywords" />
        <meta itemProp="name" content={t("page_title")} />
        <meta property="og:title" content={t("page_title")} />
        <meta name="twitter:title" content={t("page_title")} />
        <meta itemProp="description" />
        <meta property="og:description" />
        <meta name="twitter:description" />
      </Head>
      <PageContainer>
        <StaticPageAppBar />
        <StaticPageHeader
          title={t("page_header")}
          imageUrl={localHeroesImage}
        />
        <SectionContainer sx={{ px: { xs: 1, sm: 5 } }}>
          <Box sx={{ flexGrow: 1, maxWidth: 1300, mx: "auto" }}>
            <LocalHeroInfiniteScroll pageData={pageData} />
          </Box>
        </SectionContainer>
      </PageContainer>
      <MainFooter />
    </>
  );
};

export default LocalHeros;

export async function getServerSideProps({ locale }) {
  const heroes = await axiosConfig.get(
    "/Heroes?pageNumber=1",
    localeHeaderConfig(locale)
  );

  return {
    props: {
      pageData: heroes.data,
      ...(await serverSideTranslations(locale, [
        "common",
        "login_form",
        "sign_up_form",
        "drawer",
        "footer",
        "local_hero_page",
      ])),
    },
  };
}
