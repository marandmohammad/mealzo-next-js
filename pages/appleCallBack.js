import { useEffect } from "react";
// import { serverSideTranslations } from "next-i18next/serverSideTranslations";
// import { useTranslation } from "next-i18next";
import getRawBody from "raw-body";
import jwt_decode from "jwt-decode";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";
import { useAuth } from "../hooks/authentication";
import useOrder from "../hooks/orders";

import { Button, Grid, Typography } from "@mui/material";

import axiosConfig from "../config/axios";
import {
  cookiesNameSpaces,
  appInfoForAuthentication,
  accountTypes,
} from "../utilities/appWideHelperFunctions";
const { APPLE_SIGN_IN_LOCATION, APPLE_SIGN_IN_LOCALE } = cookiesNameSpaces;

export default function appleCallBack({ error, loggedInUserDetails }) {
  // const { t } = useTranslation('apple_callback_page')
  const router = useRouter();
  const { signIn } = useAuth();
  const { getOrderId } = useOrder();

  useEffect(() => {
    if (!error && loggedInUserDetails) {
      const orderId = getOrderId();
      signIn(loggedInUserDetails, orderId);
      const redirectUrl = getCookie(APPLE_SIGN_IN_LOCATION);
      router.replace(redirectUrl);
    }
  }, []);

  return (
    <Grid container mt={5} justifyContent="center" alignItems="center">
      {error ? (
        <Typography variant="subtitle1" component="div">
          An error accrued: {error} <br />
          <Button variant="text" onClick={() => router.back()}>
            try again
          </Button>
        </Typography>
      ) : (
        <Typography variant="h6" component="div">
          Welcome to Mealzo
        </Typography>
      )}
    </Grid>
  );
}

export async function getServerSideProps({ req, locale }) {
  try {
    if (req.method !== "POST")
      return {
        props: {
          error: `Apple didn't confirmed your data`,
        },
      };

    const body = await getRawBody(req);
    const stringBody = body.toString("utf-8");

    const splittedByAnd = stringBody.split("&");

    let formObject = {};

    splittedByAnd.forEach((el) => {
      const splittedBodyByEqual = el.split("=");
      formObject[splittedBodyByEqual[0]] = splittedBodyByEqual[1];
    });

    const { id_token } = formObject;

    const { sub, email } = jwt_decode(id_token);

    const reqData = {
      fullName: "",
      email: email,
      openId: sub,
      accountType: accountTypes.apple,
      ...appInfoForAuthentication(),
    };

    const mealzoLogin = await (
      await axiosConfig.post("/Account/ExternalSignup", reqData)
    ).data;

    return {
      props: {
        error: null,
        loggedInUserDetails: mealzoLogin,
        // ...(await serverSideTranslations(locale, ["apple_callback_page"])),
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
        // ...(await serverSideTranslations(locale, ["apple_callback_page"])),
      },
    };
  }
}
