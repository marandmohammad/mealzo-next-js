import { useEffect } from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import Script from "next/script";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider } from "@emotion/react";
import createEmotionCache from "../src/createEmotionCache";
import NextNProgress from "nextjs-progressbar";
import { appWithTranslation } from "next-i18next";
import { setCookie } from "cookies-next";
import { useRouter } from "next/router";

import theme from "../src/themes/mealzoTheme";

//! Custom Contexts And Providers
import AuthProvider from "../context/AuthContext";
import OrderProvider from "../context/OrderContext";
import PostCodeAddressProvider from "../context/PostCodeAndAddressContext";
import NotificationProvider from "../context/NotificationContext";

import { cookiesNameSpaces } from "../utilities/appWideHelperFunctions";

import "../src/css/StaticCss.styles.css";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  const router = useRouter();
  const { locale } = router;

  useEffect(() => {
    setCookie(cookiesNameSpaces.LOCALE, locale);
  }, [locale]);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <link type="image/x-icon" rel="shortcut icon" href="/Images/logo.png" />
        <meta
          name="viewport"
          content="initial-scale=1, width=device-width, maximum-scale=1.0, user-scalable=0"
        />
        <meta property="og:url" content={process.env.HOST} />
        <meta property="og:type" content="website" />
        <meta itemProp="image" content="/Images/header.jpg" />
        <meta property="og:image" content="/Images/header.jpg" />
        <meta name="twitter:card" content="/Images/header.jpg" />
        <meta name="twitter:image" content="/Images/header.jpg" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <NextNProgress
          color={theme.palette.primary.main}
          height={3}
          showOnShallow={false}
          options={{ showSpinner: false }}
        />

        <NotificationProvider>
          <AuthProvider>
            <PostCodeAddressProvider>
              <OrderProvider>
                <Component {...pageProps} />

                {/* Google tag (gtag.js) */}
                <Script
                  async
                  src="https://www.googletagmanager.com/gtag/js?id=G-PHNWMNS42W"
                />
                <Script
                  dangerouslySetInnerHTML={{
                    __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', 'G-PHNWMNS42W');
                    `,
                  }}
                />
              </OrderProvider>
            </PostCodeAddressProvider>
          </AuthProvider>
        </NotificationProvider>
      </ThemeProvider>
    </CacheProvider>
  );
}

export default appWithTranslation(MyApp);

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object.isRequired,
};
