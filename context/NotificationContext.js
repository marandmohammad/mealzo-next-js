import { createContext, useState, forwardRef } from "react";
import { useTranslation } from "next-i18next";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide,
} from "@mui/material";

import MainSnackBar from "../src/snackbars/Main.snackbars";

export const NotificationContext = createContext();

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function NotificationProvider({ children }) {
  const { t } = useTranslation('common');
  const DEFAULT_NOTIFICATION_DURATION = 1500;

  //! Notification State Definition
  const [notification, setNotification] = useState({
    open: false,
    autoHideDuration: DEFAULT_NOTIFICATION_DURATION,
    severity: "success",
    text: "",
    anchorOrigin: { vertical: "top", horizontal: "center" },
  });

  //! Dialog State Definition
  const [dialog, setDialog] = useState({
    open: false,
    title: "",
    text: "",
    onAgree: null,
    onDesAgree: null,
  });

  //! Notification Functions
  const hideNotification = () =>
    setNotification({ ...notification, open: false });

  const showNotification = (
    // open,
    text,
    severity = "success",
    autoHideDuration = DEFAULT_NOTIFICATION_DURATION,
    anchorOrigin = { vertical: "top", horizontal: "center" }
  ) =>
    setNotification({
      open: true,
      autoHideDuration,
      severity,
      text,
      anchorOrigin,
    });

  const isShowingNotification = notification.open;

  //! Dialog Functions
  const hideDialog = () => setDialog({ ...dialog, open: false });

  const showDialog = (title, text, onAgree, onDesAgree) =>
    setDialog({
      open: true,
      title,
      text,
      onAgree,
      onDesAgree,
    });

  const isShowingDialog = dialog.open;

  return (
    <NotificationContext.Provider
      value={{
        showNotification,
        hideNotification,
        isShowingNotification,
        showDialog,
        hideDialog,
        isShowingDialog,
      }}
    >
      {children}
      <MainSnackBar
        open={notification.open}
        handleClose={hideNotification}
        autoHideDuration={notification.autoHideDuration}
        severity={notification.severity}
        anchorOrigin={notification.anchorOrigin}
      >
        {notification.text}
      </MainSnackBar>

      <Dialog
        open={dialog.open}
        TransitionComponent={Transition}
        keepMounted
        onClose={hideDialog}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{dialog.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {dialog.text}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={dialog.onDesAgree}>{t('global_cancel')}</Button>
          <Button onClick={dialog.onAgree}>{t('global_confirm')}</Button>
        </DialogActions>
      </Dialog>
    </NotificationContext.Provider>
  );
}

export default NotificationProvider;
