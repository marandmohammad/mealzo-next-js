import { createContext, useState } from "react";
import { getCookie, removeCookies, setCookies } from "cookies-next";
import useNotification from "../hooks/useNotification";

import {
  cookiesNameSpaces,
  buildStructuredAddress,
} from "../utilities/appWideHelperFunctions";
import axiosConfig from "../config/axios";
import { saleMethods } from "../utilities/areaPathHandlers";

const { LOCATION_DETAIL, SELECTED_ADDRESS } = cookiesNameSpaces;

const initialLocationDetail = getCookie(LOCATION_DETAIL) || null;
const initialSelectedAddress = getCookie(SELECTED_ADDRESS) || null;

export const PostCodeAndAddressContext = createContext();

const PostCodeAddressProvider = ({ children }) => {
  const [locationDetail, setLocationDetail] = useState(
    initialLocationDetail
      ? JSON.parse(initialLocationDetail)
      : initialLocationDetail
  );
  const [selectedAddress, setSelectedAddress] = useState(
    initialSelectedAddress
      ? JSON.parse(initialSelectedAddress)
      : initialSelectedAddress
  );

  const { showNotification } = useNotification();

  //! Get Part
  const getLocationDetail = () => {
    if (locationDetail) return { ...locationDetail };
    return null;
  };

  const getSelectedAddress = () => {
    if (selectedAddress) return { ...selectedAddress };
    return null;
  };

  //! Get PostCode
  const postCode = locationDetail ? locationDetail.DeliveryPostCode : "all";

  //! Get Structured Address
  const getStructuredAddress = buildStructuredAddress(selectedAddress);

  //! Change Or Add New Location
  const rawLocationSet = (newLocation) => {
    locationDetail && removeCookies(LOCATION_DETAIL);
    setCookies(LOCATION_DETAIL, newLocation);
    setLocationDetail(newLocation);
  };

  const rawAddressChange = (newAddress) => {
    setCookies(SELECTED_ADDRESS, newAddress);
    setSelectedAddress(newAddress);

    const {
      deliveryAddress1,
      deliveryCity,
      deliveryPostcode,
      deliveryLat,
      deliveryLong,
    } = newAddress;

    const newLocation = {
      Address: `${deliveryAddress1}, ${deliveryCity}`,
      DeliveryPostCode: deliveryPostcode,
      Lat: deliveryLat,
      Lon: deliveryLong,
      PlaceId: null,
    };

    rawLocationSet(newLocation);
  };

  //! Check Delivery Available
  const isDeliveryAvailable = async (
    postCode,
    saleMethod,
    shopId = null,
    orderId = 0
  ) => {
    try {
      const result = await axiosConfig.patch(
        `/Basket/MinimumDelivery/${orderId}/${postCode}`,
        {
          shopId,
          saleMethod,
        }
      );

      return true;
    } catch (error) {
      if (typeof error.response !== "undefined")
        showNotification(error?.response?.data?.message[0], "error");
      else showNotification(error?.message, "error");
      return false;
    }
  };

  //! Set Part
  const addLocationDetail = async (
    newLocation,
    saleMethod,
    shopId = null,
    orderId = 0,
    checkHasDelivery = true
  ) => {
    const postCode =
      typeof newLocation.DeliveryPostCode !== "undefined"
        ? newLocation.DeliveryPostCode
        : null;

    if (saleMethod === saleMethods.delivery.code) {
      //? if we don't have shopId then we don't need to need to check delivery
      if (shopId && shopId > 0) {
        //? Check Does Shop Has Delivery To The Selected Address
        const hasDelivery = await isDeliveryAvailable(
          postCode,
          saleMethod,
          shopId,
          orderId
        );

        //? should we consider hasDelivery
        if (checkHasDelivery) {
          if (hasDelivery) rawLocationSet(newLocation);
          else return false;
        } else {
          rawLocationSet(newLocation);
        }
      } else {
        rawLocationSet(newLocation);
      }
    } else {
      rawLocationSet(newLocation);
    }
    return true;
  };

  const addSelectedAddress = async (
    newAddress,
    saleMethod,
    shopId = null,
    orderId = 0,
    setBasket = null,
    checkHasDelivery = true
  ) => {
    // console.log(checkHasDelivery, orderId, shopId);
    const postCode =
      typeof newAddress?.deliveryPostcode !== "undefined"
        ? newAddress.deliveryPostcode
        : null;

    //? Check delivery or collection
    if (saleMethod === saleMethods.delivery.code) {
      //? if we don't have shopId then we don't need to need to check delivery
      if (shopId && shopId > 0) {
        //? Check Does Shop Has Delivery To The Selected Address
        const hasDelivery = await isDeliveryAvailable(
          postCode,
          saleMethod,
          shopId,
          orderId
        );

        if (checkHasDelivery || hasDelivery) {
          if (hasDelivery) {
            if (orderId && orderId > 0) {
              try {
                const newBasket = await (
                  await axiosConfig.patch(
                    `/Basket/ChangeAddress/${shopId}/${orderId}/${newAddress.id}`
                  )
                ).data;
                setBasket(newBasket);
              } catch (error) {
                showNotification(error.message, "error");
                return false;
              }
            }

            rawAddressChange(newAddress);
          } else return false;
        } else {
          rawAddressChange(newAddress);
          return true;
        }
      } else {
        rawAddressChange(newAddress);
      }
    } else {
      rawAddressChange(newAddress);
    }

    return true;
  };

  //! Update part
  const updateLocationDetail = () => {
    const initialLocationDetail = getCookie(LOCATION_DETAIL) || null;
    setLocationDetail(
      initialLocationDetail
        ? JSON.parse(initialLocationDetail)
        : initialLocationDetail
    );
  };

  const updateSelectedAddress = () => {
    const initialSelectedAddress = getCookie(SELECTED_ADDRESS) || null;
    setSelectedAddress(
      initialSelectedAddress
        ? JSON.parse(initialSelectedAddress)
        : initialSelectedAddress
    );
  };

  return (
    <PostCodeAndAddressContext.Provider
      value={{
        getLocationDetail,
        getSelectedAddress,
        addLocationDetail,
        addSelectedAddress,
        postCode,
        getStructuredAddress,
        updateLocationDetail,
      }}
    >
      {children}
    </PostCodeAndAddressContext.Provider>
  );
};

export default PostCodeAddressProvider;

/*
    {
        "id": 111,
        "customerId": 134,
        "deliveryAddress1": "dsfsdafdsfsadf",
        "deliveryAddress2": null,
        "deliveryAddress3": null,
        "deliveryCity": "sdfasdf",
        "deliveryPostcode": "G42",
        "deliveryAddressName": null,
        "deliveryLat": 55.8319405,
        "deliveryLong": -4.2505862,
        "riderInstruction": null
    }
 */
/*
{
    "Address": "Graham St, Airdrie ML6 6DE, UK",
    "DeliveryPostCode": "ML6 6DE",
    "Lat": 55.8665458,
    "Lon": -3.9755049,
    "PlaceId": "ChIJDfMueQNsiEgRVoV0PYuc-Do"
}
 */
