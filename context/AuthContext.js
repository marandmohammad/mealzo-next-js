import { createContext, useState } from "react";
import { cookiesNameSpaces } from "../utilities/appWideHelperFunctions";
import { getCookie, setCookies, removeCookies } from "cookies-next";
import axiosConfig from "../config/axios";
import useNotification from "../hooks/useNotification";

const { AUTHENTICATION } = cookiesNameSpaces;

const currentUser = getCookie(AUTHENTICATION) || null;

export const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const { showNotification } = useNotification();
  const [user, setUser] = useState(
    currentUser ? JSON.parse(currentUser) : currentUser
  );

  const signIn = async (user, orderId = null) => {
    setUser(user);
    setCookies(AUTHENTICATION, user);
    const { customerId, accessToken } = user;
    if (orderId)
      try {
        const result = await axiosConfig.patch(
          `/Basket/UpdateCustomer/${orderId.orderId}/${customerId}`
        );
      } catch (e) {
        showNotification(e.message, "error", 3000);
      }
  };

  const signOut = () => {
    setUser(null);
    removeCookies(AUTHENTICATION);
  };

  const getUser = () => {
    let _user;
    if (user && typeof user === "object") _user = { ...user };
    else _user = user;
    return _user;
  };

  const isLogin = !!user;

  return (
    <AuthContext.Provider value={{ getUser, isLogin, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
