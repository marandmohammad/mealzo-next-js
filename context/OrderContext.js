import { createContext, useState } from "react";
import { setCookies, getCookie, removeCookies } from "cookies-next";

// import { useAuth } from "../hooks/authentication";
import useNotification from "../hooks/useNotification";
import { cookiesNameSpaces } from "../utilities/appWideHelperFunctions";
import axiosConfig from "../config/axios";

const { ORDER_ID } = cookiesNameSpaces;

export const OrderContext = createContext();

const initialOrder = getCookie(ORDER_ID) || null;

const OrderProvider = ({ children }) => {
  const { showNotification } = useNotification();
  // const { getUser } = useAuth();
  // const user = getUser();

  const [orderId, setOrderId] = useState(
    initialOrder ? JSON.parse(initialOrder) : initialOrder
  );

  const getOrderId = () => {
    let _orderId;

    if (orderId) _orderId = { ...orderId };
    else _orderId = orderId;

    return _orderId;
  };

  const addOrderId = (newOrderId) => {
    setOrderId(newOrderId);
    setCookies(ORDER_ID, newOrderId);
  };

  const removeOrderId = () => {
    removeCookies(ORDER_ID);
    setOrderId(null);
  };

  const checkMinimumDelivery = async (postCode, shopId, saleMethod) => {
    try {
      const result = await axiosConfig.patch(
        `/Basket/MinimumDelivery/${orderId.orderId}/${postCode}`,
        {
          shopId,
          saleMethod,
        }
      );
      return true;
    } catch (error) {
      if (typeof error.response !== 'undefined') showNotification(error.response.data.message[0], 'error');
      else showNotification(error.message, 'error');
      return false;
    }
  };

  // const updateBasketCustomer = async () => {
  //   const { customerId } = user;
  //   if (orderId)
  //     try {
  //       const result = await axiosConfig.patch(
  //         `/api/v1/Basket/UpdateCustomer/${orderId.orderId}/${customerId}`
  //       );
  //       console.log(result);
  //     } catch (e) {
  //       console.warn(e.message);
  //     }
  //   console.log('no orders')
  // };

  const changeOrderId = (newOrderId) => {
    removeOrderId();
    addOrderId(newOrderId);
  };

  return (
    <OrderContext.Provider
      value={{
        getOrderId,
        addOrderId,
        removeOrderId,
        changeOrderId,
        // updateBasketCustomer,
        checkMinimumDelivery,
      }}
    >
      {children}
    </OrderContext.Provider>
  );
};

export default OrderProvider;
