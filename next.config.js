const { i18n } = require("./next-i18next.config");

module.exports = {
  reactStrictMode: true,
  poweredByHeader: false,
  i18n,
  async rewrites() {
    return [
      {
        source: "/apps",
        destination: "/Apps",
      },
    ];
  },
};
