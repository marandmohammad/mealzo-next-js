import { useContext } from "react";
import { PostCodeAndAddressContext } from "../context/PostCodeAndAddressContext";

const useLocation = () => {
  const {
    getLocationDetail,
    getSelectedAddress,
    addLocationDetail,
    addSelectedAddress,
    postCode,
    getStructuredAddress,
    updateLocationDetail,
  } = useContext(PostCodeAndAddressContext);
  return {
    getLocationDetail,
    getSelectedAddress,
    addLocationDetail,
    addSelectedAddress,
    postCode,
    getStructuredAddress,
    updateLocationDetail,
  };
};

export default useLocation;
