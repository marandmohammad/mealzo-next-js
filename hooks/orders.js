import { useContext } from "react";
import { OrderContext } from "../context/OrderContext";

const useOrder = () => {
  const {
    getOrderId,
    addOrderId,
    removeOrderId,
    changeOrderId,
    checkMinimumDelivery,
  } = useContext(OrderContext);
  return {
    getOrderId,
    addOrderId,
    removeOrderId,
    changeOrderId,
    checkMinimumDelivery,
  };
};

export default useOrder;
