import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

export const useAuth = () => {
  const {getUser, isLogin, signIn, signOut} = useContext(AuthContext);
  return {getUser, isLogin, signIn, signOut};
};