const DEFAULT_ENV = "production";
const env =
  (() => {
    const { argv } = process;
    const envArgIndex = argv.indexOf("--env");
    if (envArgIndex === -1) return;
    return argv[envArgIndex + 1];
  })() || DEFAULT_ENV;

module.exports = {
  apps: [
    {
      name: "mealzo-test",
      namespace: "testGroup",
      exec_mode: "cluster",
      instances: 8,
      script: "node_modules/next/dist/bin/next",
      args: "start",
      env_development: {
        APP_ENV: "dev",
        PORT: 5603,
      },
      env_test: {
        APP_ENV: "test",
        PORT: 2001,
        HOSTNAME: "mealzo.weeorder.co.uk",
        NEXT_PUBLIC_BROWSER_HOST: "https://mealzo.weeorder.co.uk",
        NEXT_PUBLIC_API_BASE_URL: "https://testapi.mealzo.co.uk/api/v1",
      },
      // env_production: {
      //   APP_ENV: "prod",
      //   PORT: 2000,
      //   HOSTNAME: "mealzo.co.uk",
      //   NEXT_PUBLIC_BROWSER_HOST: "https://mealzo.co.uk",
      //   NEXT_PUBLIC_API_BASE_URL: "https://apis.mealzo.co.uk/api/v1",
      // },
    },
    {
      name: "mealzo-live",
      namespace: "liveGroup",
      exec_mode: "cluster",
      instances: "max",
      script: "node_modules/next/dist/bin/next",
      args: "start",
      env_development: {
        APP_ENV: "dev",
        PORT: 5603,
      },
      // env_test: {
      //   APP_ENV: "test",
      //   PORT: 2001,
      //   HOSTNAME: "mealzo.weeorder.co.uk",
      //   NEXT_PUBLIC_BROWSER_HOST: "https://mealzo.weeorder.co.uk",
      //   NEXT_PUBLIC_API_BASE_URL: "https://testapi.mealzo.co.uk/api/v1",
      // },
      env_production: {
        APP_ENV: "prod",
        PORT: 2000,
        HOSTNAME: "mealzo.co.uk",
        NEXT_PUBLIC_BROWSER_HOST: "https://mealzo.co.uk",
        NEXT_PUBLIC_API_BASE_URL: "https://apis.mealzo.co.uk/api/v1",
      },
    },
  ],
};
