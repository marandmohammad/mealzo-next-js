var autocompleteService, placesService, results, sessionToken;

function queryParamEncode(param) {
  param = param.replace(/ /g, "_");

  return param;
}

function initialize() {
  results = document.getElementById("results");
  sessionToken = new google.maps.places.AutocompleteSessionToken();

  // Bind listener for address search
  google.maps.event.addDomListener(
    document.getElementById("Address"),
    "keyup",
    function (event) {
      var validation = $("div.validation");
      validation.addClass("is-hide");
      $("div.searchInput").removeClass("validateInput");
      $(".focussearchbg").addClass("is-hide");
      var x = event.which || event.keyCode;
      if (x == 13) {
        $("button#search").trigger("click");
        return;
      }
      results.style.display = "block";
      getPlacePredictions(document.getElementById("Address").value);
    }
  );

  // Show results when address field is focused (if not empty)
  google.maps.event.addDomListener(
    document.getElementById("Address"),
    "focus",
    function () {
      if (document.getElementById("Address").value !== "") {
        results.style.display = "block";
        getPlacePredictions(document.getElementById("Address").value);
      }
    }
  );

  // Hide results when click occurs out of the results and inputs
  google.maps.event.addDomListener(document, "click", function (e) {
    if (
      e.target.parentElement !== null &&
      e.target.parentElement.className !== "pac-container" &&
      e.target.parentElement.className !== "autocompleteButton" &&
      e.target.tagName !== "INPUT"
    ) {
      results.style.display = "none";
    }
  });

  autocompleteService = new google.maps.places.AutocompleteService();
  placesService = new google.maps.places.PlacesService(
    document.getElementById("res")
  );
}

function getPlacePredictions(search) {
  autocompleteService.getPlacePredictions(
    {
      input: search,
      componentRestrictions: { country: "gb" },
      types: ["(regions)"],
      sessionToken: sessionToken,
    },
    callback
  );
}

function callback(predictions, status) {
  // Empty results container

  // Place service status error
  if (status != google.maps.places.PlacesServiceStatus.OK) {
    //results.innerHTML = '<div class="pac-item pac-item-error">Your search returned no result. Status: ' + status + '</div>';
    return;
  }
  ////// Build output with custom addresses
  ////results.innerHTML += '<div class="pac-item custom"><span class="pac-icon pac-icon-marker"></span>My home address</div>';
  ////results.innerHTML += '<div class="pac-item custom"><span class="pac-icon pac-icon-marker"></span>My work address</div>';
  results.innerHTML = "";
  var res = "";
  res += '<ul id="predictions">';
  // Build output for each prediction
  for (var i = 0, prediction; (prediction = predictions[i]); i++) {
    res += "<li>";
    res +=
      '<button type="button" class="autocompleteButton" data-placeid="' +
      prediction.place_id +
      '" data-name="' +
      prediction.terms[0].value +
      '" data-desc="' +
      prediction.description +
      '">';
    res += '<div class="autocompleteRow">';
    res += '<div class="autocompleteIcon">';
    res += '<img src="/Images/map-marker-outline.svg" />';
    // Insert output in results container
    res += "</div>";
    res += '<div class="pac-item" >';
    res += "<p>";
    res += prediction.structured_formatting.main_text;
    res += "</p>";
    res += '<p class="second">';
    res += prediction.structured_formatting.secondary_text;
    res += "</p>";
    res += "</div>";
    res += "</div>";
    res += "</button>";
    res += "</li>";
  }
  res += "</ul>";
  results.innerHTML += res;
  var items = document.getElementsByClassName("autocompleteButton");

  // Results items click
  for (var i = 0, item; (item = items[i]); i++) {
    item.onclick = function () {
      if (this.dataset.placeid) {
        getPlaceDetails(this.dataset.placeid, this.dataset.desc);
      }
    };
  }
}

function getPlaceDetails(placeId, desc) {
  var request = {
    placeId: placeId,
    fields: ["name", "geometry", "address_components"],
    //fields: ['ALL'],
    sessiontoken: sessionToken,
  };

  placesService.getDetails(request, function (place, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      let address1 = "";
      let postcode = "";
      for (const component of place.address_components) {
        const componentType = component.types[0];

        switch (componentType) {
          case "street_number": {
            address1 = component.long_name + address1;
            break;
          }

          case "route": {
            address1 += component.short_name;
            break;
          }

          case "postal_code": {
            postcode = component.long_name + postcode;
            break;
          }

          case "postal_code_prefix": {
            postcode = postcode + "-" + component.long_name;
            break;
          }
          case "locality":
            ////document.querySelector("#locality").value = component.long_name;
            break;

          case "administrative_area_level_1": {
            ////document.querySelector("#state").value = component.short_name;
            break;
          }
          case "country":
            ////document.querySelector("#country").value = component.long_name;
            break;
        }
      }

      $("input#Address").val(desc);
      $("input#AddressDesc").val(desc);
      $("input#DeliveryPostCode").val(postcode.trim());
      $("input#Lat").val(lat);
      $("input#Lon").val(lng);
      $("input#PlaceId").val(placeId);
      if (postcode == "") {
        validateAddress();
        return;
      }

      var locationDetail = {
        Address: desc,
        AddressDescription: desc,
        DeliveryPostCode: postcode.trim(),
        Lat: lat,
        Lon: lng,
        PlaceId: placeId,
      };
      Cookies.set("LocationDetail", JSON.stringify(locationDetail));
      ////var center = place.geometry.location;
      ////var marker = new google.maps.Marker({
      ////    position: center,
      ////    map: map
      ////});
      ////map.setCenter(center);

      // Hide autocomplete results
      results.style.display = "none";

      //   $("button#search").addClass(
      //     "MuiLoadingButton-root MuiLoadingButton-loading MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-fullWidth MuiButtonBase-root Mui-disabled css-lwey5m-MuiButtonBase-root-MuiButton-root-MuiLoadingButton-root"
      //   );

    //   $("button#search").attr(
    //     "class",
    //     "MuiLoadingButton-root MuiLoadingButton-loading MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-fullWidth MuiButtonBase-root Mui-disabled css-lwey5m-MuiButtonBase-root-MuiButton-root-MuiLoadingButton-root"
    //   );

    //   $("button#search").attr("disabled", "disabled");

      window.location =
        "/area/" +
        queryParamEncode(locationDetail.DeliveryPostCode) +
        "?sm=delivery";
      $("#searchForm").submit();
      //   btnsearchChangeStyle();
    }
  });
}

function validateAddress() {
  var validation = $("div.validation");
  validation.removeClass("is-hide");
  $("div.searchInput").addClass("validateInput");
}

$(document).on("click", "button#search", function (event) {
  var searchInput = $("#Address");
  if (searchInput.val().length == 0) {
    validateAddress();
    return;
  }
  var postcodes = $("ul#predictions li button.autocompleteButton");
  if (postcodes.length == 1) {
    // btnsearchChangeStyle();
    $.each(postcodes, function () {
      $(this).trigger("click");
    });
  } else {
    validateAddress();
    return;
  }
});
