import { useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";

//MUI
import {
  Box,
  Button,
  Divider,
  Drawer,
  Grid,
  // IconButton,
  // Link,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  Typography,
} from "@mui/material";

//MUI Icons
// import FavoriteIcon from "@mui/icons-material/Favorite";
import ListAltRoundedIcon from "@mui/icons-material/ListAltRounded";
// import AccountBalanceWalletRounded from "@mui/icons-material/AccountBalanceWalletRounded";
// import PaymentRoundedIcon from "@mui/icons-material/PaymentRounded";
import LocationOnRoundedIcon from "@mui/icons-material/LocationOnRounded";
// import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
// import AppleIcon from "@mui/icons-material/Apple";
// import AdbIcon from "@mui/icons-material/Adb";
import PrimaryButton from "../buttons/Primary.buttons";
import LoginModal from "../modals/Login.modals";
import SignupModal from "../modals/Signup.modals";
import ForgetPasswordModal from "../modals/ForgetPassword.modals";
import Link from "../links/Link";

import HandShakeIcon from "../../public/Images/handshake.svg";
import FAQIcon from "../../public/Images/faq.svg";
import MealzoLogo from "../../public/Images/mealzo.png";
import DownloadForAndroidImage from "../../public/Images/android.svg";
import DownloadForIOSImage from "../../public/Images/ios.svg";

const MealzoLogoLink = () => (
  <Link href="/" display="flex" width={180}>
    <Image src={MealzoLogo} />
  </Link>
);

const UserNotLoggedIn = () => {
  const router = useRouter();
  const { t } = useTranslation("drawer");

  const signinData = [
    {
      id: 1,
      name: t("option_become_partner"),
      icon: <Image src={HandShakeIcon} />,
      url: "/Partner",
    },
    {
      id: 2,
      name: t("option_faqs"),
      icon: <Image src={FAQIcon} />,
      url: "/faq",
    },
  ];

  const [openLogin, setOpenLogin] = useState(false);
  const [openSignup, setOpenSignup] = useState(false);
  const [openForgetPass, setOpenForgetPass] = useState(false);

  const loginModalHandle = () => {
    setOpenLogin(!openLogin);
  };
  const signUpModalHandler = () => {
    setOpenLogin(false);
    setOpenSignup(!openSignup);
  };
  const forgetPassHandler = () => {
    setOpenLogin(false);
    setOpenSignup(false);
    setOpenForgetPass(!openForgetPass);
  };

  return (
    <>
      <Grid container flexDirection="column" gap={4}>
        <Grid container>
          <MealzoLogoLink />
        </Grid>
        <Grid container flexWrap="nowrap" justifyContent="space-between" mb={3}>
          <Grid item xs={5.5}>
            <PrimaryButton
              fullWidth
              sx={{ whiteSpace: "nowrap", height: "100%" }}
              onClick={loginModalHandle}
            >
              {t("login_btn")}
            </PrimaryButton>
          </Grid>
          <Grid item xs={5.5}>
            <Button
              sx={{
                color: "text.secondary",
                borderRadius: "4px",
                padding: "10px 20px",
                fontSize: "12px",
                backgroundColor: "#CCCCCC",
                width: "100%",
                "&:hover": {
                  backgroundColor: "#CCCCCC",
                },
              }}
              onClick={signUpModalHandler}
            >
              {t("sign_up_btn")}
            </Button>
          </Grid>
        </Grid>
        <List sx={{ padding: 0 }}>
          {signinData.map((data) => (
            <ListItem key={data.id} disablePadding>
              <ListItemButton
                sx={{ paddingLeft: "0 !important" }}
                onClick={() => router.push(data.url)}
              >
                <ListItemIcon>{data.icon}</ListItemIcon>
                <Typography
                  sx={{ color: "text.primary", fontSize: 14 }}
                  variant="subtitle1"
                >
                  {data.name}
                </Typography>
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </Grid>

      {/* login and sign up modals */}
      <LoginModal
        open={openLogin}
        handleOpen={loginModalHandle}
        signUpHandler={signUpModalHandler}
        forgetPassHandler={forgetPassHandler}
      />
      <SignupModal
        signUpModalHandler={signUpModalHandler}
        openSignup={openSignup}
      />
      <ForgetPasswordModal
        open={openForgetPass}
        handleClose={forgetPassHandler}
      />
    </>
  );
};

const UserLoggedIn = () => {
  const router = useRouter();
  const { t } = useTranslation("drawer");
  const { getUser, signOut } = useAuth();
  const user = getUser();

  const loggedInUserOptions = [
    // {
    //   id: 1,
    //   name: "Local Favorites",
    //   icon: <FavoriteIcon sx={{ fontSize: 24 }} />,
    //   url: "/",
    // },
    {
      id: 2,
      name: t("option_orders_list"),
      icon: <ListAltRoundedIcon sx={{ fontSize: 24 }} />,
      url: "/orderlist",
    },
    // {
    //   id: 3,
    //   name: "E-Wallet & Credit",
    //   icon: <AccountBalanceWalletRounded sx={{ fontSize: 24 }} />,
    //   url: "/",
    // },
    // {
    //   id: 4,
    //   name: "Payment Methods",
    //   icon: <PaymentRoundedIcon sx={{ fontSize: 24 }} />,
    //   url: "/",
    // },
    {
      id: 5,
      name: t("option_saved_addresses"),
      icon: <LocationOnRoundedIcon sx={{ fontSize: 24 }} />,
      url: "/addresses",
    },
  ];

  return (
    <>
      <Grid container flexDirection="column" gap="30px">
        <Grid container>
          <MealzoLogoLink />
        </Grid>
        <Grid container flexDirection="column" gap={1}>
          <Grid item>
            <Typography variant="subtitle1">{t("my_account")}</Typography>
          </Grid>
          <Grid item>
            <Grid container flexWrap="nowrap" alignItems="center">
              <Grid item xs={10.5}>
                <Typography
                  fontSize={14}
                  color="primary.main"
                  maxWidth={218}
                  noWrap
                >
                  {user.email}
                </Typography>
              </Grid>
              <Grid item xs={1.5}>
                {/* <IconButton>
                  <EditOutlinedIcon
                    fontSize="small"
                    sx={{
                      color: "text.primary",
                    }}
                  />
                </IconButton> */}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
      </Grid>
      <List sx={{ padding: 0 }}>
        {loggedInUserOptions.map((data) => (
          <ListItem key={data.id} disablePadding>
            <ListItemButton
              sx={{ paddingLeft: "0 !important" }}
              onClick={() => router.push(data.url)}
            >
              <ListItemIcon>{data.icon}</ListItemIcon>
              <Typography
                sx={{ color: "text.primary", fontSize: 14 }}
                variant="subtitle1"
              >
                {data.name}
              </Typography>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Button
        variant="text"
        sx={{ maxWidth: "max-content", mt: 2 }}
        onClick={() => signOut()}
      >
        {t("drawer_sign_out")}
      </Button>
    </>
  );
};

const MainDrawer = ({ open, toggleDrawer }) => {
  const { isLogin } = useAuth();
  const { t } = useTranslation("drawer");

  return (
    <Drawer open={open} onClose={toggleDrawer(false)}>
      <Box
        sx={{
          width: 300,
          height: "100%",
          padding: "22px 25px",
          display: "flex",
          flexDirection: "column",
          gap: "20px",
        }}
        role="presentation"
      >
        {isLogin ? <UserLoggedIn /> : <UserNotLoggedIn />}
        <Box
          mt="auto"
          sx={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Divider />
          <Typography
            variant="subtitle1"
            sx={{
              color: "text.primary",
              fontSize: 16,
              fontWeight: 600,
              my: "23px",
            }}
            textAlign="center"
          >
            {t("drawer_download_app")}
          </Typography>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              gap: "17px",
              mb: "23px",
            }}
          >
            <Link
              href="https://apps.apple.com/us/app/mealzo/id1558698286"
              target="_blank"
              sx={{ textDecoration: "none" }}
            >
              {/* <Button
                sx={{
                  color: "text.secondary",
                  border: "1px solid #707070",
                  borderRadius: "50px",
                  padding: "5px 20px",
                }}
              >
                <AppleIcon fontSize="small" />
                iphone
              </Button> */}
              <Image
                src={DownloadForIOSImage}
                alt="mealzo download for ios"
                loading="lazy"
              />
            </Link>
            <Link
              href="https://play.google.com/store/apps/details?id=com.mealzowee"
              target="_blank"
              sx={{ textDecoration: "none" }}
            >
              {/* <Button
                sx={{
                  color: "text.secondary",
                  border: "1px solid #707070",
                  borderRadius: "50px",
                  padding: "5px 20px",
                }}
              >
                <AdbIcon fontSize="small" />
                android
              </Button> */}
              <Image
                src={DownloadForAndroidImage}
                alt="mealzo download for android"
                loading="lazy"
              />
            </Link>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default MainDrawer;
