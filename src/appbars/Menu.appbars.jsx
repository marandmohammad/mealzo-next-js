import { useState, useMemo } from "react";
import Image from "next/image";
import { useRouter } from "next/router";

import useOrder from "../../hooks/orders";

import {
  AppBar,
  Box,
  // CssBaseline,
  Toolbar,
  Grid,
  IconButton,
  useScrollTrigger,
  Slide,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import SearchIcon from "@mui/icons-material/Search";

import Link from "../links/Link";
import MenuDeliveryInput from "../inputs/MenuDelivery.inputs";
import MenuHeaderSearchBarInput from "../inputs/MenuHeaderSearchBar.inputs";
// import { HideOnScroll } from "./Area.appbars";
import MainDrawer from "../drawers/Main.drawers";

import MealzoLogo from "../../public/Images/mealzo.png";

export function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide
      appear={false}
      direction="down"
      in={!trigger}
      style={{
        minHeight: 97,
        ".MuiPaper-root .MuiPaper-elevation .MuiPaper-elevation4 .MuiAppBar-root .MuiAppBar-colorPrimary .MuiAppBar-positionFixed .mui-fixed":
          {
            top: !trigger ? 57 : 0,
          },
      }}
    >
      {children}
    </Slide>
  );
}

function MenuAppBar(props) {
  const {
    imageUrl = MealzoLogo,
    sx,
    shopDetail,
    handleBasketModal,
    ...restOfProps
  } = props;
  const router = useRouter();
  const { getOrderId } = useOrder();
  const orderId = getOrderId();
  const { shopId } = shopDetail.address;

  const basketClickHandler = useMemo(() => {
    if (!orderId) return handleBasketModal;
    if (orderId.shopId === shopId) return handleBasketModal;
    if (orderId.shopId !== shopId)
      return () => router.push(`/${orderId.shopIdentity}/Menu`);
  }, [orderId]);

  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  return (
    <>
      {/*<HideOnScroll {...props}>*/}
      {/* <HideOnScroll {...props}> */}
      <Box flexGrow={1}>
        <AppBar
          sx={{
            backgroundColor: "white",
            position: { xs: "static", md: "static" },
            // top: { xs: 57, md: "unset" },
            py: 2,
            px: { sm: 4, xs: 2 },
            boxShadow: "none",
            borderBottom: {
              xs: "none",
              md: "1px solid #eaeaea",
            },
            ...sx,
          }}
          {...restOfProps}
        >
          <Toolbar disableGutters>
            {/* <Grid
              container
              display={{ xs: "none", md: "none" }}
              justifyContent="space-between"
            >
              <IconButton>
                <ArrowBackIosNewIcon />
              </IconButton>
              <IconButton>
                <SearchIcon />
              </IconButton>
            </Grid> */}
            <Grid container gap={{ xs: 1, md: 0 }}>
              <Grid
                item
                display="flex"
                flexShrink={{ xs: "unset", md: 1 }}
                flexGrow={{ xs: 1, md: "unset" }}
                justifyContent="space-between"
                // mb={{ xs: 1, md: 0 }}
              >
                <IconButton
                  aria-label="menu"
                  sx={{ mr: { xs: 0, md: 2 }, px: { xs: 0, md: 1 } }}
                  onClick={toggleDrawer(true)}
                >
                  <MenuIcon />
                </IconButton>

                <IconButton
                  aria-label="basket"
                  sx={{
                    display: { xs: "inline-flex", md: "none" },
                    px: { xs: 0, md: 1 },
                  }}
                  onClick={basketClickHandler}
                >
                  <ShoppingCartOutlinedIcon />
                </IconButton>
              </Grid>

              {/* Logo */}
              <Grid
                item
                lg={2}
                md={2}
                xs={0}
                display={{ md: "flex", xs: "none" }}
                alignItems="center"
              >
                <Link href="/" display="flex" width={180} height="80%">
                  <Image src={imageUrl} alt="Mealzo logo" layout="fill" objectFit="contain" />
                </Link>
              </Grid>

              <Grid
                container
                item
                gap={{ xs: 1, md: 3 }}
                flexDirection={{ xs: "column-reverse", md: "row" }}
                lg={8}
                md={9}
                xs={12}
              >
                <Grid item flexGrow={1}>
                  <MenuHeaderSearchBarInput />
                </Grid>

                <Grid item>
                  <MenuDeliveryInput />
                </Grid>
              </Grid>
            </Grid>
            <IconButton
              aria-label="basket-md"
              edge="end"
              sx={{
                display: { xs: "none", md: "inline-flex" },
              }}
              onClick={basketClickHandler}
            >
              <ShoppingCartOutlinedIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </Box>
      {/* </HideOnScroll> */}
      {/*</HideOnScroll>*/}
      <MainDrawer open={drawerOpen} toggleDrawer={toggleDrawer} />
    </>
  );
}

export default MenuAppBar;
