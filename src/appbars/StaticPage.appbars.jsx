import { useState, Fragment } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { useAuth } from "../../hooks/authentication";

import {
  AppBar,
  Box,
  // CssBaseline,
  IconButton,
  Toolbar,
  Grid,
  NoSsr,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useTheme } from "@mui/material";

import Link from "../links/Link";
import MainDrawer from "../drawers/Main.drawers";
import LandingAppBarButton from "../buttons/LandingAppBar.buttons";
import LoginModal from "../modals/Login.modals";
import SignupModal from "../modals/Signup.modals";
import ForgetPasswordModal from "../modals/ForgetPassword.modals";
// import LanguageButton from "../buttons/Language.buttons";

import MealzoLogo from "../../public/Images/mealzo.png";

function StaticPageAppBar({
  imageUrl = MealzoLogo,
  sx,
  iconColor = "text.secondary",
  showButtons = false,
  ...restOfProps
}) {
  const { t } = useTranslation("common");
  const theme = useTheme();
  const router = useRouter();
  const { isLogin } = useAuth();

  const [drawerOpen, setDrawerOpen] = useState(false);
  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  const [openLogin, setOpenLogin] = useState(false);
  const [openSignup, setOpenSignup] = useState(false);
  const [openForgetPass, setOpenForgetPass] = useState(false);

  const loginModalHandle = () => {
    setOpenLogin(!openLogin);
  };
  const signUpModalHandler = () => {
    setOpenLogin(false);
    setOpenSignup(!openSignup);
  };
  const forgetPassHandler = () => {
    setOpenLogin(false);
    setOpenSignup(false);
    setOpenForgetPass(!openForgetPass);
  };

  return (
    <>
      {/* <CssBaseline /> */}
      <Box flexGrow={1}>
        <AppBar
          position="static"
          sx={{
            backgroundColor: theme.palette.common.white,
            py: 2,
            px: { lg: 20, md: 10, sm: 5, xs: 0.63 },
            ...sx,
          }}
          {...restOfProps}
        >
          <Toolbar>
            <Grid container flexWrap="nowrap">
              <Grid item flexShrink={1} display="flex" alignItems="center">
                <IconButton
                  edge="start"
                  // color={iconColor}
                  //   color="common.black"
                  aria-label="menu"
                  sx={{ mr: 2, color: iconColor }}
                  onClick={toggleDrawer(true)}
                >
                  <MenuIcon sx={{ fontSize: 45 }} />
                </IconButton>
              </Grid>
              <Grid item display="flex" alignItems="center">
                <Link href="/" display="flex" width={180} height="80%">
                  <Image src={imageUrl} alt="Mealzo logo" layout="fill" objectFit="contain" />
                </Link>
              </Grid>
              {/* <LanguageButton
                color="primary"
                listLeftPos={-70}
                containerStyle={{
                  ml: "auto",
                  alignSelf: "center",
                  display: { xs: "block", md: "none" },
                }}
              /> */}
              <Grid
                item
                ml="auto"
                display={{ xs: "none", md: "flex" }}
                gap={1}
                alignItems="center"
              >
                {/* <LanguageButton color="primary" /> */}
                {showButtons && (
                  <Fragment>
                    <LandingAppBarButton
                      variant="outlined"
                      onClick={() => router.push("/Partner")}
                    >
                      {t("appbar_become_partner_btn")}
                    </LandingAppBarButton>
                    <NoSsr>
                      {!isLogin && (
                        <LandingAppBarButton
                          variant="contained"
                          onClick={loginModalHandle}
                        >
                          {t("appbar_login_btn")}
                        </LandingAppBarButton>
                      )}
                    </NoSsr>
                  </Fragment>
                )}
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </Box>

      {/* Main Drawer */}
      <MainDrawer open={drawerOpen} toggleDrawer={toggleDrawer} />

      {/*login and sign up modals*/}
      <LoginModal
        open={openLogin}
        handleOpen={loginModalHandle}
        signUpHandler={signUpModalHandler}
        forgetPassHandler={forgetPassHandler}
      />
      <SignupModal
        signUpModalHandler={signUpModalHandler}
        openSignup={openSignup}
      />
      <ForgetPasswordModal
        open={openForgetPass}
        handleClose={forgetPassHandler}
      />
    </>
  );
}

export default StaticPageAppBar;
