import { useState } from "react";
import Image from "next/image";

import {
  AppBar,
  Box,
  CssBaseline,
  Toolbar,
  Grid,
  IconButton,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

import MainDrawer from "../drawers/Main.drawers";
import Link from "../links/Link";

import MealzoLogo from "../../public/Images/mealzo.png";

function RegularAppBar(props) {
  const { imageUrl = MealzoLogo, sx, ...restOfProps } = props;

  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  return (
    <>
      <CssBaseline />

      {/*<HideOnScroll {...props}>*/}
      <Box flexGrow={1}>
        <AppBar
          sx={{
            backgroundColor: { xs: "transparent", md: "white" },
            position: { xs: "static", md: "static" },
            py: 2,
            px: { sm: 4, xs: 2 },
            boxShadow: "none",
            borderBottom: { xs: "none", md: "1px solid #eaeaea" },
            ...sx,
          }}
          {...restOfProps}
        >
          <Toolbar disableGutters>
            <Grid container display={{ xs: "flex", md: "flex" }}>
              <Grid item flexShrink={1}>
                <IconButton
                  edge="start"
                  //   color="common.black"
                  aria-label="menu"
                  onClick={toggleDrawer(true)}
                  sx={{ mr: 2 }}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
              <Grid
                item
                lg={2.55}
                md={3}
                // display={{ md: "flex", xs: "none" }}
                display="flex"
                alignItems="center"
                xs={5}
              >
                <Link href="/" display="flex" width={180} height="80%" mb={1}>
                  <Image src={imageUrl} alt="Mealzo logo" layout="fill" objectFit="contain" />
                </Link>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </Box>
      {/*</HideOnScroll>*/}
      <MainDrawer open={drawerOpen} toggleDrawer={toggleDrawer} />
    </>
  );
}

export default RegularAppBar;
