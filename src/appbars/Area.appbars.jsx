import { useState, useContext, useMemo } from "react";
import Image from "next/image";
import { useRouter } from "next/router";

import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import {
  AppBar,
  Box,
  // CssBaseline,
  Toolbar,
  Slide,
  useScrollTrigger,
  Grid,
  IconButton,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";

import Link from "../links/Link";
import AreaDeliveryInput from "../inputs/AreaDelivery.inputs";
import AreaHeaderSearchBarInput from "../inputs/AreaHeaderSearchBar.inputs";
import MainDrawer from "../drawers/Main.drawers";

import MealzoLogo from "../../public/Images/mealzo.png";

import { AreaPageContext } from "../../context/AreaPageContext";

export function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide
      appear={false}
      direction="down"
      in={!trigger}
      style={{ minHeight: 97 }}
    >
      {children}
    </Slide>
  );
}

function AreaAppBar(props) {
  const { imageUrl = MealzoLogo, sx, ...restOfProps } = props;
  const { setDeliveryModal } = useContext(AreaPageContext);
  const router = useRouter();
  const { showNotification } = useNotification();
  const { getOrderId } = useOrder();
  const orderId = getOrderId();

  //* Basket button handler
  const basketClickHandler = useMemo(() => {
    if (!orderId)
      return () => showNotification("You have no basket!", "warning");
    return () => router.replace(`/${orderId.shopIdentity}/Menu`);
  }, [orderId]);

  const [drawerOpen, setDrawerOpen] = useState(false);

  //* Drawer toggle handler
  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setDrawerOpen(open);
  };
  return (
    <>
      <HideOnScroll {...props}>
        <Box flexGrow={1}>
          <AppBar
            // position="sticky"
            sx={{
              backgroundColor: "white",
              position: { md: "static" },
              py: 2,
              px: { sm: 4, xs: 2 },
              boxShadow: "none",
              borderBottom: "1px solid #eaeaea",
              ...sx,
            }}
            {...restOfProps}
          >
            <Toolbar disableGutters>
              <Grid container gap={{ xs: 1, md: 0 }}>
                <Grid
                  item
                  display="flex"
                  flexShrink={{ xs: "unset", md: 1 }}
                  flexGrow={{ xs: 1, md: "unset" }}
                  justifyContent="space-between"
                  // mb={{ xs: 1, md: 0 }}
                >
                  <IconButton
                    aria-label="menu"
                    sx={{ mr: { xs: 0, md: 2 }, px: { xs: 0, md: 1 } }}
                    onClick={toggleDrawer(true)}
                  >
                    <MenuIcon />
                  </IconButton>

                  <IconButton
                    aria-label="basket"
                    sx={{
                      display: { xs: "inline-flex", md: "none" },
                      px: { xs: 0, md: 1 },
                    }}
                    onClick={basketClickHandler}
                  >
                    <ShoppingCartOutlinedIcon />
                  </IconButton>
                </Grid>

                {/* Logo */}
                <Grid
                  item
                  lg={2}
                  md={2}
                  xs={0}
                  display={{ md: "flex", xs: "none" }}
                  alignItems="center"
                >
                  <Link href="/" display="flex" width={180} height="80%">
                    <Image
                      src={imageUrl}
                      alt="Mealzo logo"
                      layout="fill"
                      objectFit="contain"
                    />
                  </Link>
                </Grid>

                <Grid
                  container
                  item
                  gap={{ xs: 1, md: 3 }}
                  flexDirection={{ xs: "column-reverse", md: "row" }}
                  lg={8}
                  md={9}
                  xs={12}
                >
                  <Grid item flexGrow={1}>
                    <AreaHeaderSearchBarInput />
                  </Grid>
                  <Grid item>
                    <AreaDeliveryInput setDeliveryModal={setDeliveryModal} />
                  </Grid>
                </Grid>
              </Grid>

              <IconButton
                aria-label="basket-md"
                edge="end"
                sx={{
                  display: { xs: "none", md: "inline-flex" },
                }}
                onClick={basketClickHandler}
              >
                <ShoppingCartOutlinedIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
        </Box>
      </HideOnScroll>
      <Toolbar sx={{ display: { md: "none" } }} />

      {/*</HideOnScroll>*/}
      <MainDrawer open={drawerOpen} toggleDrawer={toggleDrawer} />
    </>
  );
  // }
}

export default AreaAppBar;
