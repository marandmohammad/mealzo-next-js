//Mui Components
import { Backdrop, Box, Grid, Modal, Toolbar, Fade } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

import RegularIconsButton from "../buttons/RegularIcon.buttons";

const CustomCloseIcon = () => (
  <CloseIcon sx={{ fontSize: 15, color: "primary.main" }} />
);

//Modal Style
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: { xs: "100vw", sm: 400 },
  bgcolor: "background.paper",
  outline: "none",
  overflow: "hidden",
};

const BaseModal = ({
  children,
  sx,
  handleClose,
  open,
  title,
  titleStyle,
  btnStyle,
  ModalTitle,
  id,
  absoluteTitle = false,
  innerContainerStyle = {},
  showCloseButton = true,
  ...restOfProps
}) => {
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
      disableEnforceFocus
    >
      <Fade in={open}>
        <Box sx={{ ...style, ...sx }} {...restOfProps}>
          {absoluteTitle && (
            <Toolbar
              sx={{
                position: "sticky",
                top: -1,
                bgcolor: "background.default",
              }}
            >
              <Grid
                container
                sx={{
                  alignItems: "center",
                }}
              >
                {ModalTitle}
              </Grid>
              {showCloseButton && (
                <RegularIconsButton
                  sx={{ ...btnStyle }}
                  onClick={handleClose}
                  edge="end"
                >
                  <CustomCloseIcon />
                </RegularIconsButton>
              )}
            </Toolbar>
          )}

          {/* Close Button */}
          {showCloseButton && !absoluteTitle && (
            <RegularIconsButton
              sx={{ position: "absolute", right: "10%", ...btnStyle }}
              onClick={handleClose}
            >
              <CustomCloseIcon />
            </RegularIconsButton>
          )}

          <Grid
            container
            maxHeight="85vh"
            id={id}
            sx={{
              overflowY: "auto",
              "&::-webkit-scrollbar": {
                width: 4,
                bgcolor: "transparent",
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: "5px",
                bgcolor: "#e9540dad",
              },
              ...innerContainerStyle,
            }}
          >
            {!absoluteTitle && ModalTitle && (
              <Grid
                container
                sx={{
                  alignItems: "center",
                  p: { xs: "15px 15px 8px 15px", md: "20px 30px" },
                  ...titleStyle,
                }}
              >
                {ModalTitle}
              </Grid>
            )}
            {children}
          </Grid>
        </Box>
      </Fade>
    </Modal>
  );
};

export default BaseModal;
