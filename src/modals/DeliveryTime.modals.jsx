import { useEffect, useState } from "react";
import { useTranslation } from "next-i18next";
import { useFormik } from "formik";
import { setCookies } from "cookies-next";
import { Box, Divider, Skeleton, Typography, Grow } from "@mui/material";

import BaseModal from "./Base.modals";
import CheckOutInput from "../inputs/CheckOut.inputs";
import PrimaryButton from "../buttons/Primary.buttons";

import axiosConfig from "../../config/axios";
import { cookiesNameSpaces } from "../../utilities/appWideHelperFunctions";
import { saleMethods } from "../../utilities/areaPathHandlers";

const initialValues = {
  time: "",
};

const validate = (values) => {
  let errors = {};
  if (!values.time) {
    errors.time = "required";
  }
  return errors;
};

const DeliveryTimeForm = ({ selectOptions, onSuccessfulSubmit }) => {
  const { t } = useTranslation("common");

  const formik = useFormik({
    initialValues,
    validate,
  });

  useEffect(() => {
    setTimeout(() => {
      formik.validateForm();
    }, 50);
  }, []);

  const handleSubmit = () => {
    const { DELIVERY_TIME } = cookiesNameSpaces;

    if (formik.isValid) {
      const selectedDeliveryTime = selectOptions.find(
        ({ id }) => id === parseInt(formik.values.time)
      );
      setCookies(DELIVERY_TIME, JSON.stringify(selectedDeliveryTime));
      onSuccessfulSubmit(selectedDeliveryTime);
    }
  };

  return (
    <Grow in={true}>
      <div style={{ width: "100%" }}>
        <Box sx={{ px: 3, width: "100%", mb: 8 }}>
          <CheckOutInput
            options={selectOptions}
            value={formik.values.time}
            handleChange={formik.handleChange}
            sx={{ m: 0 }}
            name="time"
          />
        </Box>
        <Box
          sx={{
            boxShadow: "0px 0px 6px #00000029",
            px: 3,
            py: 1,
            width: "100%",
            cursor: formik.isValid ? "not-allowed" : "unset",
          }}
        >
          <PrimaryButton
            onClick={handleSubmit}
            disabled={!formik.isValid}
            sx={{ textTransform: "capitalize" }}
            fullWidth
          >
            {t("schedule")}
          </PrimaryButton>
        </Box>
      </div>
    </Grow>
  );
};

const DeliveryTimeModal = ({
  deliveryTimeModalHandler,
  openDeliveryTime,
  onSuccessfulSubmit = deliveryTimeModalHandler,
  shopId,
  saleMethod,
}) => {
  const { t } = useTranslation("common");

  const [loading, setLoading] = useState(true);
  const [selectOptions, setSelectOptions] = useState([]);

  const saleMethodCode = saleMethods[saleMethod].code;

  useEffect(() => {
    setLoading(true);
    axiosConfig
      .get(`/WantedTimes/${shopId}/${saleMethodCode}`)
      .then(({ data }) => {
        setSelectOptions(data);
        setLoading(false);
      })
      .catch(({ message }) => console.error(message));
  }, [openDeliveryTime, saleMethodCode]);

  return (
    <BaseModal
      open={openDeliveryTime}
      handleClose={deliveryTimeModalHandler}
      btnStyle={{ top: "5%" }}
      absoluteTitle
      ModalTitle={
        <Typography variant="subtitle1" sx={{ fontSize: 16, fontWeight: 600 }}>
          {t(saleMethods[saleMethod].wantedTimeLabel)}
        </Typography>
      }
    >
      {loading ? (
        <>
          <Skeleton width="100%" height={80} />
          <Divider />
          <Skeleton width="100%" height={80} />
        </>
      ) : (
        <DeliveryTimeForm
          selectOptions={selectOptions}
          onSuccessfulSubmit={onSuccessfulSubmit}
        />
      )}
    </BaseModal>
  );
};

export default DeliveryTimeModal;
