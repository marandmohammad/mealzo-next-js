import { useState } from "react";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";

import { Box, Typography } from "@mui/material";

import SignUpInput from "../inputs/SignUp.inputs";
import BaseModal from "./Base.modals";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";

import axiosConfig from "../../config/axios";

const CouponModal = ({
  open,
  handleClose,
  onSuccessfulSubmit,
  shopId,
  orderId,
}) => {
  const GENERAL_TIMEOUT = 1500;
  const { t } = useTranslation("checkout_page");
  const { showNotification } = useNotification();

  //! State Definition
  const [code, setCode] = useState("");
  const [error, setError] = useState(t('voucher_modal_input_required'));
  const [loading, setLoading] = useState(false);

  //! Handle Change
  const handleChange = ({ target: { value } }) => {
    setCode(value);

    if (!value || value.trim() === "") setError(t('voucher_modal_input_required'));
    else setError(false);
  };

  //! Handle Submit
  const handleSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (error) return;

    setLoading(true);

    const reqData = {
      shopId: shopId,
      orderId: orderId,
      voucherCode: code,
    };

    try {
      const result = await axiosConfig.patch("/Basket/AddVoucherCode", reqData);
      setLoading(false);
      onSuccessfulSubmit(result.data);
      showNotification(t("voucher_successful_submit_message"), "success");
      setTimeout(() => {
        handleClose();
      }, GENERAL_TIMEOUT);
    } catch ({ response: { data } }) {
      setLoading(false);
      showNotification(data.message[0], "error");
    }
  };

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      btnStyle={{ top: "6%", right: "4%" }}
      ModalTitle={
        <Typography
          component="div"
          sx={{
            fontSize: 24,
            color: "text.primary",
            fontWeight: "bold",
            flexGrow: 1,
          }}
        >
          {t("voucher_modal_title")}
        </Typography>
      }
      titleStyle={{
        textAlign: "center",
      }}
      closeItemPos="20px"
      sx={{ boxShadow: "0px 3px 20px #0000000D", borderRadius: "8px" }}
    >
      <Box
        component="form"
        onSubmit={handleSubmit}
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 10,
          p: 6,
          flexGrow: 1,
        }}
      >
        {/* <MainSnackBar
          open={success}
          autoHideDuration={GENERAL_TIMEOUT}
          handleClose={() => setSuccess(false)}
          severity="success"
        >
          {success}
        </MainSnackBar> */}
        <SignUpInput
          type="text"
          name="offCode"
          value={code}
          handleChange={handleChange}
          error={error}
        >
          {t("voucher_modal_input_label")}
        </SignUpInput>
        <LoadingPrimaryButton
          variant="contained"
          type="submit"
          fullWidth
          // onClick={handleSubmit}
          disabled={error || loading}
          loading={loading}
        >
          {t("voucher_modal_submit_btn")}
        </LoadingPrimaryButton>
      </Box>
    </BaseModal>
  );
};

export default CouponModal;
