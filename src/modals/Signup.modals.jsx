// import SignUpPage from "../../pages/SignUp.pages";
import SignUpForm from "../forms/SignUp.forms";
import BaseModal from "./Base.modals";

const SignupModal = ({ openSignup, signUpModalHandler }) => {
  return (
    <BaseModal
      open={openSignup}
      handleClose={signUpModalHandler}
      sx={{
        // px: 5,
        pb: { xs: 1, sm: 5 },
        pt: 1,
        borderRadius: "4px",
        height: { xs: "100%", sm: "unset" },
      }}
      btnStyle={{ top: "4%", right: "4%" }}
    >
      {/*<SignUpPage sx={{ boxShadow: 0 }} containerStyle={{ padding: 0 }} />*/}
      <SignUpForm onRegistrationSuccess={() => signUpModalHandler()} />
    </BaseModal>
  );
};

export default SignupModal;
