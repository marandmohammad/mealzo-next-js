import { useTranslation } from "next-i18next";

import { Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import AddNewAddressForm from "../forms/AddNewAddress.forms";

const AddNewAddressModal = ({ open, handleClose, handleSuccessfulClose, initialValues = null }) => {
  const { t } = useTranslation('addresses')

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      absoluteTitle
      ModalTitle={
        <Typography variant="h6" component='h6' fontSize={18}>
          {t('add_new_address_label')}
        </Typography>
      }
    >
      <AddNewAddressForm onSuccessfulSubmit={handleSuccessfulClose} initialValues={initialValues} />
    </BaseModal>
  );
};

export default AddNewAddressModal;