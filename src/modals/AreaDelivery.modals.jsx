import { useState, useContext, useMemo } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";

//MUI
import { Modal, Box, Button, Fade } from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";

//custom MUI components
import {
  DeliveryModalContainer,
  DeliveryModalAccordion,
  DeliveryModalChange,
  DeliveryModalLabel,
} from "./AreaDelivery.modals.styles";

import AddressChangeModal from "./AddressChange.modals";

import {
  saleMethods,
  addressChangeHandler,
  queryKeyWords,
} from "../../utilities/areaPathHandlers";
import { pageNames } from "../../utilities/appWideHelperFunctions";

import { AreaPageContext } from "../../context/AreaPageContext";

const AreaDeliveryModal = ({
  deliveryModal,
  setDeliveryModal,
  hasInitialPostCode,
  saleMethod,
  deliveryHandler,
  postCode,
}) => {
  const { t } = useTranslation("common");
  const router = useRouter();
  const { showNotification } = useNotification();
  const saleMethodsKeys = Object.keys(saleMethods);
  const { setShops, setShopLoading } = useContext(AreaPageContext);

  const [openAddressChange, setOpenAddressChange] = useState(
    !hasInitialPostCode
  );

  const addressChangeModalHandler = () =>
    setOpenAddressChange(!openAddressChange);

  const onSuccessfulSubmit = (newAddress) => {
    addressChangeModalHandler();
    if (!hasInitialPostCode) setDeliveryModal(false);
    addressChangeHandler(
      router,
      queryKeyWords.postCodeQueryKeyWord,
      newAddress,
      setShops,
      setShopLoading
    );
  };

  const showNotClosingModalCause = () =>
    showNotification(
      "Please enter a postcode or choose a address!",
      "warning",
      3000
    );

  return (
    <Modal open={deliveryModal} onClose={() => setDeliveryModal(false)}>
      <Fade in={deliveryModal} direction="up">
        <DeliveryModalContainer>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "18px",
            }}
          >
            {/* <DeliveryModalHeader>
            <span>Delivery Details</span>
            <DeliveryModalCloseBtnContainer>
              <DeliveryModalCloseBtn onClick={() => setDeliveryModal(false)} />
            </DeliveryModalCloseBtnContainer>
          </DeliveryModalHeader> */}

            <DeliveryModalAccordion>
              {saleMethodsKeys.map((key) => {
                const { title, code, iconAddress } = saleMethods[key];
                return (
                  <DeliveryModalLabel
                    key={`${key}-2000`}
                    onClick={() => deliveryHandler(key)}
                    selected={saleMethod === key}
                    startIcon={
                      <Image
                        src={iconAddress}
                        width={48}
                        height={48}
                        alt={t(title)}
                      />
                    }
                  >
                    {t(title)}
                  </DeliveryModalLabel>
                );
              })}
            </DeliveryModalAccordion>

            <DeliveryModalChange>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: "17px",
                }}
              >
                <LocationOnIcon
                  sx={{ color: "text.secondary", fontSize: 21 }}
                />
                <span>{postCode}</span>
              </Box>
              <Button
                variant="text"
                sx={{ textTransform: "capitalize" }}
                onClick={addressChangeModalHandler}
              >
                {t("delivery_change")}
              </Button>
            </DeliveryModalChange>

            <Button
              variant="contained"
              onClick={() => setDeliveryModal(false)}
              sx={{
                textTransform: "capitalize",
                fontSize: 18,
                borderRadius: "4px",
              }}
            >
              {t("done_btn")}
            </Button>
          </Box>

          {/* Address Change Modals */}
          <AddressChangeModal
            openAddressChange={openAddressChange}
            addressChangeModalHandler={
              !hasInitialPostCode
                ? showNotClosingModalCause
                : addressChangeModalHandler
            }
            hasInitialPostCode={hasInitialPostCode}
            saleMethod={saleMethod}
            onSuccessfulSubmit={onSuccessfulSubmit}
            page={pageNames.AREA_PAGE}
            // setBasket={setBasket}
          />
        </DeliveryModalContainer>
      </Fade>
    </Modal>
  );
};

export default AreaDeliveryModal;
