import { useState } from "react";
import { Box, Grid, Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import PaymentCard from "../cards/Payment.cards";
import PrimaryButton from "../buttons/Primary.buttons";

const dummyCards = [
  {
    id: 1,
    number: "**** 6424",
    type: "visa",
  },
  {
    id: 2,
    number: "**** 4324",
    type: "master",
  },
  {
    id: 3,
    number: "**** 6546",
    type: "visa",
  },
];

const PaymentModal = ({ openCards, cardsModalHandler, setSelectedCard }) => {
  const [active, setActive] = useState("");

  const selectedCard = dummyCards.find((item) => item.id === active);

  const selectedCardHandler = () => {
    setSelectedCard(selectedCard);
  };

  const activeHandler = (id) => {
    setActive(id);
  };

  return (
    <BaseModal
      open={openCards}
      handleClose={cardsModalHandler}
      btnStyle={{ top: "6%", right: "4%" }}
      sx={{ borderRadius: "8px" }}
      ModalTitle={
        <Typography
            component='div'
          sx={{
            fontSize: { xs: 18, md: 24 },
            color: "text.primary",
            fontWeight: 600,
            p: 3,
          }}
        >
          Saved Cards
        </Typography>
      }
    >
      <Grid container spacing={2} p={4}>
        {dummyCards.map((item) => (
          <Grid key={item.id} item xs={12} md={6}>
            <PaymentCard
              item={item}
              onClick={() => activeHandler(item.id)}
              active={active}
            />
          </Grid>
        ))}
      </Grid>
      <Box p={4} sx={{ width: "100%" }}>
        <PrimaryButton sx={{ width: "100%" }} onClick={selectedCardHandler}>
          Choose
        </PrimaryButton>
      </Box>
    </BaseModal>
  );
};

export default PaymentModal;
