import { Fragment } from "react";
import Image from "next/image";
import { useTranslation } from "next-i18next";

// Mui Components
import { Box, Grid, Link, Typography } from "@mui/material";

// Mui Icons
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import AccessTimeOutlinedIcon from "@mui/icons-material/AccessTimeOutlined";
import SmartphoneIcon from "@mui/icons-material/Smartphone";

// Modal
import BaseModal from "./Base.modals";

const ShopInfoModal = ({
  shopInformation: { address, workDays },
  open,
  handleClose,
}) => {
  const { t } = useTranslation('shop_info_modal');
  const { shopDescription, city, fullAddress, shopTitle, telePhone, postCode } =
    address;

  const phonesList = telePhone
    ? telePhone.split(",").map((el) => el.trim())
    : null;

  return (
    <BaseModal
      ModalTitle={<Typography component="div">{t("modal_title")}</Typography>}
      open={open}
      absoluteTitle
      handleClose={handleClose}
      btnStyle={{ right: 20 }}
      sx={{ width: { xs: "90vw", sm: 410, md: 510 }, borderRadius: "4px" }}
    >
      {/* Description data */}
      {shopDescription && shopDescription.trim() !== "" && (
        <Box padding={{ xs: "8px 15px", md: "16px 40px" }} bgcolor="#EAEAEA">
          <Typography
            variant="subtitle1"
            color="text.newMedium"
            component="p"
            fontSize={{ xs: 12, md: 14 }}
          >
            {shopDescription}
          </Typography>
        </Box>
      )}

      {/* Address data */}
      <Grid
        container
        p={{ xs: "8px 15px 10px 15px", md: "16px 30px 8px 30px" }}
      >
        <Grid item xs={0.7} alignItems={"center"} fontWeight="semi-bold">
          <LocationOnOutlinedIcon sx={{ color: "text.newMedium", width: 20 }} />
        </Grid>
        <Grid item xs={11.3} p="2px 5px" display="flex" flexDirection="column">
          <Typography variant="h6" fontSize={14} color="text.newMedium">
            {t("address")}
          </Typography>
          <Typography
            variant="subtitle1"
            component="span"
            fontSize={12}
            my={1}
            color="text.secondary"
          >
            {fullAddress}, {city}, {postCode}
          </Typography>
          {/*<Link fontSize={12} underline="none" href="/">*/}
          {/*    view on map*/}
          {/*</Link>*/}
        </Grid>
      </Grid>

      {/* Shop phone number */}
      {phonesList && (
        <Grid
          container
          p={{ xs: "8px 15px 10px 15px", md: "16px 30px 8px 30px" }}
        >
          <Grid item xs={0.7} alignItems={"center"} fontWeight="semi-bold">
            <SmartphoneIcon sx={{ color: "text.newMedium", width: 20 }} />
          </Grid>
          <Grid
            item
            xs={11.3}
            p="2px 5px"
            display="flex"
            flexDirection="column"
          >
            <Typography variant="h6" fontSize={14} color="text.newMedium">
              {t("phone_number")}
            </Typography>
            {/* <Typography
            variant="subtitle1"
            component="span"
            fontSize={12}
            my={1}
            color="text.secondary"
          >
            {phonesList.map((el) => el)}
          </Typography> */}

            <Link fontSize={12} underline="none" href={`tel:${phonesList[0]}`}>
              {t("call")} {shopTitle}
            </Link>
          </Grid>
        </Grid>
      )}

      {/* Allergens */}
      <Grid
        container
        p={{ xs: "8px 15px 10px 15px", md: "16px 30px 8px 30px" }}
      >
        <Grid
          item
          xs={0.7}
          alignItems={"center"}
          fontWeight="semi-bold"
          pt={0.3}
        >
          {/* <LocationOnOutlinedIcon sx={{ color: "text.newMedium", width: 20 }} /> */}
          <Image src="/Images/allergy.png" width={20} height={20} />
        </Grid>
        <Grid item xs={11.3} p="2px 5px" display="flex" flexDirection="column">
          <Typography variant="h6" fontSize={14} color="text.newMedium">
            {t("allergens")}
          </Typography>
          <Typography
            variant="subtitle1"
            component="span"
            fontSize={12}
            my={1}
            color="text.secondary"
          >
            {t("allergens_description")}
          </Typography>
          {/* {phonesList.map((el) => (
            <Link fontSize={12} underline="none" href={`tel:${el}`}>
              Call {shopTitle}
            </Link>
          ))} */}
        </Grid>
      </Grid>
      {/*<Grid container p={{xs: "10px 15px", md: "16px 30px 8px 30px"}}>*/}
      {/*    <Grid item xs={0.7} alignItems={"center"} fontWeight="semi-bold" pt={0.3}>*/}
      {/*        <Image src='/Images/Group-icon.svg' width={20} height={20}/>*/}
      {/*    </Grid>*/}
      {/*    <Grid item xs={11.3} p="2px 5px" display="flex" flexDirection="column">*/}
      {/*        <Typography variant="h6" fontSize={14} color="text.newMedium">*/}
      {/*            Hygiene Rating*/}
      {/*        </Typography>*/}
      {/*        <Typography*/}
      {/*            variant="subtitle1"*/}
      {/*            component="span"*/}
      {/*            fontSize={12}*/}
      {/*            my={1}*/}
      {/*            color="text.secondary"*/}
      {/*        >*/}
      {/*            {data.rating}*/}
      {/*        </Typography>*/}
      {/*        <Link fontSize={12} underline="none" href="/">*/}
      {/*            view hygiene rating*/}
      {/*        </Link>*/}
      {/*    </Grid>*/}
      {/*</Grid>*/}
      {/*<Grid container p={{xs: "10px 15px", md: "16px 30px 8px 30px"}}>*/}
      {/*    <Grid item xs={0.7} alignItems={"center"} fontWeight="semi-bold" pt={0.3}>*/}
      {/*        <Image src='/Images/gluten-icon.svg' width={20} height={20}/>*/}
      {/*    </Grid>*/}
      {/*    <Grid item xs={11.3} p="2px 5px" display="flex" flexDirection="column">*/}
      {/*        <Typography variant="h6" fontSize={14} color="text.newMedium">*/}
      {/*            Allergen Info*/}
      {/*        </Typography>*/}
      {/*        <Typography*/}
      {/*            variant="subtitle1"*/}
      {/*            component="span"*/}
      {/*            fontSize={12}*/}
      {/*            my={1}*/}
      {/*            color="text.secondary"*/}
      {/*        >*/}
      {/*            {data.allergenInfo}*/}
      {/*        </Typography>*/}
      {/*        <Link fontSize={12} underline="none" href="/" display="flex">*/}
      {/*            <PhoneInTalkOutlinedIcon sx={{fontSize: 13, marginRight: 1}}/>*/}
      {/*            Call Javits Takeaway*/}
      {/*        </Link>*/}
      {/*    </Grid>*/}
      {/*</Grid>*/}
      <Grid container p={{ xs: "10px 15px", md: "16px 30px 16px 30px" }}>
        <Grid item xs={0.7} alignItems={"center"} fontWeight="semi-bold">
          <AccessTimeOutlinedIcon sx={{ color: "text.newMedium", width: 20 }} />
        </Grid>
        <Grid item xs={11.3} p="2px 5px">
          <Box display="flex" flexDirection="column">
            <Typography variant="h6" fontSize={14} color="text.newMedium">
              {t("working_hours")}
            </Typography>
            {workDays.map(({ dayName, shopOpenTime, shopCloseTime }, index) => (
              <Fragment key={index}>
                <Typography
                  variant="h6"
                  component="span"
                  fontSize={14}
                  color="text.secondary"
                  mt={1}
                >
                  {dayName}
                </Typography>
                <Typography
                  variant="subtitle1"
                  component="span"
                  fontSize={12}
                  color="text.secondary"
                >
                  {shopOpenTime.substring(5, -1)} To{" "}
                  {shopCloseTime.substring(5, -1)}
                </Typography>
              </Fragment>
            ))}
          </Box>
        </Grid>
      </Grid>
    </BaseModal>
  );
};

export default ShopInfoModal;
