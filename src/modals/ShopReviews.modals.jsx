import { useTranslation } from "next-i18next";
import { Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import ShopReviewsInfiniteScroll from "../infinitescrolls/ShopReviews.infinitescrolls";

const ShopReviewsModal = ({ open, handleClose, shopIdentity }) => {
  const { t } = useTranslation('common');
  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      ModalTitle={<Typography noWrap>{t('review_modal_title')}</Typography>}
      absoluteTitle
      sx={{ width: { xs: "90vw", sm: 410, md: 510 }, borderRadius: "4px" }}
      btnStyle={{ right: 20 }}
      innerContainerStyle={{
        p: { xs: 2, sm: 3 },
      }}
      id="reviewsListModal"
    >
      <ShopReviewsInfiniteScroll
        shopIdentity={shopIdentity}
      />
    </BaseModal>
  );
};

export default ShopReviewsModal;
