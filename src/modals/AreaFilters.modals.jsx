import { Modal, Backdrop, Box } from "@mui/material";

import AreaFiltersSideBar from "../sidebars/AreaFilters.sidebars";

//Modal Style
const style = {
  position: "absolute",
  width: '100vw',
  height: '100vh',
  bgcolor: "background.paper",
  outline: "none",
  border: "none",
};

const AreaFiltersModal = ({ open, handleClose, sx, ...restOfProps }) => {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 300,
      }}
    >
      <Box sx={{ ...style, ...sx }} {...restOfProps}>
        <AreaFiltersSideBar />
      </Box>
    </Modal>
  );
};

export default AreaFiltersModal;
