import { useState, Fragment } from "react";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";

//MUI
import Typography from "@mui/material/Typography";

import BaseModal from "./Base.modals";
import UserAddressesForm from "../forms/UserAddresses.forms";
import ChangePostCodeForm from "../forms/ChangePostCode.forms";
import LoginModal from "./Login.modals";
import SignupModal from "./Signup.modals";
import ForgetPasswordModal from "./ForgetPassword.modals";

const AddressChangeModal = ({
  openAddressChange,
  addressChangeModalHandler,
  hasInitialPostCode,
  saleMethod,
  setBasket,
  page,
  onSuccessfulSubmit = addressChangeModalHandler,
}) => {
  const { t } = useTranslation('addresses')
  const { isLogin } = useAuth();

  const [openLogin, setOpenLogin] = useState(false);
  const [openSignup, setOpenSignup] = useState(false);
  const [openForgetPass, setOpenForgetPass] = useState(false);

  const loginModalHandle = () => {
    setOpenLogin(!openLogin);
  };
  const signUpModalHandler = () => {
    setOpenLogin(false);
    setOpenSignup(!openSignup);
  };
  const forgetPassHandler = () => {
    setOpenLogin(false);
    setOpenSignup(false);
    setOpenForgetPass(!openForgetPass);
  };

  return (
    <BaseModal
      open={openAddressChange}
      handleClose={addressChangeModalHandler}
      btnStyle={{ top: "6%", right: "4%" }}
      showCloseButton={hasInitialPostCode}
      absoluteTitle
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
      ModalTitle={
        <Typography
          sx={{
            fontSize: 16,
            color: "text.primary",
            fontWeight: 700,
            py: 1,
          }}
        >
          {t('modal_title')}
        </Typography>
      }
    >
      {isLogin ? (
        <UserAddressesForm
          saleMethod={saleMethod}
          setBasket={setBasket}
          onSuccessfulSubmit={onSuccessfulSubmit}
          page={page}
        />
      ) : (
        <Fragment>
          <Typography variant="p" fontSize={14} px={3} mb={2}>
            {t('not_login_text_1')}{" "}
            <Typography
              component="span"
              variant="inherit"
              color="primary"
              fontSize="inherit"
              fontWeight={500}
              sx={{ cursor: "pointer" }}
              onClick={loginModalHandle}
            >
              {t('not_login_text_2')}{" "}
            </Typography>
            {t('not_login_text_3')}
          </Typography>
          <ChangePostCodeForm
            saleMethod={saleMethod}
            handleClose={onSuccessfulSubmit}
            page={page}
          />
        </Fragment>
      )}

      {/* login and sign up modals */}
      <LoginModal
        open={openLogin}
        handleOpen={loginModalHandle}
        signUpHandler={signUpModalHandler}
        forgetPassHandler={forgetPassHandler}
      />
      <SignupModal
        signUpModalHandler={signUpModalHandler}
        openSignup={openSignup}
      />
      <ForgetPasswordModal
        open={openForgetPass}
        handleClose={forgetPassHandler}
      />
    </BaseModal>
  );
};

export default AddressChangeModal;
