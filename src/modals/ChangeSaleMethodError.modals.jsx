import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { getCookie } from "cookies-next";
import useLocation from "../../hooks/useLocation";

import { Button, Typography, Grid } from "@mui/material";

import BaseModal from "./Base.modals";

import {
  queryKeyWords,
  queryParamEncode,
} from "../../utilities/areaPathHandlers";
import {
  cookiesNameSpaces,
  saleMethodChangeStatusCodes,
} from "../../utilities/appWideHelperFunctions";

const ChangeSaleMethodErrorModal = ({
  open,
  handleClose,
  message,
  statusCode,
  continueButtonCallback,
  shopDetail,
}) => {
  const router = useRouter();
  const { t } = useTranslation("common");
  const { postCode } = useLocation();
  const { postCodeQueryKeyWord, saleMethodQueryKeyWord } = queryKeyWords;
  const { PRE_ORDER_SHOP } = saleMethodChangeStatusCodes;

  let [
    backActionHandler,
    continueButtonHandler,
    backButtonText,
    continueButtonText,
  ] = [
    () =>
      router.push(
        {
          pathname: "/area/[area]",
          query: {
            [postCodeQueryKeyWord]: queryParamEncode(
              postCode !== "all" ? postCode : shopDetail?.address?.postCode
            ),
            [saleMethodQueryKeyWord]:
              getCookie(cookiesNameSpaces.SALE_METHOD) || "delivery",
          },
        },
        null,
        {
          scroll: false,
        }
      ),
    () => {
      continueButtonCallback && continueButtonCallback();
      handleClose();
    },
    t("menu_back_to_restaurant"),
    t("menu_view_menu"),
  ];

  if (statusCode === PRE_ORDER_SHOP) {
    // backActionHandler = () => router.back();
    continueButtonText = t("menu_pre_order");
  }

  return (
    <BaseModal
      open={open}
      handleClose={continueButtonHandler}
      btnStyle={{ right: "2%", top: "6%" }}
    >
      <Grid container flexDirection="column" gap={5} px={4} py={3}>
        <Grid item>
          <Typography variant="h6" fontSize={18}>
            {message}
          </Typography>
        </Grid>
        <Grid container item justifyContent="space-between" gap={1}>
          <Grid item xs={12} sm={5.5}>
            <Button
              variant="outlined"
              size="small"
              fullWidth
              onClick={continueButtonHandler}
              sx={{ height: "100%" }}
            >
              {continueButtonText}
            </Button>
          </Grid>
          <Grid item xs={12} sm={5.5}>
            <Button
              variant="outlined"
              size="small"
              fullWidth
              // sx={{ whiteSpace: "nowrap" }}
              onClick={backActionHandler}
            >
              {backButtonText}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </BaseModal>
  );
};

export default ChangeSaleMethodErrorModal;
