import { useEffect, useState, Fragment } from "react";
import useNotification from "../../hooks/useNotification";
import useLocation from "../../hooks/useLocation";

import { Typography, Grid, Skeleton } from "@mui/material";

import BaseModal from "./Base.modals";
import ShopMenuBasketSideBar from "../sidebars/ShopMenuBasket.sidebars";
import OrderReviewRatingForm from "../forms/OrderReviewRating.forms";
import GrowContainer from "../containers/Grow.containers";

import axiosConfig from "../../config/axios";

const OrderDetailModal = ({
  open,
  handleClose,
  currentOrder,
  reviewIdentity,
}) => {
  const { showNotification } = useNotification();
  const { orderId, shopTitle, currencyId, shopId, saleMethodName } =
    currentOrder;
  const { postCode } = useLocation();

  const [loading, setLoading] = useState(true);
  const [order, setOrder] = useState(null);
  const [reviewQuestions, setReviewQuestions] = useState(null);
  const [numberOfReviewRequest, setNumberOfReviewRequest] = useState(0);

  //! Fetch order detail
  useEffect(() => {
    const getOrderByOrderId = async () => {
      if (!open) return;
      setLoading(true);
      try {
        if (reviewIdentity) {
          const { basket, orderAddress, reviews, shopDetail } = await (
            await axiosConfig.get(`/Review/${reviewIdentity}`)
          ).data;
          setOrder(basket);
          setReviewQuestions(reviews);
        } else {
          const theOrder = await (
            await axiosConfig.get(`/Orders/GetByOrderId/${orderId}`)
          ).data;
          setOrder(theOrder);
        }

        setLoading(false);
      } catch (error) {
        showNotification(error.message, "error");
      }
    };

    getOrderByOrderId();
  }, [open, shopId, numberOfReviewRequest]);

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      ModalTitle={
        <Typography variant="h6" noWrap>
          {shopTitle}
        </Typography>
      }
      absoluteTitle
      btnStyle={{
        mr: { xs: 1, md: -1 },
      }}
    >
      {loading ? (
        <Skeleton
          animation="wave"
          variant="rectangular"
          height={500}
          width="100%"
        />
      ) : (
        <GrowContainer>
          <ShopMenuBasketSideBar
            basketData={order}
            postCode={postCode}
            saleMethod={saleMethodName.toLowerCase()}
            gotoCheckOutButton={false}
            orderDetailShopIdCurrencyId={{ currencyId, shopId }}
            showTitle={false}
            sx={{
              boxShadow: "none",
              top: "unset",
            }}
          />
          {reviewIdentity && reviewQuestions && (
            <Grid container p={2}>
              <OrderReviewRatingForm
                rawQuestions={reviewQuestions}
                reviewIdentity={reviewIdentity}
                onSuccessCallBack={() =>
                  setNumberOfReviewRequest(numberOfReviewRequest + 1)
                }
              />
            </Grid>
          )}
        </GrowContainer>
      )}
    </BaseModal>
  );
};

export default OrderDetailModal;
