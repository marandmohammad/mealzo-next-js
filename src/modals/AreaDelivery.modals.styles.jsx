import { Box, FormControlLabel, Button, styled } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

export const DeliveryModalContainer = styled(Box)(({ theme }) => ({
  position: "absolute",
  bottom: 0,
  left: 0,
  backgroundColor: "#fff",
  width: "100%",
  borderTopLeftRadius: 15,
  borderTopRightRadius: 15,
  padding: 24,
  paddingTop: 28,
  [theme.breakpoints.up("sm")]: {
    width: 400,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0 0 5px 0 rgba(0,0,0,0.5)",
    height: "min-content",
    borderRadius: 8
  },
}));

export const DeliveryModalHeader = styled("div")(() => ({
  display: "flex",
  justifyContent: "space-between",
  fontWeight: "500",
}));

export const DeliveryModalCloseBtnContainer = styled("span")(() => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "25px",
  height: "25px",
  border: "1px solid #f8f8f8",
}));

export const DeliveryModalCloseBtn = styled(CloseIcon)(({theme}) => ({
  fontSize: "20px",
  cursor: "pointer",
  color: theme.palette.primary.main,
  boxShadow: '2px 2px 18px #d9d9d9, -2px -2px 18px #ffffff',
}));

export const DeliveryModalAccordion = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-evenly",
  alignItems: "center",
  backgroundColor: "#dedede",
  padding: 3,
  borderRadius: 6,
  fontWeight: "400",
  gap: 4
}));

export const DeliveryModalLabel = styled(Button)(({ theme, selected }) => ({
  textAlign: "center",
  margin: 0,
  padding: "2px 13px",
  borderRadius: 4,
  flex: "1 1 auto",
  flexDirection: "column",
  justifyContent: "center",
  backgroundColor: selected ? "white" : "transparent",
  transition: "background-color ease-in 0.2s",
  color: theme.palette.text.newMedium,
  whiteSpace: "nowrap",
  "& .MuiButton-startIcon": {
    margin: 0,
  },
  "&:hover": {
    backgroundColor: selected
      ? "rgba(255, 255, 255, 0.7) "
      : "rgba(0, 0, 0, 0.1)",
  },
}));

export const DeliveryModalChange = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  padding: theme.spacing(0),
  marginBottom: 5,
  marginTop: 5,
}));

export const DeliveryModalDoneBtn = styled("button")(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  cursor: "pointer",
  border: "none",
  color: "white",
  padding: theme.spacing(1),
  fontSize: 18,
}));
