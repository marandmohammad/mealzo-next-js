import { Grid, Typography } from "@mui/material";
import { useTranslation } from "next-i18next";

import BaseModal from "./Base.modals";
import ShopMenuBasketSidebar from "../sidebars/ShopMenuBasket.sidebars";
import MenuDeliveryInput from "../inputs/MenuDelivery.inputs";

const BasketModal = ({
  open,
  handleClose,
  basketData,
  shopDetail,
  basketUpdateHandler,
  saleMethod,
  postCode,
  gotoCheckOutButton = true,
  orderDetailShopIdCurrencyId = null,
}) => {
  const { t } = useTranslation("basket");

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      ModalTitle={
        <Grid container flexDirection='column' py={2} gap={1}>
          <Typography variant="h6">{t('basket_title')}</Typography>
          <MenuDeliveryInput />
        </Grid>
      }
      absoluteTitle={true}
      sx={{
        borderRadius: "4px",
        height: { xs: "100%", md: "85vh" },
        width: { xs: "100vw", md: 400 },
      }}
      innerContainerStyle={{
        // height: "100%",
        maxHeight: "unset",
        "&::-webkit-scrollbar": {
          width: { xs: 0, sm: 4 },
          bgcolor: "transparent",
        },
        "&::-webkit-scrollbar-thumb": {
          borderRadius: "5px",
          bgcolor: "#e9540dad",
        },
      }}
      btnStyle={{
        // alignSelf: 'start',
        // mt: 3,
        // mx: 1,
        position: "absolute",
        top: 30,
        right: 30,
      }}
    >
      <ShopMenuBasketSidebar
        basketData={basketData}
        shopDetail={shopDetail}
        basketUpdateHandler={basketUpdateHandler}
        saleMethod={saleMethod}
        postCode={postCode}
        gotoCheckOutButton={gotoCheckOutButton}
        showTitle={false}
        fullHeight
        absoluteButton
      />
    </BaseModal>
  );
};

export default BasketModal;
