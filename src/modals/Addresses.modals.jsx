import { useState } from "react";

import { Box, IconButton, Typography } from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EditIcon from "@mui/icons-material/Edit";

import PrimaryButton from "../buttons/Primary.buttons";
import BaseModal from "./Base.modals";

const addresses = [
  {
    id: 1,
    address: "65 Princes St, Port Glasgow PA14 5JH",
  },
  {
    id: 2,
    address: "Floterstone information centre, Edinburgh, Penicuik EH26 0PR",
  },
  {
    id: 3,
    address: "15 High St, Penicuik EH26 8HS,United Kingdom",
  },
];

const AddressesModal = ({ openAddresses, addressesModalHandler }) => {
  const [isSelected, setIsSelected] = useState(false);

  const editClickHandler = (id) => {
    if (id === isSelected) setIsSelected(false);
    else setIsSelected(id);
  };

  return (
    <BaseModal
      sx={{ borderRadius: "8px" }}
      open={openAddresses}
      handleClose={addressesModalHandler}
      ModalTitle={
        <Typography component='div' sx={{ flexGrow: 1, fontWeight: 600, my: 4 }}>
          Saved Addresses
        </Typography>
      }
      btnStyle={{
        top: "2%",
        right: "2%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 2,
          p: "0 30px",
          width: "100%",
        }}
      >
        {addresses.map((item) => (
          <Box
            key={item.id}
            sx={{
              display: "flex",
              padding: 1,
              borderRadius: "8px",
              justifyContent: "space-between",
              alignItems: "center",
              border: "1px solid",
              borderColor: `${
                isSelected === item.id ? "primary.main" : "#707070"
              }`,
              cursor: "pointer",
              boxShadow: "0px 0px 24px #0000000D",
            }}
            onClick={() => editClickHandler(item.id)}
          >
            <Box alignItems="center" display="flex">
              <LocationOnIcon
                sx={{
                  marginRight: 2,
                  display: { xs: "none", md: "initial" },
                  color: `${
                    isSelected === item.id ? "primary.main" : "text.secondary"
                  }`,
                }}
              />
              <Typography
                variant="caption"
                component='div'
                sx={{
                  color: `${
                    isSelected === item.id ? "primary.main" : "text.secondary"
                  }`,
                }}
              >
                {item.address}
              </Typography>
            </Box>
            <IconButton width="fit-content">
              <EditIcon
                sx={{
                  color: `${
                    isSelected === item.id ? "primary.main" : "text.secondary"
                  }`,
                }}
              />
            </IconButton>
          </Box>
        ))}
        <PrimaryButton sx={{ my: 10 }} onClick={addressesModalHandler}>
          Choose
        </PrimaryButton>
      </Box>
    </BaseModal>
  );
};

export default AddressesModal;
