import { useState, useEffect } from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { useRouter } from "next/router";

// import { useAuth } from "../../hooks/authentication";
import useNotification from "../../hooks/useNotification";

import { Grid } from "@mui/material";

import BaseModal from "./Base.modals";
import StripeForm from "../forms/Stripe.forms";

import axiosConfig from "../../config/axios";


const stripePromise = loadStripe(
  process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_API_KEY, {
    locale: 'en-GB'
  }
);

const PayByStripeModal = ({ open, handleClose, shopId, orderId }) => {
  const router = useRouter();
  const { showNotification } = useNotification();

  const [clientSecret, setClientSecret] = useState("");
  const [loading, setLoading] = useState(true);

  //! Fetch PaymentIntent (Fetch Client Secret)
  useEffect(() => {
    const getClientSecret = async () => {
      setLoading(true);
      try {
        const checkout = await (
          await axiosConfig.get(
            `/Checkout${
              process.env.NODE_ENV === "development" ? "/PaymentDetails" : ""
            }/${shopId}/${orderId}`
          )
        ).data;
        const {
          clientSecret,
          customerId,
          ephemeralKeysSecret,
          publishableKey,
        } = checkout;
        setClientSecret(clientSecret);
        setLoading(false);
      } catch (error) {
        const { status } = error?.response;
        if (status && status === 406) {
          showNotification(
            `This order has been paid. please check your previous orders`,
            "error",
            3000
          );
          router.replace("/ordersteps");
        } else
          showNotification(
            `Payment process failed. Error Code: "${status}"`,
            "error",
            3000
          );
        handleClose();
      }
    };

    open && getClientSecret();

    return () => {
      setClientSecret("");
      setLoading(true);
    };
  }, [open]);

  const appearance = {
    theme: "flat",
  };
  const options = {
    clientSecret,
    appearance,
    loader: "always",
  };

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      // ModalTitle={<Typography variant="h6">Pay By Stripe</Typography>}
      absoluteTitle
      btnStyle={{ mr: { xs: "10px", sm: 0 } }}
      sx={{ borderRadius: "12px" }}
    >
      <Grid container p={3}>
        {open && !loading && clientSecret && (
          <Elements options={options} stripe={stripePromise}>
            <StripeForm clientSecret={clientSecret} handleClose={handleClose} />
          </Elements>
        )}
      </Grid>
    </BaseModal>
  );
};

export default PayByStripeModal;
