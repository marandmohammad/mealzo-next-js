import { useState, useMemo, useCallback } from "react";
import Image from "next/image";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";
import useLocation from "../../hooks/useLocation";

//MUI
import { Box, Button, Modal, Fade } from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

//custom MUI components
import {
  DeliveryModalContainer,
  DeliveryModalAccordion,
  DeliveryModalChange,
  DeliveryModalLabel,
} from "./AreaDelivery.modals.styles";
import AddressChangeModal from "./AddressChange.modals";
import DeliveryTimeModal from "./DeliveryTime.modals";

import { saleMethods } from "../../utilities/areaPathHandlers";
import { pageNames } from "../../utilities/appWideHelperFunctions";

const MenuDeliveryModal = ({
  deliveryModal,
  setDeliveryModal,
  hasInitialPostCode,
  saleMethod,
  deliveryHandler,
  // postCode,
  shopDetail,
  setBasket,
}) => {
  const { t } = useTranslation("common");
  const { showNotification } = useNotification();
  const { postCode } = useLocation();

  const [openAddressChange, setOpenAddressChange] = useState(
    !hasInitialPostCode
  );
  const addressChangeModalHandler = () =>
    setOpenAddressChange(!openAddressChange);

  const [openDeliveryTime, setOpenDeliveryTime] = useState(false);
  const deliveryTimeModalHandler = () => {
    setOpenDeliveryTime(!openDeliveryTime);
  };

  const saleMethodsKeys = Object.keys(saleMethods);
  const { address } = shopDetail;

  const showNotClosingModalCause = () =>
    showNotification(
      "Please enter a postcode or choose a address!",
      "warning",
      3000
    );

  const addressChangeModalOnSuccessSubmit = useMemo(
    () =>
      !hasInitialPostCode
        ? async () => {
            if (!hasInitialPostCode) {
              addressChangeModalHandler();
              setDeliveryModal(false);
            }
          }
        : undefined,
    [hasInitialPostCode]
  );
  
  return (
    <Modal open={deliveryModal} onClose={() => setDeliveryModal(false)}>
      <Fade in={deliveryModal}>
        <DeliveryModalContainer>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "18px",
            }}
          >
            {/* <DeliveryModalHeader>
            <span>Delivery Details</span>
            <DeliveryModalCloseBtnContainer>
              <DeliveryModalCloseBtn onClick={() => setDeliveryModal(false)} />
            </DeliveryModalCloseBtnContainer>
          </DeliveryModalHeader> */}

            {/* Sale Methods */}
            <DeliveryModalAccordion>
              {saleMethodsKeys.map((key) => {
                const { title, code, shpDetailProperty, iconAddress } =
                  saleMethods[key];
                if (address[shpDetailProperty])
                  return (
                    <DeliveryModalLabel
                      key={`${key}-2000`}
                      onClick={() => deliveryHandler(key)}
                      selected={saleMethod === key}
                      startIcon={
                        <Image
                          src={iconAddress}
                          width={48}
                          height={48}
                          alt={t(title)}
                        />
                      }
                    >
                      {t(title)}
                    </DeliveryModalLabel>
                  );
              })}
            </DeliveryModalAccordion>

            {/* PostCode */}
            {saleMethod === "delivery" && (
              <DeliveryModalChange>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    gap: "17px",
                  }}
                >
                  <LocationOnIcon
                    sx={{ color: "text.secondary", fontSize: 21 }}
                  />
                  <span>{postCode}</span>
                </Box>
                <Button
                  variant="text"
                  sx={{ textTransform: "capitalize" }}
                  onClick={addressChangeModalHandler}
                >
                  {t("delivery_change")}
                </Button>
              </DeliveryModalChange>
            )}

            {/* DeliveryTime */}
            <DeliveryModalChange>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  gap: "17px",
                }}
              >
                <AccessTimeIcon
                  sx={{ color: "text.secondary", fontSize: 21 }}
                />
                <span>{t(saleMethods[saleMethod].wantedTimeLabel)}</span>
              </Box>
              <Button
                variant="text"
                sx={{ textTransform: "capitalize" }}
                onClick={deliveryTimeModalHandler}
              >
                {t("schedule")}
              </Button>
            </DeliveryModalChange>

            <Button
              variant="contained"
              onClick={() => setDeliveryModal(false)}
              sx={{
                textTransform: "capitalize",
                fontSize: 18,
                borderRadius: "4px",
              }}
            >
              {t("done_btn")}
            </Button>
          </Box>

          {/* Address Change Modals */}
          <AddressChangeModal
            openAddressChange={openAddressChange}
            addressChangeModalHandler={
              !hasInitialPostCode
                ? showNotClosingModalCause
                : addressChangeModalHandler
            }
            hasInitialPostCode={hasInitialPostCode}
            saleMethod={saleMethod}
            setBasket={setBasket}
            onSuccessfulSubmit={addressChangeModalOnSuccessSubmit}
            page={pageNames.MENU_PAGE}
          />

          {/* Delivery Time Modal */}
          <DeliveryTimeModal
            openDeliveryTime={openDeliveryTime}
            deliveryTimeModalHandler={deliveryTimeModalHandler}
            shopId={address.shopId}
            saleMethod={saleMethod}
          />
        </DeliveryModalContainer>
      </Fade>
    </Modal>
  );
};

export default MenuDeliveryModal;
