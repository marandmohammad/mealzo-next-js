import { useTranslation } from "next-i18next";
import { Box, Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import SignInForm from "../forms/SignIn.forms";

const LoginModal = ({
  open,
  handleOpen,
  signUpHandler,
  handleCreateAccount = signUpHandler,
  forgetPassHandler = null,
}) => {
  const { t } = useTranslation("login_form");
  return (
    <Box sx={{ display: { xs: "none", md: "block" } }}>
      <BaseModal
        open={open}
        handleClose={handleOpen}
        sx={{
          px: 5,
          pb: { xs: 1, sm: 5 },
          pt: 1,
          borderRadius: "4px",
          height: { xs: "100%", sm: "unset" },
        }}
        innerContainerStyle={{
          "&::-webkit-scrollbar": {
            width: { xs: 0, sm: 4 },
            bgcolor: "transparent",
          },
          "&::-webkit-scrollbar-thumb": {
            borderRadius: "5px",
            bgcolor: "#e9540dad",
          },
        }}
        btnStyle={{ top: "5%", right: "5%" }}
        ModalTitle={
          <Typography
            component="div"
            sx={{
              fontSize: 24,
              color: "text.primary",
              flexGrow: 1,
              textAlign: "center",
            }}
          >
            {t("login_modal_title")}
          </Typography>
        }
      >
        <SignInForm
          signUpModalHandler={signUpHandler}
          onLoginSuccess={() => handleOpen()}
          handleCreateAccount={handleCreateAccount}
          forgetPassHandler={forgetPassHandler}
        />
      </BaseModal>
    </Box>
  );
};

export default LoginModal;
