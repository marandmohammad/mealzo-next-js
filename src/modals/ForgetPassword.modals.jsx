import { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";
import useNotification from "../../hooks/useNotification";

import { Box, Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import SignUpInput from "../inputs/SignUp.inputs";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";

import axiosConfig from "../../config/axios";
import { isValidEmail } from "../../utilities/formikSetups";

const ForgetPasswordModal = ({ open, handleClose }) => {
  const { t } = useTranslation("login_form");
  const { showNotification } = useNotification();

  const [email, setEmail] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  // const [timesOfTry, setTimesOfTry] = useState(0);
  // const [counter, setCounter] = useState(0);

  const changeEmailHandler = (e) => setEmail(e.target.value);

  useEffect(() => {
    if (!email || email.trim() === "")
      setError(t("validation_required_message"));
    else if (!isValidEmail(email)) {
      setError(t("not_valid_email_message"));
    } else {
      setError(null);
    }
  }, [email]);

  // useEffect(() => {
  //   const counterInterval = setInterval(() => {
  //     if (counter < 120) setCounter(counter + 1);
  //   }, 1000);

  //   return () => {
  //     clearInterval(counterInterval);
  //   };
  // }, [timesOfTry]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    await setLoading(true);
    try {
      const result = await (
        await axiosConfig.post("/Account/ForgetPassword", {
          username: email,
        })
      ).data;

      showNotification(result.message[0], "success", 3000);
      handleClose();
    } catch (error) {
      const errorMessage =
        typeof error.response !== "undefined"
          ? error.response.data.message[0]
          : error.message;
      showNotification(errorMessage, "error", 3000);
    }
    await setLoading(false);
  };

  return (
    <BaseModal
      open={open}
      handleClose={handleClose}
      sx={{
        px: 5,
        pb: { xs: 1, sm: 5 },
        pt: 1,
        borderRadius: "4px",
        height: { xs: "100%", sm: "unset" },
      }}
      innerContainerStyle={{
        "&::-webkit-scrollbar": {
          width: { xs: 0, sm: 4 },
          bgcolor: "transparent",
        },
        "&::-webkit-scrollbar-thumb": {
          borderRadius: "5px",
          bgcolor: "#e9540dad",
        },
      }}
      btnStyle={{ position: "absolute", top: 15, right: 15 }}
      // btnStyle={{ mr: -5 }}
      absoluteTitle
      ModalTitle={
        <Typography
          component="div"
          sx={{
            fontSize: 24,
            color: "text.primary",
            flexGrow: 1,
            textAlign: "center",
          }}
        >
          {/* {timesOfTry === 0 ? "Forgot Password ?" : "Check your email"} */}
          {t("forget_password_modal_title")}
        </Typography>
      }
    >
      <Box
        component="form"
        display="flex"
        flexDirection="column"
        width="100%"
        gap={5}
        onSubmit={handleSubmit}
      >
        <Typography fontSize={14} fontWeight={500} textAlign="center">
          {t("forget_password_modal_text")}
        </Typography>

        <SignUpInput
          name="email"
          type="email"
          value={email}
          onChange={changeEmailHandler}
          error={error}
        >
          {t("email_field_label")}
        </SignUpInput>

        <LoadingPrimaryButton
          variant="contained"
          disabled={error}
          loading={loading}
          type="submit"
        >
          {t("forget_password_modal_submit_btn")}
        </LoadingPrimaryButton>
      </Box>
    </BaseModal>
  );
};

export default ForgetPasswordModal;

/*
{timesOfTry === 0 ? (
          <Fragment>
            <Typography fontSize={14} fontWeight={500} textAlign="center">
              Enter the email address associated with your account and we'll
              send you a link to reset your password.
            </Typography>

            <SignUpInput
              name="email"
              type="email"
              value={email}
              onChange={changeEmailHandler}
              error={error}
            >
              Email
            </SignUpInput>
          </Fragment>
        ) : (
          <Fragment>
            <Typography fontSize={14} fontWeight={500} textAlign="center">
              We sent a password reset link to {email}, if you didn't receive
              email try again after {counter} seconds.
            </Typography>

            <SignUpInput
              name="email"
              type="hidden"
              value={email}
              onChange={changeEmailHandler}
              error={error}
            >
              Email
            </SignUpInput>
          </Fragment>
        )}
*/
