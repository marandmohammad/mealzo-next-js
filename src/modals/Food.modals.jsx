import { useState, useContext, useEffect } from "react";
import { useTranslation } from "next-i18next";
import useNotification from "../../hooks/useNotification";

import { Box, Grid, Typography } from "@mui/material";

import BaseModal from "./Base.modals";
import FoodModalForm from "../forms/FoodModal.forms";
import FoodModalLoading from "../loadings/FoodModal.loadings";

import axiosConfig from "../../config/axios";
import { MenuPageContext } from "../../context/MenuPageContext";
import { saleMethods } from "../../utilities/areaPathHandlers";

const FoodModal = ({ item, open, handleClose }) => {
  const { t } = useTranslation("food_modal");
  const { showNotification } = useNotification();

  const [subMenu, setSubMenu] = useState(null);

  const [loading, setLoading] = useState(true);

  const { shopDetail, saleMethod, setSelectedProduct } =
    useContext(MenuPageContext);

  let [
    product,
    optionId,
    id,
    title,
    options,
    productImgVisible,
    productImgWide,
    description,
    productSignTitle,
    productSignPicUrl,
    spiceUrl,
    isHaveCrust,
    isHaveSubMenu,
    optionTitle,
    productSignTitleArray,
    productSignPicUrlArray,
  ] = new Array(16).fill(null);

  if (item) {
    //? Item Detail
    product = item.product;
    optionId = item.optionId;

    //? Product Detail
    id = product.id;
    title = product.title;
    options = product.options;
    productImgVisible = product.productImgVisible;
    productImgWide = product.productImgWide;
    description = product.description;
    productSignTitle = product.productSignTitle;
    productSignPicUrl = product.productSignPicUrl;
    spiceUrl = product.spiceUrl;
    isHaveCrust = product.isHaveCrust;
    isHaveSubMenu = product.isHaveSubMenu;

    //? Option Title
    optionTitle = optionId
      ? options.filter(({ optionId }) => optionId === optionId)[0].optionTitle
      : "";

    productSignTitleArray =
      productSignTitle && productSignTitle.trim() !== ""
        ? productSignTitle.split(",").map((el) => el.trim())
        : [];
    productSignPicUrlArray =
      productSignPicUrl && productSignPicUrl.trim() !== ""
        ? productSignPicUrl.split(",").map((el) => el.trim())
        : [];
  }
  // const { product, optionId } = item;

  // const {
  //   id,
  //   title,
  //   options,
  //   productImgWide,
  //   description,
  //   productSignTitle,
  //   productSignPicUrl,
  //   spiceUrl,
  //   isHaveCrust,
  //   isHaveSubMenu,
  // } = product;

  // Option Title
  // const optionTitle = optionId
  //   ? options.filter(({ optionId }) => optionId === optionId)[0].optionTitle
  //   : "";

  // const productSignTitleArray =
  //   productSignTitle && productSignTitle.trim() !== ""
  //     ? productSignTitle.split(",").map((el) => el.trim())
  //     : [];
  // const productSignPicUrlArray =
  //   productSignPicUrl && productSignPicUrl.trim() !== ""
  //     ? productSignPicUrl.split(",").map((el) => el.trim())
  //     : [];

  useEffect(() => {
    setSubMenu(null);
    setLoading(true);

    // if (isHaveCrust !== 0 || isHaveSubMenu !== 0)
    if (item)
      axiosConfig
        .get(
          `/SubMenus/${shopDetail.address.shopId}/${
            saleMethods[saleMethod].code
          }/${id}/${optionId ? optionId : 0}`
        )
        .then(({ data }) => {
          setSubMenu(data);
          setLoading(false);
        })
        .catch((error) => showNotification(error.message, "error", 3000));

    return () => {
      setSubMenu(null);
      setLoading(true);
    };
  }, [item]);

  if (item)
    return (
      <BaseModal
        sx={{
          width: { xs: "100%", sm: "70vw", md: 540 },
          // maxHeight: { xs: "100%", md: "85vh" },
          height: { xs: "100%", sm: "unset" },
        }}
        innerContainerStyle={{
          height: { xs: "100%", sm: "unset" },
        }}
        childrenStyle={{ justifyContent: "center" }}
        open={open}
        // title={data.title}
        handleClose={handleClose}
        absoluteTitle
        ModalTitle={
          <Typography variant="h6" component="h6" noWrap sx={{ flexGrow: 1 }}>
            {title} {optionTitle}
          </Typography>
        }
        btnStyle={{ right: 20 }}
      >
        <Grid container>
          {/* Banner */}
          <Grid container p={{ xs: "0px 20px", md: "0px 30px" }}>
            {/* Banner Image */}
            <Box width="100%" height={235}>
              <img
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: productImgVisible ? "contain" : "fill",
                }}
                src={
                  productImgVisible
                    ? productImgWide
                    : "/Images/MenuFoodModalBannerPlaceHolder.jpg"
                }
                alt={`${title} ${optionTitle}`}
              />
            </Box>

            {/* Product Marks */}
            <Grid container justifyContent="space-between" py={2.5}>
              {productSignTitleArray.length > 0 &&
                productSignTitleArray.map((item, idx) => (
                  <Grid
                    item
                    key={idx}
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                  >
                    <Box width={23} height={23} overflow="hidden">
                      <img
                        src={productSignPicUrlArray[idx]}
                        style={{ width: "100%", height: "100%" }}
                        alt={item}
                      />
                    </Box>
                    <Typography
                      ml={0.5}
                      variant="subtitle1"
                      component="span"
                      fontSize={{ xs: 12, md: 16 }}
                    >
                      {item}
                    </Typography>
                  </Grid>
                ))}
              {spiceUrl && spiceUrl.trim() !== "" && spiceUrl.trim() !== "#" && (
                <Grid
                  item
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Box width={23} height={23} overflow="hidden">
                    <img
                      src={spiceUrl}
                      style={{ width: "100%", height: "100%" }}
                      alt="Hot & spicy"
                    />
                  </Box>
                  <Typography
                    ml={0.5}
                    variant="subtitle1"
                    component="span"
                    fontSize={{ xs: 12, md: 16 }}
                  >
                    {t("hot_and_spicy")}
                  </Typography>
                </Grid>
              )}
            </Grid>

            {/* Product Description */}
            <Box mt={2} pb={4}>
              <Typography
                variant="subtitle1"
                component="span"
                fontSize={{ xs: 14, md: 18 }}
              >
                {description}
              </Typography>
            </Box>
          </Grid>

          {/* Modal Accordion */}
          {loading ? (
            <FoodModalLoading />
          ) : (
            <FoodModalForm
              item={item}
              subMenu={subMenu}
              handleClose={handleClose}
            />
          )}

          {/*Modal Text Area */}
          {/*<Box width="100%">*/}
          {/*  <Box p="10px 20px" bgcolor="divider">*/}
          {/*    <Typography variant="h6" component="div" fontSize={14}>*/}
          {/*      Extra instructions*/}
          {/*    </Typography>*/}
          {/*  </Box>*/}
          {/*  <Box p="10px 20px">*/}
          {/*    <Typography variant="h6" component="div" fontSize={14}>*/}
          {/*      list any special request*/}
          {/*    </Typography>*/}
          {/*  </Box>*/}
          {/*  <Box p="10px 20px">*/}
          {/*    <TextareaAutosize*/}
          {/*      name="specialRequest"*/}
          {/*      onChange={formik.handleChange}*/}
          {/*      style={{*/}
          {/*        backgroundColor: "#EDEDED",*/}
          {/*        width: "100%",*/}
          {/*        outline: "none",*/}
          {/*        border: "none",*/}
          {/*        padding: "15px 11px",*/}
          {/*        fontSize: "12px",*/}
          {/*      }}*/}
          {/*      minRows={10}*/}
          {/*      value={formik.values.specialRequest}*/}
          {/*      placeholder="The store has choosen not to accept special requests. Contact them directly with questions about their menus."*/}
          {/*    />*/}
          {/*  </Box>*/}
          {/*</Box>*/}
        </Grid>
      </BaseModal>
    );
};

export default FoodModal;
