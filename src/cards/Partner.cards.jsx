// Mui Component
import { Typography, Button, Grid, Box } from "@mui/material";

// Icons
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import Link from "../links/Link";

const PartnerCard = ({ data, ...restOfProps }) => {
  const image = data.image;
  return (
    <Grid
      container
      sx={{
        flexDirection: { xs: "column", sm: "row" },
        flexWrap: { xs: "unset", sm: "nowrap" },
        justifyContent: { xs: "center", sm: "normal" },
      }}
      gap={2}
      {...restOfProps}
    >
      <Grid
        item
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: { xs: "center", md: "flex-start" },
        }}
      >
        <Box overflow="hidden" width={{ xs: 190, md: 150, xl: 250 }}>
          <img
            src={image}
            alt={`Mealzo ${data.title}`}
            style={{ width: "100%", height: "100%" }}
            loading="lazy"
          />
        </Box>
      </Grid>
      <Grid
        container
        item
        flexDirection="column"
        justifyContent="center"
        textAlign={{ xs: "center", sm: "left" }}
      >
        <Grid item>
          <Typography variant="h6" component="div">
            {data.title}
          </Typography>
        </Grid>
        <Grid item my={2}>
          <Typography variant="subtitle1" component="div">
            {data.body}
          </Typography>
        </Grid>
        <Grid item>
          <Link href={data.href}>
            <Button color="primary" endIcon={<KeyboardArrowRightIcon />}>
              {data.buttonContent}
            </Button>
          </Link>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PartnerCard;
