import Image from "next/image";

import { Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/material";

function AboutMealzoCard({ title, description, imageUrl, sx, ...restOfProps }) {
  const theme = useTheme();
  return (
    <Grid
      container
      sx={{
        p: 1,
        my: 5,
        alignItems: "center",
        justifyContent: "space-between",
        ...sx,
      }}
      {...restOfProps}
    >
      <Grid item md={6} sm={12} xs={12} textAlign="center">
        <Image src={imageUrl} loading="lazy" />
      </Grid>
      <Grid item container md={6} px={2} mt={{ md: 0, sm: 2, xs: 2 }}>
        <Typography
          variant="h6"
          component="h6"
          sx={{
            color: theme.palette.common.black,
            mb: 2,
          }}
        >
          {title}
        </Typography>
        <Typography variant="subtitle1" component="div">
          {description}
        </Typography>
      </Grid>
    </Grid>
  );
}

export default AboutMealzoCard;
