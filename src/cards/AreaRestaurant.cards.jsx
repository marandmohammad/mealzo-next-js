import { useMemo } from "react";
import Image from "next/image";
import { useTranslation } from "next-i18next";

//MUI
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea, Grid } from "@mui/material";

import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import AccessTimeOutlinedIcon from "@mui/icons-material/AccessTimeOutlined";
import DirectionsRunIcon from "@mui/icons-material/DirectionsRun";

//Custom styled components
import {
  CustomGradeRoundedIcon,
  Item,
  Offer,
  RestaurantCardContainer,
  ShopCardLogo,
} from "./AreaRestaurant.cards.styles";
import RestaurantCardBadgeTypography from "../typographies/RestaurantCardBadge.typographies";

import LocalHeroBadge from "../../public/Images/local-hero-badge.png";

import {
  calcPriceIconGapTime,
  calcPriceIconGapTimeUseCases,
} from "../../utilities/appWideHelperFunctions";

const RestaurantCard = ({
  item: {
    perCentOffer,
    homePageBanner,
    shoplogo,
    shopTitle,
    rate,
    shopSubjects,
    isOpen,
    deliveryFee,
    currencyId,
    mile,
    deliveryTimeGap,
    exclusiveOnMealzo,
    collectionTimeGap,
    ispromotion,
  },
  topPick = false,
  currentSaleMethod,
  differentSaleMethod,
}) => {
  const { t } = useTranslation("common");
  const { deliveryIcon, price, gapTime, categories, isCollectNow } = useMemo(
    () =>
      calcPriceIconGapTime(
        isOpen,
        currencyId,
        deliveryFee,
        deliveryTimeGap,
        shopSubjects,
        collectionTimeGap,
        currentSaleMethod,
        t,
        differentSaleMethod,
        calcPriceIconGapTimeUseCases.RESTAURANT_CART
      ),
    []
  );

  return (
    <Item>
      <CardActionArea>
        <RestaurantCardContainer>
          <Image
            loader={({ src }) => src}
            src={homePageBanner}
            alt={`Mealzo ${shopTitle}`}
            layout="fill"
            loading="lazy"
            objectFit="cover"
            placeholder="blur"
            blurDataURL="/Images/MenuFoodModalBannerPlaceHolder.jpg"
          />
        </RestaurantCardContainer>
        {/* local hero badge */}
        {/* {exclusiveOnMealzo && (
          <Grid position="absolute" top={0} right={0} width={120} height={120}>
            <Image
              src={LocalHeroBadge}
              alt="mealzo local hero badge"
              layout="fill"
            />
          </Grid>
        )} */}

        {/* need to refactor based on data structure */}
        <Offer>
          {perCentOffer !== 0 ? (
            <>
              <div>{perCentOffer}%</div>
              <div>{t("area_restaurant_off")}</div>
            </>
          ) : (
            <div>{t("area_meal_deals")}</div>
          )}
        </Offer>

        <ShopCardLogo>
          <Image
            loader={({ src }) => src}
            src={shoplogo}
            alt={shopTitle}
            layout="fill"
            loading="lazy"
            objectFit="contain"
            placeholder="blur"
            blurDataURL="/Images/LOGOSKELETON.png"
          />
        </ShopCardLogo>

        {/* Promoted & Exclusive to Mealzo */}
        <Grid
          sx={{
            display: "flex",
            flexDirection: "column",
            position: "absolute",
            bottom: topPick ? "40%" : "45%",
          }}
        >
          {ispromotion && (
            <RestaurantCardBadgeTypography promoted>
              {t("area_promoted")}
            </RestaurantCardBadgeTypography>
          )}
          {exclusiveOnMealzo && (
            <RestaurantCardBadgeTypography>
              {t("exclusive_to_mealzo")}
            </RestaurantCardBadgeTypography>
          )}
        </Grid>
        <CardContent sx={{ padding: "10px 10px 15px 10px" }}>
          <Typography
            align="left"
            // gutterBottom
            variant="h6"
            fontSize={15}
            component="div"
          >
            {shopTitle}
          </Typography>
          <Grid
            container
            alignItems="center"
            flexWrap="nowrap"
            mt={1}
            mb={0.25}
          >
            <CustomGradeRoundedIcon />
            <Typography variant="subtitle1" fontSize={13} noWrap>
              <span>
                <Typography
                  variant="subtitle1"
                  component="span"
                  sx={{ color: "#ff571e", mr: 0.6, fontSize: 13 }}
                >
                  {rate.length === 13
                    ? rate.substring(1, -1)
                    : rate.substring(3, -1)}
                </Typography>
                <span>
                  {rate.length === 13 ? rate.substring(2) : rate.substring(4)}
                </span>
              </span>
              <span>
                {categories &&
                  categories.map((item, idx) => (
                    <span key={idx}> · {item}</span>
                  ))}
              </span>
            </Typography>
          </Grid>

          {!topPick && (
            <Grid
              container
              flexDirection="row"
              flexWrap="nowrap"
              justifyContent="space-between"
              // mt={1}
            >
              {/* Price */}
              {price && (
                <Grid
                  item
                  container
                  flexDirection="row"
                  flexWrap="nowrap"
                  alignItems="center"
                  // xs={3}
                >
                  <Grid
                    item
                    height={10}
                    width={20}
                    mr={0.6}
                    position="relative"
                  >
                    {isCollectNow ? (
                      <DirectionsRunIcon
                        sx={{
                          fontSize: 18,
                          position: "absolute",
                          top: "50%",
                          transform: "translateY(-50%)",
                        }}
                      />
                    ) : (
                      <Image
                        src={`/Images/${deliveryIcon}`}
                        layout="fill"
                        objectFit="contain"
                        alt="price icon"
                      />
                    )}
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      fontSize={12}
                      noWrap
                    >
                      {price}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {/* Distance */}
              {(isOpen.toLowerCase() === "open" ||
                isOpen.toLowerCase().startsWith("opening")) && (
                <Grid
                  item
                  container
                  flexDirection="row"
                  flexWrap="nowrap"
                  justifyContent={price ? "center" : "start"}
                  alignItems="center"
                  // xs={4}
                  mr={price ? 0.5 : 0}
                >
                  <Grid item height={20}>
                    <LocationOnOutlinedIcon
                      sx={{ height: "100%", fontSize: 15 }}
                    />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      fontSize={12}
                      noWrap
                    >
                      {mile}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {/* Gap Time */}
              {gapTime && (
                <Grid
                  item
                  container
                  flexDirection="row"
                  flexWrap="nowrap"
                  justifyContent="flex-end"
                  alignItems="center"
                  // xs={4}
                >
                  <Grid item height={20} mr={0.5}>
                    <AccessTimeOutlinedIcon
                      sx={{ height: "100%", fontSize: 13 }}
                    />
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      fontSize={12}
                      noWrap
                    >
                      {gapTime}
                    </Typography>
                  </Grid>
                </Grid>
              )}
            </Grid>
          )}
        </CardContent>
      </CardActionArea>
    </Item>
  );
};

export default RestaurantCard;
