import { Grid, Avatar, Typography } from "@mui/material";

function LocalBrandsCard({ title, imageUrl }) {
  return (
    <Grid
      item
      container
      sx={{
        maxWidth: 225,
        textAlign: "center",
        justifyContent: "center",
        my: 6,
        px:1.8,
      }}
    >
      <Avatar
        src={imageUrl}
        alt={`Mealzo-${title}`}
        sx={{ width: 80, height: 80, mb: 3 }}
      />
      <Typography varaiant="subtitle1" component="div" fontSize="1rem">
        {title}
      </Typography>
    </Grid>
  );
}

export default LocalBrandsCard;
