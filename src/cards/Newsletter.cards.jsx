import { Grid, Typography } from "@mui/material";

const NewsletterCard = ({
  title,
  subtitle,
  text,
  imageUrl,
  reverse,
  ButtonComponent,
}) => {
  return (
    <Grid
      container
      flexDirection={reverse ? "row-reverse" : "row"}
      flexWrap={{ xs: "wrap", sm: "nowrap" }}
      justifyContent="space-between"
    >
      <Grid item xs={12} sm={5} md={5}>
        <img src={imageUrl} alt={title} width="100%" />
      </Grid>
      <Grid item xs={12} sm={6.5} md={6}>
        <Grid mb={5}>
          <Typography color="primary.main" fontWeight={700} fontSize={27}>
            {title}
          </Typography>
          {subtitle && (
            <Typography color="black" fontWeight={700} fontSize={27} mt={2}>
              {subtitle}
            </Typography>
          )}
        </Grid>
        <Typography color="black" fontWeight={400} fontSize={26}>
          {text}
        </Typography>

        {ButtonComponent && ButtonComponent}
      </Grid>
    </Grid>
  );
};

export default NewsletterCard;
