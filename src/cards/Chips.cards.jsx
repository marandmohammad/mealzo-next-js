// Mui Components
import { Grid, Typography } from "@mui/material";

const ChipsCard = ({ title, ...restOfProps }) => {
  return (
    <Grid
      sx={{
        textAlign: "center",
        borderRadius: ".4rem",
        // maxWidth: 150,
        bgcolor: "#eaeaea",
        cursor: "pointer",
        mx: 1,
        my: 2,
        py: 1,
        px: 3,
        color: "#373636",
        transition: "all 150ms ease-in",
        "&:hover": {
          bgcolor: "rgba(255,196,168,.57)",
          color: "#e9540d",
        },
      }}
      {...restOfProps}
    >
      <Typography
        variant="h6"
        component="div"
        fontWeight="500"
        fontSize={18}
        noWrap
      >
        {title}
      </Typography>
    </Grid>
  );
};

export default ChipsCard;
