// Mui Components
import { Grid, Typography } from "@mui/material";

const BlogsSidebarDateCard = ({ data: { blogIdentity, dateS } }) => {

  const date = new Date(dateS).toLocaleDateString("en-uk", {
    day: "numeric",
    month: "short",
  });

  return (
    <Grid container mb={2}>
      <Grid item xs={2}>
        <Typography variant="h6" fontSize={15} color="text.primary">
          {date}
        </Typography>
      </Grid>
      <Grid item xs={10}>
        <Typography variant="h6" fontSize={11} color="text.primary">
          {blogIdentity}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default BlogsSidebarDateCard;
