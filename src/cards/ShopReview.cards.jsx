import { Grid, Rating, Typography } from "@mui/material";

const ShopReviewCard = ({ review, sx }) => {
  return (
    <Grid
      container
      flexDirection="column"
      gap={1.5}
      sx={{
        p: 2,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "checkbox",
        borderRadius: "4px",
        ...sx,
      }}
    >
      <Grid item container flexWrap="nowrap">
        <Grid container item flexDirection="column">
          <Typography variant="h6" fontSize={16}>
            {/* {review.name} */}
          </Typography>
          <Typography variant="subtitle1" fontSize={12}>
            {new Date(review.dateS).toLocaleString("en-GB", {
              day: "numeric",
              month: "short",
              year: "numeric",
            })}
          </Typography>
        </Grid>
        <Grid item>
          <Rating value={review.gradeAvrage} readOnly />
        </Grid>
      </Grid>
      <Grid item>
        <Typography fontSize={14}>{review.note}</Typography>
      </Grid>
    </Grid>
  );
};

export default ShopReviewCard;
