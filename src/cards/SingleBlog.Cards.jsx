// Components
import TagsCard from "./BlogsTags.card";

// Mui components
import { Card, CardMedia, Stack, Typography } from "@mui/material";

// Mui Icons
// import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

const SingleBlogCard = ({
  cardData: {
    blogIdentity,
    newsContent,
    readTime,
    imageHeader,
    tag,
    dateS,
  },
}) => {
  const tags = tag.split(",");
  const date = new Date(dateS).toLocaleDateString("en-uk", {
    day: "numeric",
    year: "numeric",
    month: "short",
  });
  return (
    <Card>
      {/* card image */}
      <CardMedia component="img" image={imageHeader} alt={blogIdentity} />

      {/* card infos icons start */}
      <Stack direction="row" justifyContent="space-between" p={1}>
        {/* <Stack direction="row" alignItems="center">
          <PersonOutlineIcon color="primary" fontSize="22px" />
          <Stack mx={1}>
            <Typography variant="caption" color="text.primary">
              Author
            </Typography>
            <Typography variant="caption" color="initial" fontWeight="bold">
              {author}
            </Typography>
          </Stack>
        </Stack> */}
        <Stack direction="row" alignItems="center">
          <CalendarMonthIcon color="primary" fontSize="22px" />
          <Typography mx={1} variant="caption" color="text.primary">
            {date}
          </Typography>
        </Stack>
        <Stack direction="row" alignItems="center">
          <AccessTimeIcon color="primary" fontSize="22px" />
          <Typography mx={1} variant="caption" color="text.primary">
            {readTime} mins
          </Typography>
        </Stack>
      </Stack>
      {/* card infos icons end */}

      {/* blog title */}
      <Stack p={1}>
        <Typography
          variant="h6"
          fontWeight="bold"
          component="h1"
          color="initial"
          fontSize="15px"
        >
          {blogIdentity}
        </Typography>
      </Stack>

      {/* tags  */}
      <Stack direction="row" display="flex" flexWrap="wrap">
        {tags.map((tag, index) => (
          <TagsCard tag={tag} key={index} />
        ))}
      </Stack>

      {/* Content */}
      <Typography
        variant="subtitle1"
        component="div"
        sx={{
          p: 1,
          "& a": {
            color: "primary.main",
            textDecoration: "none",
          },
        }}
        dangerouslySetInnerHTML={{ __html: newsContent }}
      ></Typography>
    </Card>
  );
};

export default SingleBlogCard;
