import { Grid, Typography } from "@mui/material";
import FormatQuoteIcon from "@mui/icons-material/FormatQuote";

const FeedBackCard = ({ shopTitle, text, author, sx, ...restOfProps }) => {
  return (
    <Grid
      container
      sx={{
        pt: 4,
        pb: 2,
        px: 2,
        flexDirection: "column",
        bgcolor: "#FFF",
        position: "relative",
        boxShadow: "0px 0px 20px #0000000A",
        ...sx,
      }}
      {...restOfProps}
    >
      <FormatQuoteIcon
        sx={{
          position: "absolute",
          color: "#e5e5e5",
          top: -1,
          right: 0,
          fontSize: 75
        }}
      />
      <Typography variant="h6" color="primary" fontSize={18}>
        {shopTitle}
      </Typography>
      <Typography variant="subtitle1" fontSize={16} mt={2} mb={1.4}>
        {text}
      </Typography>
      <Typography
        variant="body1"
        color="primary"
        fontSize={16}
        textAlign="right"
      >
        {author}
      </Typography>
    </Grid>
  );
};

export default FeedBackCard;
