import { Button, Typography } from "@mui/material";

const BlogsTag = ({ tag: { name } }) => {
  return (
    <Button
      sx={{
        bgcolor: "#eaeaea",
        color: "#4a4b4d",
        borderRadius: "3px",
        margin: "5px !important",
      }}>
      <Typography
        sx={{
          fontWeight: "bold",
          fontSize: "0.83rem",
          "&:hover": {
            color: "#e9540d",
          },
        }}>
        {name}
      </Typography>
    </Button>
  );
};

export default BlogsTag;
