import { useTranslation } from "next-i18next";

//MUI
import { Box, CardMedia, Grid, Stack, Typography, Grow } from "@mui/material";
// import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import DateRangeIcon from "@mui/icons-material/DateRange";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
// import BlogsTag from "../tags/Blogs.tags";
import TagsCard from "./BlogsTags.card";
import Link from "../links/Link";

import { queryParamEncode } from "../../utilities/areaPathHandlers";

const BlogsCard = ({
  item: {
    blogIdentity,
    category,
    dateS,
    imageHeader,
    readTime,
    subSubject,
    tag,
  },
}) => {
  const { t } = useTranslation('blog_pages');
  const tags = tag.split(",");
  const date = new Date(dateS).toLocaleDateString("en-uk", {
    day: "numeric",
    year: "numeric",
    month: "short",
  });

  return (
    <Grow in={true}>
      <div style={{ height: "100%"}}>
        <Stack
          direction={"column"}
          sx={{
            width: "100%",
            minHeight: "100%",
            borderRadius: "3px",
            overflow: "hidden",
          }}
          spacing={2}
          boxShadow={1}
        >
          <Box
            sx={{
              maxHeight: { sm: "400px", md: "270px" },
              overflow: "hidden",
            }}
          >
            <CardMedia
              component="img"
              image={imageHeader}
              loading="lazy"
              alt={blogIdentity}
            />
          </Box>

          <Stack flexDirection="column" p={2} spacing={2.5}>
            <Grid
              container
              sx={{
                flexDirection: "row",
                flexWrap: "nowrap",
                alignItems: "center",
                justifyContent: "space-between",
                fontSize: "12px",
              }}
            >
              {/* <Grid item alignItems="center" xs={4} container sx={{ gap: "5px" }}>
            <PersonOutlineIcon color="primary" sx={{ fontSize: "20px" }} />
            <Stack direction="column">
              <Typography variant="span">Author</Typography>
              <Typography variant="span" fontWeight={"bold"}>
                Admin
              </Typography>
            </Stack>
          </Grid> */}
              <Grid
                item
                // xs={6}
                spacing={1}
                container
                alignItems="center"
                sx={{ gap: "5px" }}
              >
                <DateRangeIcon color="primary" sx={{ fontSize: "20px" }} />
                <Typography letterSpacing={0.5} variant="span">
                  {date}
                </Typography>
              </Grid>
              <Grid
                item
                // xs={6}
                spacing={1}
                container
                alignItems="center"
                sx={{ gap: "5px" }}
              >
                <AccessTimeIcon color="primary" sx={{ fontSize: "20px" }} />
                <Typography variant="span">{readTime} mins</Typography>
              </Grid>
            </Grid>
            <Typography variant="h6" fontSize={15}>
              <Link
                href={`/OurBlog/${category}/${queryParamEncode(blogIdentity)}`}
                color="text.primary"
              >
                {blogIdentity}
              </Link>
            </Typography>

            <Typography variant="subtitle1" fontSize={14}>
              {subSubject}
            </Typography>

            <Typography
              variant="subtitle1"
              sx={{ fontSize: "1rem", cursor: "pointer" }}
            >
              <Link
                href={`/OurBlog/${category}/${queryParamEncode(blogIdentity)}`}
              >
                {t('read_more_btn')}
              </Link>
            </Typography>
            <Stack direction="row" spacing={0.5} flexWrap="wrap">
              {tags.map((tag, index) => (
                <Link
                  href={`/OurBlog/tag/${queryParamEncode(tag)}`}
                  key={index}
                >
                  <TagsCard tag={tag} />
                </Link>
              ))}
            </Stack>
          </Stack>
        </Stack>
      </div>
    </Grow>
  );
};

export default BlogsCard;
