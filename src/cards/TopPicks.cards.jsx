import Image from "next/image";
import { Grid, Typography } from "@mui/material";

function TopPicksCard({ brandTitle, brandLogo, imageUrl, sx, ...restOfProps }) {
  return (
    <Grid
      container
      sx={{
        flexDirection: "column",
        maxWidth: 410,
        minHeight: 190,
        justifyContent: "flex-end",
        alignItems: "flex-end",
        position: "relative",
        mb: { md: 0, xs: 5 },
        cursor: "pointer",
        ...sx,
      }}
      {...restOfProps}
    >
      <Grid
        item
        id="topPickCardImage"
        sx={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "90%",
        }}
      >
        {imageUrl ? (
          <Image
            loader={() => imageUrl}
            src={imageUrl}
            alt={`Mealzo ${brandTitle}`}
            layout="fill"
            loading="lazy"
            objectFit="cover"
          />
        ) : null}
      </Grid>
      <Grid
        item
        container
        sx={{
          bgcolor: "common.white",
          width: "95%",
          flexDirection: "row",
          flexWrap: "nowrap",
          position: "relative",
          boxShadow: "0px 0px 6px #00000029",
          alignItems: "center",
          justifyContent: "start",
          zIndex: 99,
          height: 50,
        }}
      >
        <Grid
          item
          sx={{
            width: 86,
            height: 86,
            borderRadius: "50%",
            overflow: "hidden",
            bgcolor: "common.white",
            position: "absolute",
            left: { md: 15, sm: 10, xs: 5 },
            top: -60,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            // ml: 2,
            // mb: 5,
          }}
        >
          {brandLogo && (
            <Image
              loader={() => brandLogo}
              src={brandLogo}
              alt={`Mealzo ${brandTitle}`}
              width={85}
              height={85}
            />
          )}
        </Grid>
        <Grid item ml={{xs: "35%", lg: '30%'}}>
          <Typography
            variant="h6"
            component="div"
            fontSize={{ lg: "80%", md: "70%", xs: "70%" }}
          >
            {brandTitle}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default TopPicksCard;
