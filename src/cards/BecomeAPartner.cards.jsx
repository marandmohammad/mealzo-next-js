import Image from "next/image";
import { Grid, Typography } from "@mui/material";

import ShopLogoContainer from "../containers/ShopLogo.containers";

const BecomeAPartnerCard = ({ title, text, iconUrl, ...restOfProps }) => {
  return (
    <Grid
      container
      flexDirection="column"
      boxShadow="0px 0px 20px #0000000A"
      borderRadius='8px'
      rowGap={3}
      alignItems='center'
      px={4}
      py={5}
      textAlign='center'
      {...restOfProps}
    >
      <ShopLogoContainer width={85} position="relative" sx={{ borderRadius: 0 }}>
        <Image
          src={iconUrl}
          loader={({src}) => src}
          layout="fill"
          objectFit="contain"
        />
      </ShopLogoContainer>

      <Typography variant="h6" fontSize={18}>
        {title}
      </Typography>

      <Typography variant="subtitle1" fontSize={14}>
        {text}
      </Typography>
    </Grid>
  );
};

export default BecomeAPartnerCard;
