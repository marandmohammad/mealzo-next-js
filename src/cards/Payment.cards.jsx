import { Button, Typography } from "@mui/material";

import Visa from "../../public/Images/visa.png";
import MasterCard from "../../public/Images/mastercard.png";

const PaymentCard = ({ item: { id, type, number }, active, onClick }) => {
  return (
    <Button
      sx={{
        justifyContent: "space-between",
        boxShadow: "0px 0px 12px #00000017",
        width: "100%",
        p: 1.5,
        borderRadius: "5px",
        height: "100%",
        bgcolor: `${active === id ? "#3B5998" : "#fff"}`,
        "&:hover": {
          bgcolor: `${active === id ? "#3B5998" : "#fff"}`,
        },
      }}
      onClick={onClick}
    >
      <Typography
          component='div'
        sx={{
          fontSize: 16,
          fontWeight: 500,
          color: `${active === id ? "common.white" : "text.newMedium"}`,
        }}
      >
        {number}
      </Typography>
      <img src={type === "visa" ? Visa : MasterCard} alt="" width="36" />
    </Button>
  );
};

export default PaymentCard;
