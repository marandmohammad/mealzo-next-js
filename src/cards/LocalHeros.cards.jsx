import { useState, useEffect } from "react";
import { v4 as uuid4 } from 'uuid';

import { Stack, Grow, Box } from "@mui/material";
import Carousel from "react-elastic-carousel";
// import { Swiper, SwiperSlide } from "swiper/react";

import LocalHerosDescription from "./LocalHerosDescription.cards";

const LocalHerosCardImage = ({ src, alt }) => {
  return (
    // <Card sx={{ width: 230, height: 230, borderRadius: "4px" }}>
    //   <CardMedia
    //     component="img"
    //     image={src}
    //     loading="lazy"
    //     alt={alt}
    //     sx={{
    //       minHeight: "170px",
    //     }}
    //   />
    // </Card>
    <Box
      sx={{ width: 230, height: 230, borderRadius: "4px", overflow: "hidden" }}
    >
      <img
        src={src}
        alt={alt}
        loading="lazy"
        style={{ width: "100%", height: "100%", objectFit: "cover" }}
      />
    </Box>
  );
};

const LocalHerosCard = ({ cardData }) => {
  // need this for handling carousel pagination to show or hide
  let items = [];
  if (cardData.eventsPics.length === 1) {
    items = new Array(3).fill(cardData.eventsPics[0]);
  } else if (cardData.eventsPics.length === 2) {
    items = [cardData.eventsPics[1]].concat([...cardData.eventsPics]);
  } else if (cardData.eventsPics.length >= 3) {
    items = [...cardData.eventsPics];
  }

  const [win, setWin] = useState(null);
  useEffect(() => {
    setWin(window);
  }, []);

  return (
    <Grow in={true}>
      <div>
        <Stack
          spacing={2}
          sx={{
            flexDirection: { xs: "column", md: "row" },
            height: { md: 380 },
            width: { xs: "100%", md: "90%" },
            my: 5,
          }}
          position="relative"
          alignItems="center"
        >
          {win && (
            <Carousel
              itemsToShow={window.screen.width < "600px" ? 2 : 3}
              showArrows={false}
              itemPadding={[1, 1]}
              pagination={3 < items.length ? true : false}
              enableAutoPlay={true}
            >
              {items.map(({ idPicFile, picFile }) => (
                <LocalHerosCardImage
                  src={picFile}
                  key={uuid4()}
                  alt={cardData.shopTitle}
                />
              ))}
            </Carousel>
          )}
          <LocalHerosDescription heroData={cardData} />
        </Stack>
      </div>
    </Grow>
  );
};

export default LocalHerosCard;
