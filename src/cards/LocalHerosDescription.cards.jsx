import { Card, CardMedia, Stack } from "@mui/material";
import { Typography } from "@mui/material";
import { styled } from "@mui/system";
// import { HerosDesCard, HerosDesMedia } from "./LocalHerosDes.cards.styles";

import Link from "../links/Link";

const HerosDesMedia = styled(CardMedia)(() => ({
  width: "40px",
  height: 40,
}));

const HerosDesCard = styled(Card)(({ theme }) => ({
  position: "absolute",
  right: 0,
  width: 428,
  top: "50%",
  transform: "translateY(-50%)",
  boxShadow: "0px 0px 20px #00000029",
  borderRadios: "4px",

  [theme.breakpoints.down("md")]: {
    boxShadow: "none",
    position: "unset",
    width: "100%",
    transform: "unset",
  },
}));

const LocalHerosDescription = ({ heroData }) => {
  const {
    eventText,
    faceBookUrl,
    instagramUrl,
    twitterUrl,
    mediaLink,
    mediaTitle,
    shopLogo,
    shopTitle,
  } = heroData;
  return (
    <HerosDesCard>
      <Stack direction="column" justifyContent="center" alignItems="center">
        <Stack direction="row" alignItems="center" spacing={1} my={3}>
          <HerosDesMedia component="img" image={shopLogo} alt={shopTitle} loading='lazy' />
          <Typography fontWeight="bold">{shopTitle}</Typography>
        </Stack>
        <Typography variant="subtitle1" fontSize={13} px={1.5}>
          {eventText}
        </Typography>
        <Stack
          alignSelf="start"
          p={2}
          direction="row"
          alignItems="center"
          spacing={1}
        >
          <HerosDesMedia
            component="img"
            image="/Images/Icon-youtube.png"
            alt={`youtube ${shopTitle}`}
            sx={{ width: 33, height: 23 }}
          />
          <Link
            href={mediaLink}
            target="_blank"
            fontSize={14}
            underline="none"
            color="text.secondary"
          >
            Youtube Link
          </Link>
        </Stack>
        <Stack direction="row" p={2} spacing={1}>
          <Link href={twitterUrl} target="_blank" aria-label="twitter">
            <CardMedia
              component="img"
              height="25"
              image="/Images/twitter.png"
              alt={`twitter ${shopTitle}`}
            />
          </Link>
          <Link href={instagramUrl} target="_blank" aria-label="instagram">
            <CardMedia
              component="img"
              height="25"
              image="/Images/instagram.png"
              alt={`instagram ${shopTitle}`}
            />
          </Link>
          <Link href={faceBookUrl} target="_blank" aria-label="facebook">
            <CardMedia
              component="img"
              height="25"
              image="/Images/facebook.png"
              alt={`facebook ${shopTitle}`}
            />
          </Link>
        </Stack>
      </Stack>
    </HerosDesCard>
  );
};

export default LocalHerosDescription;
