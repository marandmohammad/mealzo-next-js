import { useState, Fragment } from "react";
import { useTranslation } from "next-i18next";

import { Box, Button, Grid, Typography } from "@mui/material";

import StarIcon from "@mui/icons-material/Star";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import AccessTimeOutlinedIcon from "@mui/icons-material/AccessTimeOutlined";
// import BookmarkBorderOutlinedIcon from "@mui/icons-material/BookmarkBorderOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";

import ShopLogoContainer from "../containers/ShopLogo.containers";
import ShopInfoModal from "../modals/ShopInfo.modals";
import { calcPriceIconGapTime } from "../../utilities/appWideHelperFunctions";

function OrderStepsHeaderCard({ data, saleMethod, orderNumber = null }) {
  const { t } = useTranslation("common");
  const orderStepsTranslate = useTranslation("order_steps_page").t;
  const {
    shopTitle,
    homePageBanner,
    off,
    shopLogo,
    rate = { point: "4.6", count: "49" },
    shopSubjects,
    cargo,
    isOpen,
    currencyId,
    deliveryFee,
    deliveryTimeGap,
    mile,
    shopDescription,
    collectionTimeGap,
  } = data.address;

  //States
  const [open, setOpen] = useState(false);
  //function for handle open and close modal
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { deliveryIcon, price, gapTime, categories } = calcPriceIconGapTime(
    isOpen,
    currencyId,
    deliveryFee,
    deliveryTimeGap,
    shopSubjects,
    collectionTimeGap,
    saleMethod,
    t
  );

  return (
    <Fragment>
      <Grid
        container
        sx={{
          background: {
            md: `transparent url('${homePageBanner}') 0% 0% no-repeat padding-box`,
            xs: "none",
          },
          backgroundSize: { md: "contain", xs: "unset" },
          height: { md: 400, xs: "auto" },
          display: "flex",
          alignItems: "flex-end",
          position: "relative",
        }}
      >
        <Box
          sx={{
            bgcolor: "rgba(0,0,0,0.7)",
            width: "100%",
            height: "100%",
            display: { xs: "none", md: "flex" },
            flexDirection: "column",
            // justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            zIndex: 1,
            // gap: 2,
          }}
        >
          <CheckCircleRoundedIcon sx={{ color: "#01B807", mt: 10, mb: 2 }} />
          <Typography component="div" color="white" mb={1.2}>
            {orderStepsTranslate("order_success_message")}
          </Typography>
          <Typography variant="subtitle1" component="div" color="white">
            {orderStepsTranslate("your_order_number")} : #{orderNumber}
          </Typography>
        </Box>

        {/*  Responsive Background Image */}
        <Grid
          container
          item
          sx={{
            width: "100%",
            height: 180,
            display: { xs: "block", md: "none" },
            background: `transparent url('${homePageBanner}') 0% 0% no-repeat padding-box`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            borderBottomRightRadius: { xs: 15, md: 0 },
            borderBottomLeftRadius: { xs: 15, md: 0 },
            overflow: "hidden",
            position: "relative",
          }}
        >
          <Box
            sx={{
              bgcolor: "rgba(0,0,0,0.7)",
              width: "100%",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              zIndex: 1,
              position: "absolute",
            }}
          >
            <CheckCircleRoundedIcon
              sx={{ color: "#01B807", mt: 1.5, mb: 1.5 }}
            />
            <Typography component="div" color="white" mb={1.2}>
              {orderStepsTranslate("order_success_message")}
            </Typography>
            <Typography variant="subtitle1" component="div" color="white">
              {orderStepsTranslate("your_order_number")} : #{orderNumber}
            </Typography>
          </Box>
        </Grid>

        {/* Shop Description Badge */}
        {/* <Grid
          item
          sx={{
            width: { xs: "100%", sm: 471, md: 471 },
            background:
              "transparent url('/Images/Rectangleorenge.png') 0% 0% no-repeat padding-box",
            backgroundPosition: "center",
            position: "absolute",
            bottom: { xs: 230, sm: 230, md: "34%" },
            left: 0,
            font: {
              xs: "normal normal normal 12px/15px IBM Plex Sans",
              md: "normal normal 600 11px/14px IBM Plex Sans",
            },
            color: "white",
            px: { xs: 1, md: 2 },
            py: 1,
            display: "flex",
            alignItems: "center",
            // zIndex: 1,
          }}
        >
          <img
            src="/Images/bag.png"
            style={{ height: 18, width: 18, marginRight: 5 }}
          />
          Best Kebab House in Scotland in 2018 Best Kebab House in Scotland in
          2018
        </Grid> */}

        {/*  Responsive Card Bottom */}
        <Grid
          container
          item
          sx={{
            display: { xs: "flex", md: "none" },
            backgroundColor: "white",
            borderRadius: "4px",
            boxShadow: "0px 3px 6px #00000029",
            mb: { xs: 0, md: -1.2 },
            flexDirection: "row",
            flexWrap: "nowrap",
            p: 2.25,
          }}
        >
          {/* Detail Responsive */}
          <Grid item container flexDirection="column">
            {/* Card Title Responsive */}
            <Grid item container alignItems="center">
              <ShopLogoContainer
                width={{ xs: 40, sm: 60 }}
                sx={{
                  boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.16)",
                  mr: 2,
                }}
              >
                <img src={shopLogo} style={{ width: "100%", height: "100%" }} />
              </ShopLogoContainer>
              <Typography
                variant="h6"
                component="h1"
                fontSize={16}
                flexGrow={1}
              >
                {shopTitle}
              </Typography>
              {/* <BookmarkBorderOutlinedIcon sx={{ color: "#838485" }} /> */}
            </Grid>

            {/* Shop Data Responsive */}
            <Grid
              container
              item
              display="flex"
              flexDirection={{ xs: "column", sm: "row" }}
              flexWrap={{ xs: "unset", sm: "nowrap" }}
              alignItems={{ xs: "unset", sm: "center" }}
              my={2}
            >
              <Grid item container flexWrap="nowrap">
                <Typography
                  variant="subtitle1"
                  component="div"
                  fontSize={12}
                  display="flex"
                  alignItems="center"
                  noWrap
                  mr={{ xs: 2, sm: 4 }}
                >
                  <StarIcon sx={{ color: "#EEBF03", fontSize: 18 }} />
                  <Typography
                    variant="inherit"
                    component="span"
                    color="text.rating"
                    mx={0.8}
                  >
                    {rate.length === 13
                      ? rate.substring(1, -1)
                      : rate.substring(3, -1)}
                  </Typography>
                  {rate.length === 13 ? rate.substring(2) : rate.substring(4)}
                </Typography>

                <Typography
                  variant="subtitle1"
                  fontSize={12}
                  component="div"
                  sx={{ display: "flex", alignItems: "center" }}
                  mr={4}
                >
                  <Grid
                    container
                    display="flex"
                    flexDirection="row"
                    flexWrap="nowrap"
                    minWidth={160}
                    gap={2}
                  >
                    {categories &&
                      categories.map((cat, index) => (
                        <Grid item key={index}>
                          {cat}
                        </Grid>
                      ))}
                  </Grid>
                </Typography>
              </Grid>

              <Grid item mt={1} container>
                <Typography variant="subtitle1" fontSize={12} component="div">
                  <Grid container wrap="nowrap" gap={1.5}>
                    <Grid item>
                      <img
                        src={`/Images/${deliveryIcon}`}
                        style={{ height: 9 }}
                      />
                      &nbsp;{price}
                    </Grid>
                    {/* Distance */}
                    {(isOpen.toLowerCase() === "open" ||
                      isOpen.startsWith("opening")) && (
                      <Grid item display="flex" alignItems="center">
                        <LocationOnOutlinedIcon sx={{ fontSize: 13 }} />
                        &nbsp;{mile}
                      </Grid>
                    )}

                    {gapTime && (
                      <Grid item display="flex" alignItems="center">
                        <AccessTimeOutlinedIcon sx={{ fontSize: 13 }} />
                        &nbsp;{gapTime}
                      </Grid>
                    )}
                  </Grid>
                </Typography>
              </Grid>
            </Grid>

            {/*  Shop Description Responsive */}
            {/* <Grid container item mb={2} maxWidth={{ xs: 301, sm: "unset" }}>
              <Typography
                component="div"
                color="text.newMedium"
                fontSize={14}
                fontWeight={400}
              >
                {shopDescription}
              </Typography>
            </Grid> */}

            {/* Shop Info Responsive */}
            <Grid container item mb={2}>
              <Button
                sx={{ m: 0, p: 0, textAlign: "left", alignItems: "flex-start" }}
                onClick={handleOpen}
              >
                <Grid item>
                  <InfoOutlinedIcon
                    sx={{ fontSize: 18, mr: 1, color: "text.secondary" }}
                  />
                </Grid>
                <Grid container item flexDirection="column">
                  <Grid item>
                    <Typography
                      component="div"
                      color="text.newMedium"
                      fontSize={14}
                    >
                      {t("shop_info_responsive_btn_title")}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      fontSize={12}
                    >
                      {t("shop_info_responsive_btn_subtitle")}
                    </Typography>
                  </Grid>
                </Grid>
              </Button>
            </Grid>

            {/*    Time & Sale Method & Delivery Time*/}
            {/*<Grid container item flexWrap="nowrap">*/}
            {/*    <Grid container item flexWrap="nowrap" alignItems="center">*/}
            {/*        <Typography*/}
            {/*            component="div"*/}
            {/*            color="text.newMedium"*/}
            {/*            fontSize={14}*/}
            {/*            sx={{ display: "flex", alignItems: "center" }}*/}
            {/*        >*/}
            {/*            <AccessTimeOutlinedIcon sx={{ fontSize: 18 }} />*/}
            {/*            20-35 mins*/}
            {/*        </Typography>*/}
            {/*    </Grid>*/}
            {/*    <Grid container item>*/}
            {/*        x*/}
            {/*    </Grid>*/}
            {/*</Grid>*/}
          </Grid>
        </Grid>

        {/* Card Bottom */}
        <Grid
          item
          container
          sx={{
            display: { xs: "none", md: "flex" },
            backgroundColor: "white",
            borderRadius: "4px",
            boxShadow: "0px 3px 6px #00000029",
            mb: { xs: 0, md: -1.2 },
            flexDirection: "row",
            flexWrap: "nowrap",
            p: 2.25,
            zIndex: 2,
          }}
        >
          {/* Shop Logo */}
          <Grid item mr={4}>
            <ShopLogoContainer
              width={90}
              sx={{
                boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.16)",
              }}
            >
              <img src={shopLogo} style={{ width: "100%", height: "100%" }} />
            </ShopLogoContainer>
          </Grid>

          {/* Detail */}
          <Grid item container flexDirection="column">
            {/* Card Title */}
            <Grid item container>
              <Typography
                variant="h6"
                component="h1"
                fontSize={24}
                flexGrow={1}
              >
                {shopTitle}
              </Typography>
              {/* <BookmarkBorderOutlinedIcon sx={{ color: "#838485" }} /> */}
            </Grid>

            {/* Shop Data */}
            <Grid
              item
              display="flex"
              flexDirection="row"
              flexWrap="wrap"
              alignItems="center"
              my={0.6}
            >
              <Typography
                variant="subtitle1"
                component="div"
                fontSize={12}
                display="flex"
                alignItems="center"
                mr={4}
              >
                <StarIcon sx={{ color: "#EEBF03", fontSize: 18 }} />
                <Typography
                  variant="inherit"
                  component="span"
                  color="text.rating"
                  mx={0.8}
                >
                  {rate.length === 13
                    ? rate.substring(1, -1)
                    : rate.substring(3, -1)}
                </Typography>
                {rate.length === 13 ? rate.substring(2) : rate.substring(4)}
              </Typography>

              <Typography
                variant="subtitle1"
                fontSize={12}
                component="div"
                mr={4}
              >
                <Grid
                  container
                  display="flex"
                  justifyContent="space-around"
                  flexDirection="row"
                  flexWrap="nowrap"
                  minWidth={160}
                >
                  {categories &&
                    categories.map((cat, index) => (
                      <Grid item key={index}>
                        ·{cat}
                      </Grid>
                    ))}
                </Grid>
              </Typography>

              <Typography variant="subtitle1" fontSize={12} component="div">
                <Grid container wrap="nowrap" gap={1.5}>
                  <Grid item>
                    <img
                      src={`/Images/${deliveryIcon}`}
                      style={{ height: 9 }}
                    />
                    &nbsp;{price}
                  </Grid>
                  {(isOpen.toLowerCase() === "open" ||
                    isOpen.startsWith("opening")) && (
                    <Grid item display="flex" alignItems="center">
                      <LocationOnOutlinedIcon sx={{ fontSize: 13 }} />
                      &nbsp;{mile}
                    </Grid>
                  )}
                  {gapTime && (
                    <Grid item display="flex" alignItems="center">
                      <AccessTimeOutlinedIcon sx={{ fontSize: 13 }} />
                      &nbsp;{gapTime}
                    </Grid>
                  )}
                </Grid>
              </Typography>
            </Grid>

            {/* Info Icon */}
            <Grid container item>
              <Button
                sx={{
                  m: 0,
                  p: 0,
                  textAlign: "left",
                  alignItems: "center",
                  justifyContent: "flex-start",
                }}
                onClick={handleOpen}
              >
                <InfoOutlinedIcon
                  sx={{ fontSize: 18, mr: 1, color: "text.secondary" }}
                />
                <Typography variant="subtitle1" component="div" fontSize={12}>
                  {t("shop_info_btn")}
                </Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {/* Shop Info Modal */}
      <ShopInfoModal
        shopInformation={data}
        open={open}
        handleClose={handleClose}
      />
    </Fragment>
  );
}

export default OrderStepsHeaderCard;
