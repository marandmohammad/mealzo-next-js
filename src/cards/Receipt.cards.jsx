import Image from "next/image";

//? MUI
import { Box, Grid, Typography } from "@mui/material";

//? MUI Icons
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import EventIcon from "@mui/icons-material/Event";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";

import PrimaryButton from "../buttons/Primary.buttons";

//? SVG icons
// import { ReactComponent as CashIcon } from "../../assets/icons/cash-multiple.svg";
// import { ReactComponent as PdfIcon } from "../../assets/icons/file-pdf-box-outline.svg";
import CashIcon from "../../public/Images/cash-multiple.svg";
import PdfIcon from "../../public/Images/file-pdf-box-outline.svg";

const dummyFactor = [
  {
    id: 1,
    title: "Burger",
    qty: "2",
    price: "29.99",
  },
  {
    id: 2,
    title: "Classic Burrito",
    qty: "3",
    price: "29.99",
  },
  {
    id: 3,
    title: "Pizza",
    qty: "1",
    price: "29.99",
  },
];

const ReceiptCard = ({ order }) => {

  const {
    orderDetail,
    deliveryCharge,
    layoltiUsedAmount,
    totalDiscount,
    amountForPay,
    specialNote,
    surcharge,
    carrierBag,
    asap,
    carrierBagChargeLabel,
    creditCardSurchargeLabel,
    orderId,
  } = order;
  //   const { currencyId, shopId } = shopDetail.address;
  //   const currencyMark = currencyIdToMark(currencyId);

  return (
    <Box
      sx={{
        boxShadow: 1,
        padding: 3,
        margin: "-8px 0",
      }}
    >
      <Typography variant="h6" mb={4}>
        Receipt
      </Typography>
      <Box sx={{ display: "flex", flexDirection: "column", gap: 1, mb: 9 }}>
        <Typography variant="subtitle1" sx={{ fontSize: { xs: 12, md: 14 } }}>
          Gastronomica Market
        </Typography>
        <Typography variant="subtitle1" sx={{ fontSize: { xs: 12, md: 14 } }}>
          3 Nelson Mandela Pl,Glasgow G2 lqy,Uk,Gl2
        </Typography>
        <Typography
          variant="subtitle1"
          sx={{
            display: "flex",
            alignItems: "center",
            fontSize: { xs: 12, md: 14 },
            gap: 1,
          }}
        >
          <EventIcon sx={{ color: "text.newMedium", fontSize: 18 }} />
          24 may at 11:45
        </Typography>
      </Box>
      <Box sx={{ display: "flex", flexDirection: "column", gap: 3 }}>
        <Grid
          container
          sx={{
            borderBottom: "2px solid",
            borderColor: "text.secondary",
          }}
        >
          <Grid item xs={5}>
            <Typography
              variant="subtitle1"
              sx={{ color: "text.newMedium", fontWeight: "600" }}
            >
              Your Order
            </Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography
              variant="subtitle1"
              sx={{
                color: "text.newMedium",
                fontWeight: "600",
                textAlign: "center",
              }}
            >
              QNTY
            </Typography>
          </Grid>
          <Grid item xs={5}>
            <Typography
              variant="subtitle1"
              sx={{
                color: "text.newMedium",
                fontWeight: "600",
                textAlign: "right",
              }}
            >
              Price
            </Typography>
          </Grid>
        </Grid>
        {dummyFactor.map(({ id, title, qty, price }) => (
          <Grid container key={id}>
            <Grid item xs={5}>
              <Typography
                variant="subtitle1"
                sx={{
                  color: "text.newMedium",
                  fontWeight: "600",
                  fontSize: "14px",
                }}
              >
                {title}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="subtitle1"
                sx={{
                  color: "text.newMedium",
                  fontWeight: "600",
                  fontSize: "14px",
                  textAlign: "center",
                }}
              >
                {qty}
              </Typography>
            </Grid>
            <Grid item xs={5}>
              <Typography
                variant="subtitle1"
                sx={{
                  color: "text.newMedium",
                  fontWeight: "600",
                  fontSize: "14px",
                  textAlign: "right",
                }}
              >
                £ {price}
              </Typography>
            </Grid>
          </Grid>
        ))}
        <Box sx={{ display: "flex", gap: 2, alignItems: "center" }}>
          <Image src={CashIcon} style={{ marginLeft: "5px" }} />
          {/* <CashIcon style={{ marginLeft: "5px" }} /> */}
          <Typography
            variant="subtitle1"
            sx={{
              fontSize: { xs: 14, md: 16 },
              color: "text.newMedium",
              fontWeight: 600,
            }}
          >
            Total Price : £ 100.00
          </Typography>
        </Box>
        <Box sx={{ display: "flex", gap: 2, alignItems: "flex-start" }}>
          <LocationOnOutlinedIcon
            sx={{ color: "text.newMedium", fontSize: 25 }}
          />
          <Typography
            variant="subtitle1"
            sx={{
              fontSize: { xs: 14, md: 16 },
              color: "text.newMedium",
              fontWeight: 600,
            }}
          >
            Biling Address :
          </Typography>
          <Typography
            variant="subtitle1"
            sx={{
              fontSize: { xs: 12, md: 14 },
              lineHeight: 2,
              maxWidth: { xs: "40%", md: "auto" },
            }}
          >
            15 High St, Penicuik EH26 8HS, United Kingdom
          </Typography>
        </Box>
        <Box sx={{ display: "flex", gap: 2, alignItems: "center" }}>
          <LocalShippingOutlinedIcon sx={{ marginLeft: "5px", fontSize: 20 }} />
          <Typography
            variant="subtitle1"
            sx={{
              fontSize: { xs: 14, md: 16 },
              color: "text.newMedium",
              fontWeight: 600,
            }}
          >
            Delivery By:
          </Typography>
          <Typography
            variant="subtitle1"
            sx={{
              maxWidth: "70%",
              fontSize: { xs: 12, md: 14 },
              lineHeight: 2,
            }}
          >
            Arfan UI
          </Typography>
        </Box>
        <Box sx={{ display: "flex", gap: 4, justifyContent: "center" }}>
          <PrimaryButton
            sx={{
              display: "flex",
              justifyContent: "center",
              bgcolor: "background.default",
              border: "1px solid",
              borderColor: "divider",
              borderRadius: "4px",
              padding: "5px 12px",
              "&:hover": {
                bgColor: "background.default",
              },
            }}
          >
            <EmailOutlinedIcon sx={{ color: "text.secondary", fontSize: 24 }} />
            <Typography
              variant="subtitle1"
              sx={{ fontSize: { xs: 12, md: 16 }, ml: 2 }}
            >
              Resend Email
            </Typography>
          </PrimaryButton>
          <PrimaryButton
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              bgcolor: "background.default",
              border: "1px solid",
              borderColor: "divider",
              borderRadius: "4px",
              padding: "5px 12px",
              "&:hover": {
                bgColor: "background.default",
              },
            }}
          >
            <Image src={PdfIcon} />
            {/* <PdfIcon /> */}
            <Typography
              variant="subtitle1"
              sx={{ fontSize: { xs: 12, md: 16 }, ml: 2 }}
            >
              Resend Email
            </Typography>
          </PrimaryButton>
        </Box>
      </Box>
    </Box>
  );
};

export default ReceiptCard;
