import { styled } from "@mui/system";
import Card from "@mui/material/Card";

import GradeRoundedIcon from "@mui/icons-material/GradeRounded";

export const Item = styled(Card)(({ theme }) => ({
  textAlign: "left",
  height: "100%",
  boxShadow: "0 1px 4px #00000029",
  borderRadius: "5px",
}));

export const CustomGradeRoundedIcon = styled(GradeRoundedIcon)(({ theme }) => ({
  color: "#EEBF03",
  fontSize: "20px",
}));

export const RestaurantCardContainer = styled("div")(() => ({
  backgroundColor: "white",
  backgroundSize: "cover",
  position: "relative",
  height: 170,
  marginBottom: 10,
  overflow: "hidden",
}));

export const RestaurantCardImage = styled("img")(() => ({
  width: "100%",
  height: "100%",
  objectFit: "cover",
  // minHeight: "170px",
}));

export const Offer = styled("div")(() => ({
  position: "absolute",
  top: "14px",
  left: "17px",
  width: "50px",
  height: "61px",
  color: "#fff",
  textAlign: "center",
  fontSize: "14px",
  backgroundImage: `url('/Images/offer.svg')`,
  backgroundRepeat: "no-repeat",
  backgroundSize: "contain",
  paddingTop: "9px",
  fontWeight: "500",
}));

export const ShopCardLogo = styled("div")(({ theme }) => ({
  minHeight: "66px",
  minWidth: "66px",
  maxWidth: "66px",
  maxHeight: "66px",
  marginLeft: 0,
  position: "absolute",
  right: "20px",
  backgroundColor: "#fff",
  boxShadow: "0 1px 4px #00000029",
  border: "3px solid #FFF",
  borderRadius: "50%",
  top: "50%",
  transform: "translateY(-50%)",
  overflow: "hidden",
  [theme.breakpoints.up("lg")]: {
    top: "55%",
  },
  [theme.breakpoints.up("md")]: {
    top: "50%",
  },
  [theme.breakpoints.up("sm")]: {
    top: "59%",
  },
  [theme.breakpoints.up("xs")]: {
    top: "60%",
  },
}));

export const ShopCardLogoImage = styled('img')(() => ({
  width: "100%",
  height: "100%",
  objectFit: "cover",
  // verticalAlign: "middle",
  // borderRadius: "50%",
}));
