// import Image from "next/image";

import Grid from "@mui/material/Grid";
import { Button, Typography } from "@mui/material";

import Link from "../links/Link";

function LandingRecommendOptionsCard({
  title,
  subTitle,
  iconUrl,
  buttonText,
  href,
  sx,
  ...restOfProps
}) {
  return (
    <Grid
      container
      item
      sx={{
        flexDirection: "column",
        alignItems: "center",
        textAlign: "center",
        justifyContent: "space-between",
        maxHeight: 400,
      }}
      {...restOfProps}
    >
      <Grid item width={100} height={100}>
        {iconUrl ? (
          <img src={iconUrl} alt={`Mealzo ${title}`} style={{ height: "100%", width: "100%" }} /> //*width="100%" height="100%" loading="lazy" */}
        ) : null}
      </Grid>
      <Grid item>
        <Typography variant="h6" component="div" sx={{ my: 3 }}>
          {title}
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant="subtitle1" component="div" sx={{ mb: 2 }}>
          {subTitle}
        </Typography>
      </Grid>
      <Grid item>
        <Link href={href}>
          <Button
            variant="outlined"
            sx={{ borderRadius: "3px", fontWeight: "normal", zIndex: -1 }}
          >
            {buttonText}
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
}

export default LandingRecommendOptionsCard;
