// Mui Components
import { Box, Typography } from "@mui/material";

const ArchiveDataCard = ({ data: { dateYear, countBlog } }) => {
  return (
    <Box p={1}>
      <Typography variant="caption" fontWeight="bold" color="text.primary">
        {dateYear} ({countBlog})
      </Typography>
    </Box>
  );
};

export default ArchiveDataCard;
