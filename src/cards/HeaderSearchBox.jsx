import { useState } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import useLocation from "../../hooks/useLocation";
import useNotification from "../../hooks/useNotification";

// Mui Components
import { Box, Typography, TextField, Grid } from "@mui/material";

// Components
import HeaderTextSlider from "../carousels/HeaderTextSlider";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import ReactGoogleAutoCompleteInput from "../inputs/ReactGoogleAutoComplete.inputs";

import { queryParamEncode } from "../../utilities/areaPathHandlers";
import theme from "../themes/mealzoTheme";

const HeaderSearchBox = ({ handleClickAway, open, searchRef }) => {
  const { addLocationDetail } = useLocation();
  const { showNotification } = useNotification();
  const router = useRouter();
  const { t } = useTranslation("home_page");

  const [address, setAddress] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleSearchButtonClick = () => {
    if (!address) {
      showNotification(t('empty_postcode_warning'), "warning");
      return;
    }

    addLocationDetail(address);
    setLoading(true);
    router.push(
      `/area/${queryParamEncode(address.DeliveryPostCode)}?sm=delivery`
    );
  };

  const onPlaceSelect = (placeDetail) => {
    setAddress(placeDetail);
    addLocationDetail(placeDetail);
    setLoading(true);
    router.push(
      `/area/${queryParamEncode(placeDetail.DeliveryPostCode)}?sm=delivery`
    );
  };

  return (
    <Box margin="0 auto">
      <Box
        sx={{
          zIndex: `${open ? theme.zIndex.appBar + 2 : "unset"}`,
          bottom: { md: -30, sm: -80, xs: "0%" },
          maxWidth: { md: 790, xs: "100%" },
          width: { sm: "90%", xs: "95%" },
          backgroundColor: "common.white",
          borderRadius: ".3rem",
          boxShadow: "0 0 6px #00000029",
          left: "50%",
          transform: { sm: "translateX(-50%)", xs: "translate(-50%, -50%)" },
          py: "21px",
          px: { sm: 5, xs: 2 },
          // maxHeight: { md: 230, sm: 200 },
          position: "absolute",
        }}
      >
        <Grid
          container
          gap={1}
          sx={{
            display: "flex",
            flexDirection: "row",
            // flexWrap: "nowrap",
            height: { sm: 100, xs: "unset" },
            justifyContent: "center",
          }}
        >
          <Grid item sm="auto" xs={12}>
            <Typography
              variant="h3"
              color="primary.main"
              fontWeight="bold"
              fontSize={{ md: 50, sm: "6vw", xs: "8vw" }}
              whiteSpace="nowrap"
              textAlign="center"
            >
              {t('header_postcode_box_title')}
            </Typography>
          </Grid>
          <Grid item sm="auto" xs={12} height={70} overflow="hidden">
            <HeaderTextSlider fontSize={{ md: 50, sm: "6vw", xs: "8vw" }} />
          </Grid>
        </Grid>
        <Box>
          <Typography
            variant="body1"
            component="h6"
            sx={{ textAlign: "center" }}
            color="grey.700"
            mb={1}
          >
            {t('header_postcode_box_subtitle')}
          </Typography>
        </Box>
        <Grid
          container
          spacing={{ sm: 1, xs: 1 }}
          sx={{ padding: ".3rem", display: "flex", alignItems: "center" }}
        >
          <Grid item sm={10} xs={12} position="relative">
            <ReactGoogleAutoCompleteInput
              Input={({ ...restOfProps }) => (
                <TextField
                  inputRef={searchRef}
                  type="text"
                  id="Address"
                  fullWidth
                  placeholder={t('header_postcode_box_input_placeholder')}
                  autoComplete="off"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                  }}
                  {...restOfProps}
                />
              )}
              onPlaceSelect={onPlaceSelect}
            />
            {/* <div className="validation is-hide">
              <div style={{ width: "13px" }}>
                <ErrorOutlineOutlinedIcon color="error" fontSize="10px" />
              </div>
              &nbsp; Please enter a full postcode
            </div>
            <div id="results" className="pac-container"></div>
            <div id="res"></div> */}
          </Grid>
          <Grid item sm={2} xs={12}>
            <LoadingPrimaryButton
              variant="contained"
              id="search"
              loading={loading}
              disabled={loading}
              onClick={handleSearchButtonClick}
              fullWidth
              sx={{
                height: "60px",
                borderRadius: "8px",
              }}
            >
              {t('header_postcode_box_search_btn')}
            </LoadingPrimaryButton>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default HeaderSearchBox;
