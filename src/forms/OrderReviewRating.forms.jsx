import { Fragment, useState } from "react";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";

import { Typography, Grid } from "@mui/material";

import OverViewRatingInput from "../inputs/OverViewRating.inputs";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import RegularTextAreaInput from "../inputs/RegularTextArea.inputs";

import { orderReviewRatingFormikSetup } from "../../utilities/formikSetups";
import axiosConfig from "../../config/axios";

const OrderReviewRatingForm = ({
  rawQuestions,
  title,
  reviewIdentity,
  onSuccessCallBack,
}) => {
  const { t } = useTranslation('common');
  const { showNotification } = useNotification();
  const formik = orderReviewRatingFormikSetup(rawQuestions);

  const [loading, setLoading] = useState(false);

  const handleAddDeleteReview =
    (type = "post") =>
    async () => {
      setLoading(true);
      try {
        if (type === "post" && formik.isValid) {
          const requestData = {
            orderId: reviewIdentity,
            note: formik.values.note,
            question: rawQuestions.map(({ idQ }) => ({
              idQ,
              grade: formik.values[idQ],
            })),
          };
          const result = await (
            await axiosConfig.post(`/Review`, requestData)
          ).data;
          showNotification(result.message[0]);
          if(onSuccessCallBack) onSuccessCallBack();
        } else if (type === "delete") {
          const result = await (
            await axiosConfig.delete(`/Review/${reviewIdentity}`)
          ).data;
          showNotification(result.message[0]);
          if(onSuccessCallBack) onSuccessCallBack();
        }
      } catch (error) {
        showNotification(error.message, "error", 3000);
      }

      setLoading(false);
    };

  return (
    <Fragment>
      <Typography variant="h6" marginBottom={3}>
        {title}
      </Typography>
      <Grid container flexDirection="column" gap={3}>
        {rawQuestions.map(({ idQ, title, isComplate }) => (
          <OverViewRatingInput
            title={title}
            name={idQ.toString()}
            value={formik.values[idQ]}
            error={formik.errors[idQ]}
            onChange={formik.handleChange}
            readOnly={isComplate}
            key={idQ.toString()}
          />
        ))}
        <Grid container>
          <RegularTextAreaInput
            name="note"
            onChange={formik.handleChange}
            value={formik.values.note}
            maxLength={150}
            minRows={5}
            placeholder={t('review_form_input_placeholder')}
            disabled={formik.values.isComplete}
            style={{
              padding: 16,
            }}
          />
        </Grid>

        {!formik.values.isComplete ? (
          <LoadingPrimaryButton
            onClick={handleAddDeleteReview("post")}
            variant="contained"
            fullWidth
            disabled={!formik.isValid || loading}
            loading={loading}
          >
            {t('review_form_submit')}
          </LoadingPrimaryButton>
        ) : (
          <LoadingPrimaryButton
            color="error"
            onClick={handleAddDeleteReview("delete")}
            variant="contained"
            fullWidth
            disabled={!formik.isValid || loading}
            loading={loading}
          >
            {t('review_form_delete')}
          </LoadingPrimaryButton>
        )}
      </Grid>
    </Fragment>
  );
};

export default OrderReviewRatingForm;
