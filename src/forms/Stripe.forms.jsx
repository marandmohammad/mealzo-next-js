import { useEffect, useState } from "react";
import {
  PaymentElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import { useRouter } from "next/router";

import useNotification from "../../hooks/useNotification";

import { Box } from "@mui/material";

import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
// import { addLocaleToStripeCallbackUrl } from "../../utilities/appWideHelperFunctions";

export default function StripeForm({ clientSecret, handleClose }) {
  // const router = useRouter();
  const { showNotification } = useNotification();
  const stripe = useStripe();
  const elements = useElements();

  // const [message, setMessage] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (!stripe) {
      return;
    }

    if (!clientSecret) {
      return;
    }

    stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
      switch (paymentIntent?.status) {
        case "succeeded":
          showNotification("Payment succeeded!", "success");
          break;
        case "processing":
          showNotification("Your payment is processing.", "warning");
          break;
        case "requires_payment_method":
          // showNotification(
          //   "Your payment was not successful, please try again.",
          //   "error"
          // );
          break;
        default:
          showNotification("Something went wrong.", "error");
          break;
      }
    });

    elements._elements[0].on("ready", (event) => {
      setIsLoading(false);
    });
  }, [stripe]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    setIsLoading(true);

    // const return_url = addLocaleToStripeCallbackUrl(router);

    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        // Make sure to change this to your payment completion page
        // return_url,
        return_url: process.env.NEXT_PUBLIC_STRIPE_CALLBACK_URL,
      },
    });

    // This point will only be reached if there is an immediate error when
    // confirming the payment. Otherwise, your customer will be redirected to
    // your `return_url`. For some payment methods like iDEAL, your customer will
    // be redirected to an intermediate site first to authorize the payment, then
    // redirected to the `return_url`.
    if (error.type === "card_error" || error.type === "validation_error") {
      showNotification(`${error.message} Please try again !`, "error", 3000);
    } else {
      showNotification(
        "An unexpected error occurred. Please try again !",
        "error",
        3000
      );
    }

    setIsLoading(false);
  };

  return (
    <Box component="form" width="100%" onSubmit={handleSubmit}>
      <PaymentElement />
      <LoadingPrimaryButton
        variant="contained"
        disabled={isLoading || !stripe || !elements}
        loading={isLoading}
        fullWidth
        sx={{ mt: 3, borderRadius: "12px", height: "50px" }}
        type="submit"
      >
        Pay now
      </LoadingPrimaryButton>
      {/* Show any error or success messages */}
      {/* {message && <div id="payment-message">{message}</div>} */}
    </Box>
  );
}
