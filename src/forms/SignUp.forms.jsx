import { useEffect, useState } from "react";
import { useFormik } from "formik";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import { Box, Typography } from "@mui/material";

import SignUpInput from "../inputs/SignUp.inputs";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import Link from "../links/Link";
import PasswordInput from "../inputs/Password.inputs";

import axiosConfig from "../../config/axios";
import { appInfoForAuthentication } from "../../utilities/appWideHelperFunctions";
import { signUpFormikSetup } from "../../utilities/formikSetups";

export default function SignUpForm({ onRegistrationSuccess, sx }) {
  const { getOrderId } = useOrder();
  const { signIn } = useAuth();
  const { showNotification } = useNotification();
  const { t } = useTranslation("sign_up_form");
  const formik = signUpFormikSetup(
    t("validation_required_message"),
    t("not_valid_email_message"),
    t("phone_min_char_message"),
    t("not_valid_phone_message")
  );

  const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   setTimeout(() => {
  //     formik.validateForm();
  //   }, 50);
  // }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!formik.isValid) return;
    // const { AUTHENTICATION } = cookiesNameSpaces;
    setLoading(true);
    const { name, email, phone, password } = formik.values;
    const reqData = {
      fullName: name,
      email: email,
      pass: password,
      phoneNumber: phone,
      ...appInfoForAuthentication(),
    };

    try {
      const result = await axiosConfig.post("/Account/Signup", reqData);
      await signIn(result.data, getOrderId());
      showNotification(t("successful_sign_up_message"));
      onRegistrationSuccess &&
        setTimeout(() => {
          onRegistrationSuccess();
        }, 1500);
    } catch (error) {
      const errorMessage =
        typeof error.response !== "undefined"
          ? error.response.data.message[0]
          : error.message;
      showNotification(errorMessage, "error", 6000);
    }

    setLoading(false);
  };

  return (
    <Box
      onSubmit={handleSubmit}
      component="form"
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
        boxShadow: "0px 3px 20px #0000000D",
        gap: "36px",
        padding: "0 54px",
        margin: "auto",
        ...sx,
      }}
    >
      <Typography variant="h6" component="div" sx={{ pt: 3 }}>
        {t("sign_up_modal_title")}
      </Typography>
      <SignUpInput
        type="text"
        handleChange={formik.handleChange}
        value={formik.values.name}
        name="name"
        error={formik.errors.name}
      >
        {t("name_field_label")}
      </SignUpInput>
      <SignUpInput
        type="email"
        handleChange={formik.handleChange}
        value={formik.values.email}
        name="email"
        error={formik.errors.email}
      >
        {t("email_field_label")} *
      </SignUpInput>
      <SignUpInput
        type="text"
        handleChange={formik.handleChange}
        value={formik.values.phone}
        name="phone"
        error={formik.errors.phone}
      >
        {t("phone_field_label")}
      </SignUpInput>
      <PasswordInput
        handleChange={formik.handleChange}
        value={formik.values.password}
        name="password"
        error={formik.errors.password}
        inputProps={{
          autoComplete: "new-password",
        }}
      >
        {t("password_field_label")} *
      </PasswordInput>
      <LoadingPrimaryButton
        type="submit"
        variant="contained"
        fullWidth
        disabled={!formik.isValid}
        loading={loading}
      >
        {t("sign_up_form_submit_btn")}
      </LoadingPrimaryButton>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: "100%",
          mt: "-15px",
        }}
      >
        <Box>
          <Typography
            component="span"
            sx={{ fontSize: "14px", color: "text.newMedium" }}
          >
            {t("sign_up_terms_text_first")}{" "}
            <Link href="/terms-and-conditions">
              {t("sign_up_terms_tandcs")}
            </Link>
          </Typography>
        </Box>

        <Box>
          <Typography
            component="span"
            sx={{ fontSize: "14px", color: "text.newMedium" }}
          >
            {t("sign_up_terms_text_second")}{" "}
            <Link href="/privacy-policy">
              {t("sign_up_terms_privacy_policy")}
            </Link>
          </Typography>
        </Box>
      </Box>
      <Box sx={{ textAlign: "center", mb: 5 }}>
        <Typography
          component="span"
          sx={{
            fontSize: "14px",
            color: "text.newMedium",
          }}
        >
          {t("sign_up_last_text")}
        </Typography>
      </Box>
    </Box>
  );
}
