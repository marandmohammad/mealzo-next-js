import { useState } from "react";
import { usePlacesWidget } from "react-google-autocomplete";
import { useTranslation } from "next-i18next";
import { getCookie } from "cookies-next";

import useLocation from "../../hooks/useLocation";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import { Box, TextField } from "@mui/material";

import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";

import GrowContainer from "../containers/Grow.containers";
import PrimaryButton from "../buttons/Primary.buttons";

import { cookiesNameSpaces, pageNames } from "../../utilities/appWideHelperFunctions";
import { saleMethods } from "../../utilities/areaPathHandlers";

const ChangePostCodeForm = ({ saleMethod, handleClose, page }) => {
  // const { setPostCode } = useContext(MenuPageContext);
  const { t } = useTranslation("addresses");
  const { getOrderId } = useOrder();
  const { addLocationDetail } = useLocation();
  const [address, setAddress] = useState(null);
  const { showNotification } = useNotification();

  const handleSubmit = async () => {
    const { SHOP_ID } = cookiesNameSpaces;

    let shopId = null;
    if (page === pageNames.MENU_PAGE) {
      shopId = getCookie(SHOP_ID) ? getCookie(SHOP_ID) : null;
    } else if (page === pageNames.CHECKOUT_PAGE) {
      shopId = Number(router.query.shopId);
    } else if (page === pageNames.AREA_PAGE) {
      shopId = null;
    } else {
      shopId = getCookie(SHOP_ID) ? getCookie(SHOP_ID) : null;
    }

    const rawOrderId = getOrderId();
    const orderId = rawOrderId ? rawOrderId.orderId : 0;

    if (!address) return;
    const result = await addLocationDetail(
      address,
      saleMethods[saleMethod].code,
      shopId,
      orderId,
      page !== "menu"
    );

    if (result) handleClose(address);
  };

  const { ref } = usePlacesWidget({
    apiKey: process.env.NEXT_PUBLIC_GOOGLE_LOCATION_API_KEY,
    onPlaceSelected: (details) => {
      try {
        const { address_components, geometry, formatted_address, place_id } =
          details;
        let selectedPostCode = null;

        address_components.forEach(({ long_name, short_name, types }) => {
          if (types.includes("postal_code") && !selectedPostCode)
            selectedPostCode = short_name;
        });

        const locationDetail = {
          Address: formatted_address,
          // AddressDescription: desc,
          DeliveryPostCode: selectedPostCode.trim(),
          Lat: geometry.location.lat(),
          Lon: geometry.location.lng(),
          PlaceId: place_id,
        };

        setAddress(locationDetail);
      } catch (error) {
        showNotification(
          "Please select a location then press search button !",
          "error",
          3000
        );
      }
    },
    options: {
      types: ["(regions)"],
      componentRestrictions: { country: "gb" },
    },
  });
  ref.current?.setAttribute(
    "placeholder",
    t("not_login_postcode_input_placeholder")
  );

  return (
    <GrowContainer>
      <Box
        sx={{
          px: 3,
          width: "100%",
          display: "flex",
          flexDirection: "column",
          gap: 4,
          mb: 3,
        }}
      >
        <TextField
          InputProps={{
            endAdornment: (
              <LocationOnOutlinedIcon
                sx={{ color: "text.secondary", fontSize: 15 }}
              />
            ),
            notched: true,
          }}
          inputRef={ref}
          variant="outlined"
          size="small"
          fullWidth
          sx={{
            boxShadow: "0px 0px 24px #24242419",
            ".MuiOutlinedInput-root": {
              borderRadius: "0px",
              border: "none",
              outline: "none",
            },
            ".MuiInputLabel-root, .MuiOutlinedInput-input": { fontSize: 13 },
          }}
        />

        {/*<Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>*/}
        {/*  <KeyboardAltOutlinedIcon*/}
        {/*    sx={{ color: "text.secondary", fontSize: 25 }}*/}
        {/*  />*/}
        {/*  <Typography color="#1c1c1c" fontSize={14} fontWeight="medium">*/}
        {/*    Enter Address Manually*/}
        {/*  </Typography>*/}
        {/*</Box>*/}
      </Box>
      <Box
        sx={{
          width: "100%",
          px: 3,
          py: 1,
          boxShadow: "0px 0px 6px #00000029",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <PrimaryButton
          sx={{ fontSize: 16, fontWeight: 600, width: "100%", py: 0.5 }}
          disabled={!address}
          onClick={handleSubmit}
        >
          {t("not_login_confirm_btn")}
        </PrimaryButton>
      </Box>
    </GrowContainer>
  );
};

export default ChangePostCodeForm;
