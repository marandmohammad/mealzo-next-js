import { useEffect, useState, Fragment } from "react";
import { getCookie } from "cookies-next";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

import { useAuth } from "../../hooks/authentication";
import useLocation from "../../hooks/useLocation";
import useOrder from "../../hooks/orders";

import {
  Box,
  Button,
  Divider,
  Typography,
  Grid,
  IconButton,
} from "@mui/material";
import KeyboardAltOutlinedIcon from "@mui/icons-material/KeyboardAltOutlined";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

import AddNewAddressModal from "../modals/AddNewAddress.modals";
import GrowContainer from "../containers/Grow.containers";
import PrimaryButton from "../buttons/Primary.buttons";

import axiosConfig from "../../config/axios";
import {
  buildStructuredAddress,
  cookiesNameSpaces,
  pageNames,
} from "../../utilities/appWideHelperFunctions";
import { saleMethods } from "../../utilities/areaPathHandlers";

const UserAddressesForm = ({
  saleMethod,
  setBasket,
  onSuccessfulSubmit,
  page,
}) => {
  const router = useRouter();
  const { t } = useTranslation("addresses");
  const { getOrderId } = useOrder();
  const { getUser } = useAuth();
  const { addSelectedAddress } = useLocation();
  const { customerId } = getUser();

  const [userAddresses, setUserAddresses] = useState([]);
  const [address, setAddress] = useState(null);
  const [editData, setEditData] = useState(null);

  const [addAddressOpen, setAddAddressOpen] = useState(false);
  const handleAddressModal = () => setAddAddressOpen(!addAddressOpen);

  //! Handle Selected Address
  const addressHandler = (id) => {
    if (id === address) setAddress(null);
    else setAddress(id);
  };

  //! Get User Addresses
  useEffect(() => {
    axiosConfig
      .get(`/DeliveryAddresses/${customerId}`)
      .then(({ data }) => setUserAddresses(data));
  }, []);

  //! Add Or Edit Address Form Success Handler
  const handleSuccessfulClose = async (newAddresses) => {
    await setUserAddresses(newAddresses);
    await handleAddressModal();

    //? reset
    if (editData) setEditData(null);
  };

  //! Handle Submit
  const handleSubmit = async () => {
    if (!address) return;
    const selectedAddress = userAddresses.find((el) => el.id === address);
    if (!selectedAddress) return;

    const { SHOP_ID } = cookiesNameSpaces;

    let shopId = null;
    if (page === pageNames.MENU_PAGE) {
      shopId = getCookie(SHOP_ID) ? getCookie(SHOP_ID) : null;
    } else if (page === pageNames.CHECKOUT_PAGE) {
      shopId = Number(router.query.shopId);
    } else if (page === pageNames.AREA_PAGE) {
      shopId = null;
    } else {
      shopId = getCookie(SHOP_ID) ? getCookie(SHOP_ID) : null;
    }

    const rawOrderId = getOrderId();
    const orderId = rawOrderId ? rawOrderId.orderId : 0;

    const result = await addSelectedAddress(
      selectedAddress,
      saleMethods[saleMethod].code,
      shopId,
      orderId,
      setBasket,
      page !== "menu"
    );

    if (result) onSuccessfulSubmit(selectedAddress);
  };

  //! Click Add New Button
  const handleAddNewButtonClick = async () => {
    await setEditData(null);
    handleAddressModal();
  };

  //! Click Edit Button
  const handleEditButtonClick = async (itemForEdit) => {
    await setEditData(itemForEdit);
    await handleAddressModal();
  };

  //! Click Delete Button
  const handleDeleteButtonClick = async (itemForDelete) => {
    const { id } = itemForDelete;

    try {
      const result = await axiosConfig.delete(
        `/DeliveryAddresses/${customerId}/${id}`
      );
      await setUserAddresses(result.data.deliveryAddresses);
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <GrowContainer>
      <Box
        sx={{
          px: 3,
          width: "100%",
          display: "flex",
          flexDirection: "column",
          gap: 4,
          mb: 3,
        }}
      >
        {/*<TextField*/}
        {/*  InputProps={{*/}
        {/*    endAdornment: <LocationOnOutlinedIcon sx={{ color: "text.secondary", fontSize: 15 }} />,*/}
        {/*    notched: true,*/}
        {/*  }}*/}
        {/*  name="address"*/}
        {/*  value={address ? address.title : ""}*/}
        {/*  onChange={(e) => setAddress(e.target.value)}*/}
        {/*  placeholder="Card Number"*/}
        {/*  variant="outlined"*/}
        {/*  size="small"*/}
        {/*  fullWidth*/}
        {/*  sx={{*/}
        {/*      boxShadow: "0px 0px 24px #24242419",*/}
        {/*    ".MuiOutlinedInput-root": { borderRadius: "0px", border: "none", outline: "none" },*/}
        {/*    ".MuiInputLabel-root, .MuiOutlinedInput-input": { fontSize: 13 },*/}
        {/*  }}*/}
        {/*/>*/}
        <Button
          sx={{
            display: "flex",
            gap: 1,
            alignItems: "center",
            justifyContent: "flex-start",
          }}
          onClick={handleAddNewButtonClick}
        >
          <KeyboardAltOutlinedIcon
            sx={{ color: "text.secondary", fontSize: 25 }}
          />
          <Typography color="#1c1c1c" fontSize={14} fontWeight="medium">
            {t("add_new_address_btn")}
          </Typography>
        </Button>
        <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
          <Box sx={{ display: "flex", gap: 2 }}>
            <LocationOnIcon sx={{ color: "text.secondary", fontSize: 25 }} />
            <Typography sx={{ fontSize: 14, fontWeight: "medium" }}>
              {t("recent_locations")}
            </Typography>
          </Box>
          {userAddresses &&
            userAddresses.map((item, idx) => (
              <Fragment key={item.id}>
                <Grid container flexWrap="nowrap" alignItems="center">
                  <Grid item flexGrow={1}>
                    <Button
                      variant="text"
                      fullWidth
                      sx={{ justifyContent: "flex-start" }}
                      onClick={() => addressHandler(item.id)}
                    >
                      <Typography
                        sx={{
                          fontSize: 12,
                          color: `${
                            address === item.id
                              ? "primary.light"
                              : "text.medium"
                          }`,
                        }}
                      >
                        {item.deliveryAddressName
                          ? item.deliveryAddressName
                          : buildStructuredAddress(item)}
                      </Typography>
                    </Button>
                  </Grid>
                  <Grid item flexShrink={1}>
                    <IconButton onClick={() => handleEditButtonClick(item)}>
                      <EditOutlinedIcon
                        sx={{
                          color:
                            address === item.id
                              ? "primary.main"
                              : "text.secondary",
                          fontSize: 20,
                        }}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item flexShrink={1}>
                    <IconButton onClick={() => handleDeleteButtonClick(item)}>
                      <DeleteOutlinedIcon
                        sx={{
                          color:
                            address === item.id
                              ? "primary.main"
                              : "text.secondary",
                          fontSize: 20,
                        }}
                      />
                    </IconButton>
                  </Grid>
                </Grid>
                {idx < userAddresses.length - 1 && <Divider sx={{ my: -1 }} />}
              </Fragment>
            ))}
        </Box>
      </Box>
      <Box
        sx={{
          width: "100%",
          px: 3,
          py: 1,
          boxShadow: "0px 0px 6px #00000029",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <PrimaryButton
          sx={{ fontSize: 16, fontWeight: 600, width: "100%", py: 0.5 }}
          disabled={!address}
          onClick={handleSubmit}
        >
          {t("not_login_confirm_btn")}
        </PrimaryButton>

        <AddNewAddressModal
          open={addAddressOpen}
          handleClose={handleAddressModal}
          handleSuccessfulClose={handleSuccessfulClose}
          initialValues={editData}
        />
      </Box>
    </GrowContainer>
  );
};

export default UserAddressesForm;
