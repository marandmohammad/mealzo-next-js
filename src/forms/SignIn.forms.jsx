import { useState, useEffect } from "react";
// import { useFormik } from "formik";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import { Box, Button, Divider, Typography } from "@mui/material";

import SignUpInput from "../inputs/SignUp.inputs";
import PasswordInput from "../inputs/Password.inputs";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import GoogleOAuthSignInButton from "../buttons/GoogleOAuthSignIn.buttons";
import FacebookOAuthSignInButton from "../buttons/FacebookOAuthSignIn.buttons";
import AppleOAuthSignInButton from "../buttons/AppleOAuthSignIn.buttons";

// import AppleIcon from "@mui/icons-material/Apple";

import {
  cookiesNameSpaces,
  appInfoForAuthentication,
} from "../../utilities/appWideHelperFunctions";
import axiosConfig from "../../config/axios";
import { loginFormikSetup } from "../../utilities/formikSetups";

const SignInForm = ({
  signUpModalHandler,
  onLoginSuccess,
  handleCreateAccount,
  forgetPassHandler,
  sx,
}) => {
  const { t } = useTranslation("login_form");
  const { signIn } = useAuth();
  const { getOrderId } = useOrder();
  const { showNotification } = useNotification();

  const [loading, setLoading] = useState(false);

  const formik = loginFormikSetup(
    t("validation_required_message"),
    t("not_valid_email_message")
  );

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!formik.isValid) return;
    setLoading(true);

    try {
      const { email, password } = formik.values;
      const reqData = {
        username: email,
        password: password,
        ...appInfoForAuthentication(),
      };
      const data = await (
        await axiosConfig.post("/Account/Login", reqData)
      ).data;
      await signIn(data, getOrderId());
      showNotification(t("successful_login_message"), "success", 3000);
      onLoginSuccess &&
        setTimeout(() => {
          onLoginSuccess();
        }, 3000);
    } catch (error) {
      typeof error.response !== "undefined" && error.response.data
        ? showNotification(error.response.data.message[0], "error", 3000)
        : showNotification(error.message, "error", 3000);
    }

    setLoading(false);
  };

  const handleGoogleAuthSuccess = () => {
    showNotification(t("successful_login_message"), "success");
    setTimeout(() => {
      onLoginSuccess();
    }, 3000);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 3,
        alignItems: "center",
        width: "100%",
        overflowY: "auto",
        p: "0 10px",
        ...sx,
      }}
      component="form"
    >
      <Box
        sx={{
          width: "100%",
        }}
      >
        <FacebookOAuthSignInButton onLoginSuccess={onLoginSuccess} />

        <GoogleOAuthProvider
          clientId={`${process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}.apps.googleusercontent.com`}
        >
          <GoogleOAuthSignInButton onLoginSuccess={handleGoogleAuthSuccess} />
        </GoogleOAuthProvider>

        <AppleOAuthSignInButton />

        <Divider
          sx={{
            width: "100%",
            fontSize: 12,
            left: "50%",
          }}
        >
          OR
        </Divider>
      </Box>

      <SignUpInput
        type="email"
        handleChange={formik.handleChange}
        name="email"
        error={formik.errors.email}
        value={formik.values.email}
      >
        {t("email_field_label")}
      </SignUpInput>
      <PasswordInput
        // type="password"
        handleChange={formik.handleChange}
        name="password"
        error={formik.errors.password}
        value={formik.values.password}
      >
        {t("password_field_label")}
      </PasswordInput>
      {/* <Link
        sx={{
          textDecoration: "none",
          color: "#031CC1",
          mt: "-20px",
          cursor: "pointer",
          alignSelf: "flex-start",
        }}
      > */}
      <Button
        variant="text"
        onClick={forgetPassHandler}
        sx={{ color: "#031CC1", fontSize: 12, alignSelf: "start", p: 0 }}
      >
        {t("forget_password")}
      </Button>
      {/* </Link> */}
      <LoadingPrimaryButton
        variant="contained"
        fullWidth
        onClick={handleSubmit}
        type="submit"
        disabled={!formik.isValid}
        loading={loading}
      >
        {t("login_form_submit_btn")}
      </LoadingPrimaryButton>
      <Box sx={{ mb: 0 }}>
        <Typography component="span" sx={{ fontSize: "14px" }}>
          {t("new_to_mealzo")}{" "}
        </Typography>
        {/* <Link
          sx={{
            textDecoration: "none",
            color: "#031CC1",
            cursor: "pointer",
          }}
        > */}
        <Typography
          component="span"
          sx={{ fontSize: "14px", color: "#031CC1", cursor: "pointer" }}
          onClick={handleCreateAccount}
        >
          {t("login_create_account")}
        </Typography>
        {/* </Link> */}
      </Box>
      {/* <MainSnackBar
        open={error.open}
        handleClose={handleSnackBarClose}
        severity={error.severity}
        autoHideDuration={4000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      >
        {typeof error.message !== "string" ? error.message() : error.message}
      </MainSnackBar> */}
    </Box>
  );
};

export default SignInForm;
