import { useState, Fragment } from "react";

import { Grid, IconButton } from "@mui/material";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";

import GrowContainer from "../containers/Grow.containers";
import SignInForm from "./SignIn.forms";
import SignUpForm from "./SignUp.forms";
import ForgetPasswordModal from "../modals/ForgetPassword.modals";

const SignUpAndLoginForm = ({ md }) => {
  const [isSignUp, setIsSignUp] = useState(false);
  const [openForgetPass, setOpenForgetPass] = useState(false);

  const forgetPassHandler = () => setOpenForgetPass(!openForgetPass);
  return (
    <Grid container flexWrap="nowrap" justifyContent="center" gap={1}>
      <Grid item md={md ?? 6} xs={11} sm={5} position="relative">
        <GrowContainer
          isShown={isSignUp}
          style={{
            display: "flex",
            justifyContent: "flex-end",
            position: "absolute",
            transform: "translateX(-50%)",
            width: "max-content",
            top: 20,
            left: 20,
          }}
        >
          <IconButton onClick={() => setIsSignUp(false)}>
            <ArrowBackIosNewIcon />
          </IconButton>
        </GrowContainer>
        {isSignUp ? (
          <SignUpForm />
        ) : (
          <Fragment>
            <SignInForm
              sx={{
                px: 4,
                py: 5,
                boxShadow: "0px 3px 20px #0000000D",
              }}
              handleCreateAccount={() => setIsSignUp(true)}
              forgetPassHandler={forgetPassHandler}
            />
            <ForgetPasswordModal
              open={openForgetPass}
              handleClose={forgetPassHandler}
            />
          </Fragment>
        )}
      </Grid>
    </Grid>
  );
};

export default SignUpAndLoginForm;
