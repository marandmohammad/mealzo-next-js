import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "next-i18next";
import { useAuth } from "../../hooks/authentication";
import useNotification from "../../hooks/useNotification";

import { Box, TextField } from "@mui/material";

import GrowContainer from "../containers/Grow.containers";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import {
  buildStructuredAddress,
  cookiesNameSpaces,
} from "../../utilities/appWideHelperFunctions";
import { setCookies } from "cookies-next";
import { usePlacesWidget } from "react-google-autocomplete";
import { addNewAddressFormikSetup } from "../../utilities/formikSetups";
import HelperTextInput from "../inputs/HelperText.inputs";
import axiosConfig from "../../config/axios";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";

const inputStyle = {
  boxShadow: "0px 0px 24px #24242419",
  ".MuiOutlinedInput-root": {
    borderRadius: "0px",
    border: "none",
    outline: "none",
  },
  ".MuiInputLabel-root, .MuiOutlinedInput-input": { fontSize: 13 },
};

const AddNewAddressForm = ({ onSuccessfulSubmit, initialValues = null }) => {
  const { t } = useTranslation('addresses')
  const { getUser } = useAuth();
  const { showNotification } = useNotification();

  const [address, setAddress] = useState(null);
  const [loading, setLoading] = useState(false);

  const formik = useCallback(
    () => addNewAddressFormikSetup(initialValues),
    [initialValues]
  )();

  //! Set Value To Address On Edit Mode
  useEffect(() => {
    if (initialValues)
      setAddress({
        Address: buildStructuredAddress(initialValues),
        DeliveryPostCode: initialValues.deliveryPostcode,
        Lat: initialValues.deliveryLat,
        Lon: initialValues.deliveryLong,
        PlaceId: null,
      });
  }, [initialValues]);

  //! Initial Validation
  useEffect(() => {
    setTimeout(() => {
      formik.validateForm();
    }, 10);
  }, [formik.values]);

  //! Submit Handler
  const handleSubmit = async () => {
    if (!address || !formik.isValid) return;
    setLoading(true);

    const { customerId, accessToken } = getUser();
    const { LOCATION_DETAIL, SELECTED_ADDRESS } = cookiesNameSpaces;
    setCookies(LOCATION_DETAIL, JSON.stringify(address));
    const {
      deliveryAddress1,
      deliveryAddress2,
      deliveryAddress3,
      deliveryCity,
      deliveryPostcode,
      deliveryAddressName,
      deliveryLat,
      deliveryLong,
    } = formik.values;

    const reqData = {
      deliveryAddress1,
      deliveryAddress2:
        deliveryAddress2 && deliveryAddress2.trim() !== ""
          ? deliveryAddress2
          : null,
      deliveryAddress3:
        deliveryAddress3 && deliveryAddress3.trim() !== ""
          ? deliveryAddress3
          : null,
      deliveryCity,
      deliveryPostcode,
      deliveryAddressName:
        deliveryAddressName && deliveryAddressName.trim() !== ""
          ? deliveryAddressName
          : null,
      deliveryLat,
      deliveryLong,
    };

    //? Add id for edit data
    if (initialValues) reqData.id = initialValues.id;

    try {
      const result = await axiosConfig[`${initialValues ? "put" : "post"}`](
        `/DeliveryAddresses/${customerId}`,
        reqData
      );

      await onSuccessfulSubmit(result.data.deliveryAddresses);
    } catch (e) {
      if (typeof e.response.data !== "undefined")
        showNotification(e.response.data.message[0], "error", 3000);
      else showNotification(e.message, "error", 3000);
    }
    setLoading(false);
  };

  //! Google Places Hook
  const { ref } = usePlacesWidget({
    apiKey: process.env.NEXT_PUBLIC_GOOGLE_LOCATION_API_KEY,
    onPlaceSelected: ({
      address_components,
      geometry,
      formatted_address,
      place_id,
    }) => {
      let selectedPostCode = null;

      address_components.forEach(({ long_name, short_name, types }) => {
        if (types.includes("postal_code") && !selectedPostCode)
          selectedPostCode = short_name;
      });

      const locationDetail = {
        Address: formatted_address,
        // AddressDescription: desc,
        DeliveryPostCode: selectedPostCode.trim(),
        Lat: geometry.location.lat(),
        Lon: geometry.location.lng(),
        PlaceId: place_id,
      };

      formik.setFieldValue("deliveryPostcode", selectedPostCode.trim());
      formik.setFieldValue("deliveryLat", geometry.location.lat());
      formik.setFieldValue("deliveryLong", geometry.location.lng());

      setAddress(locationDetail);
    },
    options: {
      types: ["(regions)"],
      componentRestrictions: { country: "gb" },
    },
  });
  // ref.current?.setAttribute("placeholder", t('not_login_postcode_input_placeholder'))

  return (
    <GrowContainer>
      <Box
        sx={{
          px: 3,
          width: "100%",
          display: "flex",
          flexDirection: "column",
          gap: 3,
          mb: 3,
          pt: 1,
        }}
      >
        <TextField
          variant="outlined"
          fullWidth
          label={t('address_name_input_label')}
          size="small"
          sx={inputStyle}
          name="deliveryAddressName"
          value={formik.values.deliveryAddressName}
          onChange={formik.handleChange}
        />

        <TextField
          variant="outlined"
          fullWidth
          label={t('address_1_input_label')}
          required
          size="small"
          sx={inputStyle}
          name="deliveryAddress1"
          value={formik.values.deliveryAddress1}
          onChange={formik.handleChange}
          helperText={
            typeof formik.errors.deliveryAddress1 !== "undefined" ? (
              <HelperTextInput errorText={formik.errors.deliveryAddress1} />
            ) : (
              ""
            )
          }
        />

        <TextField
          variant="outlined"
          fullWidth
          label={t('address_2_input_label')}
          size="small"
          sx={inputStyle}
          name="deliveryAddress2"
          value={formik.values.deliveryAddress2}
          onChange={formik.handleChange}
        />

        {/* <TextField
          variant="outlined"
          fullWidth
          label="Address 3"
          size="small"
          sx={inputStyle}
          name="deliveryAddress3"
          value={formik.values.deliveryAddress3}
          onChange={formik.handleChange}
        /> */}

        {/* <TextField
          variant="outlined"
          fullWidth
          label="City"
          required
          size="small"
          sx={inputStyle}
          name="deliveryCity"
          value={formik.values.deliveryCity}
          onChange={formik.handleChange}
          helperText={
            typeof formik.errors.deliveryCity !== "undefined" ? (
              <HelperTextInput errorText={formik.errors.deliveryCity} />
            ) : (
              ""
            )
          }
        /> */}

        <TextField
          InputProps={{
            endAdornment: (
              <LocationOnOutlinedIcon
                sx={{ color: "text.secondary", fontSize: 15 }}
              />
            ),
            notched: true,
          }}
          inputRef={ref}
          variant="outlined"
          size="small"
          fullWidth
          sx={inputStyle}
          placeholder={`${t('post_code_input_label')} *`}
          required
          helperText={
            typeof formik.errors.deliveryPostcode !== "undefined" ? (
              <HelperTextInput errorText={formik.errors.deliveryPostcode} />
            ) : (
              `${t('chosen_post_code')}: ${formik.values.deliveryPostcode}`
            )
          }
        />

        {/*<Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>*/}
        {/*  <KeyboardAltOutlinedIcon*/}
        {/*    sx={{ color: "text.secondary", fontSize: 25 }}*/}
        {/*  />*/}
        {/*  <Typography color="#1c1c1c" fontSize={14} fontWeight="medium">*/}
        {/*    Enter Address Manually*/}
        {/*  </Typography>*/}
        {/*</Box>*/}
      </Box>
      <Box
        sx={{
          width: "100%",
          px: 3,
          py: 1,
          boxShadow: "0px 0px 6px #00000029",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <LoadingPrimaryButton
          // sx={{ fontSize: 16, fontWeight: 600, width: "100%", py: 0.5 }}
          variant="contained"
          fullWidth
          disabled={!formik.isValid || loading}
          loading={loading}
          onClick={handleSubmit}
        >
          {t('not_login_confirm_btn')}
        </LoadingPrimaryButton>
      </Box>
    </GrowContainer>
  );
};

export default AddNewAddressForm;
