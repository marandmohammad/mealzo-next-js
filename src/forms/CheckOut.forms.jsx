import { useEffect, useState, useMemo } from "react";
import { useTranslation } from "next-i18next";
// import Image from "next/image";

import useLocation from "../../hooks/useLocation";
import { useAuth } from "../../hooks/authentication";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import {
  Box,
  Checkbox,
  FormControlLabel,
  Grid,
  Typography,
  NoSsr,
  TextareaAutosize,
  // Button,
  Skeleton,
  // IconButton,
} from "@mui/material";
// import DoneAllIcon from "@mui/icons-material/DoneAll";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";

import SignUpInput from "../inputs/SignUp.inputs";
import CheckOutInput from "../inputs/CheckOut.inputs";
import CouponModal from "../modals/Coupon.modals";
import AddressChangeModal from "../modals/AddressChange.modals";
import CheckoutAddressCouponsButton from "../buttons/CheckoutAddressCoupons.buttons";

import { saleMethods } from "../../utilities/areaPathHandlers";
import { checkoutFormikSetup } from "../../utilities/formikSetups";
import {
  // cookiesNameSpaces,
  getDeliveryTime,
  buildStructuredAddress,
  pageNames,
} from "../../utilities/appWideHelperFunctions";
import axiosConfig from "../../config/axios";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import PayByStripeModal from "../modals/PayByStripe.modals";
import Link from "../links/Link";

const CheckOutForm = ({
  shopDetail,
  saleMethod,
  setBasket,
  isHaveCouponCode,
}) => {
  const { t } = useTranslation("checkout_page");
  const { showNotification } = useNotification();
  const { getUser } = useAuth();
  const { phoneNumber, isConfirmSms } = getUser();
  const { getSelectedAddress, addSelectedAddress } = useLocation();
  const initialDeliveryTime = useMemo(() => getDeliveryTime(), [saleMethod]);
  const initialSelectedAddress = useMemo(() => getSelectedAddress(), []);

  const { getOrderId } = useOrder();
  const rawOrderDetail = getOrderId();
  const orderId = rawOrderDetail ? rawOrderDetail.orderId : null;

  const { shopId } = shopDetail.address;
  const saleMethodCode = saleMethods[saleMethod].code;

  const [isAddressProcessing, setIsAddressProcessing] = useState(true);

  //! Setup formik
  const formik = checkoutFormikSetup(
    saleMethod,
    phoneNumber,
    isConfirmSms,
    null,
    initialDeliveryTime,
    isHaveCouponCode
  );

  //! Validate Address
  useEffect(() => {
    const checkAddress = async () => {
      if (!saleMethod === "delivery" || !initialSelectedAddress) {
        setIsAddressProcessing(false);
        return;
      }

      const isCorrectAddress = await addSelectedAddress(
        initialSelectedAddress,
        saleMethods[saleMethod].code,
        shopId,
        orderId,
        setBasket
      );
      if (isCorrectAddress)
        formik.setFieldValue("deliveryAddress", initialSelectedAddress);
      setIsAddressProcessing(false);
    };

    checkAddress();
  }, []);

  //! States
  const [openCode, setOpenCode] = useState(false);
  const [deliveryTimeOptions, setDeliveryTimeOptions] = useState([]);
  const [loading, setLoading] = useState(true);
  const [formLoading, setFormLoading] = useState(false);

  //! Manually Validate Form
  useEffect(() => {
    formik.validateForm();
  }, [loading]);

  //? Stripe Modal
  const [stripeModal, setStripeModal] = useState(false);
  const handleStripeModal = () => setStripeModal(!stripeModal);

  const codeModalHandle = () => {
    setOpenCode(!openCode);
  };

  const handleCouponCodeDelete = async () => {
    try {
      const newBasket = await (
        await axiosConfig.patch(
          `/Basket/DeleteVoucherCode/${shopId}/${orderId}`
        )
      ).data;
      setBasket(newBasket);
      formik.setFieldValue("offCode", false);
      showNotification("Removed successfully!");
    } catch (error) {
      showNotification(error.message, "error", 3000);
    }
  };

  //form submitting
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!formik.isValid) {
      const errorKeys = Object.keys(formik.errors);
      showNotification(formik.errors[errorKeys[0]], "error", 3000);
      return;
    }

    await setFormLoading(true);
    try {
      // const { accessToken } = getUser();
      const { phone, specialRequest, dTime, receive } = formik.values;
      const wantedTime = deliveryTimeOptions.find(
        (el) => el.id === parseInt(dTime)
      ).deliveryTime;
      let requestData = {
        specialNote: specialRequest,
        smsConfirm: receive,
        wantedTime: wantedTime,
        phoneNumber: phone,
      };

      //? Check sale method for instruction for rider
      if (saleMethod === "delivery")
        requestData.riderInstruction = formik.values.instructionForRider;
      else requestData.riderInstruction = null;

      const result = await axiosConfig.patch(
        `/Basket/UpdateOrder/${orderId}`,
        requestData
      );

      handleStripeModal();
    } catch (error) {
      showNotification(error.message, "error");
    }
    await setFormLoading(false);
  };

  const [openAddressChange, setOpenAddressChange] = useState(false);
  const addressChangeModalHandler = () => {
    setOpenAddressChange(!openAddressChange);
  };

  useEffect(() => {
    const getDeliveryTimeOptions = async () => {
      await setLoading(true);
      try {
        const deliveryTimeOptionsData = await (
          await axiosConfig.get(`/WantedTimes/${shopId}/${saleMethodCode}`)
        ).data;
        await setDeliveryTimeOptions(deliveryTimeOptionsData);
      } catch (e) {
        console.error(e.message);
      }
      await setLoading(false);
    };

    getDeliveryTimeOptions();
  }, [saleMethod, shopId]);

  return (
    <Box
      sx={{
        bgcolor: "white",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        position: "relative",
      }}
      component="form"
      onSubmit={handleSubmit}
    >
      {/* Delivery Address */}
      {saleMethod === "delivery" && (
        <Grid container px={3} py={2}>
          <Box
            sx={{
              display: "flex",
              gap: { xs: 0, md: 3 },
              alignItems: { xs: "flex-start", md: "center" },
              flexDirection: { xs: "column", md: "row" },
              mb: { xs: 3, md: 0 },
            }}
          >
            <Typography
              variant="h6"
              component="div"
              sx={{ fontSize: 24, my: 3 }}
            >
              {t("delivery_section_label")}
            </Typography>
          </Box>

          {/* Address Change Modals */}
          <AddressChangeModal
            openAddressChange={openAddressChange}
            addressChangeModalHandler={addressChangeModalHandler}
            saleMethod={saleMethod}
            setBasket={setBasket}
            page={pageNames.CHECKOUT_PAGE}
            onSuccessfulSubmit={(newAddress) => {
              formik.setFieldValue("deliveryAddress", newAddress);
              addressChangeModalHandler();
            }}
          />

          {/* delivery address inputs */}
          <Grid container gap={4}>
            <NoSsr>
              {formik.values.deliveryAddress ? (
                <CheckoutAddressCouponsButton
                  onClick={addressChangeModalHandler}
                  color="secondary"
                  loading={isAddressProcessing}
                >
                  {buildStructuredAddress(formik.values.deliveryAddress)}
                </CheckoutAddressCouponsButton>
              ) : (
                <CheckoutAddressCouponsButton
                  onClick={addressChangeModalHandler}
                  loading={isAddressProcessing}
                >
                  {t("add_address_btn")} +
                </CheckoutAddressCouponsButton>
              )}
            </NoSsr>
          </Grid>
        </Grid>
      )}

      {/* Fulfillment details */}
      <Grid container px={3} py={2}>
        <Grid container display="inline-flex" alignItems="center" gap={3}>
          <Typography variant="h6" component="div" sx={{ fontSize: 24, my: 3 }}>
            {t("fulfillment_section_label")}
          </Typography>
        </Grid>
        <Grid
          container
          flexDirection="column"
          p={3}
          bgcolor="white"
          boxShadow="0px 0px 15px rgba(0, 0, 0, 0.1)"
          borderRadius="8px"
        >
          <Grid container justifyContent="space-between" mb={3}>
            <Grid
              item
              xs={12}
              sm={5.8}
              display="flex"
              alignItems="center"
              height={48}
              mb={{ xs: 3, md: 0 }}
            >
              <SignUpInput
                type="text"
                name="phone"
                value={formik.values.phone}
                handleChange={formik.handleChange}
                error={formik.errors.phone}
                placeholder={t("phone_input_placeholder")}
                sx={{ height: "100%" }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              sm={5.8}
              display="flex"
              alignItems="center"
              height={48}
            >
              {loading ? (
                <Skeleton
                  variant="rectangular"
                  animation="wave"
                  width="100%"
                  sx={{ height: "100%" }}
                />
              ) : (
                <CheckOutInput
                  name="dTime"
                  value={formik.values.dTime}
                  options={deliveryTimeOptions}
                  handleChange={formik.handleChange}
                  error={formik.errors.dTime}
                  placeholder={saleMethods[saleMethod].wantedTimeLabel}
                  sx={{ height: "100%" }}
                />
              )}
            </Grid>
          </Grid>

          <Grid container justifyContent="space-between">
            {saleMethod === "delivery" && (
              <Grid
                item
                xs={12}
                sm={5.8}
                display="flex"
                alignItems="center"
                mb={{ xs: 2, md: 0 }}
              >
                <TextareaAutosize
                  name="instructionForRider"
                  onChange={formik.handleChange}
                  maxLength={150}
                  style={{
                    backgroundColor: "white",
                    minWidth: "100%",
                    outline: "none",
                    border: "1px solid #ced4da",
                    padding: "15px 11px",
                    fontSize: 13,
                    minHeight: 100,
                    maxHeight: 100,
                    borderRadius: "4px",
                    fontFamily: "inherit",
                  }}
                  minRows={5}
                  placeholder={t("instruction_rider_input_placeholder")}
                  value={formik.values.instructionForRider}
                />
              </Grid>
            )}

            <Grid
              item
              xs={12}
              sm={saleMethod === "delivery" ? 5.8 : 12}
              display="flex"
              alignItems="center"
              mb={{ xs: 2, md: 0 }}
            >
              <TextareaAutosize
                name="specialRequest"
                onChange={formik.handleChange}
                maxLength={150}
                style={{
                  backgroundColor: "white",
                  minWidth: "100%",
                  outline: "none",
                  border: "1px solid #ced4da",
                  padding: "15px 11px",
                  fontSize: 13,
                  minHeight: 100,
                  maxHeight: 100,
                  borderRadius: "4px",
                  fontFamily: "inherit",
                }}
                minRows={5}
                placeholder={t("special_request_input_placeholder")}
                value={formik.values.specialRequest}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {/* Coupon Section */}
      <Grid container px={3} py={2}>
        <Box
          sx={{
            display: "flex",
            gap: { xs: 0, md: 3 },
            alignItems: { xs: "flex-start", md: "center" },
            flexDirection: { xs: "column", md: "row" },
            mb: { xs: 3, md: 0 },
          }}
        >
          <Typography variant="h6" component="div" sx={{ fontSize: 24, my: 3 }}>
            {t("Voucher_section_label")}
          </Typography>
        </Box>

        {/* delivery address inputs */}
        <Grid container gap={4}>
          <NoSsr>
            {!formik.values.offCode ? (
              <CheckoutAddressCouponsButton
                disabled={formik.values.offCode}
                onClick={codeModalHandle}
              >
                {t("add_voucher_btn")} +
              </CheckoutAddressCouponsButton>
            ) : (
              <CheckoutAddressCouponsButton
                color="error"
                startIcon={<DeleteOutlineOutlinedIcon />}
                disabled={!formik.values.offCode}
                onClick={handleCouponCodeDelete}
              >
                {t("remove_voucher_btn")}
              </CheckoutAddressCouponsButton>
            )}
          </NoSsr>
        </Grid>
      </Grid>

      {/* Coupon Modal */}
      <CouponModal
        open={openCode}
        // codeModalHandle={codeModalHandle}
        formik={formik}
        handleClose={codeModalHandle}
        onSuccessfulSubmit={(newBasket) => {
          setBasket(newBasket);
          formik.setFieldValue("offCode", true);
        }}
        shopId={shopId}
        orderId={orderId}
      />

      {/* Description */}
      <Grid container px={3} py={2}>
        <Typography
          variant="subtitle1"
          component="div"
          sx={{ fontSize: 14, color: "#878787" }}
        >
          {t("terms_text_1")}{" "}
          <Link
            href="/terms-and-conditions"
            target="_blank"
            sx={{ whiteSpace: "nowrap" }}
          >
            {t("terms_text_2")}
          </Link>{" "}
          {t("terms_text_3")}
        </Typography>
      </Grid>

      {/* Subscription */}
      <Grid container px={3} py={2}>
        <FormControlLabel
          control={
            <Checkbox
              size="small"
              // value={formik.values.receive}
              checked={formik.values.receive}
              onChange={formik.handleChange}
              name="receive"
            />
          }
          label={
            <Typography
              variant="subtitle1"
              component="div"
              sx={{ fontSize: 14, color: "#878787" }}
            >
              {t("sms_checkbox_label")}
            </Typography>
          }
        />
      </Grid>

      {/* Stripe Modal */}
      <PayByStripeModal
        open={stripeModal}
        handleClose={handleStripeModal}
        shopId={shopId}
        orderId={orderId}
      />

      {/* Submit Button */}
      <Grid container px={3} py={2}>
        <Grid item xs={12}>
          <LoadingPrimaryButton
            type="submit"
            fullWidth
            variant="contained"
            disabled={!formik.isValid || formLoading}
            loading={formLoading}
            sx={{ fontSize: 21, whiteSpace: "nowrap", borderRadius: "8px" }} //p: "10px 50px",
          >
            {t("checkout_form_submit_btn")}
          </LoadingPrimaryButton>
        </Grid>
      </Grid>
    </Box>
  );
};

export default CheckOutForm;
