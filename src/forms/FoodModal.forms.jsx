import { useCallback, useContext, useMemo, useState } from "react";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

import useOrder from "../../hooks/orders";
import useLocation from "../../hooks/useLocation";
import useNotification from "../../hooks/useNotification";
import { useAuth } from "../../hooks/authentication";

import { Box, Grid, Typography } from "@mui/material";

import FoodModalCrustAccordion from "../accordions/FoodModalCrust.accordions";
import FoodModalInputAccordion from "../accordions/FoodModalInput.accordions";
import FoodModalOneSelectAccordion from "../accordions/FoodModalOneSelect.accordions";
import RoundedPlusMinusButton from "../buttons/RoundedPlusMinus.buttons";
import LoadingPrimaryButton from "../buttons/LoadingPrimary.buttons";
import GrowContainer from "../containers/Grow.containers";
import NumberCountCarousel from "../carousels/NumberCount.carousels";

import { MenuPageContext } from "../../context/MenuPageContext";
import currencyIdToMark from "../../utilities/currencyIdToMark";
import {
  calcTotalPrice,
  setupInitialFormikConfig,
} from "../../utilities/menuPageHelperFunctions";
import { saleMethods } from "../../utilities/areaPathHandlers";
import axiosConfig from "../../config/axios";

const FoodModalForm = ({ item, subMenu, handleClose }) => {
  const { t } = useTranslation("food_modal");
  const router = useRouter();
  const { mealzoIdentity } = router.query;
  const { getOrderId, addOrderId, changeOrderId } = useOrder();
  const { postCode } = useLocation();
  const { showNotification, showDialog, hideDialog } = useNotification();
  const { getUser } = useAuth();

  const [loading, setLoading] = useState(false);
  const { shopDetail, saleMethod, updateBasket } = useContext(MenuPageContext);
  // const currencyMark = currencyIdToMark(shopDetail.address.currencyId);

  // const [crust, oneSelection, restOfSubMenu] = [[], [], []];

  // subMenu.forEach((item) => {
  //   if (item.isCrust > 0) crust.push(item);
  //   else if (item.isCrust === 0 && item.minSelect === 1 && item.maxSelect === 1)
  //     oneSelection.push(item);
  //   else restOfSubMenu.push(item);
  // });

  const setupFormik = useCallback(() => {
    return setupInitialFormikConfig(subMenu);
  }, [subMenu]);

  //Formik
  const formik = setupFormik();

  // function for handle increase and decrease button
  const clickHandler = (type) => {
    const prevValue = formik.values["counter"];
    if (type === "plus") {
      formik.setFieldValue("counter", prevValue + 1);
    } else if (prevValue > 1) {
      formik.setFieldValue("counter", prevValue - 1);
    }
  };

  //! Total Value
  let totalValue = useMemo(
    () => calcTotalPrice(subMenu, item, formik),
    [formik.values, subMenu, item]
  );

  const handleSubmit = async () => {
    if (!formik.isValid) return;

    const user = getUser();
    const formData = formik.values;
    const { product, optionId } = item;
    const { shopId } = shopDetail.address;
    const crustId = formData["crust"] || 0;
    const count = formData["counter"] || 0;
    const rawOrderId = getOrderId() || 0;
    let orderId = 0;
    if (rawOrderId !== 0 && rawOrderId.shopId === shopId) {
      orderId = rawOrderId.orderId;
    }

    let subItems = [];
    const names = Object.keys(formik.values);
    names.forEach((name) => {
      if (name !== "crust" && name !== "counter") {
        const [nameSubMenuId, nameSubMenuItemId] = name
          .split("-")
          .map((el) => parseInt(el));

        const currentSubMenu = subMenu.find(
          ({ subMenuId }) => subMenuId === nameSubMenuId
        );

        const {
          subMenuId,
          productSubMenuId,
          subMenuItems,
          minSelect,
          maxSelect,
        } = currentSubMenu;
        const SubMenuItemId = nameSubMenuItemId;

        if (minSelect === 1 && maxSelect === 1) {
          if (formData[name] > 0)
            subItems.push({
              ShopId: shopId,
              SubMenuId: subMenuId,
              SubMenuItemId: formData[name],
              ProductSubMenuId: productSubMenuId,
              Count: 1,
            });
        } else {
          if (formData[name] > 0)
            subItems.push({
              ShopId: shopId,
              SubMenuId: subMenuId,
              SubMenuItemId: SubMenuItemId,
              ProductSubMenuId: productSubMenuId,
              Count: formData[name],
            });
        }
      }
    });

    const reqData = {
      shopId: shopId,
      orderId: orderId,
      saleMethod: saleMethods[saleMethod].code,
      productId: product.id,
      optionId: optionId || 0,
      crustId: crustId,
      customerId: user ? parseInt(user.customerId) : 0,
      count: count,
      postcode: postCode,
      subItems: subItems,
    };

    setLoading(true);

    let res = null;
    try {
      const { shopTitle } = shopDetail.address;
      if (rawOrderId !== 0) {
        if (rawOrderId.shopId !== shopId) {
          showDialog(
            t("start_new_order"),
            t("new_order_message").replace("__sh_n__", rawOrderId.shopTitle),
            async () => {
              res = await (await axiosConfig.post("/Basket", reqData)).data;
              await updateBasket(res);
              changeOrderId({
                orderId: res.orderId,
                shopId: shopId,
                shopTitle: shopTitle,
                shopIdentity: mealzoIdentity,
              });
              showNotification(t("confirmed_successfully"));
              hideDialog();
            },
            () => {
              showNotification(t("canceled_successfully"));
              hideDialog();
              handleClose();
            }
          );
        } else {
          res = await (await axiosConfig.post("/Basket", reqData)).data;
          await updateBasket(res);
        }
      } else {
        res = await (await axiosConfig.post("/Basket", reqData)).data;
        await updateBasket(res);
        addOrderId({
          orderId: res.orderId,
          shopId: shopId,
          shopTitle: shopTitle,
          shopIdentity: mealzoIdentity,
        });
      }

      handleClose();
    } catch (e) {
      showNotification(t("there_was_a_problem"), "error", 3000);
    }
    setLoading(false);
  };

  // console.log(subMenu);
  // const reference = useRef({});
  // console.log(reference);
  // setTimeout(() => {
  //   reference.current['3'].scrollIntoView();
  // }, 3000);

  return (
    <GrowContainer>
      {/*{crust.map((data, index) => (
        <FoodModalCrustAccordion
          formik={formik}
          key={index}
          data={data}
          reference={(ref) => (reference.current[data.subMenuId] = ref)}
        />
      ))}

       {subMenu.map((data, index) => {
        if (data.isCrust === 0)
          return data.minSelect === 1 && data.maxSelect === 1 ? (
            <FoodModalOneSelectAccordion
              formik={formik}
              key={index}
              data={data}
              name={`${data.subMenuId}`}
              reference={(ref) => (reference.current[data.subMenuId] = ref)}
            />
          ) : (
            <FoodModalInputAccordion
              formik={formik}
              key={index}
              data={data}
              subMenuId={data.subMenuId}
              reference={(ref) => (reference.current[data.subMenuId] = ref)}
            />
          );
      })} */}

      {subMenu
        .filter(({ isCrust }) => isCrust > 0)
        .map((data, index) => (
          <FoodModalCrustAccordion formik={formik} key={index} data={data} />
        ))}

      {subMenu.map((data, index) => {
        if (data.isCrust === 0)
          return data.minSelect === 1 && data.maxSelect === 1 ? (
            <FoodModalOneSelectAccordion
              formik={formik}
              key={index}
              data={data}
              name={`${data.subMenuId}`}
            />
          ) : (
            <FoodModalInputAccordion
              formik={formik}
              key={index}
              data={data}
              subMenuId={data.subMenuId}
            />
          );
      })}

      <Box
        sx={{
          position: { xs: "fixed", sm: "sticky" },
          left: 0,
          bottom: -1,
          boxShadow: "0px 0px 6px #00000029",
          width: "100%",
          height: 65,
        }}
      >
        <Grid
          container
          sx={{
            p: "10px 20px",
            backgroundColor: "background.default",
            height: "100%",
          }}
        >
          <Grid
            container
            item
            flexDirection="row"
            flexWrap="nowrap"
            alignItems={"center"}
            // justifyContent="space-between"
            gap={5}
            height="100%"
          >
            <Grid
              container
              item
              flexShrink={1}
              alignItems="center"
              height="100%"
            >
              <Grid item>
                <RoundedPlusMinusButton
                  onClick={() => clickHandler("minus")}
                  disabled={formik.values["counter"] <= 1}
                  width={20}
                />
              </Grid>
              <Grid item mx={1} overflow="hidden">
                <Typography
                  variant="subtitle1"
                  component="div"
                  fontSize={18}
                  fontWeight={500}
                >
                  <NumberCountCarousel number={formik.values["counter"]} />
                </Typography>
              </Grid>
              <Grid item>
                <RoundedPlusMinusButton
                  type="plus"
                  onClick={() => clickHandler("plus")}
                  width={20}
                />
              </Grid>
            </Grid>
            <Grid container item sx={{ cursor: "not-allowed" }} height="100%">
              <LoadingPrimaryButton
                fullWidth
                onClick={handleSubmit}
                variant="contained"
                sx={{
                  py: 0.5,
                  px: 1,
                  fontSize: 16,
                  whiteSpace: "nowrap",
                  height: "100%",
                }}
                disabled={!formik.isValid || loading}
                loading={loading}
                // loadingIndicator="Loading..."
              >
                {t("add_to_basket")} -{" "}
                {currencyIdToMark(shopDetail.address.currencyId, totalValue)}
              </LoadingPrimaryButton>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </GrowContainer>
  );
};

export default FoodModalForm;
