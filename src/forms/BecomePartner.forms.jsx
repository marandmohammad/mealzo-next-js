import { useState } from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-mui";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import useNotification from "../../hooks/useNotification";

import {
  Box,
  Button,
  Grid,
  Typography,
  LinearProgress,
  Modal,
  IconButton,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

import axiosConfig from "../../config/axios";
import Link from "../links/Link";

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  height: 240,
  boxShadow: "0 0 6px #00000012",
  bgcolor: "background.default",
  border: "none",
  outline: "none",
  py: 3,
  px: 4,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
};

function BecomePartnerForm() {
  const { t } = useTranslation('become_partner_page');
  const router = useRouter();
  const { showNotification } = useNotification();
  const [modalOpen, setModalOpen] = useState(false);
  const handleClose = () => {
    router.push("/");
    setModalOpen(false);
  };

  return (
    <Grid
      item
      container
      sx={{
        bgcolor: "background.default",
        p: 4,
        maxWidth: 580,
        "& form": {
          flex: 1,
          display: "flex",
          flexDirection: "column",
        },
        "& form div": {
          my: 0.5,
        },
      }}
    >
      <Typography
        variant="h6"
        component="h3"
        my={3}
        width="100%"
        textAlign="center"
      >
        {t('form_header')}
      </Typography>
      <Formik
        initialValues={{
          fullName: "",
          restaurantName: "",
          phone: "",
          email: "",
          address: "",
          referalCode: "",
        }}
        validate={({ fullName, restaurantName, phone, email, address }) => {
          const errors = {};
          if (!fullName || fullName.trim() === "") errors.fullName = "Required";
          if (!restaurantName || restaurantName.trim() === "")
            errors.restaurantName = "Required";
          if (!address || address.trim() === "") errors.address = "Required";

          if (!email) errors.email = "Required";
          else if (
            !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
              email
            )
          ) {
            errors.email = "Invalid email address";
          }

          if (!phone || phone.trim() === "") errors.phone = "Required";
          else if (phone.length !== 11) errors.phone = "Must be 11 characters";
          else if (!/07[0-9]{9}/.test(phone))
            errors.phone = "Invalid phone number.";

          return errors;
        }}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            //* Send Request
            const result = await axiosConfig.post("/Partners", values);

            //* Open Modal
            setModalOpen(true);

            //* Enable Button
            setSubmitting(false);
          } catch (error) {
            showNotification(error.message, "error", 3000);
          }
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form>
            <Field
              component={TextField}
              name="fullName"
              type="text"
              label={t('name_input_label')}
            />
            <Field
              component={TextField}
              name="restaurantName"
              type="text"
              label={t('business_name_input_label')}
            />
            <Field
              component={TextField}
              name="phone"
              type="text"
              label={t('phone_input_label')}
            />
            <Field
              component={TextField}
              name="email"
              type="email"
              label={t('email_input_label')}
            />
            <Field
              component={TextField}
              name="address"
              type="text"
              label={t('business_address_input_label')}
            />
            <Field
              component={TextField}
              name="referalCode"
              type="text"
              label={t('referral_code_input_label')}
            />
            {isSubmitting && <LinearProgress />}
            <Button
              variant="contained"
              disabled={isSubmitting}
              onClick={submitForm}
              sx={{ my: 4 }}
            >
              {t('form_submit_btn')}
            </Button>
          </Form>
        )}
      </Formik>

      {/* Success Message Modal */}
      <Modal
        open={modalOpen}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <IconButton
            onClick={handleClose}
            sx={{
              position: "absolute",
              top: 18,
              right: 18,
            }}
            size="small"
          >
            <CloseIcon sx={{ fontSize: 20 }} />
          </IconButton>
          <Typography
            variant="h6"
            component="h2"
            color="primary"
            textAlign="center"
            fontSize={20}
          >
            {t('modal_message_1')}
          </Typography>
          <Typography
            variant="subtitle1"
            fontSize={16}
            sx={{ mt: 2 }}
            textAlign="center"
          >
            {t('modal_message_2')}
            <Link href="/" ml={1}>
              mealzo.co.uk
            </Link>
          </Typography>
        </Box>
      </Modal>
    </Grid>
  );
}

export default BecomePartnerForm;
