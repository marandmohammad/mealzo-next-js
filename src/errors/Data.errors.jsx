import { Typography } from "@mui/material";

export default function DataError() {
  return (
    <Typography variant="subtitle1" component="h6">
      You see this message because of a reasons below: <br/>
      There is no data for your request. <br/>
      There was a problem on fetching data. <br/>
    </Typography>
  );
}
