import Image from "next/image";
import { useTranslation } from "next-i18next";
import { Grid, Typography } from "@mui/material";

import didNotFoundAnyThingImage from "../../public/Images/didntfoundanything.png";
import didNotFoundAnyThingImage2 from "../../public/Images/didntfoundanything2.png";
import didNotFoundAnyThingImage3 from "../../public/Images/didntfoundanything3.png";

const images = [
  didNotFoundAnyThingImage,
  didNotFoundAnyThingImage2,
  didNotFoundAnyThingImage3,
];

const AreaNoDataError = ({ imageIdx = Math.floor(Math.random() * 2) }) => {
  const { t } = useTranslation("area_page");

  return (
    <Grid
      container
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      py={5}
    >
      <Grid item width={200} height={200} mb={1}>
        <Image src={images[imageIdx]} alt="Mealzo didNotFoundAnyThingImage" />
      </Grid>
      <Grid item>
        <Typography variant="h6" component="h2">
          {t("area_no_restaurant_message")}
        </Typography>
      </Grid>
      <Grid item>
        <Typography
          variant="h6"
          fontWeight={400}
          component="div"
          textAlign="center"
        >
          {t("area_no_restaurant_hint")}
        </Typography>
        <Typography
          variant="subtitle1"
          fontSize={22}
          component="div"
          textAlign="center"
        >
          {t("area_no_restaurant_hint_2")}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default AreaNoDataError;
