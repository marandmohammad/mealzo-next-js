import { useTranslation } from "next-i18next";

// Mui Components
import Grid from "@mui/material/Grid";
import PartnerCard from "../cards/Partner.cards";

const Partner = () => {
  const { t } = useTranslation("home_page");

  // Data
  const partnerData = [
    {
      id: 1,
      image: "/Images/BecomePartner.png",
      title: t('partner_become_partner_title'),
      body: t('partner_become_partner_text'),
      buttonContent: t('partner_become_partner_button_text'),
      href: "/Partner",
    },
    {
      id: 2,
      image: "/Images/Commission.png",
      title: t('partner_abs_no_co_title'),
      body: t('partner_abs_no_co_text'),
      buttonContent: t('partner_abs_no_co_button_text'),
      href: "/Commission",
    },
  ];

  return (
    <Grid
      container
      sx={{
        border: "1px solid #CCC",
        // padding: "1rem 2rem",
        justifyContent: "space-between",
        flexWrap: "wrap",
        py: { xs: 5, md: 10 },
        px: { xs: 3, sm: 4, md: 5 },
        gap: { xs: 10, md: "unset" },
      }}
    >
      {partnerData.map((item) => (
        <PartnerCard item xs={12} md={6} data={item} key={item.id} />
      ))}
    </Grid>
  );
};

export default Partner;
