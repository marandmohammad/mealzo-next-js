import Grid from "@mui/material/Grid";

function LandingSectionContainer({ children, sx, csx, ...restOfProps }) {
  return (
    <Grid item container sx={{ px: { md: 10, sm: 5, xs: 2 }, my: 10, ...sx }}>
      <Grid
        container
        sx={{ maxWidth: 1300, m: "0 auto", ...csx }}
        {...restOfProps}
      >
        {children}
      </Grid>
    </Grid>
  );
}

export default LandingSectionContainer;
