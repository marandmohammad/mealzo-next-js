import { Grid } from "@mui/material";

function PageContainer({ children, sx, ...restOfProps }) {
  return (
    <Grid container sx={sx} {...restOfProps}>
      {children}
    </Grid>
  );
}

export default PageContainer;
