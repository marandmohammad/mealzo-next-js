import { Box } from "@mui/material";

function ShopLogoContainer({ children, width = 80, sx, ...restOfProps }) {
  return (
    <Box
      sx={{
        width: width,
        height: width,
        borderRadius: "50%",
        overflow: "hidden",
        backgroundColor: "white",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        p: 0.8,
        ...sx
      }}
      {...restOfProps}
    >
      {children}
    </Box>
  );
}

export default ShopLogoContainer;
