import Grid from "@mui/material/Grid";

function SectionContainer({ children, sx, ...restOfProps }) {
  return (
    <Grid
      item
      container
      sx={{ px: { md: 10, sm: 5, xs: 2 }, my: 10, ...sx }}
      {...restOfProps}
    >
      {children}
    </Grid>
  );
}

export default SectionContainer;
