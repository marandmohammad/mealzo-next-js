import { Grow } from "@mui/material";

const GrowContainer = ({ isShown = true, children, style }) => {
  return (
    <Grow in={isShown}>
      <div style={{ width: "100%", ...style }}>{children}</div>
    </Grow>
  );
};

export default GrowContainer;
