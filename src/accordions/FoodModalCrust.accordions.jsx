import { useState, useContext } from "react";
import { useTranslation } from "next-i18next";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
// import CheckIcon from "@mui/icons-material/Check";
import {
  Accordion,
  AccordionDetails,
  // Box,
  Button,
  FormControl,
  FormControlLabel,
  // FormLabel,
  Grid,
  // Radio,
  RadioGroup,
  Typography,
} from "@mui/material";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import RestartAltIcon from "@mui/icons-material/RestartAlt";

import FoodModalRadioInput from "../inputs/FoodModalRadio.inputs";

import { MenuPageContext } from "../../context/MenuPageContext";
import currencyIdToMark from "../../utilities/currencyIdToMark";

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={
      <ArrowForwardIosSharpIcon
        sx={{
          fontSize: "0.9rem",
        }}
      />
    }
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  ".css-o4b71y-MuiAccordionSummary-content": {
    margin: 0,
  },
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "none",
    // paddingTop: 4,
  },
  ".MuiSvgIcon-root": {
    alignSelf: "center",
    // alignSelf: "flex-start",
    // marginTop: "-8px",
  },
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded .MuiSvgIcon-root": {
    transform: "rotate(90deg)",
    // marginTop: "8px",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
  },
}));

function FoodModalCrustAccordion({ data, formik, reference }) {
  const { t } = useTranslation('food_modal');
  const { currencyId } = useContext(MenuPageContext).shopDetail.address;
  // const currencyMark = currencyIdToMark(address.currencyId, item.amount);

  const preTitle =
    data.preTitle &&
    data.preTitle.trim() !== "" &&
    data.preTitle !== 0 &&
    data.preTitle !== "0"
      ? data.preTitle
      : "";
  const { minSelect, maxSelect } = data;

  data.name = "crust";

  const [expanded, setExpanded] = useState("panel1");
  //Change handler for accordion
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <Grid container ref={reference ? reference : null}>
      <Accordion
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
        sx={{
          width: "100%",
          ".MuiButtonBase-root": {
            padding: "5px 30px 5px 20px",
          },
          ".MuiButtonBase-root, .Mui-expanded": {
            maxHeight: "50px !important",
            minHeight: "50px !important",
          },
        }}
      >
        <AccordionSummary
          aria-controls="panel1d-content"
          id="panel1d-header"
          sx={{ backgroundColor: "divider" }}
        >
          <Grid
            container
            justifyContent="space-between"
            alignItems="center"
            ml={1.1}
          >
            <Grid item>
              <Typography variant="h6" fontSize={14}>
                {preTitle} {data.productName}
              </Typography>
              {/* <Typography variant="subtitle1" fontSize={12}>
                Min Select: {minSelect}, Max Select: {maxSelect}
              </Typography> */}
            </Grid>
            <Grid item>
              {/* {typeof formik.errors[data.name] === "string" ? (
                <Typography
                  variant="subtitle1"
                  fontWeight="bold"
                  bgcolor="#F6FF7B"
                  fontSize={12}
                  p="5px 10px"
                  borderRadius="4px"
                >
                  Required
                </Typography>
              ) : (
                <Box display="flex" alignItems="center">
                  <Typography
                    variant="subtitle1"
                    fontWeight="bold"
                    fontSize={12}
                    p="5px"
                    borderRadius="4px"
                  >
                    Required
                  </Typography>
                  <div>
                    <CheckIcon
                      sx={{ fontSize: 14, m: 0, alignSelf: "unset" }}
                    />
                  </div>
                </Box>
              )} */}
              <Button
                variant="text"
                size="small"
                endIcon={<RestartAltIcon sx={{ fontSize: 16 }} />}
                sx={{ px: "10px !important", fontSize: 14 }}
                onClick={(e) => {
                  e.stopPropagation();
                  formik.setFieldValue(data.name, null);
                }}
                disabled={!formik.values[data.name]}
              >
                {t('reset')}
              </Button>
            </Grid>
          </Grid>
        </AccordionSummary>
        <AccordionDetails sx={{ my: 3, p: "0px 30px" }}>
          <FormControl fullWidth>
            <RadioGroup
              name={data.name}
              value={formik.values[data.name]}
              onChange={formik.handleChange}
            >
              {data.subMenuItems.map((item, idx) => (
                <FormControlLabel
                  value={item.id}
                  key={idx}
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    ".MuiFormControlLabel-label": {
                      width: "100%",
                    },
                    "& .MuiButtonBase-root": {
                      padding: "5px",
                    },
                    ml: 0,
                  }}
                  label={
                    <Grid container>
                      <Typography
                        variant="subtitle1"
                        fontSize={{ xs: 12, md: 14 }}
                      >
                        {item.title}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        component="div"
                        fontSize={12}
                        textAlign="right"
                        noWrap
                        sx={{ ml: "auto" }}
                      >
                        {item.amount > 0 &&
                          `${currencyIdToMark(currencyId, item.amount)}`}
                      </Typography>
                    </Grid>
                  }
                  control={<FoodModalRadioInput />}
                  labelPlacement="start"
                />
              ))}
            </RadioGroup>
          </FormControl>
        </AccordionDetails>
      </Accordion>
    </Grid>
  );
}

export default FoodModalCrustAccordion;
