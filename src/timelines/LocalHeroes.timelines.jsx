import { v4 as uuid4 } from "uuid";

import { Link, Box } from "@mui/material";
import {
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineSeparator,
} from "@mui/lab";
import Scrollspy from "react-scrollspy";

function LocalHeroesTimeline({ monthArray, monthIdsArray }) {
  return (
    <Box
      sx={{
        flex: "0 0 250px",
        display: { md: "block", xs: "none" },
      }}
    >
      <Box sx={{ position: "sticky", top: "2%", "& ul": { p: 0, m: 0 } }}>
        <Scrollspy
          items={monthIdsArray}
          currentClassName="timeline-active"
          // style={{ position: "sticky", top: "2%" }}
        >
          {monthArray.map(({ id, title }, index) => (
            <TimelineItem
              sx={{ "&:before": { padding: "0", content: "unset" } }}
              key={id}
            >
              <TimelineSeparator>
                <TimelineDot />
                {monthArray.length - 1 !== index && <TimelineConnector />}
              </TimelineSeparator>
              <TimelineContent>
                <Link href={`#${id}`} underline="none" color="text.primary">
                  {title}
                </Link>
              </TimelineContent>
            </TimelineItem>
          ))}
        </Scrollspy>

        {/* <Timeline></Timeline> */}
      </Box>
    </Box>
  );
}

export default LocalHeroesTimeline;
