import { useState } from "react";
import { useTranslation } from "next-i18next";

import { Grid, Typography, Button } from "@mui/material";
//Icons
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import TodayIcon from "@mui/icons-material/Today";
import CheckIcon from "@mui/icons-material/Check";

import ShopLogoContainer from "../containers/ShopLogo.containers";
import OrderDetailModal from "../modals/OrderDetail.modals";

import timeToAMPM from "../../utilities/timeToAMPM";
import currencyIdToMark from "../../utilities/currencyIdToMark";

function OrdersListItem({ order }) {
  const { t } = useTranslation('previous_orders_page')

  const [detailModal, setDetailModal] = useState(false);
  const handleDetailModal = () => setDetailModal(!detailModal);

  const {
    shopId,
    shopTitle,
    shopLogo,
    saleMethodName,
    orderTime,
    orderId,
    orderDate,
    forPay,
    deliveryAddress,
    currencyId,
    countDetails,
    orderIdEncrypt,
  } = order;

  return (
    <Grid
      container
      item
      p={{ xs: "12px 16px", md: "12px 22px" }}
      my={1}
      sx={{ flexDirection: "row", boxShadow: "0px 0px 20px #00000014" }}
    >
      <Grid item xs={2} md={1} m={{ md: "auto" }}>
        <ShopLogoContainer
          width={{ xs: 40, sm: 75, md: 55, lg: 65 }}
          sx={{ boxShadow: "0px 1px 4px #00000029" }}
        >
          <img width="100%" src={shopLogo} alt={shopTitle} />
        </ShopLogoContainer>
      </Grid>
      <Grid container item xs={10} md={7} sx={{ flexDirection: "row" }}>
        <Grid
          container
          spacing={1}
          item
          sx={{ flexDirection: "row", alignItems: "center" }}
        >
          <Grid item md="auto" xs={12}>
            <Typography
              variant="h6"
              component="h6"
              mt={{ xs: "7px", md: "auto" }}
              fontSize={{ xs: 14, md: 18 }}
            >
              {shopTitle}
            </Typography>
          </Grid>
          <Grid item md={6} display={{ xs: "none", md: "flex" }}>
            {/* <Link
              sx={{ fontSize: 14, textDecoration: "none", fontWeight: "bold" }}
              href="/"
            >
              View Receipt
            </Link> */}
          </Grid>
        </Grid>
        <Grid
          item
          sx={{
            display: "flex",
            flexWrap: "wrap",
            gap: { xs: "10px", sm: "15px", md: "15px" },
            my: 2,
          }}
        >
          {/* Delivery Address */}
          {deliveryAddress && (
            <Grid item sx={{ display: "flex", alignItems: "center" }}>
              <LocationOnIcon
                sx={{ fontSize: 15, color: "text.secondary", marginRight: 0.5 }}
              />
              <Typography
                variant="h6"
                component={"span"}
                fontSize={12}
                color="text.secondary"
              >
                {deliveryAddress}
              </Typography>
            </Grid>
          )}

          <Grid item sx={{ display: "flex", alignItems: "center" }}>
            <AccessTimeIcon
              sx={{ fontSize: 15, color: "text.secondary", marginRight: 0.5 }}
            />
            <Typography
              variant="h6"
              component={"span"}
              fontSize={12}
              color="text.secondary"
            >
              {timeToAMPM(orderTime.split(".")[0])}
            </Typography>
          </Grid>
          <Grid item sx={{ display: "flex", alignItems: "center" }}>
            <TodayIcon
              sx={{ fontSize: 15, color: "text.secondary", marginRight: 0.5 }}
            />
            <Typography
              variant="h6"
              component={"span"}
              fontSize={12}
              color="text.secondary"
            >
              {new Date(orderDate).toDateString()}
            </Typography>
          </Grid>
          <Grid item sx={{ display: "flex", alignItems: "center" }}>
            <Typography
              variant="h6"
              component={"span"}
              fontSize={12}
              color="text.secondary"
            >
              {currencyIdToMark(currencyId, forPay)}
            </Typography>
          </Grid>
          <Grid item sx={{ display: "flex", alignItems: "center" }}>
            <Typography
              variant="h6"
              component={"span"}
              fontSize={12}
              color="text.secondary"
            >
              {saleMethodName}
            </Typography>
            <CheckIcon
              sx={{ fontSize: 15, color: "text.secondary", marginLeft: 0.5 }}
            />
          </Grid>
        </Grid>
        <Grid container item alignItems="center" gap={1}>
          {/*  <Grid item xs="auto" md="auto">
            <Typography
              variant="h6"
              components="span"
              whiteSpace="nowrap"
              fontSize={12}
              color="text.secondary"
            >
              {item} Items:
            </Typography>
          </Grid>
          <Grid container item gap={0.5} xs="auto" md="auto">
            {images.map((image, index) => (
              <Grid item maxWidth="48px" maxHeight="24px" key={index}>
                <img
                  style={{ maxWidth: "48px", maxHeight: "24px" }}
                  src={image}
                  alt=""
                />
              </Grid>
            ))}
          </Grid> */}
          <Grid item display={{ xs: "flex", md: "flex" }}>
            {/* <Link
              sx={{
                fontSize: 12,
                // color: "text.secondary",
                textDecoration: "none",
              }}
              href={`/orderlist/${orderId}`}
            >
              See More Details
            </Link> */}
            <Button
              variant="text"
              size="small"
              sx={{ fontSize: 12 }}
              onClick={handleDetailModal}
            >
              {t('see_more_detail')}
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Grid
        container
        item
        xs={12}
        md={4}
        alignItems="center"
        justifyContent={{ xs: "flex-end", md: "center" }}
        flexWrap="nowrap"
        ml="auto"
      >
        {/*<Grid item xs={"auto"} px={1}>
          <PrimaryButton
            variant="outlined"
            sx={{
              width: { xs: 75, sm: "100%", md: "100%" },
              maxWidth: 155,
              minWidth: 100,
              backgroundColor: "transparent",
              padding: {
                xs: "5px 16px",
                sm: "8px auto",
                md: "8px 45px 8px 45px",
              },
              marginTop: { xs: 1, md: "auto" },
              border: { md: "0.699999988079071px solid #F15928", xs: "none" },
              color: "primary.main",
              "&:hover": {
                backgroundColor: "transparent",
              },
            }}
          >
            Review
          </PrimaryButton>
        </Grid>
        <Grid item xs={"auto"} display={{ md: "flex", xs: "none" }} px={1}>
          <PrimaryButton
            sx={{
              width: { xs: 75, sm: "100%", md: "100%" },
              maxWidth: 155,
              minWidth: 100,
              padding: "8px 45px 8px 45px",
              backgroundColor: "primary.main",
              color: "common.white",
              border: "0.699999988079071px solid #F15928",
              "&:hover": {
                backgroundColor: "primary.main",
              },
            }}
          >
            Reorder
          </PrimaryButton>
        </Grid> */}
      </Grid>
      <OrderDetailModal
        open={detailModal}
        handleClose={handleDetailModal}
        currentOrder={order}
        reviewIdentity={orderIdEncrypt}
      />
    </Grid>
  );
}

export default OrdersListItem;
