import Image from "next/image";

import { Button, Grid, Typography } from "@mui/material";
// import { styled } from "@mui/material/styles";

import MenuProductImagePlaceHolder from "../../public/Images/MenuFoodPlaceHolderImage.jpg";

import currencyIdToMark from "../../utilities/currencyIdToMark";

// const ShopMenuProductImage = styled("img")(({ theme }) => ({
//   objectFit: "cover",
//   width: "100%",
//   height: "100%",
//   borderRadius: 0,
//   [theme.breakpoints.down("md")]: {
//     borderRadius: "4px",
//   },
// }));

function ShopMenuCategoryListItem({
  title,
  description,
  amount,
  imageUrl,
  inBasket,
  basketCount = 0,
  productImgVisible,
  onClick,
  shopDetail,
}) {
  const { currencyId } = shopDetail.address;
  const markedAmount = currencyIdToMark(currencyId, amount);

  return (
    <Button
      sx={{
        display: "flex",
        flex: 1,
        textAlign: "left",
        justifyContent: "flex-start",
        boxShadow: "0px 3px 6px #00000029",
        p: 1.5,
        borderLeft: inBasket
          ? {
              xs: "3px solid #FF571E",
              sm: "4px solid #FF571E",
              md: "5px solid #FF571E",
            }
          : "none",
        borderRadius: { xs: "5px", md: '0 0 4px 4px' },
      }}
      onClick={onClick}
    >
      <Grid
        item
        container
        sx={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "nowrap",
          // boxShadow: "0px 3px 6px #00000029",
          // p: 1.5,
          // borderLeft: inBasket ? {xs: "3px solid #FF571E", sm: "4px solid #FF571E", md: "5px solid #FF571E"} : "none",
          // borderRadius: {xs: '5px', md: 0}
        }}
      >
        {/* Text */}
        <Grid container item flexDirection="column">
          <Typography variant="h6" component="h6" fontSize={12} mb={1}>
            {inBasket && (
              <span style={{ color: "#FF571E", marginRight: 5 }}>
                {basketCount}X
              </span>
            )}
            {title}
          </Typography>
          <Typography variant="subtitle1" component="div" fontSize={10}>
            {description}
          </Typography>
          <Typography
            variant="subtitle1"
            component="div"
            justifySelf="flex-end"
            mt={"auto"} //1.6
          >
            {markedAmount}
          </Typography>
        </Grid>

        {/* Likes */}
        {/* Important Future */}
        {/*<Grid item>*/}
        {/*  <FavoriteIcon sx={{ color: "primary.main", fontSize: 20 }} />*/}
        {/*  <Typography*/}
        {/*    variant="subtitle1"*/}
        {/*    color="text.rating"*/}
        {/*    fontSize={10}*/}
        {/*  >*/}
        {/*    602*/}
        {/*  </Typography>*/}
        {/*</Grid>*/}

        {/* Card Image */}
        <Grid
          item
          sx={{
            width: 150,
            height: 100,
            position: "relative",
            overflow: "hidden",
            ml: "10px",
            borderRadius: { xs: "4px", md: 0 },
          }}
        >
          {productImgVisible ? (
            <Image
              src={imageUrl}
              loader={({ src }) => src}
              layout="fill"
              objectFit="cover"
              blurDataURL="/Images/MenuFoodPlaceHolderImage.jpg"
              placeholder="blur"
              loading="lazy"
              alt={title}
            />
          ) : (
            <Image
              src={MenuProductImagePlaceHolder}
              layout="fill"
              objectFit="cover"
              placeholder="blur"
              loading="lazy"
              alt={title}
            />
          )}
        </Grid>
      </Grid>
    </Button>
  );
}

export default ShopMenuCategoryListItem;
