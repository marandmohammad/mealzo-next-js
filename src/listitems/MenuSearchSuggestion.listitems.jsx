import Image from "next/image";

import Box from "@mui/material/Box";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
// import { styled } from "@mui/material/styles";

import MenuProductImagePlaceHolder from "../../public/Images/MenuFoodPlaceHolderImage.jpg";

const MenuSearchSuggestionListItem = ({
  title,
  subTitle,
  productImgVisible,
  imageUrl,
  onClick,
}) => {
  return (
    <ListItemButton onClick={onClick}>
      <Box
        width={75}
        height={75}
        mr={1}
        borderRadius={{ xs: "4px", md: 0 }}
        overflow="hidden"
        position="relative"
      >
        {productImgVisible ? (
          <Image
            src={imageUrl}
            loader={({ src }) => src}
            layout="fill"
            objectFit="cover"
            blurDataURL="/Images/MenuFoodPlaceHolderImage.jpg"
            placeholder="blur"
            loading="lazy"
          />
        ) : (
          <Image
            src={MenuProductImagePlaceHolder}
            layout="fill"
            objectFit="cover"
            placeholder="blur"
            loading="lazy"
          />
        )}
      </Box>
      <ListItemText
        primary={title}
        secondary={subTitle}
        sx={{
          ".MuiListItemText-primary": {
            color: "text.primary",
            fontSize: 14,
          },
          ".MuiListItemText-secondary": {
            color: "text.secondary",
            fontSize: 12,
          },
        }}
      ></ListItemText>
    </ListItemButton>
  );
};

export default MenuSearchSuggestionListItem;
