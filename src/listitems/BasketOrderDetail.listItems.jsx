import { Grid, Typography } from "@mui/material";

import NumberCountCarousel from "../carousels/NumberCount.carousels";
import SquarePlusMinusButton from "../buttons/SquarePlusMinus.buttons";

import currencyIdToMark from "../../utilities/currencyIdToMark";

const BasketOrderDetailListItem = ({
  orderDetail,
  currencyId,
  loading,
  handleChangeCount,
  changeableCount = true,
}) => {
  {
    const {
      id,
      titlePosition,
      titleInLast,
      customTitleInLast,
      count,
      customeTitle,
      titleOption,
      titleCrust,
      subMenus,
      title,
      amount,
    } = orderDetail;

    const isTitle = titlePosition ? !titleInLast : titleInLast;
    const space = " ";

    // Showing Count OR Not
    const showCount = count > 1 ? true : false;

    let shownTitle = "";
    if (customeTitle && customeTitle.trim() !== "" && !customTitleInLast) {
      shownTitle += customeTitle + space;
    }
    if (isTitle) {
      if (titleOption !== null) {
        shownTitle += titleOption + space;
      }
      if (titleCrust !== null) {
        shownTitle += titleCrust + space;
      }
      shownTitle += title + space;
    } else {
      shownTitle += title + space;
      if (titleOption !== null) {
        shownTitle += titleOption + space;
      }
      if (titleCrust !== null) {
        shownTitle += titleCrust + space;
      }
    }

    if (customeTitle && customeTitle.trim() !== "" && customTitleInLast) {
      shownTitle += customeTitle + space;
    }

    return (
      <Grid container mb={1.5} height="max-content">
        <Grid container>
          <Grid item xs={8} md={7} lg={8}>
            <Typography
              variant="subtitle1"
              color="text.newMedium"
              fontWeight={500}
              fontSize={14}
            >
              {shownTitle}
            </Typography>
          </Grid>
          <Grid
            container
            item
            display="grid"
            gridTemplateColumns="auto 0.5fr auto 50px"
            gridTemplateRows="1fr"
            gap={{ xs: 0.8, md: 0.4, xl: 0.8 }}
            alignItems="center"
            lg={4}
            md={5}
            xs={4}
          >
            <Grid
              item
              sx={{
                cursor: loading ? "progress" : "pointer",
                textAlign: "center",
              }}
            >
              {changeableCount && (
                <SquarePlusMinusButton
                  type="minus"
                  onClick={() => handleChangeCount(id, count - 1)}
                  disabled={loading}
                  aria-label='decrease-basket'
                />
              )}
            </Grid>
            <Grid item overflowY="hidden">
              <Typography
                variant="subtitle1"
                fontSize={14}
                textAlign="center"
                // minWidth={5}
              >
                <NumberCountCarousel
                  number={`${count}${!changeableCount ? "X" : ""}`}
                />
              </Typography>
            </Grid>
            <Grid
              item
              sx={{
                cursor: loading ? "progress" : "pointer",
                textAlign: "center",
              }}
            >
              {changeableCount && (
                <SquarePlusMinusButton
                  type="plus"
                  onClick={() => handleChangeCount(id, count + 1)}
                  disabled={loading}
                  aria-label='increase-basket'
                />
              )}
            </Grid>
            <Grid item>
              <Typography
                variant="subtitle1"
                fontSize={14}
                width="100%"
                textAlign="right"
                pr={0.25}
                noWrap
              >
                {currencyIdToMark(currencyId, amount)}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        {subMenus.length > 0 &&
          subMenus.map((sm, smIndex) => {
            const { titleSubMenu, isAfter, isFrist, subMenuItems } = sm;
            let subMenuText = "";
            let rowAmount = 0;

            if (titleSubMenu && titleSubMenu.trim() !== "") {
              if (smIndex > 0 && !isAfter) {
                //* subMenuText += ", ";
              }
              if (isFrist) {
                subMenuText += titleSubMenu;
              }
            }
            if (subMenuItems.length > 0)
              subMenuItems.forEach((smi, smiIndex) => {
                const { titleSubMenuItem, isAfter, xCount, subMenuItemAmount } =
                  smi;
                if (titleSubMenuItem && titleSubMenuItem.trim() !== "") {
                  if (smiIndex > 0 && !isAfter) {
                    subMenuText += ", ";
                  }
                  if (xCount > 1) {
                    subMenuText += xCount + "X ";
                  }
                  rowAmount += subMenuItemAmount;
                  subMenuText += titleSubMenuItem.trim();
                }
              });

            return (
              <Grid container justifyContent="space-between" key={smIndex}>
                <Grid container item maxWidth={"70%"}>
                  <Typography
                    variant="subtitle1"
                    fontSize={12}
                    lineHeight={1.2}
                  >
                    {subMenuText}
                  </Typography>
                </Grid>
                <Grid item flexShrink={1}>
                  <Typography
                    variant="subtitle1"
                    fontSize={12}
                    lineHeight={1.2}
                  >
                    {currencyIdToMark(currencyId, rowAmount)}
                  </Typography>
                </Grid>
              </Grid>
            );
          })}
      </Grid>
    );
  }
};

export default BasketOrderDetailListItem;
