import { useState, useEffect } from "react";

import useNotification from "../../hooks/useNotification";

import InfiniteScroll from "react-infinite-scroll-component";
import ShopReviewsList from "../lists/ShopReviews.lists";
import ReviewsListLoading from "../loadings/ReviewsList.loadings";

import axiosConfig from "../../config/axios";

const ShopReviewsInfiniteScroll = ({ shopIdentity }) => {
  const { showNotification } = useNotification();

  const [items, setItems] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [pageNumber, setPageNumber] = useState(1);
  const [loading, setLoading] = useState(true);
  const [countOfData, setCountOfData] = useState(0);

  //! get reviews request
  const getReviews = async () => {
    try {
      const data = await (
        await axiosConfig.get(`/Review/${shopIdentity}/${pageNumber}`)
      ).data;
      return data;
    } catch (error) {
      showNotification(error.message, "error", 3000);
      return false;
    }
  };

  //! initial data
  useEffect(() => {
    const getInitialReviews = async () => {
      setLoading(true);

      const reviewsData = await getReviews();

      if (reviewsData) {
        const { countOfReview, reviews } = reviewsData;
        setItems(reviews);
        setCountOfData(countOfReview);
        setHasMore(!(reviews.length >= countOfReview));
      }

      setLoading(false);
    };

    getInitialReviews();
  }, []);

  //! request handler
  const fetchMoreData = async () => {
    if (items.length >= countOfData) {
      setHasMore(false);
      return;
    }

    setPageNumber(++pageNumber);

    const reviewsData = await getReviews();

    if (reviewsData) {
      const { countOfReview, reviews } = reviewsData;
      setItems([...items, ...reviews]);
    }
  };

  return (
    <InfiniteScroll
      dataLength={items.length}
      next={fetchMoreData}
      hasMore={hasMore}
      // loader={<div className="loader"></div>}
      scrollThreshold={0.5}
      endMessage={
        <p style={{ textAlign: "center", marginTop: 60 }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
      scrollableTarget={"reviewsListModal"}
    >
      {loading ? <ReviewsListLoading /> : <ShopReviewsList reviews={items} />}
    </InfiniteScroll>
  );
};

export default ShopReviewsInfiniteScroll;
