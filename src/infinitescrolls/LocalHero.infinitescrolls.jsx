import { useState, useEffect } from "react";
import { v4 as uuid4 } from "uuid";
import InfiniteScroll from "react-infinite-scroll-component";
import { useTranslation } from "next-i18next";

import { Box, Grid, Stack, Typography } from "@mui/material";

import LocalHerosCard from "../cards/LocalHeros.cards";
import DataError from "../errors/Data.errors";
import BlogLoading from "../loadings/Blog.loadings";
import LocalHeroesTimeline from "../timelines/LocalHeroes.timelines";

import axiosConfig from "../../config/axios";

function monthSplitter(eventsGroup) {
  if (eventsGroup === null) return [null, null];

  const monthArray = [];
  const monthIdsArray = [];
  eventsGroup.forEach(({ evantMonth }) => {
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    const splitDate = evantMonth.split("");
    const formattedDate = [];
    splitDate.forEach((el, index) => {
      if (index === 4) formattedDate.push("-");
      formattedDate.push(el);
    });
    const date = new Date(formattedDate.join(""));
    const eventYear = date.getFullYear();
    const eventMonthName = monthNames[date.getMonth()];
    const currentYear = new Date().getFullYear();

    monthIdsArray.push(evantMonth);
    monthArray.push({
      id: evantMonth,
      title:
        eventYear === currentYear
          ? eventMonthName
          : `${eventMonthName} ${eventYear}`,
    });
  });

  return [monthArray, monthIdsArray];
}

function getDataCount(eventsGroup) {
  if (
    typeof eventsGroup === "undefined" ||
    eventsGroup === null ||
    eventsGroup.length <= 0
  )
    return 0;

  let dataCount = 0;
  eventsGroup.forEach(({ events }) => (dataCount += events.length));

  return dataCount;
}

function LocalHeroInfiniteScroll({ pageData }) {
  const { t } = useTranslation("local_hero_page");

  const initialEventsGroup =
    pageData.eventsGroup.length > 0 ? pageData.eventsGroup : null;
  const [initialMonthArray, initialMonthIdsArray] =
    monthSplitter(initialEventsGroup);

  const [items, setItems] = useState(initialEventsGroup);
  const [monthArray, setMonthArray] = useState(initialMonthArray);
  const [monthIdsArray, setMonthIdsArray] = useState(initialMonthIdsArray);

  const [hasMore, setHasMore] = useState(
    items ? getDataCount(items) >= pageData.count : true
  );
  const [pageNumber, setPageNumber] = useState(1);
  const [loading, setLoading] = useState(false);

  const [SSR, setSSR] = useState(true);
  const [show, setShow] = useState(false);

  useEffect(() => {
    setItems(initialEventsGroup);
    setMonthArray(initialMonthArray);
    setMonthIdsArray(initialMonthIdsArray);
    setHasMore(true);
    setPageNumber(1);
    setLoading(false);
    setSSR(false);
    setShow(false);
  }, [initialEventsGroup]);

  const fetchMoreData = async () => {
    if (getDataCount(items) >= pageData.count) {
      await setHasMore(false);
      return;
    }

    await setLoading(true);
    await setPageNumber(++pageNumber);

    const newEventsReq = await axiosConfig.get(
      `/Heroes?pageNumber=${pageNumber}`
    );
    const newEventsGroup = newEventsReq.data.eventsGroup;
    const [newMonthArray, newMonthIdsArray] = monthSplitter(newEventsGroup);

    await setItems([...items, ...newEventsGroup]);
    await setMonthArray([...monthArray, ...newMonthArray]);
    await setMonthIdsArray([...monthIdsArray, ...newMonthIdsArray]);

    await setLoading(false);
  };

  setTimeout(() => {
    setShow(true);
  }, 1500);

  if (items && !SSR) {
    return (
      <Stack direction="row" spacing={{ xs: 0, md: 2 }}>
        {/* time line placeholder */}
        {show && (
          <LocalHeroesTimeline
            monthArray={monthArray}
            monthIdsArray={monthIdsArray}
          />
        )}

        <Grid container position="relative">
          <div>
            <Box mb={2}>
              <Typography component='p' variant="h6" fontSize="1rem" mb={3}>
                {t("text_1")}
              </Typography>
              <Typography component='p' variant="h6" fontSize="1rem">
                {t("text_2")}
              </Typography>
            </Box>
          </div>
          {show ? (
            <InfiniteScroll
              dataLength={items.length}
              next={fetchMoreData}
              hasMore={hasMore}
              // loader={<div className="loader"></div>}
              scrollThreshold={0.4}
              endMessage={
                <p style={{ textAlign: "center", marginTop: 60 }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
            >
              {items.map(({ evantMonth, events }, index) => {
                const { title } = monthArray.filter(
                  (el) => el.id === evantMonth
                )[0];
                return (
                  <Box my={2} id={evantMonth} key={index}>
                    <Typography mb={4} variant="h6">
                      {title} Top Heroes
                    </Typography>
                    {events.map((cardData) => (
                      <LocalHerosCard cardData={cardData} key={uuid4()} />
                    ))}
                  </Box>
                );
              })}
              {loading && <div className="loader" style={{ bottom: 6 }}></div>}
            </InfiniteScroll>
          ) : (
            <BlogLoading />
          )}
        </Grid>
      </Stack>
    );
  } else return <DataError />;
}

export default LocalHeroInfiniteScroll;
