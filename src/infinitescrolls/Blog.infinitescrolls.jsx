import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import InfiniteScroll from "react-infinite-scroll-component";
import { Grid } from "@mui/material";

import BlogsCard from "../cards/Blogs.cards";
import DataError from "../errors/Data.errors";
import BlogLoading from "../loadings/Blog.loadings";

import axiosConfig from "../../config/axios";

function BlogInfiniteScroll({ initialPosts, pageData }) {
  const [items, setItems] = useState(initialPosts);
  const [hasMore, setHasMore] = useState(
    items ? items.length >= pageData.count : true
  );
  const [pageNumber, setPageNumber] = useState(pageData.pageNumber);
  const [loading, setLoading] = useState(false);

  const [SSR, setSSR] = useState(true);
  const [show, setShow] = useState(false);

  useEffect(() => {
    setItems(initialPosts);
    setHasMore(true);
    setPageNumber(pageData.pageNumber);
    setLoading(false);
    setSSR(false);
    setShow(false);
  }, [initialPosts]);

  const router = useRouter();

  const fetchMoreData = async () => {
    if (items.length >= pageData.count) {
      await setHasMore(false);
      return;
    }

    await setLoading(true);
    await setPageNumber(++pageNumber);

    let extraQuery = "";
    if (typeof router.query.tag !== "undefined")
      extraQuery = `tag=${router.query.tag}&`;
    else if (typeof router.query.archive !== "undefined")
      extraQuery = `archive=${router.query.archive}&`;

    const newPostsReq = await axiosConfig.get(
      `/Blogs?${extraQuery}pageNumber=${pageNumber}`
    );
    const newPosts = newPostsReq.data.blogListViewModel;

    await setItems([...items, ...newPosts]);
    await setLoading(false);
  };

  setTimeout(() => {
    setShow(true);
  }, 1500);

  if (items && !SSR) {
    if (show) {
      return (
        <InfiniteScroll
          dataLength={items.length}
          next={fetchMoreData}
          hasMore={hasMore}
          // loader={<div className="loader"></div>}
          scrollThreshold={0.4}
          endMessage={
            <p style={{ textAlign: "center", marginTop: 60 }}>
              <b>Yay! You have seen it all</b>
            </p>
          }
        >
          <Grid container spacing={{ xs: 4, md: 2 }} sx={{ pb: 1 }}>
            {items.map((item) => (
              <Grid key={item.id} item xs={12} md={6}>
                <BlogsCard item={item} />
              </Grid>
            ))}
          </Grid>
          {loading && <div className="loader"></div>}
        </InfiniteScroll>
      );
    } else return <BlogLoading />;
  } else return <DataError />;
}

export default BlogInfiniteScroll;
