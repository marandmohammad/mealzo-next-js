import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          scrollBehavior: "smooth",
        },
        "*": {
          scrollbarColor: "#e9540d #F8F8F9",
          // scrollbarWidth: 'thin',
        },
        ".pac-container": {
          zIndex: 9999,
        },
        'input[type="email"], input[type="text"], input[type="search"], select':
          {
            // "-webkit-appearance": "none",
            WebkitAppearance: "none",
          },
        body: {
          scrollbarColor: "#e9540d #F8F8F9",
          "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
            backgroundColor: "#F8F8F9",
            width: 13,
          },
          "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
            borderRadius: 0,
            backgroundColor: "#e9540d",
            minHeight: 24,
          },
          "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus":
            {
              backgroundColor: "#959595",
            },
          "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active":
            {
              backgroundColor: "#959595",
            },
          "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover":
            {
              backgroundColor: "#e9540d",
            },
          "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
            backgroundColor: "#2b2b2b",
          },
        },
      },
    },
  },
  palette: {
    common: {
      white: "#f8f8f8",
    },
    primary: {
      light: "#ff571e",
      main: "#e9540d",
    },
    secondary: {
      light: "#1c6f7d",
      main: "#215d67",
      dark: "#2d707b",
    },
    text: {
      primary: "#373636",
      medium: "#474747",
      newMedium: "#4A4B4D",
      secondary: "#707070",
      rating: "#E9540D",
    },
    background: {
      default: "#FFF",
    },
    divider: "#DEDEDE",
    checkbox: "#C2C0C0",
  },
  typography: {
    fontFamily: [
      "IBM Plex Sans",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    fontSize: 16,
    h6: {
      fontFamily: ["IBM Plex Sans", "Roboto", "sans-serif"].join(","),
      fontSize: 24,
      fontWeight: "bold",
    },
    subtitle1: {
      fontFamily: ["IBM Plex Sans", "Roboto", "sans-serif"].join(","),
      fontSize: 16,
      fontWeight: 400,
      color: "#707070",
    },
    // subtitle2: {
    //   fontFamily: ["IBM Plex Sans", "Roboto", "sans-serif"].join(","),
    //   fontSize: 16,
    //   fontWeight: 400,
    //   color: "#707070",
    // },
    button: {
      fontWeight: 500,
      textTransform: "none",
    },
  },
  shape: {
    borderRadius: 0,
  },
});

export default theme;
