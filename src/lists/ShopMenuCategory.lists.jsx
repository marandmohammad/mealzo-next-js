import { useState } from "react";
import { Grid, Typography } from "@mui/material";

import ShopMenuCategoryListItem from "../listitems/ShopMenuCategory.listitems";
// import { MenuPageContext } from "../../context/MenuPageContext";
import FoodModal from "../modals/Food.modals";

function ShopMenuCategoryList({ category, shopDetail, sx }) {
  const { categoryId, categoryTitle, categoryDescription, products } = category;
  // const { isHaveCrust, isHaveSubMenu } = products;

  // const { setSelectedProduct } = useContext(MenuPageContext);
  const [selectedProduct, setSelectedProduct] = useState(null);
  //function for handle open and close modal

  const [foodModal, setFoodModal] = useState(false);
  const handleFoodModalOpen = () => setFoodModal(true);
  const handleFoodModalClose = () => setFoodModal(false);

  const handleProductClick = async (product, optionId = null) => {
    await setSelectedProduct({ product, optionId });
    await handleFoodModalOpen();
  };

  return (
    <Grid
      container
      flexDirection="column"
      px={{ xs: 1.5, md: 0 }}
      sx={{ ...sx }}
      id={categoryId}
    >
      <Grid item container flexDirection="column" mb={1.5}>
        <Typography variant="h6" component="h2" fontSize={20}>
          {categoryTitle}
        </Typography>
        <Typography variant="subtitle1" component="p" fontSize={14}>
          {categoryDescription}
        </Typography>
      </Grid>
      <Grid
        item
        container
        display="grid"
        gridTemplateColumns={{ xs: "1fr", sm: "1fr 1fr" }}
        gap={3}
      >
        {products.map((product, index) => {
          const {
            id,
            title,
            productImgSmall,
            productImgWide,
            amount,
            productImgVisible,
            options,
            inBasket,
            basketCount,
            description,
          } = product;
          if (
            !amount &&
            amount === 0 &&
            options.length > 0 &&
            options[0] !== null
          )
            return options.map(
              ({
                optionId,
                optionTitle,
                optionAmount,
                inBasket,
                basketCount,
              }) => (
                <ShopMenuCategoryListItem
                  title={`${title} ${optionTitle}`}
                  description={description}
                  amount={amount && amount > 0 ? amount : optionAmount}
                  imageUrl={`${productImgWide}`}
                  inBasket={inBasket}
                  basketCount={basketCount}
                  productImgVisible={productImgVisible}
                  shopDetail={shopDetail}
                  onClick={() => handleProductClick(product, optionId)}
                  key={optionId}
                />
              )
            );
          else
            return (
              <ShopMenuCategoryListItem
                title={title}
                description={description}
                amount={amount}
                imageUrl={`${productImgWide}`}
                inBasket={inBasket}
                basketCount={basketCount}
                productImgVisible={productImgVisible}
                shopDetail={shopDetail}
                onClick={() => handleProductClick(product)}
                key={index}
              />
            );
        })}
      </Grid>

      {/* Food Modal */}
      <FoodModal
        item={selectedProduct}
        open={foodModal}
        handleClose={handleFoodModalClose}
      />
    </Grid>
  );
}

export default ShopMenuCategoryList;
