import { styled } from "@mui/system";
import { Typography } from "@mui/material";

// export const FoodsContainer = styled(Grid)(({ theme }) => ({
//   width: "100%",
//   padding: theme.spacing(2),
//   display: "flex",
//   flexDirection: "column",
//   justifyContent: "space-between",
//   [theme.breakpoints.up("lg")]: {
//     maxWidth: "1400px",
//   },
// }));

export const FoodsTitle = styled(Typography)(({ theme }) => ({
  fontSize: "25px",
  marginBottom: "20px",
}));
