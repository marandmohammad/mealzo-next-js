import { useRouter } from "next/router";

import {
  Box,
  Grow,
  List,
  ListItemButton,
  ListItemText,
  Typography,
} from "@mui/material";

import { localesList } from "../../utilities/appWideHelperFunctions";
import theme from "../themes/mealzoTheme";

const ChangeLanguageList = ({
  top,
  left,
  routeChangeCallback,
  ...restOfProps
}) => {
  const router = useRouter();

  const handleChangeRoute = (locale) => () => {
    router.push(router.asPath, null, { locale });
    if (routeChangeCallback) routeChangeCallback();
  };

  return (
    <Grow {...restOfProps}>
      <div
        style={{
          position: "absolute",
          top: top ?? 45,
          left: left ?? -20,
          width: "max-content",
          backgroundColor: "#FFF",
          boxShadow: "0 0 6px #00000029",
          borderRadius: 4,
        }}
      >
        <Box p={1}>
          <Box
            sx={{
              "&::-webkit-scrollbar": {
                width: 5,
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: "15px",
              },
            }}
          >
            <List disablePadding>
              {router.locales.map((locale, idx) => (
                <ListItemButton
                  onClick={handleChangeRoute(locale)}
                  sx={{
                    zIndex: theme.zIndex.appBar + 1,
                    py: 0.5,
                    borderRadius: "3px",
                  }}
                  key={idx}
                >
                  <ListItemText
                    primary={
                      <Typography
                        fontSize={14}
                        fontWeight={500}
                        color="text.newMedium"
                      >
                        {localesList[locale]}
                      </Typography>
                    }
                  />
                </ListItemButton>
              ))}
            </List>
          </Box>
        </Box>
      </div>
    </Grow>
  );
};

export default ChangeLanguageList;
