import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Box from "@mui/material/Box";
// import Slide from "@mui/material/Slide";
import Fade from "@mui/material/Fade";
import Typography from "@mui/material/Typography";

import SearchIcon from "@mui/icons-material/Search";

import theme from '../themes/mealzoTheme';

function AreaSearchSuggestionList({
  show,
  searchResult,
  handleSelectSuggestedItem,
  ...restOfProps
}) {
  if (searchResult.length <= 0) return;
  const restaurants = [];
  const categories = [];
  searchResult.forEach((el) => {
    if (el.groupName.toLowerCase() === "cuisine") categories.push(el);
    else restaurants.push(el);
  });
  return (
    <Fade {...restOfProps}>
      <div>
        <Box
          sx={{
            width: "100%",
            position: "absolute",
            top: 52,
            right: 0,
            zIndex: theme.zIndex.appBar + 1,
            bgcolor: "white",
            boxShadow: "0 0 6px #00000029",
            color: "text.primary",
            display: "flex",
            flexDirection: "column",
          }}
        >
          {categories.length > 0 && (
            <List
              component="nav"
              aria-labelledby="area-search-suggestion-list"
              subheader={
                <ListSubheader component="div" id="area-search-suggestion-list">
                  Categories
                </ListSubheader>
              }
            >
              {categories.map(({ title, countCuisine }) => (
                <ListItemButton
                  onClick={() => handleSelectSuggestedItem(title)}
                >
                  <ListItemIcon>
                    <SearchIcon />
                  </ListItemIcon>
                  <ListItemText>
                    <Typography color="text.primary" fontSize={14}>
                      {title} ({countCuisine})
                    </Typography>
                  </ListItemText>
                </ListItemButton>
              ))}
            </List>
          )}
          {restaurants.length > 0 && (
            <List
              component="nav"
              aria-labelledby="area-search-suggestion-list"
              subheader={
                <ListSubheader component="div" id="area-search-suggestion-list">
                  Restaurants
                </ListSubheader>
              }
            >
              {restaurants.map(
                ({ title, openStatus, mealzoHomePageBanner }) => (
                  <ListItemButton
                    onClick={() => handleSelectSuggestedItem(title)}
                  >
                    <Box width={75} height={35} mr={1}>
                      <img
                        src={mealzoHomePageBanner}
                        style={{
                          width: "100%",
                          height: "100%",
                        }}
                        alt={title}
                      />
                    </Box>
                    <ListItemText
                      primary={title}
                      secondary={openStatus}
                      sx={{
                        ".MuiListItemText-primary": {
                          color: "text.primary",
                          fontSize: 14,
                        },
                        ".MuiListItemText-secondary": {
                          color: "text.secondary",
                          fontSize: 12,
                        },
                      }}
                    ></ListItemText>
                  </ListItemButton>
                )
              )}
            </List>
          )}
        </Box>
      </div>
    </Fade>
  );
}

export default AreaSearchSuggestionList;
