import Grid from "@mui/material/Grid";

import ShopReviewCard from "../cards/ShopReview.cards";
import GrowContainer from "../containers/Grow.containers";

const ShopReviewsList = ({ reviews }) => {
  return (
    <GrowContainer>
      <Grid container rowGap={2}>
        {reviews.map((item, idx) => (
          <ShopReviewCard review={item} key={idx} />
        ))}
      </Grid>
    </GrowContainer>
  );
};

export default ShopReviewsList;
