// Mui Components
import { Typography, Grid } from "@mui/material";
// Components
import ChipsCard from "../cards/Chips.cards";

const LandingTopCuisinesList = ({ topCuisines, focusHandler }) => {
  return (
    <>
      <Grid item xs={12}>
        <Typography
          variant="h6"
          component="div"
          fontSize={{ sm: 40, xs: "6vw" }}
          textAlign="center"
          marginY={2}
        >
          Fancy something different?
        </Typography>
      </Grid>
      <Grid container item p={2} xs={12} justifyContent="center">
        {topCuisines.map((item) => (
          <ChipsCard
            onClick={focusHandler}
            key={item.id}
            title={item.title}
            item
            xl={1.5}
            lg={2}
            md={2.6}
            sm={3.6}
            xs={10}
          />
        ))}
      </Grid>
    </>
  );
};

export default LandingTopCuisinesList;
