import { Grid, Typography } from "@mui/material";

import OrdersListItem from "../listitems/Orders.listItems";
import GrowContainer from "../containers/Grow.containers";

const PreviousOrdersList = ({ previousOrders }) => {
  return (
    <GrowContainer>
      <Grid container px={{ xs: 1, md: 0 }}>
        {previousOrders.length > 0 ? (
          previousOrders.map((order, index) => (
            <OrdersListItem key={index} order={order} />
          ))
        ) : (
          <Typography
            variant="subtitle1"
            component="h6"
            color="primary.main"
            textAlign="center"
            width="100%"
          >
            You have no orders yet!
          </Typography>
        )}
      </Grid>
    </GrowContainer>
  );
};

export default PreviousOrdersList;
