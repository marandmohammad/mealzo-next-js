import { v4 as uuid4 } from "uuid";
import { useTranslation } from "next-i18next";

import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import Box from "@mui/material/Box";
import Slide from "@mui/material/Slide";

import MenuSearchSuggestionListItem from "../listitems/MenuSearchSuggestion.listitems";

function MenuSearchSuggestionList({
  show,
  searchResult,
  searchString,
  handleSelectSuggestedItem,
  ...restOfProps
}) {
  const { t } = useTranslation("common");

  return (
    <Slide {...restOfProps}>
      <div>
        <Box
          sx={{
            width: "100%",
            position: "absolute",
            top: 52,
            right: 0,
            zIndex: 1101,
            bgcolor: "white",
            boxShadow: "0 0 6px #00000029",
            color: "text.primary",
            display: "flex",
            flexDirection: "column",
            maxHeight: { xs: "60vh", md: "75vh" },
            overflowY: "scroll",
          }}
        >
          <List
            component="nav"
            aria-labelledby="area-search-suggestion-list"
            subheader={
              <ListSubheader component="div" id="area-search-suggestion-list">
                {searchResult.length} {t("n_results_for")} “{searchString}”
              </ListSubheader>
            }
          >
            {searchResult.map((product, index) => {
              const {
                id,
                title,
                productImgSmall,
                productImgWide,
                amount,
                productImgVisible,
                options,
                // inBasket,
                // basketCount,
              } = product;
              if (options.length > 0 && options[0] !== null)
                return options.map(
                  ({
                    optionId,
                    optionTitle,
                    optionAmount,
                    // inBasket,
                    // basketCount,
                  }) => (
                    <MenuSearchSuggestionListItem
                      title={`${title} ${optionTitle}`}
                      subTitle={optionAmount}
                      productImgVisible={productImgVisible}
                      imageUrl={productImgWide}
                      onClick={() =>
                        handleSelectSuggestedItem(product, optionId)
                      }
                      key={uuid4()}
                    />
                  )
                );
              return (
                <MenuSearchSuggestionListItem
                  title={title}
                  subTitle={amount}
                  productImgVisible={productImgVisible}
                  imageUrl={productImgWide}
                  onClick={() => handleSelectSuggestedItem(product)}
                  key={uuid4()}
                />
              );
            })}
          </List>
        </Box>
      </div>
    </Slide>
  );
}

export default MenuSearchSuggestionList;
