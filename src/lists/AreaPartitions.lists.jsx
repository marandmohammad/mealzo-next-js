import { Fragment } from "react";
import { setCookies } from "cookies-next";
import { useTranslation } from "next-i18next";

import { Box, Grid } from "@mui/material";

import RestaurantCard from "../cards/AreaRestaurant.cards";
import GrowContainer from "../containers/Grow.containers";
import AreaNoDataError from "../errors/AreaNoData.errors";
import Link from "../links/Link";
import BlogLoading from "../loadings/Blog.loadings";
import { FoodsTitle } from "./AreaFoods.lists.styles";

import { saleMethods } from "../../utilities/areaPathHandlers";

const AreaPartitionsList = ({
  title,
  data,
  shopLoading,
  saleMethod,
  openData,
}) => {
  const { t } = useTranslation("area_page");

  return (
    <GrowContainer>
      <Grid container mt={5}>
        <Grid item xs={12}>
          {title ? (
            <FoodsTitle variant="h6" component="h2">
              {title}
              <span
                style={{ fontSize: "14px", fontWeight: "600", marginLeft: 5 }}
              >
                ({data.length} {t('area_restaurants')})
              </span>
            </FoodsTitle>
          ) : (
            <FoodsTitle variant="h6" component="h2">
              {data.length} {t('area_restaurants')}
            </FoodsTitle>
          )}
        </Grid>
        {shopLoading ? (
          <BlogLoading area />
        ) : (
          <Box
            sx={{
              flexGrow: 1,
            }}
          >
            {data.length > 0 ? (
              <Grid container spacing={{ xs: 2, md: 1.5 }}>
                {!openData ? (
                  data.map((item, index) => (
                    <Grid key={index} item xs={12} sm={6} lg={4} xl={3}>
                      <Link
                        href={`/${item.snapIdenti}/Menu`}
                        onClick={() => {
                          setCookies("saleMethod", saleMethod);
                        }}
                      >
                        <RestaurantCard
                          item={item}
                          currentSaleMethod={saleMethod}
                        />
                      </Link>
                    </Grid>
                  ))
                ) : (
                  <Fragment>
                    {data
                      .filter(
                        (item) =>
                          item.saleMethod === saleMethods[saleMethod].code
                      )
                      .map((item, index) => (
                        <Grid key={index} item xs={12} sm={6} lg={4} xl={3}>
                          <Link
                            href={`/${item.snapIdenti}/Menu`}
                            onClick={() => {
                              setCookies("saleMethod", saleMethod);
                            }}
                          >
                            <RestaurantCard
                              item={item}
                              currentSaleMethod={saleMethod}
                            />
                          </Link>
                        </Grid>
                      ))}
                    {data
                      .filter(
                        (item) =>
                          item.saleMethod !== saleMethods[saleMethod].code
                      )
                      .map((item, index) => {
                        let differentSaleMethod = null;
                        for (const key in saleMethods) {
                          if (saleMethods[key].code === item.saleMethod)
                            differentSaleMethod = key;
                        }
                        return (
                          <Grid key={index} item xs={12} sm={6} lg={4} xl={3}>
                            <Link
                              href={`/${item.snapIdenti}/Menu`}
                              onClick={() => {
                                setCookies("saleMethod", differentSaleMethod);
                              }}
                            >
                              <RestaurantCard
                                item={item}
                                currentSaleMethod={saleMethod}
                                differentSaleMethod={differentSaleMethod}
                              />
                            </Link>
                          </Grid>
                        );
                      })}
                  </Fragment>
                )}
              </Grid>
            ) : (
              <AreaNoDataError />
            )}
          </Box>
        )}
      </Grid>
    </GrowContainer>
  );
};

export default AreaPartitionsList;
