import { useContext } from "react";
import { useTranslation } from "next-i18next";

import AreaPartitionsList from "./AreaPartitions.lists";

import { AreaPageContext } from "../../context/AreaPageContext";

const AreaFoodsList = () => {
  const { t } = useTranslation('area_page');
  const { shops, shopLoading, saleMethod } = useContext(AreaPageContext);

  // Split Data
  const open = [];
  const preOrder = [];
  const close = [];
  const nonIncludeOffer = [];

  shops &&
    shops.length > 0 &&
    shops.forEach((el) => {
      try {
        const { isOpen } = el;
        if (el.includeOffer === 1) {
          if (isOpen.toLowerCase() === "open") open.push(el);
          else if (isOpen.toLowerCase().startsWith("opening"))
            preOrder.push(el);
          else close.push(el);
        } else nonIncludeOffer.push(el);
      } catch (error) {}
    });

  return (
    <>
      <AreaPartitionsList
        // title="Taking orders"
        data={open}
        shopLoading={shopLoading}
        saleMethod={saleMethod}
        openData
      />

      {preOrder.length > 0 && !shopLoading && (
        <AreaPartitionsList
          title={t('area_pre_orders')}
          data={preOrder}
          shopLoading={shopLoading}
          saleMethod={saleMethod}
        />
      )}

      {close.length > 0 && !shopLoading && (
        <AreaPartitionsList
          title={t('area_currently_closed')}
          data={close}
          shopLoading={shopLoading}
          saleMethod={saleMethod}
        />
      )}

      {nonIncludeOffer.length > 0 && !shopLoading && (
        <AreaPartitionsList
          title={t('area_you_may_also_like')}
          data={nonIncludeOffer}
          shopLoading={shopLoading}
          saleMethod={saleMethod}
        />
      )}
    </>
  );
};

export default AreaFoodsList;
