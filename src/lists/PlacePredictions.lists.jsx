import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Grow from "@mui/material/Grow";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import LocationOnIcon from "@mui/icons-material/LocationOn";

import theme from "../themes/mealzoTheme";

const PlacePredictionsList = ({
  predictions,
  onPlaceSelect,
  ...restOfProps
}) => {
  return (
    <Grow {...restOfProps}>
      <div
        className="customized-predictions"
        style={{
          position: "absolute",
          top: 65,
          left: 0,
          width: "100%",
          backgroundColor: "#FFF",
          boxShadow: "0 0 6px #00000029",
          borderRadius: 4,
          zIndex: theme.zIndex.appBar + 1,
        }}
      >
        <Box
          sx={{
            p: 1,
            "&::after": {
              content: '""',
              padding: "1px 1px 1px 0",
              height: 18,
              boxSizing: "border-box",
              textAlign: "right",
              display: "block",
              backgroundImage:
                "url(https://maps.gstatic.com/mapfiles/api-3/images/powered-by-google-on-white3.png)",
              backgroundPosition: "right",
              backgroundRepeat: "no-repeat",
              backgroundSize: "120px 14px",
            },
          }}
        >
          <Box
            sx={{
              maxHeight: { xs: "34vh", sm: "unset" },
              overflowY: { xs: "auto", sm: "unset" },
              "&::-webkit-scrollbar": {
                width: 5,
              },
              "&::-webkit-scrollbar-thumb": {
                borderRadius: "15px",
              },
            }}
          >
            <List disablePadding>
              {predictions.length
                ? predictions.map((prediction, idx) => (
                    <ListItemButton
                      onClick={() => onPlaceSelect(prediction)}
                      sx={{ zIndex: theme.zIndex.appBar + 1, py: 0.5, borderRadius: "3px" }}
                      key={idx}
                    >
                      <ListItemIcon sx={{ minWidth: 45 }}>
                        <LocationOnIcon color="text.secondary" />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <Typography fontSize={14} fontWeight={500}>
                            {prediction.structured_formatting.main_text}
                          </Typography>
                        }
                        secondary={
                          <Typography fontSize={12}>
                            {prediction.structured_formatting.secondary_text}
                          </Typography>
                        }
                      />
                    </ListItemButton>
                  ))
                : null}
            </List>
          </Box>
        </Box>
      </div>
    </Grow>
  );
};

export default PlacePredictionsList;
