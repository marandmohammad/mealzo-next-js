import { Box, Grid, IconButton, Typography } from "@mui/material";

import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EditIcon from "@mui/icons-material/Edit";

import { buildStructuredAddress } from "../../utilities/appWideHelperFunctions";

function UserAddressViewList({
  userAddresses,
  isSelected,
  //   tabClickHandler,
  handleEditButtonClick,
  handleDeleteAddress,
}) {
  return userAddresses && userAddresses.length > 0 ? (
    userAddresses.map((item) => (
      <Grid
        item
        key={item.id}
        sx={{
          display: "flex",
          padding: 1,
          borderRadius: 1,
          // justifyContent: "space-between",
          alignItems: "center",
          border: "1px solid transparent",
          boxShadow: "0px 0px 24px #0000000D",
          marginTop: 3,
        }}
        className={`${isSelected === item.id && "isActive"}`}
      >
        <Box
          alignItems="center"
          display="flex"
          sx={{ cursor: "pointer", flex: 1 }}
          // onClick={() => tabClickHandler(item.id)}
        >
          <LocationOnIcon
            sx={{
              marginRight: 2,
              display: { xs: "none", md: "initial" },
              color: `${
                isSelected === item.id ? "primary.main" : "text.secondary"
              }`,
            }}
          />
          <Typography
            variant="caption"
            sx={{
              color: `${
                isSelected === item.id ? "primary.main" : "text.secondary"
              }`,
            }}
          >
            {item.deliveryAddressName
              ? item.deliveryAddressName
              : buildStructuredAddress(item)}
          </Typography>
        </Box>
        <Box>
          <IconButton
            width="fit-content"
            onClick={() => handleEditButtonClick(item)}
          >
            <EditIcon
              sx={{
                color: `${
                  isSelected === item.id ? "primary.main" : "text.secondary"
                }`,
              }}
            />
          </IconButton>
          <IconButton
            width="fit-content"
            onClick={() => handleDeleteAddress(item)}
          >
            <DeleteOutlinedIcon
              sx={{
                color: `${
                  isSelected === item.id ? "primary.main" : "text.secondary"
                }`,
              }}
            />
          </IconButton>
        </Box>
      </Grid>
    ))
  ) : (
    <Typography variant="h6" component="div" color="primary.main" fontSize={16}>
      You have no saved address !
    </Typography>
  );
}

export default UserAddressViewList;
