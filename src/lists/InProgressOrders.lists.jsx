import Grid from "@mui/material/Grid";

import OrdersListItem from "../listitems/Orders.listItems";

const ordersData = [
  {
    title: "Marmaris EdinBurgh",
    location: "weetech office",
    time: "12:00 AM",
    date: "24 may at 11:45",
    price: "9.99",
    status: "Delivered",
    item: "5",
    images: [
      "https://mealzo.co.uk/SiteContent/Images/HomePageHeader/131_Darioshawlands.jpg",
      "https://mealzo.co.uk/SiteContent/Images/HomePageHeader/131_Darioshawlands.jpg",
      "https://mealzo.co.uk/SiteContent/Images/HomePageHeader/131_Darioshawlands.jpg",
    ],
  },
];

const InProgressOrdersList = () => {
  return (
    <Grid container>
      {ordersData.map((order, index) => (
        <OrdersListItem key={index} order={order} />
      ))}
    </Grid>
  );
};

export default InProgressOrdersList;
