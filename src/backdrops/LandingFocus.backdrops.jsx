import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

const LandingFocusBackdrops = styled(Stack)(
  ({ theme }) => `
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.5);
    z-index: ${theme.zIndex.appBar + 1};
    backdrop-filter: blur(2px);
    -webkit-backdrop-filter: blur(2px);
`
);

export default LandingFocusBackdrops;
