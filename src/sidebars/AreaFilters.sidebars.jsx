import { useState, useContext } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
//MUI
import { Checkbox, Typography, Box } from "@mui/material";
import KeyboardArrowDownRoundedIcon from "@mui/icons-material/KeyboardArrowDownRounded";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

//custom MUI Styled
import {
  FiltersContainer,
  FiltersTitle,
  FiltersSort,
  FilterSortTitle,
  FilterSortContent,
  CustomRadioBtn,
  FiltersOffers,
  FilterOffersContent,
  CustomRadioLabel,
  FiltersCategory,
  CheckBoxGroup,
  FilterCategoriesContent,
  CustomFiltersButton,
} from "./AreaFilters.sidebars.styles";

import { AreaPageContext } from "../../context/AreaPageContext";
import {
  areaCategoryChangeHandler,
  areaFilterChangeHandler,
  areaFilterAndSearchReset,
} from "../../utilities/areaPathHandlers";

const AreaFiltersSideBar = () => {
  const { t } = useTranslation("area_page");

  //filter expands toggle
  const [sortOpen, setSortOpen] = useState(false);
  const [offerOpen, setOfferOpen] = useState(true);
  const [categoryOpen, setCategoryOpen] = useState(true);

  // Order: 1. Data   2. State   3. SetState
  const {
    // Category Group
    categoriesData,
    categories,
    setCategories,
    categoryQueryKeyWord,

    // Sort Group
    sortData,
    sort,
    setSort,
    sortQueryKeyWord,

    // Offers Group
    offersData,
    offer,
    setOffer,
    offerQueryKeyWord,

    // Shop Group
    setShops,
    shopLoading,
    setShopLoading,

    setFilterOpen,
  } = useContext(AreaPageContext);

  const router = useRouter();

  // categories Change Handler
  const onChangeCategories = (e) => {
    areaCategoryChangeHandler(
      router,
      e.target.value,
      e.target.name,
      categories,
      setCategories,
      categoryQueryKeyWord,
      setShops,
      shopLoading,
      setShopLoading
    );
  };

  // sort Change handler
  const onChangeSort = (e) => {
    areaFilterChangeHandler(
      router,
      sortQueryKeyWord,
      parseInt(e.target.value),
      setSort,
      sortData,
      setShops,
      shopLoading,
      setShopLoading
    );
  };

  // offers Change handler
  const onChangeOffer = (e) => {
    areaFilterChangeHandler(
      router,
      offerQueryKeyWord,
      parseInt(e.target.value),
      setOffer,
      offersData,
      setShops,
      shopLoading,
      setShopLoading
    );
  };

  return (
    // <GrowContainer style={{ position: "sticky", top: "2%", zIndex: 9999 }}>
    <FiltersContainer>
      <Box
        sx={{
          overflowY: "auto",
          "&::-webkit-scrollbar": {
            width: "8px",
            marginRight: "5px",
          },
          "&::-webkit-scrollbar-thumb": {
            borderRadius: "10px",
            background: "#c0c2c269",
          },
          pr: 1,
        }}
      >
        <FiltersTitle>{t("area_filters_header")}</FiltersTitle>
        <FiltersSort>
          <FilterSortTitle onClick={() => setSortOpen(!sortOpen)}>
            <div>
              <KeyboardArrowDownRoundedIcon
                fontSize="10px"
                sx={{
                  transform: `rotate(${sortOpen ? "180deg" : "0"})`,
                  transition: "transform 0.25s ease-in",
                  mr: 1,
                }}
              />
              {t("area_filters_sort")}
            </div>

            <Typography
              sx={{ cursor: "pointer" }}
              fontSize="small"
              color="primary.main"
              onClick={() =>
                areaFilterAndSearchReset(
                  router,
                  sortQueryKeyWord,
                  setSort,
                  setShops,
                  shopLoading,
                  setShopLoading
                )
              }
            >
              {t("area_filters_reset")}
            </Typography>
          </FilterSortTitle>

          <FilterSortContent sortOpen={sortOpen}>
            <RadioGroup name="sort" value={sort} onChange={onChangeSort}>
              {sortData.map(({ id, title }) => (
                <FormControlLabel
                  sx={{
                    fontSize: "12px",
                  }}
                  value={id}
                  control={<CustomRadioBtn size="small" />}
                  label={<CustomRadioLabel>{title}</CustomRadioLabel>}
                  key={id}
                />
              ))}
            </RadioGroup>
          </FilterSortContent>
        </FiltersSort>

        {/* Offer Filters */}
        <FiltersOffers>
          <FilterSortTitle onClick={() => setOfferOpen(!offerOpen)}>
            <div>
              <KeyboardArrowDownRoundedIcon
                fontSize="10px"
                sx={{
                  transform: `rotate(${offerOpen ? "180deg" : "0"})`,
                  transition: "transform 0.25s ease-in",
                  mr: 1,
                }}
              />
              {t("area_filters_offers")}
            </div>
            <Typography
              sx={{ cursor: "pointer" }}
              fontSize="small"
              color="primary.main"
              onClick={() =>
                areaFilterAndSearchReset(
                  router,
                  offerQueryKeyWord,
                  setOffer,
                  setShops,
                  shopLoading,
                  setShopLoading
                )
              }
            >
              {t("area_filters_reset")}
            </Typography>
          </FilterSortTitle>
          <FilterOffersContent offerOpen={offerOpen}>
            <RadioGroup
              aria-labelledby="offers-controlled-radio-buttons-group"
              name="offers"
              value={offer}
              onChange={onChangeOffer}
            >
              {offersData.map(({ id, title, countOfOffer }) => (
                <FormControlLabel
                  value={id}
                  control={<CustomRadioBtn size="small" />}
                  label={
                    <CustomRadioLabel>
                      <span>{title}</span>
                      <span>({countOfOffer})</span>
                    </CustomRadioLabel>
                  }
                  key={id}
                />
              ))}
            </RadioGroup>
          </FilterOffersContent>
        </FiltersOffers>

        {/* Category Filters */}
        <FiltersCategory>
          <FilterSortTitle onClick={() => setCategoryOpen(!categoryOpen)}>
            <div>
              <KeyboardArrowDownRoundedIcon
                fontSize="10px"
                sx={{
                  transform: `rotate(${categoryOpen ? "180deg" : "0"})`,
                  transition: "transform 0.25s ease-in",
                  mr: 1,
                }}
              />
              {t("area_filters_categories")}
            </div>
            <Typography
              sx={{ cursor: "pointer" }}
              fontSize="small"
              color="primary.main"
              onClick={() =>
                areaFilterAndSearchReset(
                  router,
                  categoryQueryKeyWord,
                  setCategories,
                  setShops,
                  shopLoading,
                  setShopLoading,
                  categoriesData
                )
              }
            >
              {t("area_filters_reset")}
            </Typography>
          </FilterSortTitle>
          <FilterCategoriesContent
            categoryOpen={categoryOpen}
            itemsCount={categoriesData.length}
          >
            <CheckBoxGroup>
              {categoriesData.map((category, index) => (
                <FormControlLabel
                  value={category.id}
                  name={category.title}
                  control={
                    <Checkbox
                      size="small"
                      sx={{ color: "checkbox" }}
                      checked={categories[category.id]}
                    />
                  }
                  label={
                    <CustomRadioLabel>
                      <span>{category.title}</span>
                      <span>({category.countOfSubject})</span>
                    </CustomRadioLabel>
                  }
                  onChange={onChangeCategories}
                  key={index}
                />
              ))}
            </CheckBoxGroup>
          </FilterCategoriesContent>
        </FiltersCategory>
      </Box>
      <Box
        sx={{
          padding: "16px",
          bottom: 0,
          boxShadow: "0 1px 4px #00000029",
          marginTop: "auto",
          position: "sticky",
          display: { xs: "block", md: "none" },
          backgroundColor: "#fff",
        }}
      >
        <CustomFiltersButton onClick={() => setFilterOpen(false)}>
          {t("area_filters_done")}
        </CustomFiltersButton>
      </Box>
    </FiltersContainer>
    // </GrowContainer>
  );
};

export default AreaFiltersSideBar;
