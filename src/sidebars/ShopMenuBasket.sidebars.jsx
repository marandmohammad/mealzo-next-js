import { useState } from "react";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRouter } from "next/router";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";
// import useLocation from "../../hooks/useLocation";

import { Box, Grid, Typography } from "@mui/material";

import PrimaryButton from "../buttons/Primary.buttons";
import BasketOrderDetailListItem from "../listitems/BasketOrderDetail.listItems";
import NumberCountCarousel from "../carousels/NumberCount.carousels";

import axiosConfig from "../../config/axios";
import currencyIdToMark from "../../utilities/currencyIdToMark";

import EmptyBasketImage from "../../public/Images/emptybasket.png";
// import { saleMethods } from "../../utilities/areaPathHandlers";

const ShopMenuBasketSidebar = ({
  basketData,
  shopDetail,
  basketUpdateHandler,
  saleMethod,
  postCode,
  gotoCheckOutButton = true,
  orderDetailShopIdCurrencyId = null,
  showTitle = true,
  fullHeight = false,
  absoluteButton = false,
  sx,
}) => {
  const router = useRouter();
  const { t } = useTranslation("basket");
  const { checkMinimumDelivery } = useOrder();
  const { showNotification } = useNotification();
  // const { getSelectedAddress, addSelectedAddress } = useLocation();

  const [inDecLoading, setInDecLoading] = useState(false);

  const {
    orderDetail,
    deliveryCharge,
    layoltiUsedAmount,
    totalDiscount,
    amountForPay,
    specialNote,
    surcharge,
    carrierBag,
    asap,
    carrierBagChargeLabel,
    creditCardSurchargeLabel,
    orderId,
  } = basketData || 0;
  const { currencyId, shopId } = shopDetail
    ? shopDetail?.address
    : orderDetailShopIdCurrencyId;

  const handleChangeCount = async (orderDetailId, count) => {
    const reqData = {
      shopId: shopId,
      orderId: orderId,
      orderDetailId: orderDetailId,
      count: count,
    };
    setInDecLoading(true);
    let res = null;
    try {
      res = await (
        await axiosConfig.patch("/Basket/UpdateCount", reqData)
      ).data;
      basketUpdateHandler(res);
    } catch (e) {
      alert("There was a problem adding item to basket!");
    }
    setInDecLoading(false);
  };

  const gotoCheckOut = async () => {
    if (orderDetail.length > 0)
      if (saleMethod === "delivery") {
        const isMinimumCheckOk = await checkMinimumDelivery(postCode);
        if (isMinimumCheckOk) router.push(`/checkout/${shopId}`);
      } else router.push(`/checkout/${shopId}`);
    else showNotification("Your basket is empty!", "error");
  };

  return (
    <Grid
      container
      boxShadow="0px 3px 6px #00000029"
      position="sticky"
      top={5}
      height={fullHeight ? "calc(100vh - 122px)" : "max-content"}
      alignItems="start"
      sx={sx}
    >
      <Grid item container px={2} py={3} mb={1}>
        {showTitle && orderDetail && orderDetail.length > 0 && (
          <Grid container mb={3}>
            <Typography variant="h6">{t("basket_title")}</Typography>
          </Grid>
        )}

        {orderDetail && orderDetail.length ? (
          <Grid container>
            {/*<Grid item flexGrow={1}>*/}
            {/*  <Typography*/}
            {/*    variant="subtitle1"*/}
            {/*    color="text.newMedium"*/}
            {/*    fontSize={16}*/}
            {/*    mb={0.5}*/}
            {/*  >*/}
            {/*    Free delivery for orders above £14.99*/}
            {/*  </Typography>*/}
            {/*</Grid>*/}

            {/*<Grid container>*/}
            {/*  <Grid item xs={10}>*/}
            {/*    <Slider size="small" />*/}
            {/*  </Grid>*/}
            {/*  <Grid item xs={2}>*/}
            {/*    <Typography*/}
            {/*      variant="subtitle1"*/}
            {/*      color="primary.main"*/}
            {/*      fontSize={12}*/}
            {/*      textAlign="right"*/}
            {/*    >*/}
            {/*      £11.50*/}
            {/*    </Typography>*/}
            {/*  </Grid>*/}
            {/*</Grid>*/}

            <Box
              sx={{
                maxHeight: { xs: "50vh", sm: "70vh", md: "67vh" },
                overflowY: "auto",
                overflowX: "hidden",
                width: "100%",
                "&::-webkit-scrollbar": {
                  width: 4,
                  bgcolor: "transparent",
                },
                "&::-webkit-scrollbar-thumb": {
                  borderRadius: "5px",
                  bgcolor: "#e9540dad",
                },
                pr: 1,
              }}
            >
              {orderDetail.map((el, index) => (
                <BasketOrderDetailListItem
                  orderDetail={el}
                  currencyId={currencyId}
                  loading={inDecLoading}
                  handleChangeCount={handleChangeCount}
                  changeableCount={gotoCheckOutButton}
                  key={index}
                />
              ))}
            </Box>

            <Grid container mt={3}>
              {/* Total Discount */}
              <Grid container justifyContent="space-between">
                <Grid item>
                  <Typography variant="subtitle1" fontSize={12}>
                    {t("basket_discount")}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle1" fontSize={12}>
                    {currencyIdToMark(currencyId, totalDiscount)}
                  </Typography>
                </Grid>
              </Grid>

              {/* Carrier Bag */}
              {carrierBag > 0 && (
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {carrierBagChargeLabel}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {currencyIdToMark(currencyId, carrierBag)}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {/* Sure Charge */}
              {surcharge > 0 && (
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {creditCardSurchargeLabel}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {currencyIdToMark(currencyId, surcharge)}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {layoltiUsedAmount > 0 && (
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {t("basket_loyalty_used_amount")}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {currencyIdToMark(currencyId, layoltiUsedAmount)}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {/* Delivery Fee */}
              {saleMethod === "delivery" && (
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {t("basket_delivery_fee")}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="subtitle1" fontSize={12}>
                      {currencyIdToMark(currencyId, deliveryCharge)}
                    </Typography>
                  </Grid>
                </Grid>
              )}

              {/* Total */}
              <Grid container justifyContent="space-between">
                <Grid item>
                  <Typography variant="h6" color="primary" fontSize={14}>
                    {t("basket_total")}
                  </Typography>
                </Grid>
                <Grid item overflowY="hidden">
                  <Typography variant="h6" fontSize={14}>
                    <NumberCountCarousel
                      number={currencyIdToMark(currencyId, amountForPay)}
                    />
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        ) : (
          <Grid
            container
            py={10.2}
            flexDirection="column"
            textAlign="center"
            alignItems='center'
          >
            <Grid item position="relative"  width={200} height={200}>
              <Image src={EmptyBasketImage} layout='fill' />
            </Grid>
            <Typography variant="h6" color="text.newMedium">
              Your basket is empty!
            </Typography>
            <Typography variant="h6" color="text.secondary" fontWeight={400}>
              Get started
            </Typography>
          </Grid>
        )}
      </Grid>

      {gotoCheckOutButton && orderDetail && orderDetail.length > 0 && (
        <Grid
          item
          xs={12}
          sx={
            absoluteButton
              ? {
                  position: "fixed",
                  bottom: 0,
                  width: "100%",
                }
              : {
                  mt: 2,
                }
          }
        >
          <PrimaryButton
            onClick={gotoCheckOut}
            sx={{
              width: "100%",
              // mt: 2,
              fontSize: "20px",
              borderRadius: 0,
              maxHeight: 50,
            }}
          >
            {t("basket_go_to_checkout")}
          </PrimaryButton>
        </Grid>
      )}
    </Grid>
  );
};

export default ShopMenuBasketSidebar;
