import { styled } from "@mui/system";
import { Box, Typography } from "@mui/material";
import Radio from "@mui/material/Radio";

export const FiltersContainer = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  color: theme.palette.text.medium,
  gap: theme.spacing(2),
  // position: "fixed",
  // top: 0,
  // left: 0,
  // zIndex: 1300,
  width: "100%",
  height: "100%",
  backgroundColor: "white",
  marginTop: 15,
  padding: "15px 15px 15px 10px",
  [theme.breakpoints.up("md")]: {
    position: "sticky",
    top: "2%",
    boxShadow: "0px 3px 6px #00000029",
    borderRadius: "5px",
    maxHeight: "95vh",
    // marginBottom: "20px"
  },
}));

export const FiltersTitle = styled("div")((theme) => ({
  fontWeight: 700,
  fontSize: "16px",
  margin: "8px",
  paddingBottom: 10,
  // paddingRight: 10,
  borderBottom: "1px solid #d8d8d8",
}));

export const FiltersSort = styled(Box)(() => ({
  fontWeight: 600,
  fontSize: "14px",
  height: "auto",
  padding: "8px",
}));

export const FilterSortTitle = styled(Box)(() => ({
  display: "flex",
  alignItems: "center",
  alignSelf: "start",
  gap: "2px",
  marginBottom: "10px",
  justifyContent: "space-between",
  cursor: "pointer",
}));

export const FilterSortContent = styled(Box)((props) => ({
  display: "flex",
  flexDirection: "column",
  paddingLeft: "8px",
  gap: "5px",
  color: "#474747",
  maxHeight: `${props.sortOpen ? "250px" : "0"}`,
  overflow: "hidden",
  transition: "max-height 0.45s ease-in",
  borderBottom: "1px solid #d8d8d8",
}));

export const CustomRadioBtn = styled(Radio)(({ theme }) => ({
  color: theme.palette.checkbox,
  "&.MuiRadio-root": {
    padding: "6px",
  },
}));

export const CustomRadioLabel = styled(Typography)(({ theme }) => ({
  color: 'theme.palette.text.medium',
  fontSize: "12px",
  display: "flex",
  gap: "4px",
}));

export const FiltersOffers = styled(Box)(() => ({
  fontWeight: 600,
  fontSize: "14px",
  height: "auto",
  padding: "8px",
}));

export const FilterOffersContent = styled(Box)((props) => ({
  display: "flex",
  flexDirection: "column",
  paddingLeft: "8px",
  gap: "8px",
  color: props.theme.palette.text.medium,
  maxHeight: `${props.offerOpen ? "250px" : "0"}`,
  overflow: "hidden",
  transition: "max-height 0.45s ease-in",
  borderBottom: "1px solid #d8d8d8",
}));

export const FiltersCategory = styled(Box)(() => ({
  fontWeight: 600,
  fontSize: "14px",
  height: "auto",
  padding: "8px",
}));

export const FilterCategoriesContent = styled(Box)(({theme, itemsCount, categoryOpen}) => ({
  display: "flex",
  flexDirection: "column",
  paddingLeft: "8px",
  // gap: "8px",
  color: theme.palette.text.medium,
  maxHeight: `${categoryOpen ? itemsCount * 45 + "px" : 0}`,
  overflow: "hidden",
  transition: "max-height 0.45s ease-in",
  borderBottom: "1px solid #d8d8d8",
}));

export const CheckBoxGroup = styled(Box)(() => ({
  display: "flex",
  flexDirection: "column",
  // gap: "4px",
}));

export const CustomFiltersButton = styled("button")(({ theme }) => ({
  width: "100%",
  padding: "10px",
  backgroundColor: theme.palette.primary.main,
  color: "#fff",
  border: "none",
  borderRadius: theme.spacing(0.5),
  fontSize: "18px",
  fontWeight: "bold",
}));
