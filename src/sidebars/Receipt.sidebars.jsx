import Box from "@mui/material/Box";

import ReceiptCard from "../cards/Receipt.cards";

// import RepeatImg from "../../public/Images/repeat.png";

const ReceiptSidebar = ({ order }) => {
  return (
    <>
      <Box sx={{ display: { xs: "block", md: "block" } }}>
        <img
          src={"/Images/repeat.png"}
          style={{ width: "100%", transform: "rotate(180deg)" }}
          alt=""
        />
      </Box>
      <ReceiptCard order={order} />
      <Box sx={{ display: { xs: "block", md: "block" } }}>
        <img src={"/Images/repeat.png"} style={{ width: "100%" }} alt="" />
      </Box>
    </>
  );
};

export default ReceiptSidebar;
