import { v4 as uuidv4 } from "uuid";
import { useTranslation } from "next-i18next";

// Components
import TagsCard from "../cards/BlogsTags.card";
import ArchiveDataCard from "../cards/BlogsArchiveData.cards";
import SidebarDateCards from "../cards/BlogsSideBarDate.cards";

// Mui Components
import { Divider, Grid, Typography } from "@mui/material";

import Link from "../links/Link";

import { queryParamEncode } from "../../utilities/areaPathHandlers";

const BlogsSidebar = ({ tags, recentPosts, archive }) => {
  const { t } = useTranslation('blog_pages');
  
  return (
    <Grid
      container
      sx={{
        flexDirection: "column",
        p: 3,
        boxShadow: 2,
        maxHeight: 750,
        position: { md: "sticky", xs: "none" },
        top: 20,
        borderRadius: "5px"
      }}
    >
      <Typography variant="h6" fontSize="16px" color="primary">
        {t('tags')}
      </Typography>
      {/* tags */}
      <Grid container direction="row" flexWrap="wrap" spacing={0.5} my={2}>
        {tags.map(({ id, title }, index) => (
          <Link href={`/OurBlog/tag/${queryParamEncode(title)}`} key={uuidv4()}>
            <TagsCard tag={title} />
          </Link>
        ))}
      </Grid>
      <Divider color="#E5DCC3" />
      <Grid container my={2}>
        <Typography color="primary" variant="h6" fontSize={16} mb={3}>
          {t('recent_articles')}
        </Typography>
        {recentPosts.map((item, index) => (
          <Link
            href={`/OurBlog/${item.category}/${queryParamEncode(
              item.blogIdentity
            )}`}
            width="100%"
            key={uuidv4()}
          >
            <SidebarDateCards data={item} />
          </Link>
        ))}
      </Grid>
      <Divider color="#E5DCC3" />
      <Grid container flexDirection="column" my={2}>
        <Typography color="primary" variant="h6" fontSize={16} mb={2}>
          {t('archive_by_date')}
        </Typography>
        {archive.map((item, index) => (
          <Link href={`/OurBlog/archive/${item.dateYear}`} key={uuidv4()}>
            <ArchiveDataCard data={item} />
          </Link>
        ))}
      </Grid>
    </Grid>
  );
};

export default BlogsSidebar;
