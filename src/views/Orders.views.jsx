import { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";
import useNotification from "../../hooks/useNotification";

//Mui
import { Grid, Typography } from "@mui/material";

import AddressPageLoading from "../loadings/AddressPage.loadings";
import PageContainer from "../containers/Page.containers";

import PreviousOrdersList from "../lists/PreviousOrders.lists";
import axiosConfig from "../../config/axios";

// function TabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`tabpanel-${index}`}
//       aria-labelledby={`tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box sx={{ p: 3 }}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// function a11yProps(index) {
//   return {
//     id: `tab-${index}`,
//     "aria-controls": `tabPanel-${index}`,
//   };
// }

const OrdersView = () => {
  const { t } = useTranslation('previous_orders_page')
  const { getUser } = useAuth();
  const { showNotification } = useNotification();

  const [previousOrders, setPreviousOrders] = useState([]);
  const [loading, setLoading] = useState(true);
  // const [value, setValue] = useState(0);

  // const changeHandler = (event, newValue) => {
  //   setValue(newValue);
  // };

  useEffect(() => {
    const getOrders = async () => {
      const { customerId } = getUser();
      setLoading(true);
      try {
        const orders = await (
          await axiosConfig.get(`/Orders/${customerId}`)
        ).data;

        setPreviousOrders(orders);
      } catch (error) {
        showNotification(error.message, "error");
      }

      setLoading(false);
    };

    getOrders();
  }, []);

  return (
    <PageContainer>
      <Grid
        item
        // width="100%"
        xs={12}
        sm="auto"
        md="auto"
        // bgcolor="divider"
        // borderRadius="50px"
        p="7px 10px"
        mb={{ xs: 2, sm: 4, md: 7 }}
      >
        <Typography variant="h6">
          {t('list_title')} (
          {previousOrders ? previousOrders.length : previousOrders})
        </Typography>
        {/* <Tabs
          indicatorColor="transparent"
          value={value}
          onChange={changeHandler}
          aria-label="tabs"
          sx={{
            ".css-1nvvwk6-MuiButtonBase-root-MuiTab-root.Mui-selected": {
              backgroundColor: "background.default",
              color: "text.secondary",
            },
            ".css-1nvvwk6-MuiButtonBase-root-MuiTab-root": {
              p: "5px 25px",
              width: "50%",
              whiteSpace: "nowrap",
            },
          }}
        >
          <Tab
            label="In Progress Orders (1)"
            {...a11yProps(0)}
            sx={{
              color: "text.secondary",
              fontSize: { xs: 12, sm: 12, md: 18 },
              borderRadius: "24px",
            }}
          />
          <Tab
            label="Previous Orders (100)"
            {...a11yProps(1)}
            sx={{
              color: "text.secondary",
              fontSize: { xs: 12, sm: 12, md: 18 },
              borderRadius: "24px",
            }}
          />
        </Tabs> */}
      </Grid>
      <Grid item xs={12}>
        {/* <TabPanel value={value} index={0}>
          <InProgressOrdersLists />
        </TabPanel> */}
        {/* <TabPanel value={value} index={1}> */}
        {loading ? (
          <AddressPageLoading />
        ) : (
          <PreviousOrdersList previousOrders={previousOrders} />
        )}

        {/* </TabPanel> */}
      </Grid>
    </PageContainer>
  );
};

export default OrdersView;
