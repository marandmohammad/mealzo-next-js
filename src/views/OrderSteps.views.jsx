import { Fragment } from "react";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import useLocation from "../../hooks/useLocation";

import { Box, Grid, Typography } from "@mui/material";

import OrderStepsHeaderCard from "../cards/OrderStepsHeader.cards";
import PrimaryButton from "../buttons/Primary.buttons";
import Link from "../links/Link";
// import OrderReviewRatingForm from "../forms/OrderReviewRating.forms";

import {
  queryKeyWords,
  queryParamEncode,
} from "../../utilities/areaPathHandlers";

const OrderStepsView = ({ shopDetail, saleMethod, orderNumber }) => {
  const router = useRouter();
  const { t } = useTranslation("order_steps_page");
  const { postCode } = useLocation();
  const { postCodeQueryKeyWord, saleMethodQueryKeyWord } = queryKeyWords;
  const { telePhone, shopTitle } = shopDetail.address;

  return (
    <Fragment>
      <OrderStepsHeaderCard
        data={shopDetail}
        saleMethod={saleMethod}
        orderNumber={orderNumber}
      />
      <Grid
        container
        mt={5}
        flexDirection="column"
        gap={2}
        px={{ xs: 2, md: 0 }}
      >
        <Box>
          <Typography variant="subtitle1">
            {t('contact_message')}{" "}
            <Link href={`tel:${telePhone}`}>{telePhone}</Link>
          </Typography>
          {/* <Typography variant="subtitle1">
            Liked Your Experience ?{" "}
            <Typography
              component="span"
              sx={{ color: "primary.light", fontSize: 16 }}
            >
              Rate Us
            </Typography>
          </Typography> */}
        </Box>
        <Box sx={{ display: "flex", gap: 1 }}>
          <PrimaryButton
            sx={{ p: "8px 28px", fontSize: 14 }}
            onClick={() =>
              router.push({
                pathname: "/area/[area]",
                query: {
                  [postCodeQueryKeyWord]: queryParamEncode(postCode),
                  [saleMethodQueryKeyWord]: saleMethod,
                },
              })
            }
          >
            {t('keep_shopping_btn')}
          </PrimaryButton>
          {/* <PrimaryButton
            sx={{
              p: "8px 28px",
              fontSize: 14,
              width: "154px",
              bgcolor: "background.white",
              color: "primary.light",
              "&:hover": {
                bgcolor: "background.white",
              },
              border: "1px solid",
              borderColor: "primary.light",
            }}
          >
            Re Order
          </PrimaryButton> */}
        </Box>
      </Grid>

      {/* Order Review */}
      {/* <Grid container mt={5}>
        <OrderReviewRatingForm title={`How Was ${shopTitle} ?!`} />
      </Grid> */}
    </Fragment>
  );
};

export default OrderStepsView;
