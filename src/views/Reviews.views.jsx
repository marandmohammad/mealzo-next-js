import { Fragment } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { Box, Grid } from "@mui/material";

// import PrimaryButton from "../buttons/Primary.buttons";
import OrderReviewRatingForm from "../forms/OrderReviewRating.forms";
// import Link from '../links/Link';

// import {
  // queryKeyWords,
  // queryParamEncode,
// } from "../../utilities/areaPathHandlers";

const ReviewsView = ({
  shopDetail,
  saleMethod,
  orderNumber,
  postCode,
  reviewQuestions,
  reviewIdentity,
}) => {
  const { t } = useTranslation("order_steps_page");
  const router = useRouter();
  // const { postCodeQueryKeyWord, saleMethodQueryKeyWord } = queryKeyWords;
  const { shopTitle } = shopDetail;

  const onSuccessfulFormSubmit = () => {
    setTimeout(() => {
      router.replace("/");
    }, 1000);
  };

  return (
    <Fragment>
      {/* <Grid
        container
        mt={5}
        flexDirection="column"
        gap={2}
        px={{ xs: 2, md: 0 }}
      >
        <Box sx={{ display: "flex", gap: 1 }}>
          <PrimaryButton
            sx={{ p: "8px 28px", fontSize: 14 }}
            LinkComponent={Link}
            href={`/${router.locale}/area/${queryParamEncode(postCode)}?${saleMethodQueryKeyWord}=${saleMethod}`}
            // onClick={() =>
              // router.push(`/area/${queryParamEncode(postCode)}?${saleMethodQueryKeyWord}=${saleMethod}`
                // {
                //   pathname: "/area/[area]",
                //   query: {
                //     [postCodeQueryKeyWord]: queryParamEncode(postCode),
                //     [saleMethodQueryKeyWord]: saleMethod,
                //   },
                // }
              // )
            // }
          >
            {t("keep_shopping_btn")}
          </PrimaryButton>
        </Box>
      </Grid> */}

      {/* Order Review */}
      <Grid container mt={0} px={{xs: 2, sm: 4, md: 0}}>
        <OrderReviewRatingForm
          title={`${t("order_review_form_title")} ${shopTitle} ?!`}
          rawQuestions={reviewQuestions}
          reviewIdentity={reviewIdentity}
          onSuccessCallBack={onSuccessfulFormSubmit}
        />
      </Grid>
    </Fragment>
  );
};

export default ReviewsView;
