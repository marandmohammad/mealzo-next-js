import { Fragment } from "react";
import { useTranslation } from "next-i18next";

import { Grid, Typography } from "@mui/material";

import LandingRecommendOptionsCard from "../cards/LandingRecommendOptions.cards";
import LandingSectionContainer from "../containers/LandingSection.containers";
import DataError from "../errors/Data.errors";
import TopPicksCard from "../cards/TopPicks.cards";
import LandingTopCuisinesList from "../lists/LandingTopCuisines.lists";
import Partner from "../Partner/Partner";

// import LandingData from "../../utilities/LandingData";
// const { RecommendOptions } = LandingData;
// console.log(JSON.stringify(RecommendOptions));
function LandingView({ topPicks, topCuisines, chipClickHandler }) {
  const { t } = useTranslation('home_page');

  return (
    <Fragment>
      <LandingSectionContainer
        flexWrap="wrap-reverse"
        sx={{ mt: { xs: 10, sm: 20, md: 10 } }}
      >
        {/* {RecommendOptions.map(
          ({ title, subtitle, imageUrl, buttonText, href }, index) => {
            return (
              <LandingRecommendOptionsCard
                title={title}
                subTitle={subtitle}
                iconUrl={imageUrl}
                buttonText={buttonText}
                href={href}
                key={index}
                px={2}
                my={{ md: 0, xs: 3 }}
                md={4}
              />
            );
          }
        )} */}
        <LandingRecommendOptionsCard
          title={t('ro_find_local_flavors_title')}
          subTitle={t('ro_find_local_flavors_text')}
          iconUrl="/Images/yourlocal.svg"
          buttonText={t('ro_find_local_flavors_button_text')}
          href="/local-flavours"
          px={2}
          my={{ md: 0, xs: 3 }}
          md={4}
        />
        <LandingRecommendOptionsCard
          title={t('ro_special_perks_title')}
          subTitle={t('ro_special_perks_text')}
          iconUrl="/Images/SpecialPerks.svg"
          buttonText={t('ro_special_perks_button_text')}
          href="/Special-Perks"
          px={2}
          my={{ md: 0, xs: 3 }}
          md={4}
        />
        <LandingRecommendOptionsCard
          title={t('ro_get_app_title')}
          subTitle={t('ro_get_app_text')}
          iconUrl="/Images/GettheApp1.svg"
          buttonText={t('ro_get_app_button_text')}
          href='/Apps'
          px={2}
          my={{ md: 0, xs: 3 }}
          md={4}
        />
      </LandingSectionContainer>

      {topPicks ? (
        topPicks.map(({ groupId, groupName, topPicks }, index) => (
          <LandingSectionContainer
            justifyContent={{
              md: "space-between",
              sm: "space-between",
              xs: "center",
            }}
            py={6}
            sx={{
              my: 0,
              bgcolor: index % 2 === 0 ? "rgba(255,196,168,.2)" : null,
            }}
            key={groupId}
          >
            <Grid item xs={12} mb={6}>
              <Typography
                variant="h6"
                component="div"
                fontSize={{ sm: 40, xs: "7vw" }}
                textAlign="center"
              >
                {groupName}
              </Typography>
            </Grid>
            <Grid
              item
              container
              xs={12}
              sx={{
                overflowX: "scroll",
                flexDirection: "row",
                flexWrap: "nowrap",
                scrollSnapType: "x mandatory",
                "&::-webkit-scrollbar": {
                  display: "none",
                },
              }}
            >
              {topPicks.map(
                ({ shopTitle, shopLogo, homePageBanner }, index) => (
                  <TopPicksCard
                    brandTitle={shopTitle}
                    brandLogo={shopLogo}
                    imageUrl={homePageBanner}
                    onClick={chipClickHandler}
                    key={index}
                    mx={1}
                    sx={{
                      minWidth: 300,
                      scrollSnapAlign: "start",
                    }}
                    item
                    xs={12}
                    sm={3.9}
                    md={3.8}
                  />
                )
              )}
            </Grid>
          </LandingSectionContainer>
        ))
      ) : (
        <DataError />
      )}

      {/* Top Cuisines */}
      <LandingSectionContainer sx={{ mb: 0 }}>
        {topCuisines ? (
          <LandingTopCuisinesList
            topCuisines={topCuisines}
            focusHandler={chipClickHandler}
          />
        ) : (
          <DataError />
        )}
      </LandingSectionContainer>

      {/* Partners */}
      <LandingSectionContainer>
        <Partner />
      </LandingSectionContainer>
    </Fragment>
  );
}

export default LandingView;
