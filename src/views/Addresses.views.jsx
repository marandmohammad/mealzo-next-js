import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { useAuth } from "../../hooks/authentication";
import useNotification from "../../hooks/useNotification";

// Mui Components
import { Box, Stack, Typography, Grid, Button, useTheme } from "@mui/material";

// Mui Icons
import AddIcon from "@mui/icons-material/Add";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";

import UserAddressViewList from "../lists/UserAddressView.lists";
import RegularIconsButton from "../buttons/RegularIcon.buttons";
import AddNewAddressModal from "../modals/AddNewAddress.modals";
import AddressPageLoading from "../loadings/AddressPage.loadings";
import GrowContainer from "../containers/Grow.containers";

import theme from "../themes/mealzoTheme";
import axiosConfig from "../../config/axios";

const AddressesView = () => {
  const { t } = useTranslation("addresses");
  const mealzoTheme = useTheme(theme);
  const router = useRouter();
  const { getUser } = useAuth();
  // const { addSelectedAddress } = useLocation();
  const { customerId } = getUser();
  const { showNotification } = useNotification();

  const [userAddresses, setUserAddresses] = useState([]);
  // const [address, setAddress] = useState(null);
  const [editData, setEditData] = useState(null);

  const [addAddressOpen, setAddAddressOpen] = useState(false);
  const handleAddressModal = () => setAddAddressOpen(!addAddressOpen);

  const [isSelected, setIsSelected] = useState(false);
  const [loading, setLoading] = useState(true);

  //! Get User Addresses
  useEffect(() => {
    setLoading(true);
    axiosConfig
      .get(`/DeliveryAddresses/${customerId}`)
      .then(({ data }) => {
        setUserAddresses(data);
        setLoading(false);
      })
      .catch((e) => {
        showNotification(e.message, "error", 3000);
        setLoading(false);
      });
  }, []);

  //! Add Or Edit Address Form Success Handler
  const handleSuccessfulClose = async (newAddresses) => {
    await setUserAddresses(newAddresses);
    await handleAddressModal();

    //! reset
    if (editData) setEditData(null);
  };

  //! Click Add Button
  const handleAddButtonClick = async () => {
    await setEditData(null);
    await handleAddressModal();
  };

  //! Click Edit Button
  const handleEditButtonClick = async (itemForEdit) => {
    await setEditData(itemForEdit);
    await handleAddressModal();
  };

  //! Handle Delete Address
  const handleDeleteAddress = async (itemForDelete) => {
    const { id } = itemForDelete;

    try {
      const result = await axiosConfig.delete(
        `/DeliveryAddresses/${customerId}/${id}`
      );
      await setUserAddresses(result.data.deliveryAddresses);
    } catch (error) {
      showNotification(error.message, "error", 3000);
    }
  };

  return (
    <Grid
      container
      sx={{
        borderRadius: "4px",
        border: `1px solid ${mealzoTheme.palette.divider}`,
        padding: 3,
      }}
    >
      {/* top section for add a new address */}
      <Stack direction="row" display="flex" alignItems="center">
        <RegularIconsButton
          onClick={() => router.back()}
          sx={{
            boxShadow: "0px 0px 24px #24242419",
            borderRadius: 0.4,
            padding: 1,
            marginRight: 2,
            display: { md: "none" },
          }}
        >
          <ArrowBackIosNewIcon sx={{ fontSize: 10 }} />
        </RegularIconsButton>
        <Box>
          <Typography
            variant="h6"
            sx={{ fontSize: { xs: "16px", md: "24px" } }}
          >
            {t("address_list_title")}
          </Typography>
        </Box>
      </Stack>

      {/* Addresses section */}
      <Grid container direction="column">
        <Grid
          item
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            order: { xs: "1", md: "initial" },
            alignSelf: { xs: "flex-start", md: "flex-end" },
            mt: { xs: 3, md: "unset" },
          }}
        >
          <Button
            variant="text"
            startIcon={<AddIcon sx={{ fontSize: { xs: 16, md: 20 } }} />}
            onClick={handleAddButtonClick}
          >
            {t("address_page_add_a_new_address")}
          </Button>
          {/* <IconButton onClick={addClickHndler}>
            <AddIcon sx={{ fontSize: { xs: 16, md: 20 } }} />
          </IconButton>
          <Typography
            variant="subtitle2"
            color="text.secondary"
            fontSize={{ xs: "12px", md: "16px" }}
          >
            Add a New Address
          </Typography> */}
        </Grid>
        {!loading ? (
          <GrowContainer>
            <UserAddressViewList
              userAddresses={userAddresses}
              isSelected={isSelected}
              // tabClickHandler={tabClickHandler}
              handleEditButtonClick={handleEditButtonClick}
              handleDeleteAddress={handleDeleteAddress}
            />
          </GrowContainer>
        ) : (
          <AddressPageLoading />
        )}
      </Grid>

      {/* New Address Modal */}
      <AddNewAddressModal
        open={addAddressOpen}
        handleClose={handleAddressModal}
        handleSuccessfulClose={handleSuccessfulClose}
        initialValues={editData}
      />
    </Grid>
  );
};

export default AddressesView;
