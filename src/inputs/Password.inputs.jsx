import { useState } from "react";
import {
  FormControl,
  InputLabel,
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
} from "@mui/material";
import { styled } from "@mui/material/styles";

import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

import GrowContainer from "../containers/Grow.containers";

const BootstrapInput = styled(OutlinedInput)(({ theme, bordercolor }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },

  height: 45,
  borderRadius: 4,
  backgroundColor: "#fcfcfb",
  border: "1px solid #ced4da",
  fontSize: 16,
  transition: theme.transitions.create([
    "border-color",
    "background-color",
    "box-shadow",
  ]),
  "&:hover": {
    "& fieldset": {
      // border: "1px solid #ced4da",
      borderColor: "#ced4da !important",
      borderWidth: "1px !important",
    },
  },
  "&.Mui-focused": {
    "& fieldset": {
      borderWidth: "1px !important",
      borderColor: "#ced4da !important",
    },
  },
}));

const PasswordInput = ({
  value,
  children,
  name,
  handleChange,
  sx,
  bordercolor,
  error,
  ...restOfProps
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const toggleShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <FormControl variant="standard" sx={{ width: "100%", ...sx }}>
      <InputLabel shrink htmlFor={children} sx={{ fontWeight: 500 }}>
        {children}
      </InputLabel>
      <BootstrapInput
        name={name}
        type={showPassword ? "text" : "password"}
        value={value}
        id={children}
        onChange={handleChange}
        bordercolor={bordercolor}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={toggleShowPassword}
              onMouseDown={handleMouseDownPassword}
              edge="end"
            >
              {showPassword ? <VisibilityOff color="text.secondary" /> : <Visibility color="text.secondary" />}
            </IconButton>
          </InputAdornment>
        }
        {...restOfProps}
      />

      <GrowContainer isShown={error ? true : false}>
        <FormHelperText sx={{ color: "error.main" }}>{error}</FormHelperText>
      </GrowContainer>
    </FormControl>
  );
};

export default PasswordInput;
