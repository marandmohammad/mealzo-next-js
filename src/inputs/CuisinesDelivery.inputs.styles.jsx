import { Box } from "@mui/material";
import { styled } from "@mui/system";

export const DeliveryContainer = styled(Box)(({ theme }) => ({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  flex: "1",
  [theme.breakpoints.up("md")]: {
    maxWidth: "280px",
  },
}));

export const DeliveryBox = styled(Box)(({ theme }) => ({
  display: "flex",
  backgroundColor: "#F8F8F8",
  alignItems: "center",
  padding: "2px 5px",
  borderRadius: "4px",
  width: "100%",
  [theme.breakpoints.up("md")]: {
    borderRadius: "22px",
    // paddingLeft: "15px",
  },
}));

export const DeliveryButton = styled("button")(({ theme }) => ({
  // display: "flex",
  // flexDirection: "row",
  // flexWrap: "nowrap",
  fontSize: "12px",
  fontFamily: "IBM Plex Sans ,sans-serif",
  border: "none",
  backgroundColor: "transparent",
  width: "90%",
  textAlign: "left",
  cursor: "pointer",
  color: theme.palette.text.medium,
  a: {
    color: "#FF5733",
    textDecoration: "none",
  },
}));

export const DeliveryModalButton = styled("button")(({ theme }) => ({
  marginLeft: "auto",
  padding: "5px 15px",
  backgroundColor: "white",
  border: "none",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "2px",
  fontSize: "13px",
  cursor: "pointer",
  color: theme.palette.text.medium,
  whiteSpace: "nowrap",
  [theme.breakpoints.up("md")]: {
    borderRadius: "22px",
  },
}));

export const DeliveryBoxImage = styled("img")(() => ({
  width: 41,
  height: 41,
}));
