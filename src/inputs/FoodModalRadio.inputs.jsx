import Radio from "@mui/material/Radio";

const FoodModalRadioInput = (props) => {
  return (
    <Radio
      sx={{
        borderRadius: "50%",
        width: 50,
        height: 50,
        padding: "5px",
        "& .MuiSvgIcon-root": {
          fontSize: 18,
        },
      }}
      {...props}
    />
  );
};

export default FoodModalRadioInput;
