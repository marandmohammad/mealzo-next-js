import { useMemo } from "react";
import RoundedPlusMinusButton from "../buttons/RoundedPlusMinus.buttons";
import { Grid, TextField, Typography } from "@mui/material";

import currencyIdToMark from "../../utilities/currencyIdToMark";
import NumberCountCarousel from "../carousels/NumberCount.carousels";

const FoodModalNumberInput = ({
  item,
  subMenuId,
  maxSelect,
  formik,
  currencyId,
}) => {
  //! Find a group of items count
  let itemsCount = 0;
  const names = Object.keys(formik.values);
  names.forEach((name) => {
    const values = formik.values;
    if (name !== "crust" && name !== "counter") {
      const [lowerSMI, lowerSMII] = name.split("-").map((el) => parseInt(el));
      if (subMenuId === lowerSMI) itemsCount += values[name];
    }
  });

  //! Handle change value
  const handleChangeValue = (name, value) => {
    const prevVal = formik.values[name];

    if (!(prevVal === 0 && value === -1) || !(prevVal >= maxSelect)) {
      formik.setFieldValue(name, prevVal + value);
    }
  };

  //! Control Plus Button Disable Status
  const isPlusButtonDisabled = useMemo(
    () => (maxSelect === 0 ? false : itemsCount >= maxSelect),
    [formik.values[`${subMenuId}-${item.id}`], itemsCount]
  );

  return (
    <>
      <TextField
        name={`${subMenuId}-${item.id}`}
        type="number"
        InputProps={{ inputProps: { min: 0 } }}
        placeholder="0"
        value={formik.values[`${subMenuId}-${item.id}`]}
        onChange={formik.handleChange}
        sx={{ padding: "3px", fontSize: 14, display: "none" }}
      />
      <Grid
        display="flex"
        flexDirection="row"
        flexWrap="nowrap"
        gap={1}
        flexShrink={1}
        mr="10px"
      >
        <Grid item display="flex" alignItems="center">
          <Typography variant="subtitle1" component="div" fontSize={12} noWrap>
            {item.amount > 0 && `${currencyIdToMark(currencyId, item.amount)}`}
          </Typography>
        </Grid>
        <Grid item>
          <RoundedPlusMinusButton
            onClick={() => handleChangeValue(`${subMenuId}-${item.id}`, -1)}
            disabled={formik.values[`${subMenuId}-${item.id}`] === 0}
          />
        </Grid>
        <Grid item display="flex" alignItems="center">
          <Typography variant="subtitle1" component="div" fontSize={15} noWrap>
            <NumberCountCarousel
              number={formik.values[`${subMenuId}-${item.id}`]}
            />
          </Typography>
        </Grid>
        <Grid item>
          <RoundedPlusMinusButton
            width="18px !important"
            type="plus"
            onClick={() => handleChangeValue(`${subMenuId}-${item.id}`, 1)}
            disabled={isPlusButtonDisabled} // itemsCount >= maxSelect
          />
        </Grid>
        <Grid item display="none" width={35} alignItems="center">
          <Typography
            variant="subtitle1"
            component="div"
            fontSize={12}
            textAlign="right"
            noWrap
          >
            {item.amount > 0 &&
              formik.values[`${subMenuId}-${item.id}`] > 0 &&
              `${currencyIdToMark(
                currencyId,
                formik.values[`${subMenuId}-${item.id}`] * item.amount
              )}`}
          </Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default FoodModalNumberInput;
