import { useContext } from "react";
import { useTranslation } from "next-i18next";
import useLocation from "../../hooks/useLocation";

import { Typography, NoSsr } from "@mui/material";

//MUI icons
// import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import ArrowDropDownOutlinedIcon from "@mui/icons-material/ArrowDropDownOutlined";

//custom mui components styled
import {
  DeliveryContainer,
  DeliveryBox,
  DeliveryButton,
  DeliveryModalButton,
  DeliveryBoxImage,
} from "./CuisinesDelivery.inputs.styles";

import { MenuPageContext } from "../../context/MenuPageContext";
import { saleMethods } from "../../utilities/areaPathHandlers";

const MenuDeliveryInput = () => {
  const { t } = useTranslation("common");
  const { postCode } = useLocation();
  const { saleMethod, setDeliveryModal } = useContext(MenuPageContext);

  let currentSaleMethodIcon = saleMethods[saleMethod].iconAddress;

  return (
    <DeliveryContainer>
      <DeliveryBox>
        <DeliveryBoxImage src={currentSaleMethodIcon} alt={saleMethod} />
        <DeliveryButton onClick={() => setDeliveryModal(true)}>
          <NoSsr>{postCode} - </NoSsr>
          <Typography component="span" color="primary.main" fontSize="small">
            {t("delivery_change")}
          </Typography>
        </DeliveryButton>
        <DeliveryModalButton onClick={() => setDeliveryModal(true)}>
          {saleMethod && t(saleMethods[saleMethod].title)}
          <ArrowDropDownOutlinedIcon />
        </DeliveryModalButton>
      </DeliveryBox>
    </DeliveryContainer>
  );
};

export default MenuDeliveryInput;
