import { useContext } from "react";
import { useTranslation } from "next-i18next";
// import Image from "next/image";
import useLocation from "../../hooks/useLocation";

//MUI icons
// import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import ArrowDropDownOutlinedIcon from "@mui/icons-material/ArrowDropDownOutlined";

import { AreaPageContext } from "../../context/AreaPageContext";

// import DeliveryIcon from "../../public/Images/sale-method-icons/delivery-icon.png";
// import CollectionIcon from "../../public/Images/sale-method-icons/collection-icon.png";
// import DineInIcon from "../../public/Images/sale-method-icons/dine-in-icon.png";

//custom mui components styled
import {
  DeliveryContainer,
  DeliveryBox,
  DeliveryButton,
  DeliveryModalButton,
  DeliveryBoxImage,
} from "./CuisinesDelivery.inputs.styles";

import { saleMethods } from "../../utilities/areaPathHandlers";
import { Typography } from "@mui/material";

const AreaDeliveryInput = ({ setDeliveryModal }) => {
  const { t } = useTranslation("common");
  const { postCode } = useLocation();
  const { saleMethod } = useContext(AreaPageContext);

  let currentSaleMethodIcon = saleMethods[saleMethod].iconAddress;

  return (
    <DeliveryContainer>
      <DeliveryBox>
        <DeliveryBoxImage src={currentSaleMethodIcon} alt={t(saleMethod)} />
        <DeliveryButton onClick={() => setDeliveryModal(true)}>
          <Typography
            component="span"
            fontSize="inherit"
            color="inherit"
            // noWrap
          ></Typography>
          {postCode} -{" "}
          <Typography
            component="span"
            fontSize={12}
            color="primary.main"
            noWrap
          >
            {t("delivery_change")}
          </Typography>
        </DeliveryButton>
        <DeliveryModalButton onClick={() => setDeliveryModal(true)}>
          {saleMethod && t(saleMethods[saleMethod].title)}
          <ArrowDropDownOutlinedIcon />
        </DeliveryModalButton>
      </DeliveryBox>
    </DeliveryContainer>
  );
};

export default AreaDeliveryInput;
