import { Typography } from "@mui/material";

import GrowContainer from "../containers/Grow.containers";

const HelperTextInput = ({ errorText }) => {
  return (
    <GrowContainer>
      <Typography color="error" fontSize={12} fontWeight={500} sx={{ pl: 0.5 }}>
        {errorText}
      </Typography>
    </GrowContainer>
  );
};

export default HelperTextInput;
