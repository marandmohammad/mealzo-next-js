import { useState, useEffect, useContext, useMemo, Fragment } from "react";
import { useTranslation } from "next-i18next";

//MUI icons
import SearchIcon from "@mui/icons-material/Search";
import ClickAwayListener from "@mui/material/ClickAwayListener";

// custom MUI components
import {
  SearchBox,
  SearchContainer,
  CustomInput,
} from "./AreaHeaderSearchBar.inputs.styles";
import MenuSearchSuggestionList from "../lists/MenuSearchSuggestion.lists";
import FoodModal from "../modals/Food.modals";

import { MenuPageContext } from "../../context/MenuPageContext";

const MenuHeaderSearchBarInput = () => {
  const { t } = useTranslation('common');
  const { products } = useContext(MenuPageContext);

  const [selectedProduct, setSelectedProduct] = useState(null);

  //? function for handle open and close modal
  const [foodModal, setFoodModal] = useState(false);
  const handleFoodModalOpen = () => setFoodModal(true);
  const handleFoodModalClose = () => setFoodModal(false);

  //? Trigger When An Item Of List Selected
  const handleProductClick = async (product, optionId = null) => {
    await setSelectedProduct({ product, optionId });
    await handleFoodModalOpen();
  };

  const [searchInputValue, setSearchInputValue] = useState("");

  const [searchResult, setSearchResult] = useState([]);
  const [searchOpen, setSearchOpen] = useState(false);

  //! Make 500ms Delay Sending Request
  useEffect(() => {
    const timeOutId = setTimeout(async () => {
      let suggestions = [];
      products.forEach(({ products, categoryId }) => {
        if (products.length > 0 && categoryId !== "popularItems") {
          const filteredProducts = products.filter(({ title }) =>
            title.toLowerCase().includes(searchInputValue.trim())
          );
          suggestions = [...suggestions, ...filteredProducts];
        }
      });
      setSearchResult(suggestions);
    }, 100);
    return () => clearTimeout(timeOutId);
  }, [searchInputValue]);

  // Handle Changes
  const handleChange = (e) => {
    setSearchInputValue(e.target.value.toLowerCase());
  };

  const isSearchSuggestionListVisible = useMemo(
    () =>
      searchOpen && searchResult.length > 0 && searchInputValue.trim() !== "",
    [searchOpen, searchResult.length]
  );

  return (
    <Fragment>
      <ClickAwayListener onClickAway={() => setSearchOpen(false)}>
        <SearchContainer>
          <SearchBox>
            <SearchIcon fontSize="medium" sx={{ color: "checkbox" }} />
            <CustomInput
              type="search"
              value={searchInputValue}
              placeholder={t("menu_appbar_search_placeholder")}
              onChange={handleChange}
              // ref={inputRef}
              onFocus={() => setSearchOpen(true)}
            />
          </SearchBox>

          <MenuSearchSuggestionList
            in={isSearchSuggestionListVisible}
            direction="up"
            // mountOnEnter
            // unmountOnExit
            // container={containerRef.current}
            searchResult={searchResult}
            searchString={searchInputValue}
            handleSelectSuggestedItem={handleProductClick}
          />
        </SearchContainer>
      </ClickAwayListener>

      {/* Food Modal */}
      <FoodModal
        item={selectedProduct}
        open={foodModal}
        handleClose={handleFoodModalClose}
      />
    </Fragment>
  );
};

export default MenuHeaderSearchBarInput;
