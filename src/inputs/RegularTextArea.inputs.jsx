import { styled } from "@mui/material/styles";
import TextareaAutosize from "@mui/material/TextareaAutosize";

const RegularTextAreaInput = styled(TextareaAutosize)(({ theme }) => ({
  backgroundColor: "white",
  minWidth: "100%",
  outline: "none",
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: theme.palette.checkbox,
  padding: "15px 11px",
  fontSize: 13,
  minHeight: 100,
  maxHeight: 100,
  borderRadius: "4px",
  fontFamily: "inherit",
}));

export default RegularTextAreaInput;
