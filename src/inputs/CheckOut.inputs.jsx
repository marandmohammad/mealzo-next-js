import { FormControl, InputLabel, MenuItem, Select, InputBase } from "@mui/material";
import { styled } from "@mui/material/styles";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import HelperTextInput from "./HelperText.inputs";
import GrowContainer from "../containers/Grow.containers";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    // marginTop: theme.spacing(0),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      backgroundColor: "white",
      // borderRadius: 4,
      // borderColor: "#80bdff",
      // boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}));

const CheckOutInput = ({
  options,
  children,
  value,
  handleChange,
  name,
  error,
  sx,
}) => {
  return (
    <FormControl sx={{ ...sx }} variant="filled" fullWidth>
      <InputLabel
        htmlFor={children}
        sx={{ fontSize: { xs: 14, md: 16 }, fontWeight: 600 }}
      >
        {children}
      </InputLabel>
      <Select
        name={name}
        IconComponent={ExpandMoreIcon}
        sx={{
          color: "text.secondary",
        }}
        id={children}
        value={value}
        onChange={handleChange}
        input={<BootstrapInput />}
      >
        {options.map((item) => (
          <MenuItem
            key={item.id}
            value={item.id.toString()}
            sx={{ fontSize: { xs: 12, md: 16 } }}
          >
            {item.timeDiv}
          </MenuItem>
        ))}
      </Select>
      <GrowContainer isShown={error ? true : false}>
        <HelperTextInput sx={{ color: "error.main" }}>{error}</HelperTextInput>
      </GrowContainer>
    </FormControl>
  );
};

export default CheckOutInput;
