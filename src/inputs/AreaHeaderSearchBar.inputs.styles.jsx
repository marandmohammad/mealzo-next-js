import { styled } from "@mui/system";
import { Box } from "@mui/material";

export const SearchContainer = styled(Box)(({ theme }) => ({
  width: "100%",
  display: "flex",
  flexDirection: "column",
  position: "relative",
  justifyContent: "space-between",
  flex: "1",
  [theme.breakpoints.up("lg")]: {
    flex: 2,
  },
}));

export const SearchBox = styled(Box)(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  backgroundColor: "#F8F8F8",
  padding: "8px 5px",
  borderRadius: "2px",
  width: "100%",
  // [theme.breakpoints.up("md")]: {
  //   width: "30%",
  // },
}));

export const CustomInput = styled("input")`
  width: 90%;
  border: none;
  background-color: transparent;
  margin-left: 4px;
  border-radius: 4px;
  flex: 1;
  &:focus-visible {
    border: none;
    outline: none;
  }
`;
