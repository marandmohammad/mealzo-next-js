import { useState, useEffect, useMemo } from "react";

import TextField from "@mui/material/TextField";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import Fade from "@mui/material/Fade";

import useGoogle from "react-google-autocomplete/lib/usePlacesAutocompleteService";
import useNotification from "../../hooks/useNotification";

import PlacePredictionsList from "../lists/PlacePredictions.lists";

const ReactGoogleAutoCompleteInput = ({ Input, onPlaceSelect }) => {
  const { showNotification } = useNotification();
  const {
    placePredictions,
    getPlacePredictions,
    isPlacePredictionsLoading,
    placesService,
  } = useGoogle({
    apiKey: process.env.NEXT_PUBLIC_GOOGLE_LOCATION_API_KEY,
    options: {
      types: ["(regions)"],
      componentRestrictions: { country: "gb" },
    },
    sessionToken: true,
  });

  const [value, setValue] = useState("");
  const [place, setPlace] = useState(null);
  const [predictionOpen, setPredictionOpen] = useState(false);

  const internalOnPlaceSelect = (prediction) => {
    if (!prediction) {
      showNotification("Selected location has no detail!", "error", 3000);
      return;
    }

    try {
      placesService.getDetails(
        {
          placeId: prediction.place_id,
        },
        (placeDetails) => {
          const { address_components, geometry, formatted_address, place_id } =
            placeDetails;
          let selectedPostCode = null;

          address_components.forEach(({ long_name, short_name, types }) => {
            if (types.includes("postal_code") && !selectedPostCode)
              selectedPostCode = short_name;
          });

          const locationDetail = {
            Address: formatted_address,
            // AddressDescription: desc,
            DeliveryPostCode: selectedPostCode.trim(),
            Lat: geometry.location.lat(),
            Lon: geometry.location.lng(),
            PlaceId: place_id,
          };
          setPlace(locationDetail);
          setValue(formatted_address);
          setPredictionOpen(false);
          onPlaceSelect && onPlaceSelect(locationDetail);
        }
      );
    } catch (error) {
      showNotification(error.message, "error", 3000);
    }
  };

  const onChange = (e) => {
    getPlacePredictions({ input: e.target.value });
    setValue(e.target.value);
  };

  const onKeyDown = (e) => {
    if (
      e.key === "Enter" &&
      !isPlacePredictionsLoading &&
      placePredictions.length
    ) {
      e.preventDefault();
      internalOnPlaceSelect(placePredictions[0]);
    }
  };

  const showPredictions = useMemo(
    () => placePredictions.length > 0 && predictionOpen,
    [predictionOpen, placePredictions]
  );

  return (
    <ClickAwayListener onClickAway={() => setPredictionOpen(false)}>
      <Box width="100%" position="relative">
        {Input ? (
          <Input
            id="react-google-autocomplete"
            value={value}
            onFocus={() => setPredictionOpen(true)}
            onChange={onChange}
            onKeyDown={onKeyDown}
          />
        ) : (
          <TextField
            id="react-google-autocomplete"
            value={value}
            onFocus={() => setPredictionOpen(true)}
            onChange={onChange}
            onKeyDown={onKeyDown}
          />
        )}

        <Fade in={isPlacePredictionsLoading}>
          <LinearProgress
            color="primary"
            sx={{
              width: { xs: "98%", md: "99%" },
              borderRadius: "10px",
              position: "absolute",
              bottom: 1,
              left: "50%",
              transform: "translateX(-50%)",
              zIndex: -1,
            }}
          />
        </Fade>

        <PlacePredictionsList
          predictions={placePredictions}
          onPlaceSelect={internalOnPlaceSelect}
          in={showPredictions}
        />
      </Box>
    </ClickAwayListener>
  );
};

export default ReactGoogleAutoCompleteInput;
