import { useState, useRef, useEffect, useContext, useMemo } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import useLocation from "../../hooks/useLocation";

//MUI icons
import SearchIcon from "@mui/icons-material/Search";
import TuneIcon from "@mui/icons-material/Tune";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import LinearProgress from "@mui/material/LinearProgress";
import Fade from "@mui/material/Fade";

// custom MUI components
import {
  SearchBox,
  SearchContainer,
  CustomInput,
} from "./AreaHeaderSearchBar.inputs.styles";
import AreaSearchSuggestionList from "../lists/AreaSearchSuggestion.lists";

import { AreaPageContext } from "../../context/AreaPageContext";
import axiosConfig from "../../config/axios";
import {
  saleMethods,
  saleMethodAndSearchChangeHandler,
  areaFilterAndSearchReset,
  queryKeyWords,
} from "../../utilities/areaPathHandlers";
import { queryParamDecode } from "../../utilities/areaPathHandlers";

const AreaHeaderSearchBarInput = () => {
  const { t } = useTranslation("area_page");
  const router = useRouter();
  const { postCode } = useLocation();
  const { searchQueryKeyWord } = queryKeyWords;
  const {
    // Search Group
    searchInputValue,
    setSearchInputValue,
    // searchQueryKeyWord,
    setFilterOpen,

    // postCode,
    saleMethod,

    // Shop Group
    shops,
    setShops,
    shopLoading,
    setShopLoading,
  } = useContext(AreaPageContext);

  const containerRef = useRef(null);

  const [searchResult, setSearchResult] = useState([]);
  const [searchOpen, setSearchOpen] = useState(false);
  const [searchLoading, setSearchLoading] = useState(false);

  // Trigger Cancel Search Button
  const handleSearch = () =>
    router.query[searchQueryKeyWord]
      ? areaFilterAndSearchReset(
          router,
          searchQueryKeyWord,
          setSearchInputValue,
          setShops,
          shopLoading,
          setShopLoading
        )
      : setSearchInputValue("");

  // Make 500ms Delay To Send Request
  useEffect(() => {
    const timeOutId = setTimeout(async () => {
      if (router.query[searchQueryKeyWord] && !searchInputValue) {
        handleSearch();
      } else if (searchInputValue) {
        setSearchLoading(true);
        const suggestions = await (
          await axiosConfig.get(
            `/SuggestedRestaurants?searchKey=${searchInputValue}&postCode=${postCode}&saleMethode=${saleMethods[saleMethod].code}`
          )
        ).data;
        setSearchResult(suggestions);
        setSearchLoading(false);
      }
    }, 500);
    return () => clearTimeout(timeOutId);
  }, [searchInputValue]);

  // Handle Changes
  const handleChange = (e) => {
    setSearchInputValue(e.target.value);
  };

  // Trigger When An Item Of List Selected
  const handleSelectSuggestedItem = (itemTitle) => {
    saleMethodAndSearchChangeHandler(
      router,
      searchQueryKeyWord,
      itemTitle,
      setSearchInputValue,
      setShops,
      shopLoading,
      setShopLoading
    );
    // setSearchInputValue(itemTitle);
    setSearchOpen(false);
    setTimeout(() => {
      setSearchResult([]);
    }, 1000);
  };

  const isListOpen = useMemo(
    () =>
      searchOpen &&
      searchResult.length > 0 &&
      searchInputValue &&
      !searchLoading,
    [searchLoading, searchOpen, searchInputValue, searchResult.length]
  );

  return (
    <ClickAwayListener onClickAway={() => setSearchOpen(false)}>
      <SearchContainer ref={containerRef}>
        <SearchBox>
          <SearchIcon fontSize="medium" sx={{ color: "checkbox" }} />
          <CustomInput
            type="search"
            value={searchInputValue}
            placeholder={t("area_appbar_search_input_placeholder")}
            onChange={handleChange}
            ref={(element) => ((element || {}).onsearch = handleSearch)}
            onFocus={() => setSearchOpen(true)}
          />
          <TuneIcon
            sx={{
              // ml: "auto",
              display: { xs: "block", md: "none" },
              color: "text.primary",
            }}
            onClick={setFilterOpen}
          />
          <Fade
            in={searchLoading}
            style={{
              position: "absolute",
              bottom: 0,
              left: 0,
              width: "100%",
            }}
          >
            <LinearProgress />
          </Fade>
        </SearchBox>

        <AreaSearchSuggestionList
          in={
            // searchOpen &&
            // searchResult.length > 0 &&
            // searchInputValue &&
            // !searchLoading
            isListOpen
          }
          direction="left"
          mountOnEnter
          unmountOnExit
          container={containerRef.current}
          searchResult={searchResult}
          handleSelectSuggestedItem={handleSelectSuggestedItem}
        />
      </SearchContainer>
    </ClickAwayListener>
  );
};

export default AreaHeaderSearchBarInput;
