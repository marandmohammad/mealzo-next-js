import {
  FormControl,
  InputBase,
  InputLabel,
  FormHelperText,
} from "@mui/material";
import { styled } from "@mui/material/styles";

import GrowContainer from "../containers/Grow.containers";

const BootstrapInput = styled(InputBase)(({ theme, bordercolor }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: "#fcfcfb",
    border: "1px solid #ced4da",
    borderColor: bordercolor,
    fontSize: 16,
    padding: "10px 12px",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    // Use the system font instead of the default Roboto font.
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      // borderColor: theme.palette.primary.main,
      color: "text.primary",
      borderColor: bordercolor,
    },
  },
}));

const SignUpInput = ({
  value,
  children,
  type,
  name,
  handleChange,
  sx,
  bordercolor,
  error,
  ...restOfProps
}) => {
  return (
    <FormControl variant="standard" sx={{ width: "100%", ...sx }}>
      {children && (
        <InputLabel shrink htmlFor={children} sx={{ fontWeight: 500 }}>
          {children}
        </InputLabel>
      )}
      <BootstrapInput
        name={name}
        type={type}
        value={value}
        id={children}
        onChange={handleChange}
        bordercolor={bordercolor}
        {...restOfProps}
      />

      <GrowContainer isShown={error ? true : false}>
        <FormHelperText sx={{ color: "error.main" }}>{error}</FormHelperText>
      </GrowContainer>
    </FormControl>
  );
};

export default SignUpInput;
