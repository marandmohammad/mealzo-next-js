import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Rating from "@mui/material/Rating";

const OverViewRatingInput = ({
  title,
  name,
  onChange,
  value,
  error,
  readOnly,
  sx,
  ...restOfProps
}) => {
  return (
    <Grid
      sx={{
        width: "100%",
        p: 2,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: !error ? "checkbox" : "error.main",
        borderRadius: "4px",
        ...sx,
      }}
      {...restOfProps}
    >
      <Typography
        component="legend"
        fontSize={16}
        mb={1.25}
      >
        {title}
      </Typography>
      <Rating
        name={name}
        value={value}
        onChange={onChange}
        readOnly={readOnly}
      />
    </Grid>
  );
};

export default OverViewRatingInput;
