import { Fragment, useEffect } from "react";
import { useAuth } from "../hooks/authentication";

const useScript = (url, widgetCode) => {
  const { getUser, isLogin } = useAuth();

  useEffect(() => {
    if (window.$zoho) {
      window.$zoho.salesiq.floatbutton?.visible("show");
    } else {
      const script = document.createElement("script");
      script.setAttribute("type", "text/javascript");

      let code = `var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode: "${widgetCode}", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="${url}";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.innerHTML = "<div id='zsiqwidget'></div>";`;

      script.appendChild(document.createTextNode(code));
      document.body.appendChild(script);
    }

    if (isLogin) {
      const { email, fullName, phoneNumber } = getUser();

      //* Check And Set Visitor Name
      fullName &&
        fullName.trim() !== "" &&
        $zoho.salesiq.visitor?.name(fullName);

      //* Check And Set Visitor Email
      email && email.trim() !== "" && $zoho.salesiq.visitor?.email(email);

      //* Check And Set Visitor Phone Number
      phoneNumber &&
        phoneNumber.trim() !== "" &&
        $zoho.salesiq.visitor?.contactnumber(phoneNumber);
    }

    return () => {
      window.$zoho.salesiq.floatbutton?.visible("hide");
      window.$zoho.salesiq.floatwindow?.close();
    };
  }, [url, isLogin]);
};

export default function ZohoSalesIQ() {
  return (
    <Fragment>
      {useScript(
        "https://salesiq.zoho.eu/widget",
        process.env.NEXT_PUBLIC_WIDGET_CODE
      )}
    </Fragment>
  );
}
