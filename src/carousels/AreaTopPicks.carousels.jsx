import { useState, useContext, Fragment } from "react";
import { setCookies } from "cookies-next";
import { useTranslation } from "next-i18next";

import { Box, Grid, Grow, Typography } from "@mui/material";

// Swiper
import { Swiper, SwiperSlide } from "swiper/react";

import RestaurantCard from "../cards/AreaRestaurant.cards";
import Link from "../links/Link";
import TopPicksLoadings from "../loadings/TopPicks.loadings";
import AreaCategoryCarouselNextButton from "../buttons/AreaCategoryCarouselNext.buttons";
import AreaCategoryCarouselPrevButton from "../buttons/AreaCategoryCarouselPrev.buttons";
import GrowContainer from "../containers/Grow.containers";

import { AreaPageContext } from "../../context/AreaPageContext";
import { saleMethods } from "../../utilities/areaPathHandlers";

import "swiper/css";

const AreaTopPicksCarousel = () => {
  const { t } = useTranslation('area_page');
  const { shops, shopLoading, saleMethod } = useContext(AreaPageContext);

  const topPickShops = shops
    .filter(
      (shop) =>
        shop.topPicks &&
        (shop.isOpen.toLowerCase() === "open" ||
          shop.isOpen.toLowerCase().startsWith("opening")) && 
          shop.saleMethod === saleMethods[saleMethod].code
    )
    .slice(0, 10);

  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  // return <TopPicksLoadings />;
  if (topPickShops.length > 0)
    return (
      <GrowContainer>
        <Box>
          <Grid container>
            <Typography variant="h6" mb={3}>
              {t('area_top_picks')}
            </Typography>
          </Grid>
          <Box>
            <Swiper
              onSlideChange={(swiper) => {
                setIsBeginning(swiper.isBeginning);
                setIsEnd(swiper.isEnd);
              }}
              breakpoints={{
                0: {
                  slidesPerView: 1.2,
                  slidesPerGroup: 1,
                },
                600: {
                  slidesPerView: 2.2,
                  slidesPerGroup: 2,
                },
                1100: {
                  slidesPerView: 2.5,
                  slidesPerGroup: 2,
                },
                1370: {
                  slidesPerView: 3.1,
                  slidesPerGroup: 3,
                },
                1620: {
                  slidesPerView: 4.1,
                  slidesPerGroup: 3,
                },
                1770: {
                  slidesPerView: 4.5,
                  slidesPerGroup: 1,
                },
                1800: {
                  slidesPerView: 5,
                  slidesPerGroup: 1,
                },
              }}
              spaceBetween={10}
              className="mySwiper"
              style={{
                paddingBottom: 5,
              }}
            >
              {shopLoading
                ? new Array(10).fill(null).map((_, index) => (
                    <SwiperSlide key={index}>
                      <TopPicksLoadings />{" "}
                    </SwiperSlide>
                  ))
                : topPickShops.map((item, index) => (
                    <SwiperSlide key={index}>
                      <Box
                        width="100%"
                        //maxWidth={300}
                      >
                        <Link
                          href={`/${item.snapIdenti}/Menu`}
                          onClick={() => setCookies("saleMethod", saleMethod)}
                        >
                          <RestaurantCard item={item} topPick />
                        </Link>
                      </Box>
                    </SwiperSlide>
                  ))}

              {topPickShops.length > 3 && (
                <Fragment>
                  <Grow in={!isEnd}>
                    <div>
                      <AreaCategoryCarouselNextButton sx={{ right: 0 }} />
                    </div>
                  </Grow>
                  <Grow in={!isBeginning}>
                    <div>
                      <AreaCategoryCarouselPrevButton sx={{ left: 0 }} />
                    </div>
                  </Grow>
                </Fragment>
              )}
            </Swiper>
          </Box>
        </Box>
      </GrowContainer>
    );
};

export default AreaTopPicksCarousel;
