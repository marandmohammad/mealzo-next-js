import { useState, useContext } from "react";

import { Grid, Grow } from "@mui/material";

//Swiper
import { Navigation } from "swiper";
import { SwiperSlide } from "swiper/react";

// MUI
import { styled } from "@mui/system";

//Custom Swiper and MUI components
import { CustomSwiper } from "./AreaCategories.carousels.styles";

import AreaCategoryCarouselNextButton from "../buttons/AreaCategoryCarouselNext.buttons";
import AreaCategoryCarouselPrevButton from "../buttons/AreaCategoryCarouselPrev.buttons";

import { AreaPageContext } from "../../context/AreaPageContext";
import GrowContainer from "../containers/Grow.containers";
import Image from "next/image";

const OptionItem = styled("a")(({ theme }) => ({
  display: "block",
  borderRadius: "10px",
  overflow: "hidden",
  position: "relative",
  width: "100%",
  height: 160,
  [theme.breakpoints.up("sm")]: {
    height: 170,
  },
}));

const AreaOptionsCarousel = () => {
  const { promotionsData } = useContext(AreaPageContext);

  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  return (
    <GrowContainer>
      <Grid container position="relative" mb={5}>
        <CustomSwiper
          onSlideChange={(swiper) => {
            setIsBeginning(swiper.isBeginning);
            setIsEnd(swiper.isEnd);
          }}
          slidesPerView={1.2}
          spaceBetween={10}
          slidesPerGroup={1}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            480: {
              slidesPerView: 2.2,
              slidesPerGroup: 2,
            },
            1100: {
              slidesPerView: 2.5,
              slidesPerGroup: 2,
            },
            1370: {
              slidesPerView: 3.1,
              slidesPerGroup: 3,
            },
            1620: {
              slidesPerView: 4.1,
              slidesPerGroup: 3,
            },
            1770: {
              slidesPerView: 4.5,
              slidesPerGroup: 1,
            },
            1800: {
              slidesPerView: 4.8,
              slidesPerGroup: 1,
            },
          }}
          // navigation={true}
          modules={[Navigation]}
          className="mySwiper"
        >
          {promotionsData &&
            promotionsData.map(({ id, title, picUrl, link }, idx) => {
              return (
                <SwiperSlide key={idx}>
                  <OptionItem
                    href={link}
                    target="_blank"
                    referrerPolicy="no-referrer-when-downgrade"
                  >
                    <Image
                      src={picUrl}
                      loader={({ src }) => src}
                      alt={title}
                      layout="fill"
                    />
                  </OptionItem>
                </SwiperSlide>
              );
            })}

          <Grow in={!isEnd}>
            <div>
              <AreaCategoryCarouselNextButton />
            </div>
          </Grow>
          <Grow in={!isBeginning}>
            <div>
              <AreaCategoryCarouselPrevButton />
            </div>
          </Grow>
        </CustomSwiper>
      </Grid>
    </GrowContainer>
  );
};

export default AreaOptionsCarousel;
