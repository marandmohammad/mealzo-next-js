import { useState } from "react";
import { useTranslation } from "next-i18next";

import { Grid, Grow, Typography } from "@mui/material";
import { Swiper, SwiperSlide } from "swiper/react";

import TopPicksCard from "../cards/TopPicks.cards";
import AreaCategoryCarouselNextButton from "../buttons/AreaCategoryCarouselNext.buttons";
import AreaCategoryCarouselPrevButton from "../buttons/AreaCategoryCarouselPrev.buttons";

import "swiper/css";

const ExclusiveShopsCarousel = ({ shops }) => {
const { t } = useTranslation('become_partner_page');

  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  return (
    <Grid container flexDirection="column" flexWrap="nowrap" rowGap={5}>
      <Typography variant="h6" fontSize={32} textAlign="center">
        {t('top_brands')}
      </Typography>

      <Grid item xs={12} position="relative">
        <Swiper
          onSlideChange={(swiper) => {
            setIsBeginning(swiper.isBeginning);
            setIsEnd(swiper.isEnd);
          }}
          breakpoints={{
            480: {
              slidesPerView: 1.2,
              slidesPerGroup: 2,
            },
            640: {
              slidesPerView: 1.8,
              slidesPerGroup: 2,
            },
            1100: {
              slidesPerView: 2.5,
              slidesPerGroup: 2,
            },
            1370: {
              slidesPerView: 3.1,
              slidesPerGroup: 3,
            },
            1620: {
              slidesPerView: 4.1,
              slidesPerGroup: 3,
            },
            1770: {
              slidesPerView: 4.3,
              slidesPerGroup: 1,
            },
            1800: {
              slidesPerView: 4.5,
              slidesPerGroup: 1,
            },
          }}
          spaceBetween={24}
          className="mySwiper"
        >
          {shops.map(
            ({ shopId, shopTitle, mealzoLogo, mealzoHomePageBanner, city }) => (
              <SwiperSlide key={shopId}>
                <TopPicksCard
                  brandTitle={`${shopTitle} (${city})`}
                  brandLogo={mealzoLogo}
                  imageUrl={mealzoHomePageBanner}
                  sx={{
                    cursor: "default",
                    width: "100%",
                    maxWidth: "unset",
                    "& #topPickCardImage": {
                      borderRadius: "4px",
                      overflow: "hidden",
                    },
                  }}
                />
              </SwiperSlide>
            )
          )}

          <Grow in={!isEnd}>
            <div>
              <AreaCategoryCarouselNextButton sx={{ right: 0 }} />
            </div>
          </Grow>
          <Grow in={!isBeginning}>
            <div>
              <AreaCategoryCarouselPrevButton sx={{ left: 0 }} />
            </div>
          </Grow>
        </Swiper>
      </Grid>
    </Grid>
  );
};

export default ExclusiveShopsCarousel;
