import { useEffect, useState, useContext } from "react";
import { useRouter } from "next/router";

import { Grid, Grow } from "@mui/material";

//swiper
import { SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import {
  // CategoryContainer,
  CategoryItem,
  CategoryItemTitle,
  CustomSwiper,
} from "./AreaCategories.carousels.styles";

import AreaCategoryCarouselNextButton from "../buttons/AreaCategoryCarouselNext.buttons";
import AreaCategoryCarouselPrevButton from "../buttons/AreaCategoryCarouselPrev.buttons";

// import SelectedCuisineImage from "../../public/Images/selectedCuisine.svg";

import { AreaPageContext } from "../../context/AreaPageContext";
import { areaCategoryChangeHandler } from "../../utilities/areaPathHandlers";
import GrowContainer from "../containers/Grow.containers";

const AreaCategoriesCarousel = () => {
  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  const {
    categoriesData,
    categories,
    setCategories,
    categoryQueryKeyWord,
    setShops,
    shopLoading,
    setShopLoading,
  } = useContext(AreaPageContext);

  const categoriesForTop = categoriesData
    .filter(({ positionType }) => positionType > 1)
    .slice(0, 10)
    .sort((a, b) => (a.topSort > b.topSort ? 1 : -1));

  const router = useRouter();

  const [SSR, setSSR] = useState(true);
  useEffect(() => {
    setSSR(false);
  }, []);

  return (
    <GrowContainer>
      <Grid container position="relative" mb={2}>
        <CustomSwiper
          onSlideChange={(swiper) => {
            setIsBeginning(swiper.isBeginning);
            setIsEnd(swiper.isEnd);
          }}
          slidesPerView={2.5}
          spaceBetween={10}
          slidesPerGroup={2}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            430: {
              slidesPerView: 3.5,
              slidesPerGroup: 3,
            },
            560: {
              slidesPerView: 4,
              slidesPerGroup: 4,
            },
            710: {
              slidesPerView: 5,
              slidesPerGroup: 5,
            },
            830: {
              slidesPerView: 6,
              slidesPerGroup: 6,
            },
            900: {
              slidesPerView: 4,
              slidesPerGroup: 4,
            },
            1200: {
              slidesPerView: 6,
              slidesPerGroup: 6,
            },
            1490: {
              slidesPerView: 8,
              slidesPerGroup: 8,
            },
            1600: {
              slidesPerView: 10,
              slidesPerGroup: 10,
            },
          }}
          // navigation={true}
          modules={[Navigation]}
          // className="mySwiper"
          style={{ paddingRight: 20, paddingTop: 9 }}
        >
          {categoriesForTop.map(({ id, title, picSource, imageUrl }, idx) => {
            return (
              <SwiperSlide key={id}>
                <CategoryItem
                  isfirstelement={idx === 0}
                  onClick={() =>
                    areaCategoryChangeHandler(
                      router,
                      id,
                      title,
                      categories,
                      setCategories,
                      categoryQueryKeyWord,
                      setShops,
                      shopLoading,
                      setShopLoading
                    )
                  }
                >
                  {categories && categories[id] && (
                    <img
                      src="/Images/selectedCuisine.svg"
                      style={{
                        position: "absolute",
                        top: -15,
                        left: 10,
                        width: 66,
                        height: 52,
                      }}
                    />
                  )}

                  <span>
                    <img src={picSource + imageUrl} alt={title} />
                  </span>
                  <CategoryItemTitle>{title}</CategoryItemTitle>
                </CategoryItem>
              </SwiperSlide>
            );
          })}

          {!SSR && window.innerWidth < 1600 && (
            <>
              <Grow in={!isEnd}>
                <div>
                  <AreaCategoryCarouselNextButton />
                </div>
              </Grow>
              <Grow in={!isBeginning}>
                <div>
                  <AreaCategoryCarouselPrevButton />
                </div>
              </Grow>
            </>
          )}
        </CustomSwiper>
      </Grid>
    </GrowContainer>
  );
};

export default AreaCategoriesCarousel;
