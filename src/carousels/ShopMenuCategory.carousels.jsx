import { useState, useEffect } from "react";
import { Box, Typography } from "@mui/material";

import Scrollspy from "react-scrollspy";
import Link from "../links/Link";
import mealzoTheme from "../themes/mealzoTheme";

import "swiper/css";

function ShopMenuCategoryCarousel({ pageData }) {
  // const [isEnd, setIsEnd] = useState(false);
  // const [isBeginning, setIsBeginning] = useState(true);
  // const linkRef = useRef(null);

  const [activeId, setActiveId] = useState(null);
  const [win, setWin] = useState(null);
  const [isOnScroll, setIsOnScroll] = useState(false);

  const topCategories = pageData.map(({ categoryId, categoryTitle }) => ({
    categoryId,
    categoryTitle,
  }));

  useEffect(() => {
    setWin(window);
    //* Set active element
    const scrollFunction = () => {
      const active = document.querySelector(".active");
      if (active) {
        const currentActiveId = active.href.split("#")[1];
        setActiveId(currentActiveId);
      }
    };
    window.addEventListener("scroll", scrollFunction);

    //* Scroll on mouse wheel
    const scrollContainer = document.querySelector("#myScrolledCategory");
    const mouseWheelEvent = (evt) => {
      evt.preventDefault();
      scrollContainer.scrollLeft += evt.deltaY * 2;
    };
    scrollContainer.addEventListener("wheel", mouseWheelEvent);

    //* Scroll by grabbing and dragging
    let isDown = false;
    let startX;
    let scrollLeft;

    scrollContainer.addEventListener("mousedown", (e) => {
      isDown = true;
      scrollContainer.classList.add("active");
      startX = e.pageX - scrollContainer.offsetLeft;
      scrollLeft = scrollContainer.scrollLeft;
    });
    scrollContainer.addEventListener("mouseleave", () => {
      isDown = false;
      scrollContainer.classList.remove("active");
    });
    scrollContainer.addEventListener("mouseup", () => {
      isDown = false;
      scrollContainer.classList.remove("active");
    });
    scrollContainer.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - scrollContainer.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      scrollContainer.scrollLeft = scrollLeft - walk;
    });
    return () => {
      window.removeEventListener("scroll", scrollFunction);
    };
  }, []);

  useEffect(() => {
    if (!isOnScroll) {
      const categoryContainer = document.getElementById("myScrolledCategory");
      const active = document.querySelector(".active");
      if (active) categoryContainer.scrollLeft = active.offsetLeft - 10;
    }
  }, [activeId]);

  const handleLinkClick = (e) => {
    setIsOnScroll(true);
    const scrollFunction = () => {
      const categoryContainer = document.getElementById("myScrolledCategory");
      const active = document.querySelector(".active");
      if (active) {
        const currentActiveId = active.href.split("#")[1];
        setActiveId(currentActiveId);
      }
    };
    win.removeEventListener("scroll", scrollFunction);
    setTimeout(() => {
      setIsOnScroll(false);
    }, 1000);
  };

  return (
    <Box
      sx={{
        boxShadow: "0px 3px 6px #00000029",
        borderRadius: { xs: 0, md: "0 0 4px 4px" },
        // bgcolor: "white",
        bgcolor: "transparent",
        backdropFilter: 'blur(10px)',
        WebkitBackdropFilter: 'blur(10px)',
        position: "sticky",
        top: 0,
        borderBottom: "1px solid #00000029",
        py: 2,
        px: 1,
        mt: 5,
        overflowX: "auto",
        zIndex: mealzoTheme.zIndex.appBar,
        scrollBehavior: "smooth",
        "&::-webkit-scrollbar": {
          height: 5,
          display: "none",
        },
        scrollbarWidth: "thin",
        "&::-webkit-scrollbar-thumb": {
          borderRadius: "20px",
        },
        "& ul .active div": {
          color: { xs: "white", md: "primary.main" },
          bgcolor: { xs: "primary.main", md: "unset" },
          borderRadius: { xs: "50px", md: 0 },
          px: { xs: 2, md: "unset" },
          transition: "background-color 0.2s ease-out",
          transition: "color 0.2s ease-out",
        },
      }}
      id="myScrolledCategory"
    >
      <Scrollspy
        items={topCategories.map(({ categoryId }) => `${categoryId}`)}
        className="scrollspy-shopmenu"
        currentClassName="active"
        style={{
          overflow: "unset",
          position: "relative",
          // paddingRight: 10,
        }}
      >
        {topCategories.map(({ categoryId, categoryTitle }, index) => (
          <Link
            href={`#${categoryId}`}
            onClick={handleLinkClick}
            mx={1}
            key={index}
          >
            <Typography
              component="div"
              color="text.primary"
              fontWeight="500"
              fontSize={16}
              px={1}
              noWrap
            >
              {categoryTitle}
            </Typography>
          </Link>
        ))}
      </Scrollspy>
    </Box>
  );
}

export default ShopMenuCategoryCarousel;
