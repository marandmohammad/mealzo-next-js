import ReactTextTransition from "react-text-transition";

const NumberCountCarousel = ({ number, separated, ...restOfProps }) => {
  if (separated)
    return `${number}`
      .split("")
      .map((n, i) => (
        <ReactTextTransition
          key={i}
          children={n}
          delay={i * 50}
          direction="up"
          {...restOfProps}
        />
      ));

  return (
    <ReactTextTransition
      direction="up"
      style={{ height: "unset", alignItems: "center", minWidth: 10 }}
      {...restOfProps}
    >
      {number}
    </ReactTextTransition>
  );
};

export default NumberCountCarousel;
