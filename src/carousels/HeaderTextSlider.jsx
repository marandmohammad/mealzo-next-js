import { useEffect, useState } from "react";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";

import { Box, Typography } from "@mui/material";

// Import Swiper styles
import "swiper/css";

// Timer For Slider
import NextSlideTimer from "./NextSlideTimer";
import { useTranslation } from "next-i18next";

const HeaderTextSlider = ({ fontSize }) => {
  const [SSR, setSSR] = useState(true);
  useEffect(() => {
    setSSR(false);
  });

  const { t } = useTranslation('home_page')
  const Data = t('header_postcode_box_slider').split(',');

  return (
    <Box sx={{ height: "100%" }}>
      {!SSR && (
        <Swiper
          direction={"vertical"}
          slidesPerView={1}
          spaceBetween={10}
          speed={500}
          loop={true}
          modules={[Autoplay]}
          style={{ height: "100%" }}
        >
          {Data.map((item, idx) => {
            return (
              <SwiperSlide key={idx}>
                <Typography
                  variant="h3"
                  color="primary"
                  fontWeight="bold"
                  textAlign={{ xs: "center", sm: "left" }}
                  fontSize={fontSize}
                >
                  {item}
                </Typography>
              </SwiperSlide>
            );
          })}
          <NextSlideTimer />
        </Swiper>
      )}
    </Box>
  );
};

export default HeaderTextSlider;
