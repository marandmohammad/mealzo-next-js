import { styled } from "@mui/system";
import { Container } from "@mui/material";
import { Swiper } from "swiper/react";

export const CategoryContainer = styled(Container)(() => ({
  width: "100%",
  position: "relative",
  margin: 0,
  padding: 0,
}));

export const CustomSwiper = styled(Swiper)(() => ({
  width: "100%",
  height: "100%",
  position: "static !important",
}));

export const CategoriesDiv = styled("div")(() => ({
  width: "100%",
  height: "100%",
}));

export const CategoryItem = styled("div")(({theme, isfirstelement}) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  alignItems: "center",
  backgroundColor: isfirstelement ? theme.palette.primary.main : "#01707e", //"#e9540d"
  borderRadius: "5px",
  // height: "100px",
  width: "100%",
  cursor: "pointer",
  img: {
    width: "88px",
    height: "88px",
  },
  margin: "10px 0",
  transition: 'box-shadow ease-out 0.2s',
  transition: 'transform ease-out 0.2s',
  "&:hover":{
    boxShadow: `0 0 15px ${isfirstelement ? theme.palette.primary.main : theme.palette.secondary.main}90`,
    transform: 'translateY(1px)',
  }
}));

export const CategoryItemTitle = styled("span")(() => ({
  position: "absolute",
  bottom: 10,
  left: 5,
  alignSelf: "stretch",
  paddingBottom: "2px",
  paddingLeft: "5px",
  color: "white",
  fontWeight: "600",
  width: "100px",
  fontSize: 14,
  // transition: 'all ease-in 0.3s',
}));
