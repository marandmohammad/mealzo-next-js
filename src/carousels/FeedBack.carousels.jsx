import { useState } from "react";
import { useTranslation } from "next-i18next";

import { Grid, Typography } from "@mui/material";
import { Swiper, SwiperSlide } from "swiper/react";

import FeedBackCard from "../cards/FeedBack.cards";
import AreaCategoryCarouselNextButton from "../buttons/AreaCategoryCarouselNext.buttons";
import AreaCategoryCarouselPrevButton from "../buttons/AreaCategoryCarouselPrev.buttons";

import "swiper/css";

const fakeFeedBacks = new Array(10).fill(null).map((item, idx) => ({
  id: idx + 1,
  brandTitle: "Javits Takeaway",
  text: "Cool app. And it cost substantially less than other food ordering apps. I'm very happy that I've found this app.",
  writer: "G Watts",
}));

const FeedBackCarousel = ({ feedBack = fakeFeedBacks }) => {
  const { t } = useTranslation("become_partner_page");

  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  return (
    <Grid
      container
      flexDirection="column"
      flexWrap="nowrap"
      rowGap={5}
      position="relative"
    >
      <Typography variant="h6" fontSize={32} textAlign="center">
        {t("partners_feedBack")}
      </Typography>

      <Grid item xs={12}>
        <Swiper
          onSlideChange={(swiper) => {
            setIsBeginning(swiper.isBeginning);
            setIsEnd(swiper.isEnd);
          }}
          breakpoints={{
            480: {
              slidesPerView: 1.2,
              slidesPerGroup: 2,
            },
            640: {
              slidesPerView: 1.8,
              slidesPerGroup: 2,
            },
            1100: {
              slidesPerView: 2.5,
              slidesPerGroup: 2,
            },
            1370: {
              slidesPerView: 3.1,
              slidesPerGroup: 3,
            },
            1620: {
              slidesPerView: 4.1,
              slidesPerGroup: 3,
            },
            1770: {
              slidesPerView: 4.3,
              slidesPerGroup: 1,
            },
            1800: {
              slidesPerView: 4.5,
              slidesPerGroup: 1,
            },
          }}
          spaceBetween={24}
          className="mySwiper"
          style={{ position: "unset" }}
        >
          {feedBack.map(({ shopId, shopTitle, feedBack, ownerName }) => (
            <SwiperSlide key={shopId}>
              <FeedBackCard
                shopTitle={shopTitle}
                text={feedBack}
                author={ownerName}
                sx={{
                  height: "100%",
                }}
              />
            </SwiperSlide>
          ))}

          <Grid display="inline-flex" position="absolute" top={15} right={0}>
            <AreaCategoryCarouselPrevButton
              disabled={!isBeginning}
              sx={{ position: "unset", mr: 1, transform: "unset" }}
            />
            <AreaCategoryCarouselNextButton
              disabled={!isEnd}
              sx={{ position: "unset", transform: "unset" }}
            />
          </Grid>
        </Swiper>
      </Grid>
    </Grid>
  );
};

export default FeedBackCarousel;
