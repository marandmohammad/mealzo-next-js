import { useSwiper } from "swiper/react";

export default function NextSlideTimer() {
  const swiper = useSwiper();
  swiper.params.speed = 500;
  setInterval(() => {
    swiper.slideNext(500);
  }, 3000);
  return <div></div>;
}
