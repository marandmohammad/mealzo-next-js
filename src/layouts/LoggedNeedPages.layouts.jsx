import { useAuth } from "../../hooks/authentication";

import { NoSsr, Grid } from "@mui/material";

import PageContainer from "../containers/Section.containers";
import RegularAppBar from "../appbars/Regular.appbars";
import SignUpAndLoginForm from "../forms/SignUpAndLogin.forms";

const LoggedNeedPagesLayout = ({ children }) => {
  const { isLogin } = useAuth();

  return (
    <PageContainer flexDirection="column" sx={{ m: 0, px: 0 }}>
      <Grid container>
        <RegularAppBar />
      </Grid>
      <Grid
        container
        item
        my={{ xs: 0, md: 3 }}
        px={{ xs: 0, md: 2 }}
        mx="auto"
        maxWidth={1200}
      >
        <NoSsr>{isLogin ? children : <SignUpAndLoginForm md={4} />}</NoSsr>
      </Grid>
    </PageContainer>
  );
};

export default LoggedNeedPagesLayout;
