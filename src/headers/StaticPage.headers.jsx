import Image from "next/image";

import { Grid, Typography } from "@mui/material";

import GetToKnowUsTitleTypography from "../typographies/GetToKnowUsTitle.typographies";

function StaticPageHeader({ title, subtitle, imageUrl, bgColor = "primary" }) {
  return (
    <Grid
      container
      sx={{
        backgroundImage: `url(${
          bgColor === "primary" ? "/Images/mmmealzo.svg" : "/Images/mmmealzo1.svg"
        })`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "right bottom",
        bgcolor: `${bgColor}.light`,
        alignItems: "center",
        flexWrap: { md: "nowrap", xs: "wrap-reverse" },
        justifyContent: "center",
        px: { lg: 20, md: 10, sm: 5, xs: 5 },
        py: 2,
      }}
    >
      <Grid item flexGrow={1}>
        <GetToKnowUsTitleTypography component="h1" sx={{ width: "100%" }}>
          {title}
        </GetToKnowUsTitleTypography>
        {subtitle ? (
          <Typography variant="h6" component="div" color="common.white">
            {subtitle}
          </Typography>
        ) : null}
      </Grid>
      <Grid item sx={{ width: 250, height: 250 }}>
        <Image
          src={imageUrl}
          style={{ width: "100%", height: "100%" }}
          alt={title}
        />
      </Grid>
    </Grid>
  );
}

export default StaticPageHeader;
