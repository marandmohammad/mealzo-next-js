import Image from "next/image";
import { useTranslation } from "next-i18next";

import { Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";

import Link from "../links/Link";
import GrowContainer from "../containers/Grow.containers";
// import FooterLanguageButton from "../buttons/FooterLanguage.buttons";

import FooterDataObject from "../../utilities/FooterData";
import DownloadForAndroidImage from "../../public/Images/android.svg";
import DownloadForIOSImage from "../../public/Images/ios.svg";

const FooterTitleTypography = ({ children }) => {
  return (
    <Typography
      variant="h6"
      component="div"
      noWrap
      sx={{
        color: "common.white",
        mb: 2,
        fontSize: { lg: 17, md: "1.5vw" },
      }}
    >
      {children}
    </Typography>
  );
};

const FooterDataColumns = ({ title, sublinks, ...restOfProps }) => {
  const theme = useTheme();
  return (
    <Grid
      sx={{
        bgcolor: theme.palette.secondary.dark,
        px: 2,
        py: 3,
        borderRadius: "4px",
      }}
      item
      {...restOfProps}
    >
      <FooterTitleTypography>{title}</FooterTitleTypography>

      {sublinks?.map(({ text, href }, index) => (
        <Link href={href} key={index}>
          <Typography
            variant="subtitle1"
            fontSize={14}
            component="div"
            sx={{
              color: theme.palette.common.white,
              mb: 1,
            }}
          >
            {text}
          </Typography>
        </Link>
      ))}
    </Grid>
  );
};

function MainFooter() {
  const theme = useTheme();
  const { t } = useTranslation("footer");
  let getToKnowUs = [];
  let legal = [];
  try {
    getToKnowUs = JSON.parse(t("footer_get_to_know_us"));
    legal = JSON.parse(t("footer_legal"));

  } catch (e) {
    getToKnowUs = FooterDataObject[0];
    legal = FooterDataObject[3];
  }

  return (
    <GrowContainer>
      <Grid
        container
        component="footer"
        sx={{
          bgcolor: theme.palette.secondary.main,
          py: 4,
          px: { lg: 15, md: 10, sm: 5, xs: 2 },
          justifyContent: "space-between",
        }}
      >
        {/* Get to know us */}
        <FooterDataColumns
          title={getToKnowUs?.title}
          sublinks={getToKnowUs?.sublinks}
          md={2}
          xs={12}
          mb={{ md: 0, xs: 3 }}
        />

        {/* Dynamic columns */}
        {FooterDataObject.map(({ title, sublinks }, index) =>
          index === 1 || index === 2 ? (
            <FooterDataColumns
              title={title}
              sublinks={sublinks}
              md={2}
              xs={12}
              mb={{ md: 0, xs: 3 }}
              key={index}
            />
          ) : null
        )}

        {/* Legal */}
        <FooterDataColumns
          title={legal?.title}
          sublinks={legal?.sublinks}
          md={2}
          xs={12}
          mb={{ md: 0, xs: 3 }}
        />

        {/* Download our apps */}
        <Grid
          item
          container
          md={3}
          xs={12}
          sx={{
            px: 2,
            py: 3,
            bgcolor: theme.palette.secondary.dark,
            borderRadius: "4px",
          }}
        >
          <Grid item xs={12}>
            <FooterTitleTypography>
              {t("footer_download_our_apps")}
            </FooterTitleTypography>
          </Grid>
          <Grid item xs={6} md={5} lg={4} pr={1}>
            <Link
              href="https://apps.apple.com/us/app/mealzo/id1558698286"
              target="_blank"
            >
              <Image
                src={DownloadForIOSImage}
                alt="Mealzo download for ios"
                loading="lazy"
              />
            </Link>
          </Grid>
          <Grid item xs={6} md={5} lg={4} pr={1}>
            <Link
              href="https://play.google.com/store/apps/details?id=com.mealzowee"
              target="_blank"
            >
              <Image
                src={DownloadForAndroidImage}
                alt="Mealzo download for android"
                loading="lazy"
              />
            </Link>
          </Grid>
          <Grid item xs={12}>
            <FooterTitleTypography>
              {t("footer_follow_us")}
            </FooterTitleTypography>
          </Grid>
          <Grid item container xs={12} justifyContent="flex-start">
            <Grid width={40} height={40} mx={0.5}>
              <Link href="https://www.facebook.com/Mealzo/" target="_blank">
                <Image
                  src="/Images/facebook.svg"
                  alt="Mealzo facebook"
                  loading="lazy"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Grid>

            <Grid width={40} height={40} mx={0.5}>
              <Link
                href="https://www.instagram.com/mealzo.co.uk/"
                target="_blank"
              >
                <Image
                  src="/Images/instagram.svg"
                  alt="Mealzo instagram"
                  loading="lazy"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Grid>

            <Grid width={40} height={40} mx={0.5}>
              <Link href="https://www.youtube.com/Mealzo" target="_blank">
                <Image
                  src="/Images/youtube.svg"
                  alt="Mealzo youtube"
                  loading="lazy"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Grid>

            <Grid width={40} height={40} mx={0.5}>
              <Link href="https://twitter.com/Mealzo_uk" target="_blank">
                <Image
                  src="/Images/twitter.svg"
                  alt="Mealzo twitter"
                  loading="lazy"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Grid>

            <Grid width={40} height={40} mx={0.5}>
              <Link
                href="https://www.tiktok.com/@mealzo.co.uk?_t=8W3whZ633cT&_r=1"
                target="_blank"
              >
                <Image
                  src="/Images/TikTok.svg"
                  alt="Mealzo TikTok"
                  loading="lazy"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Grid>
          </Grid>
          {/* <Grid item mt={2}>
            <FooterLanguageButton />
          </Grid> */}
        </Grid>
      </Grid>
    </GrowContainer>
  );
}

export default MainFooter;
