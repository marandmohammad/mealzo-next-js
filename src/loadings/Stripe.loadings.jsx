import { Grid, Skeleton } from "@mui/material";

const StripeLoading = () => {
  return (
    <Grid container gap="0.75rem">
      <Grid item xs={12}>
        <Skeleton animation="wave" variant="rectangular" height={65} />
      </Grid>
      <Grid container item>
        <Grid item xs={6} sx={{ pr: "0.75rem" }}>
          <Skeleton animation="wave" variant="rectangular" height={65} />
        </Grid>
        <Grid item xs={6} sx={{ pl: "0.75rem" }}>
          <Skeleton animation="wave" variant="rectangular" height={65} />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Skeleton animation="wave" variant="rectangular" height={65} />
      </Grid>
      <Grid item xs={12}>
        <Skeleton animation="wave" variant="rectangular" height={65} />
      </Grid>
    </Grid>
  );
};

export default StripeLoading;
