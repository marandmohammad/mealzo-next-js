import { Skeleton } from "@mui/material";

const AppBarLoading = () => {
  return (
    <Skeleton variant="rectangular" animation="wave" width="100%" height={97} />
  );
};

export default AppBarLoading;
