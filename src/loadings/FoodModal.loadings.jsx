import { Grid, Skeleton } from "@mui/material";

const FoodModalLoading = () => {
  return (
    <Grid container flexDirection="column">
      <Skeleton variant="rectangular" animation='wave' height={50} sx={{ mb: 2 }} />
      {new Array(3).fill(null).map((el, idx) => (
        <Grid container py={1} px={5} key={idx}>
          <Grid item xs={10} display="flex" alignItems="center">
            <Skeleton variant="rectangular" animation='wave' width="90%" height={10} />
          </Grid>
          <Grid item xs={2} display="flex" alignItems="center">
            <Skeleton
              variant="circular"
              animation='wave'
              width={20}
              height={20}
              sx={{ display: "inline-block" }}
            />
            <Skeleton
              variant="rectangular"
              animation='wave'
              width={20}
              height={20}
              sx={{ mx: 1, display: "inline-block" }}
            />
            <Skeleton
              variant="circular"
              animation='wave'
              width={20}
              height={20}
              sx={{ display: "inline-block" }}
            />
          </Grid>
        </Grid>
      ))}
      <Skeleton variant="rectangular" animation='wave' height={50} sx={{ mb: 2, mt: 2 }} />
      {new Array(3).fill(null).map((el, idx) => (
        <Grid container py={1} px={5} key={idx}>
          <Grid item xs={10} display="flex" alignItems="center">
            <Skeleton variant="rectangular" animation='wave' width="90%" height={10} />
          </Grid>
          <Grid item xs={2} display="flex" alignItems="center">
            <Skeleton
              variant="circular"
              animation='wave'
              width={20}
              height={20}
              sx={{ display: "inline-block" }}
            />
            <Skeleton
              variant="rectangular"
              animation='wave'
              width={20}
              height={20}
              sx={{ mx: 1, display: "inline-block" }}
            />
            <Skeleton
              variant="circular"
              animation='wave'
              width={20}
              height={20}
              sx={{ display: "inline-block" }}
            />
          </Grid>
        </Grid>
      ))}
    </Grid>
  );
};

export default FoodModalLoading;
