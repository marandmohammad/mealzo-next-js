import { Grid, Skeleton } from "@mui/material";

const AreaPageLoading = () => {
  return (
    <Grid container flexDirection="column" gap={2}>
      <Grid item>
        <Skeleton
          variant="rectangular"
          animation="wave"
          width="100%"
          height={130}
          component="div"
        />
      </Grid>
      <Grid item>
        <Skeleton
          variant="rectangular"
          animation="wave"
          width="100%"
          height={185}
          component="div"
        />
      </Grid>
      <Grid item>
        <Skeleton
          variant="rectangular"
          animation="wave"
          width="100%"
          height={330}
          component="div"
        />
      </Grid>
      <Grid item>
        <Skeleton
          variant="rectangular"
          animation="wave"
          width="100%"
          height={360}
          component="div"
        />
      </Grid>
    </Grid>
  );
};

export default AreaPageLoading;
