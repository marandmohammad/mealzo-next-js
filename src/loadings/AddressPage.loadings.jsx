import { Grid, Skeleton } from "@mui/material";

const AddressPageLoading = () => {
  return (
    <Grid container flexDirection="column" gap={3} mt={3} p={1}>
      {new Array(4).fill(null).map((el, index) => (
        <Skeleton
          variant="rectangular"
          animation="wave"
          sx={{ height: 60 }}
          key={index}
        />
      ))}
    </Grid>
  );
};

export default AddressPageLoading;
