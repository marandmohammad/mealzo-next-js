import { Fragment } from "react";
import { Skeleton, Grid } from "@mui/material";

const MenuPageLoading = () => {
  return (
    <Fragment>
      <Skeleton
        variant="rectangular"
        animation="wave"
        sx={{
          width: "100%",
          height: { xs: 180, md: 97 },
        }}
      />
      <Grid
        container
        my={{ xs: 0.5, md: 2 }}
        px={{ xs: 0, md: 2 }}
        mx="auto"
        maxWidth={1200}
      >
        <Grid container item flexGrow={1} md={8} xs={12}>
          <Skeleton
            variant="rectangular"
            animation="wave"
            sx={{
              width: "100%",
              height: { xs: 396, md: 400 },
            }}
          />
          <Skeleton
            variant="rectangular"
            animation="wave"
            sx={{
              width: "100%",
              height: 57,
              mt: 5,
            }}
          />

          <Grid
            container
            flexDirection="column"
            px={{ xs: 1.5, md: 0 }}
            mt={2}
          >
            <Grid item container flexDirection="column" mb={1.5}>
              <Skeleton
                variant="text"
                animation="wave"
                sx={{
                  width: 150,
                }}
              />
            </Grid>
            <Grid
              item
              container
              display="grid"
              gridTemplateColumns={{ xs: "1fr", sm: "1fr 1fr" }}
              gap={3}
            >
              {new Array(8).fill(null).map((item, index) => (
                <Skeleton
                  variant="rectangular"
                  animation="wave"
                  sx={{
                    width: "100%",
                    height: 125,
                    borderRadius: { xs: "5px", md: "0 0 4px 4px" },
                  }}
                  key={index}
                />
              ))}
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={4} pl={3} display={{ xs: "none", md: "block" }}>
          <Skeleton
            variant="rectangular"
            animation="wave"
            sx={{
              width: "100%",
              height: 523,
              borderRadius: "4px",
            }}
          />
        </Grid>
        <Grid
          container
          sx={{
            position: "fixed",
            bottom: 0,
            p: 2,
            bgcolor: "white",
            boxShadow: "0px -3px 6px #00000029",
            display: { xs: "flex", md: "none" },
            zIndex: 1101,
            // boxShadow: '0 -1px 4px rgb(0 0 0 / 8%)'
          }}
        >
          <Skeleton
            variant="rectangular"
            animation="wave"
            sx={{
              width: "100%",
              height: 40,
              borderRadius: "4px",
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MenuPageLoading;
