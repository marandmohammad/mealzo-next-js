import { Skeleton } from "@mui/material";

const MainFooterLoading = () => {
  return (
    <Skeleton
      variant="rectangular"
      animation="wave"
      width="100%"
      height={300}
    />
  );
};

export default MainFooterLoading;
