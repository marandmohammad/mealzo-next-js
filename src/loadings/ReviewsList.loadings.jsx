import { Grid, Skeleton } from "@mui/material";

const ReviewsListLoading = () => {
  return (
    <Grid container flexDirection="column" rowGap={2}>
      {new Array(5).fill(null).map((item, idx) => (
        <Skeleton
          variant="rectangular"
          animation="wave"
          sx={{
            height: 105,
            width: "100%",
            borderRadius: "4px",
          }}
          key={idx}
        />
      ))}
    </Grid>
  );
};

export default ReviewsListLoading;
