import { Box, Skeleton } from "@mui/material";

const TopPicksLoading = () => {
  return (
    <Box
      sx={{
        width: 300,
        position: "relative",
      }}
    >
      <Skeleton
        variant="rectangular"
        animation="wave"
        width="100%"
        height={150}
        sx={{ mb: 1 }}
      />
      <Skeleton
        variant="circular"
        animation="wave"
        sx={{
          position: "absolute",
          right: 10,
          bottom: 25,
          width: 80,
          height: 80,
        }}
      />
      <Skeleton variant="text" animation="wave" width="50%" />
      <Skeleton variant="text" animation="wave" />
    </Box>
  );
};

export default TopPicksLoading;
