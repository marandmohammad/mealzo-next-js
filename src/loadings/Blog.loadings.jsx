import { Skeleton, Grid } from "@mui/material";

function BlogLoading({ area = false }) {
  return (
    <Grid
      container
      sx={{
        height: "100%",
        width: "100%",
        minHeight: "40vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
      spacing={1}
    >
      {/*<div*/}
      {/*    style={{*/}
      {/*        border: "5px solid #CCC",*/}
      {/*        borderTop: "5px solid #e9540d",*/}
      {/*        borderRadius: "50%",*/}
      {/*        width: "30px",*/}
      {/*        height: "30px",*/}
      {/*        animation: "spin 1s linear infinite",*/}
      {/*    }}*/}
      {/*></div>*/}

      {new Array(area ? 4 : 2).fill(null).map((_, index) => (
        <Grid item xs={12} sm={6} md={area ? 3 : 6} key={index}>
          <Skeleton
            variant="rectangular"
            animation="wave"
            width={"100%"}
            height={area ? 118 : 245}
          />
          <Skeleton variant="text" animation="wave" />
          <Skeleton variant="text" animation="wave" />
          {!area && (
            <>
              <Skeleton variant="text" animation="wave" />
              <Skeleton variant="text" animation="wave" />
            </>
          )}
        </Grid>
      ))}
    </Grid>
  );
}

export default BlogLoading;
