import { useState, useEffect } from "react";

function CsrSafeImage({
  src,
  alternativeSrc,
  Component = "img",
  ...restOfProps
}) {
  const [imageSrc, setImageSrc] = useState(alternativeSrc);

  useEffect(() => {
    const getImage = async () => {
      try {
        const image = await fetch(src);
        await setImageSrc(image.url);
      } catch (error) {
        await setImageSrc(alternativeSrc);
      }
    };

    getImage();

    return () => {
      setImageSrc(null);
    };
  }, [src]);

  return <Component src={imageSrc} {...restOfProps} />;
}

export default CsrSafeImage;
