import { useSwiper } from "swiper/react";
import { IconButton } from "@mui/material";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";

function AreaCategoryCarouselPrevButton({ sx }) {
  const swiper = useSwiper();
  return (
    <IconButton
      sx={{
        width: 35,
        height: 35,
        position: "absolute",
        top: "50%",
        left: -20,
        transform: "translateY(-50%)",
        boxShadow: "-6px 0 6px #f8f8f8",
        backgroundColor: "#FFF !important",
        border: "1px solid #c0c2c2",
        borderRadius: "50%",
        zIndex: 9,
        ...sx,
      }}
      aria-label="previous slide"
      onClick={() => swiper.slidePrev()}
    >
      <ChevronLeftIcon sx={{ color: "#938888" }} />
    </IconButton>
  );
}

export default AreaCategoryCarouselPrevButton;
