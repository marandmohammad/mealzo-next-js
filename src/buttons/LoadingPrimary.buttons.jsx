import { LoadingButton } from "@mui/lab";

const LoadingPrimaryButton = ({ sx, children, ...restOfProps }) => {
  return (
    <LoadingButton
      sx={{
        borderRadius: "4px",
        ...sx,
      }}
      {...restOfProps}
    >
      {children}
    </LoadingButton>
  );
};

export default LoadingPrimaryButton;
