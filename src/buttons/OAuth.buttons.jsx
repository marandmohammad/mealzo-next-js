import { LoadingButton } from "@mui/lab";

const OAuthButton = ({ children, sx, ...restOfProps }) => {
  return (
    <LoadingButton
      sx={{
        display: "flex",
        gap: 3,
        height: "35px",
        justifyContent: "center",
        border: "1px solid #3B5998",
        width: { xs: "100%", md: "318px" },
        bgcolor: "#3B5998",
        borderRadius: "20px",
        my: "10px",
        p: 0,
        "&:disabled": {
          cursor: "not-allowed",
          bgcolor: "#cccccc69",
        },
        ...sx,
      }}
      {...restOfProps}
    >
      {children}
    </LoadingButton>
  );
};

export default OAuthButton;
