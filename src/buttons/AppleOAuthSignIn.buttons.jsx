import AppleLogin from "react-apple-login";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { Typography } from "@mui/material";
import AppleIcon from "@mui/icons-material/Apple";

import OAuthButton from "./OAuth.buttons";

import { cookiesNameSpaces } from "../../utilities/appWideHelperFunctions";
import { setCookies } from "cookies-next";
const { APPLE_SIGN_IN_LOCATION, APPLE_SIGN_IN_LOCALE } = cookiesNameSpaces;

const AppleOAuthSignInButton = () => {
  const router = useRouter();
  const { t } = useTranslation("login_form");

  return (
    <AppleLogin
      clientId={process.env.NEXT_PUBLIC_APPLE_CLIENT_ID}
      redirectURI={process.env.NEXT_PUBLIC_APPLE_CALLBACK_URL}
      usePopup={false}
      responseType="code id_token"
      responseMode="form_post"
      scope="name email"
      render={(appleLoginProps) => (
        <OAuthButton
          onClick={() => {
            setCookies(APPLE_SIGN_IN_LOCATION, router.asPath);
            setCookies(APPLE_SIGN_IN_LOCALE, router.locale);
            appleLoginProps.onClick();
          }}
          disabled={appleLoginProps.disabled}
          sx={{
            // pl: 5,
            mb: 5,
            mx: "auto",
            width: "100%",
            borderColor: "#EAEAEA",
            bgcolor: "background.default",
            "&:hover": {
              bgcolor: "background.default",
            },
          }}
        >
          <AppleIcon sx={{ color: "text.newMedium" }} />
          <Typography
            component="span"
            sx={{
              color: "text.newMedium",
              fontSize: "14px",
            }}
          >
            {t('sign_in_with_apple')}
          </Typography>
        </OAuthButton>
      )}
    />
  );
};

export default AppleOAuthSignInButton;
