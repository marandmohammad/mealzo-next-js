import { Button } from "@mui/material";
import { styled } from "@mui/material/styles";

const LandingAppBarButton = styled(Button)(({ theme, variant }) => {
  switch (variant) {
    case "contained":
      return {
        maxHeight: 40,
        borderRadius: "24px",
        backgroundColor: theme.palette.primary.main,
        color: "#F9F9F9",
        // backgroundColor: "#F9F9F9",
        // color: theme.palette.primary.main,
        padding: "10px 37px",
        fontSize: 16,
        fontWeight: "bold",
        "&:hover": {
          // backgroundColor: "rgba(255, 255, 255, 1)",
          boxShadow: 1,
        },
      };

    case "outlined":
      return {
        maxHeight: 40,
        borderRadius: "24px",
        backgroundColor: "transparent",
        // color: "#F9F9F9",
        color: theme.palette.primary.main,
        padding: "10px 25px",
        fontSize: 16,
        fontWeight: "bold",
        // borderColor: "#F9F9F9",
        borderColor: theme.palette.primary.main,
        "&:hover": {
          backgroundColor: "transparent",
          boxShadow: 1,
          // borderColor: "#F9F9F9",
          borderColor: theme.palette.primary.main,
        },
      };

    default:
      return {
        maxHeight: 40,
        borderRadius: "24px",
        backgroundColor: "transparent",
        color: "#F9F9F9",
        padding: "10px 25px",
        fontSize: 16,
        fontWeight: "bold",
        borderColor: "#F9F9F9",
        border: "none",
        "&:hover": {
          backgroundColor: "transparent",
          boxShadow: 1,
          border: "none",
        },
      };
  }
});

export default LandingAppBarButton;
