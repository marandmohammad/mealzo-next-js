import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import Image from "next/image";
import { useTranslation } from "next-i18next";

//! Custom Hooks
import { useAuth } from "../../hooks/authentication";
import useOrder from "../../hooks/orders";
import useNotification from "../../hooks/useNotification";

import { Typography } from "@mui/material";

import OAuthButton from "./OAuth.buttons";

import FaceBookIcon from "../../public/Images/facebook-icon.svg";

import {
  accountTypes,
  appInfoForAuthentication,
} from "../../utilities/appWideHelperFunctions";
import axiosConfig from "../../config/axios";

const FacebookOAuthSignInButton = ({ onLoginSuccess }) => {
  const { t } = useTranslation("login_form");
  const { signIn } = useAuth();
  const { getOrderId } = useOrder();
  const { showNotification } = useNotification();

  const responseFacebook = async (response) => {
    try {
      const { userID, name, email } = response;

      const reqData = {
        fullName: name,
        email: email,
        openId: userID,
        accountType: accountTypes.facebook,
        ...appInfoForAuthentication(),
      };

      const mealzoLogin = await axiosConfig.post(
        "/Account/ExternalSignup",
        reqData
      );

      await signIn(mealzoLogin.data, getOrderId());
      onLoginSuccess();
    } catch (error) {
      showNotification(error.message, "error", 3000);
    }
  };

  return (
    <FacebookLogin
      appId={process.env.NEXT_PUBLIC_FACEBOOK_APP_ID}
      autoLoad={false}
      fields="name,email"
      scope="email,public_profile"
      cssClass="facebook-login-button"
      icon={<Image src={FaceBookIcon} />}
      callback={responseFacebook}
      render={(renderProps) => (
        <OAuthButton
          sx={{
            // pl: 6,
            mx: "auto",
            "&:hover": {
              bgcolor: "#3B5998",
            },
            width: "100%",
          }}
          onClick={renderProps.onClick}
        >
          {/*<FaceBookIcon />*/}
          <Image src="/Images/facebook-icon.svg" width={8} height={17} />
          <Typography
            component="span"
            sx={{
              color: "common.white",
              fontSize: "14px",
            }}
          >
            {t('login_with_facebook')}
          </Typography>
        </OAuthButton>
      )}
    />
  );
};

export default FacebookOAuthSignInButton;
