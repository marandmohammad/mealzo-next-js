import { useState } from "react";

import { Box, ClickAwayListener, IconButton } from "@mui/material";

import LanguageIcon from "@mui/icons-material/Language";

import ChangeLanguageList from "../lists/ChangeLanguage.lists";

const LanguageButton = ({
  color,
  sx,
  containerStyle,
  listTopPos,
  listLeftPos,
  ...restOfProps
}) => {
  const [open, setOpen] = useState(false);

  const toggleOpen = () => {
    setOpen(!open);
  };

  return (
    <ClickAwayListener onClickAway={() => setOpen(false)}>
      <Box position="relative" sx={containerStyle}>
        <IconButton
          color={color}
          onClick={toggleOpen}
          sx={{
            borderWidth: 1,
            borderColor: color,
            borderStyle: "solid",
          }}
          {...restOfProps}
        >
          <LanguageIcon fontSize="small" />
        </IconButton>
        <ChangeLanguageList
          in={open}
          routeChangeCallback={toggleOpen}
          top={listTopPos}
          left={listLeftPos}
        />
      </Box>
    </ClickAwayListener>
  );
};

export default LanguageButton;
