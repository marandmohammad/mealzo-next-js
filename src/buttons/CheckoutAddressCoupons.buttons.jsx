import Button from "@mui/material/Button";

function CheckoutAddressCouponsButton({
  children,
  sx,
  onClick,
  loading,
  ...restOfProps
}) {
  return (
    <Button
      sx={{
        height: 72,
        bgcolor: "white",
        boxShadow: "0px 0px 15px rgba(0, 0, 0, 0.1)",
        borderRadius: "8px",
      }}
      disabled={loading}
      fullWidth
      onClick={onClick}
      {...restOfProps}
    >
      {children}
    </Button>
  );
}

export default CheckoutAddressCouponsButton;
