import { useState } from "react";
import Image from "next/image";
import { useGoogleLogin } from "@react-oauth/google";
import { useTranslation } from "next-i18next";
import axios from "axios";
import { useAuth } from "../../hooks/authentication";
import useOrder from "../../hooks/orders";

import { Typography } from "@mui/material";
import OAuthButton from "./OAuth.buttons";

import googleIcon from "../../public/Images/googleIcon.png";

import {
  appInfoForAuthentication,
  // decodeJwt,
} from "../../utilities/appWideHelperFunctions";
import { accountTypes } from "../../utilities/appWideHelperFunctions";
import axiosConfig from "../../config/axios";

const GoogleOAuthSignInButton = ({ onLoginSuccess }) => {
  const { t } = useTranslation("login_form");
  const { signIn } = useAuth();
  const { getOrderId } = useOrder();
  const [loading, setLoading] = useState(false);

  const login = useGoogleLogin({
    onSuccess: async (tokenResponse) => {
      await setLoading(true);

      const { access_token } = tokenResponse;
      const userInfo = await axios.get(
        "https://www.googleapis.com/oauth2/v1/userinfo?alt=json",
        {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        }
      );
      const { id, given_name, family_name, email, name, picture } =
        userInfo.data;

      const reqData = {
        fullName: name,
        email: email,
        openId: id,
        accountType: accountTypes.google,
        ...appInfoForAuthentication(),
      };

      const mealzoLogin = await axiosConfig.post(
        "/Account/ExternalSignup",
        reqData
      );

      await signIn(mealzoLogin.data, getOrderId());
      await setLoading(false);

      onLoginSuccess();
    },
    onError: (err) => console.error(err),
  });

  return (
    <OAuthButton
      sx={{
        mx: "auto",
        // pl: 5.5,
        width: "100%",
        borderColor: "#EAEAEA",
        bgcolor: "background.default",
        "&:hover": {
          bgcolor: "background.default",
        },
      }}
      onClick={login}
      disabled={loading}
      loading={loading}
    >
      <Image src={googleIcon} alt="" />
      <Typography
        component="span"
        sx={{
          color: "text.newMedium",
          fontSize: "14px",
        }}
      >
        {t('sign_in_with_google')}
      </Typography>
    </OAuthButton>
  );
};

export default GoogleOAuthSignInButton;
