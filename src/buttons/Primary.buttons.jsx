import { Button } from "@mui/material";

const PrimaryButton = ({ sx, children, ...restOfProps}) => {
  return (
    <Button
      sx={{
        color: "common.white",
        borderRadius: "4px",
        padding: "10px 50px",
        backgroundColor: "primary.main",
        fontSize: "12px",
        border: "none",
        "&:hover": {
          backgroundColor: "primary.main",
        },
          "&:disabled": {
              bgcolor: "#e9540d7d",
              borderColor: "#e9540d7d",
              color: "#FFF",
          },
        ...sx,
      }}
      {...restOfProps}
    >
      {children}
    </Button>
  );
};

export default PrimaryButton;
