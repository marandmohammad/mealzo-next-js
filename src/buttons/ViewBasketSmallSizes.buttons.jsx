import { Grid, Button } from "@mui/material";

import currencyIdToMark from "../../utilities/currencyIdToMark";
import { calculateNumberOfBasketItems } from "../../utilities/appWideHelperFunctions";

const ViewBasketSmallSizesButton = ({
  buttonText = "View basket",
  basket,
  currencyId,
  onClick,
}) => {
  const [numberOfBasketItems, totalAmount] = basket
    ? [
        calculateNumberOfBasketItems(basket),
        currencyIdToMark(currencyId, basket.subTotal),
      ]
    : [null, null];

  return (
    <Button
      variant="contained"
      fullWidth
      onClick={onClick}
      sx={{ borderRadius: "4px" }}
    >
      {numberOfBasketItems ? (
        <Grid
          container
          flexWrap="nowrap"
          alignItems="center"
          justifyContent="space-between"
          height="100%"
        >
          <Grid
            item
            display="flex"
            height="100%"
            minWidth={28}
            bgcolor="rgba(0, 0, 0, 0.2)"
            borderRadius="4px"
            fontSize={14}
            alignItems="center"
            justifyContent="center"
          >
            {numberOfBasketItems}
          </Grid>
          <Grid item>{buttonText}</Grid>
          <Grid item fontSize={14}>
            {totalAmount}
          </Grid>
        </Grid>
      ) : (
        buttonText
      )}
    </Button>
  );
};

export default ViewBasketSmallSizesButton;
