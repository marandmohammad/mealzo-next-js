import { IconButton } from "@mui/material";

function RegularIconsButton({
  children,
  sx,
  width = 22,
  onClick,
  ...restOfProps
}) {
  return (
    <IconButton
      sx={{
        p: 0,
        borderRadius: "4px",
        // boxShadow: "0px 0px 24px #24242419",
        boxShadow: '2px 2px 18px #d9d9d9, -2px -2px 18px #ffffff',
        background: "#ffffff",
        width: width,
        height: width,
        // borderWidth: 1,
        // borderStyle: "solid",
        // borderColor: 'primary.main',
        ...sx,
      }}
      onClick={onClick}
      {...restOfProps}
    >
      {children}
    </IconButton>
  );
}

export default RegularIconsButton;
