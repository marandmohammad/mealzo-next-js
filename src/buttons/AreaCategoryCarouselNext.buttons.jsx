import { useSwiper } from "swiper/react";
import { IconButton } from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

function AreaCategoryCarouselNextButton({ sx }) {
  const swiper = useSwiper();
  return (
    <IconButton
      sx={{
        width: 35,
        height: 35,
        position: "absolute",
        top: "50%",
        right: -10,
        transform: "translateY(-50%)",
        boxShadow: "-6px 0 6px #f8f8f8",
        backgroundColor: "#FFF !important",
        border: "1px solid #c0c2c2",
        borderRadius: "50%",
        zIndex: 9,
        ...sx,
      }}
      aria-label="next slide"
      onClick={() => swiper.slideNext()}
    >
      <ChevronRightIcon sx={{ color: "#938888" }} />
    </IconButton>
  );
}

export default AreaCategoryCarouselNextButton;
