import { IconButton } from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";

const iconStyle = { color: "white", fontSize: 13 };

function RoundedPlusMinusButton({
  width = 18,
  type = "minus",
  onClick,
  sx,
  ...restOfProps
}) {
  return (
    <IconButton
      onClick={onClick}
      sx={{
        borderRadius: "50%",
        borderWidth: "1px",
        borderStyle: "solid",
        borderColor: "primary.main",
        width: width,
        height: width,
        bgcolor: "primary.main",
        "&:hover": {
          bgcolor: "primary.main",
        },
        "&:disabled": {
          bgcolor: "#e9540d7d",
          borderColor: "#e9540d7d",
        },
        ...sx,
      }}
      {...restOfProps}
    >
      {type === "minus" ? (
        <RemoveIcon sx={iconStyle} />
      ) : (
        <AddOutlinedIcon sx={iconStyle} />
      )}
    </IconButton>
  );
}

export default RoundedPlusMinusButton;
