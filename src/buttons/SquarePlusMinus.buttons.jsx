import { IconButton } from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";

function SquarePlusMinusButton({
  width = 16,
  type = "minus",
  onClick,
  sx,
  ...restOfProps
}) {
  return (
    <IconButton
      onClick={onClick}
      sx={{
        borderRadius: 0,
        borderWidth: "1px",
        borderStyle: "solid",
        borderColor: "primary.main",
        width: width,
        height: width,
        bgcolor: type === "minus" ? "transparent" : "primary.main",
        "&:hover": {
          bgcolor: type === "minus" ? "transparent" : "primary.main",
        },
        "&:disabled": {
          bgcolor: "#e9540d7d",
          borderColor: "#e9540d7d",
          cursor: "progress",
        },
        ...sx,
      }}
      {...restOfProps}
    >
      {type === "minus" ? (
        <RemoveIcon sx={{ color: "primary.main", fontSize: 13 }} />
      ) : (
        <AddOutlinedIcon sx={{ color: "white", fontSize: 13 }} />
      )}
    </IconButton>
  );
}

export default SquarePlusMinusButton;
