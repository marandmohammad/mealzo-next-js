import { Typography } from "@mui/material";

function ErrorPagesTypography({ children, ...restOfProps }) {
  return (
    <Typography
      fontWeight={700}
      fontSize={64}
      color="#505050"
      textAlign="center"
      {...restOfProps}
    >
      {children}
    </Typography>
  );
}

export default ErrorPagesTypography;
