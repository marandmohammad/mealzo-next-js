import PropTypes from "prop-types";

import { Typography } from "@mui/material";
import { useTheme } from "@mui/material";

function GetToKnowUsTitleTypography({ children, sx, ...restOfProps }) {
  const theme = useTheme();
  return (
    <Typography
      sx={{
        fontFamily: ["Montserrat", "sans-serif"].join(","),
        fontWeight: 800,
        fontSize: children.length <= 22 ? { md: 80, sm: 60, xs: 40 } : { md: 50, sm: 40, xs: 20 },
        color: theme.palette.common.white,
        textAlign: { md: "left", sm: "center", xs: "center" },
        ...sx,
      }}
      {...restOfProps}
    >
      {children}
    </Typography>
  );
}

GetToKnowUsTitleTypography.propTypes = {
  children: PropTypes.node,
  sx: PropTypes.object,
};

export default GetToKnowUsTitleTypography;
