import { Typography } from "@mui/material";
import { styled } from "@mui/material/styles";

const RestaurantCardBadgeTypography = styled(Typography)(
  ({ theme, promoted }) => ({
    color: promoted ? "#f9f9f9" : "#f9f9f9",
    backgroundColor: promoted ? theme.palette.secondary.main : theme.palette.primary.main,
    padding: "5px 10px",
    whiteSpace: "nowrap",
    fontSize: 12,
    fontWeight: "bold",
    borderTopRightRadius: "5px",
    borderBottomRightRadius: "5px",
    width: promoted ? "max-content" : "unset",
  })
);

export default RestaurantCardBadgeTypography;
