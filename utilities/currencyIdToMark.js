export default function currencyIdToMark(currencyId, amount) {
  if (amount === null || amount === undefined || !currencyId) return null;
  const fixedAmount = amount.toFixed(2);
  switch (currencyId) {
    case 1002501:
      return "£" + fixedAmount;
    case 1002502:
      return "$" + fixedAmount;
    case 1002503:
      return fixedAmount + "€";
    default:
      return null;
  }
}
/*
1002501	Pound
1002502	Dollar
1002503	Euro
 */
