import { queryParamEncode, queryKeyWords } from "./areaPathHandlers";
const { postCodeQueryKeyWord, saleMethodQueryKeyWord } = queryKeyWords;

const buildHrefForFooterCities = (decodePostCode) => {
  return {
    pathname: "/area/[area]",
    query: {
      [postCodeQueryKeyWord]: queryParamEncode(
        decodePostCode,
        postCodeQueryKeyWord
      ),
      [saleMethodQueryKeyWord]: "delivery",
    },
  };
};

export default [
  {
    title: "Get to know us",
    sublinks: [
      {
        text: "About Mealzo",
        href: "/about-us",
      },
      {
        text: "Our Blog",
        href: "/OurBlog",
      },
      {
        text: "Become a Partner",
        href: "/Partner",
      },
      {
        text: "Careers",
        href: "/careers",
      },
      {
        text: "Help",
        href: "/help",
      },
      {
        text: "Top local Brands",
        href: "/brands",
      },
      {
        text: "Mealzo Local Heroes",
        href: "/local-heroes",
      },
    ],
  },
  {
    title: "Top cuisines",
    sublinks: [
      {
        text: "Indian",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Indian",
          },
        },
      },
      {
        text: "Pizza",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Pizza",
          },
        },
      },
      {
        text: "Halal",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Halal",
          },
        },
      },
      {
        text: "Kebabs",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Kebabs",
          },
        },
      },
      {
        text: "Italian",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Italian",
          },
        },
      },
      {
        text: "Fish & Chips",
        href: {
          pathname: "/area/all",
          query: {
            sm: "delivery",
            cuisine: "Fish-and-Chips",
          },
        },
      },
    ],
  },
  {
    title: "Popular locations",
    sublinks: [
      {
        text: "Glasgow",
        href: buildHrefForFooterCities("G1 1AB"),
      },
      {
        text: "Edinburgh",
        href: buildHrefForFooterCities("EH1 1BB"),
      },
      {
        text: "Dunfermline",
        href: buildHrefForFooterCities("KY12 7AU"),
      },
      {
        text: "Livingston",
        href: buildHrefForFooterCities("EH54 6BN"),
      },
      {
        text: "Bathgate",
        href: buildHrefForFooterCities("EH48 1PH"),
      },
      {
        text: "Paisley",
        href: buildHrefForFooterCities("PA1 2AB"),
      },
    ],
  },
  {
    title: "Legal",
    sublinks: [
      {
        text: "Cookie Policy",
        href: "/cookies-policy",
      },
      {
        text: "Modern Slavery",
        href: "/modern-slavery-statement",
      },
      {
        text: "Privacy Policy",
        href: "/privacy-policy",
      },
      {
        text: "Terms and Conditions",
        href: "/terms-and-conditions",
      },
    ],
  },
];
