import { getCookie } from "cookies-next";
import currencyIdToMark from "./currencyIdToMark";
import { saleMethods } from "./areaPathHandlers";

export const cookiesNameSpaces = {
  DELIVERY_TIME: "deliveryTime",
  LOCATION_DETAIL: "LocationDetail",
  ORDER_ID: "orderId",
  SHOP_IDENTITY: "mealzoIdentity",
  AUTHENTICATION: "userDetail",
  SELECTED_ADDRESS: "selectedAddress",
  SHOP_ID: "shopId",
  SALE_METHOD: "saleMethod",
  APPLE_SIGN_IN_LOCATION: "appleSignInLocation",
  APPLE_SIGN_IN_LOCALE: "appleSignInLocale",
  LOCALE: "Locale",
};

export const saleMethodChangeStatusCodes = {
  CLOSE_SHOP: 423,
  NO_DELIVERY_SHOP: 406,
  PRE_ORDER_SHOP: 100,
  OK_SHOP: 200,
};

export const accountTypes = {
  google: "1002603",
  apple: "1002604",
  facebook: "1002602",
};

export const localesList = {
  "en-GB": "English-UK",
  "ta-IN": "Tamil-India",
};

export const calcPriceIconGapTimeUseCases = {
  RESTAURANT_CART: "RESTAURANT_CART",
  SHOP_MENU_HEADER: "SHOP_MENU_HEADER",
};

export const pageNames = {
  AREA_PAGE: "area",
  MENU_PAGE: "menu",
  CHECKOUT_PAGE: "checkout",
};

export const appInfoForAuthentication = () => {
  return {
    version: process.env.NEXT_PUBLIC_APP_VERSION,
    deviceType: process.env.NEXT_PUBLIC_APP_DEVICE_TYPE,
    licesnse: process.env.NEXT_PUBLIC_APP_LICENSE,
    Language: process.env.NEXT_PUBLIC_APP_LANGUAGE,
  };
};

export const decodeJwt = (token) => {
  let base64Url = token.split(".")[1];
  let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  let jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};

export const getDeliveryTime = () => {
  const rawDeliveryTime = getCookie(cookiesNameSpaces.DELIVERY_TIME) || null;
  return rawDeliveryTime ? JSON.parse(rawDeliveryTime) : rawDeliveryTime;
};

export const buildStructuredAddress = (selectedAddress) => {
  return selectedAddress && typeof selectedAddress === "object"
    ? `${selectedAddress.deliveryAddress1 + ", "}${
        selectedAddress.deliveryAddress2
          ? selectedAddress.deliveryAddress2 + ", "
          : ""
      }${
        selectedAddress.deliveryCity ? selectedAddress.deliveryCity + ", " : ""
      }${
        selectedAddress.deliveryPostcode ? selectedAddress.deliveryPostcode : ""
      }`
    : null;

  /*
    ${
        selectedAddress.deliveryAddress3
          ? selectedAddress.deliveryAddress3 + ", "
          : ""
      }
    */
};

export const getUserAccessToken = () => {
  const rawUser = getCookie(cookiesNameSpaces.AUTHENTICATION) || null;
  if (!rawUser) return rawUser;

  const { accessToken } = JSON.parse(rawUser);
  return accessToken;
};

export const calcPriceIconGapTime = (
  isOpen,
  currencyId,
  deliveryFee,
  deliveryTimeGap,
  shopSubjects,
  collectionTimeGap,
  saleMethod,
  t,
  differentSaleMethod = null,
  useCase = calcPriceIconGapTimeUseCases.SHOP_MENU_HEADER
) => {
  let deliveryIcon = "";
  // let currencyMark = "";
  let price = "";
  let gapTime = false;
  let isCollectNow = false;

  const checkDelivery = () => {
    if (currencyId === 0) {
      deliveryIcon = "wing.png";
      price = t("restaurant_card_free_delivery");
    } else {
      if (parseFloat(deliveryFee) > 0) {
        deliveryIcon = "winggray.png";
        price = currencyIdToMark(currencyId, parseFloat(deliveryFee));
      } else {
        deliveryIcon = "wing.png";
        price = t("restaurant_card_free_delivery");
      }
    }
  };

  // Price Part Conditions And Data Split
  if (isOpen.toLowerCase() === "open") {
    if (differentSaleMethod) {
      if (differentSaleMethod === "delivery") {
        checkDelivery();
      } else if (differentSaleMethod === "collection") {
        price = t("restaurant_card_collect_now");
        deliveryIcon = "collect-delivery-now-icon.png";
        isCollectNow = true;
      }
    } else if (saleMethod === "delivery") {
      checkDelivery();
    } else if (saleMethod === "collection") {
      if (useCase === calcPriceIconGapTimeUseCases.RESTAURANT_CART) {
        price = "";
        deliveryIcon = "";
      } else {
        checkDelivery();
      }
    }
  } else {
    deliveryIcon = "winggray.png";
    price = isOpen;
  }

  // Gap Time Conditions And Data Split
  if (isOpen.toLowerCase() === "open") {
    if (saleMethod === "delivery") gapTime = deliveryTimeGap;
    else gapTime = collectionTimeGap;
  } else if (isOpen.toLowerCase().startsWith("opening"))
    gapTime = t("restaurant_card_pre_order");

  const categories =
    shopSubjects && shopSubjects.trim() !== "" ? shopSubjects.split(",") : null;

  return { deliveryIcon, price, gapTime, categories, isCollectNow };
};

export const validateSaleMethod = (saleMethod) => {
  if (typeof saleMethod !== "string" || saleMethod === "") {
    return false;
  }

  for (const saleMethodKey in saleMethods)
    if (saleMethodKey.toLowerCase() === saleMethod.toLowerCase())
      return saleMethodKey;

  return false;
};

export const addLocaleToStripeCallbackUrl = (router) => {
  let returnUrl = process.env.NEXT_PUBLIC_STRIPE_CALLBACK_URL;

  if (router.locale !== router.defaultLocale) {
    const url = new URL(process.env.NEXT_PUBLIC_STRIPE_CALLBACK_URL);
    const splitPathName = url.pathname.split("/");
    splitPathName[0] = router.locale;
    url.pathname = splitPathName.join("/");
    returnUrl = url.href;
  }

  return returnUrl;
};

export const getLocaleFromCookie = (backend) => {
  if (backend) {
    const { req, res } = backend;
    return getCookie(cookiesNameSpaces.LOCALE, { req, res });
  }
  return getCookie(cookiesNameSpaces.LOCALE);
};

export const calculateNumberOfBasketItems = (basket) => {
  let itemCount = 0;
  for (let idx = 0; idx < basket.orderDetail.length; idx++) {
    itemCount += basket.orderDetail[idx].count;
  }
  return itemCount;
};
