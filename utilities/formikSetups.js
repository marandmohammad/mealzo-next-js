import { useFormik } from "formik";

//! validate by pattern
export const isValidPhone = (phone) => /07[0-9]{9}/.test(phone);

export const isValidEmail = (email) =>
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );

//! validate and return error message object
export const validatePhoneNumber = (
  phone,
  requiredMessage = "Required",
  phoneLowCharMessage = "Must be 11 characters",
  invalidPhoneMessage = "Invalid phone number"
) => {
  let errors = {};

  if (!phone || phone.trim() === "") errors.phone = requiredMessage;
  else if (phone.length !== 11) errors.phone = phoneLowCharMessage;
  else if (!isValidPhone(phone)) errors.phone = invalidPhoneMessage;

  return errors.phone;
};

export const validateEmail = (email, requiredMessage, notValidMessage) => {
  let errors = {};

  if (!email) errors.email = requiredMessage;
  else if (!isValidEmail(email)) {
    errors.email = notValidMessage;
  }

  return errors.email;
};

export const addNewAddressFormikSetup = (editInitialValues = null) => {
  /*
      {
        "deliveryAddress1": "string",
        "deliveryAddress2": "string",
        "deliveryAddress3": "string",
        "deliveryCity": "string",
        "deliveryPostcode": "string",
        "deliveryAddressName": "string",
        "deliveryLat": 0,
        "deliveryLong": 0
      }
       */

  const initialValues = editInitialValues
    ? {
        deliveryAddress1: editInitialValues.deliveryAddress1
          ? editInitialValues.deliveryAddress1
          : "",
        deliveryAddress2: editInitialValues.deliveryAddress2
          ? editInitialValues.deliveryAddress2
          : "",
        deliveryAddress3: editInitialValues.deliveryAddress3
          ? editInitialValues.deliveryAddress3
          : "",
        deliveryCity: editInitialValues.deliveryCity
          ? editInitialValues.deliveryCity
          : "",
        deliveryPostcode: editInitialValues.deliveryPostcode
          ? editInitialValues.deliveryPostcode
          : "",
        deliveryAddressName: editInitialValues.deliveryAddressName
          ? editInitialValues.deliveryAddressName
          : "",
        deliveryLat: editInitialValues.deliveryLat
          ? editInitialValues.deliveryLat
          : "",
        deliveryLong: editInitialValues.deliveryLong
          ? editInitialValues.deliveryLong
          : "",
      }
    : {
        deliveryAddress1: "",
        deliveryAddress2: "",
        deliveryAddress3: "",
        deliveryCity: "",
        deliveryPostcode: "",
        deliveryAddressName: "",
        deliveryLat: 0,
        deliveryLong: 0,
      };

  const validate = (values) => {
    let errors = {};
    const { deliveryAddress1, deliveryCity, deliveryPostcode } = values;

    if (!deliveryAddress1 || deliveryAddress1.trim() === "")
      errors.deliveryAddress1 = "Required";
    // if (!deliveryCity || deliveryCity.trim() === "")
    //   errors.deliveryCity = "Required";
    if (!deliveryPostcode || deliveryPostcode.trim() === "")
      errors.deliveryPostcode = "Required";

    return errors;
  };
  const formik = useFormik({ initialValues, validate });
  return formik;
};

export const checkoutFormikSetup = (
  saleMethod,
  userPhoneNumber,
  isConfirmSms = true,
  initDeliveryAddress = null,
  dTime = null,
  isHaveCouponCode = null
) => {
  let initialValues;
  let validate;

  const phoneNumber =
    typeof userPhoneNumber === "string" && userPhoneNumber.trim() !== ""
      ? userPhoneNumber
      : "";

  const dTimeInitialValue =
    dTime && typeof dTime === "object" ? dTime.id.toString() : "0";
  const initialCouponCode = isHaveCouponCode ? true : false;

  if (saleMethod === "delivery") {
    initialValues = {
      deliveryAddress: initDeliveryAddress,
      phone: phoneNumber,
      instructionForRider: "",
      specialRequest: "",
      dTime: dTimeInitialValue,
      offCode: initialCouponCode,
      receive: isConfirmSms,
    };

    validate = (values) => {
      const { deliveryAddress, phone, dTime } = values;
      let errors = {};

      if (!deliveryAddress) errors.deliveryAddress = "Required";

      const isPhoneValid = validatePhoneNumber(phone);
      if (isPhoneValid) errors.phone = isPhoneValid;
      if (!dTime || dTime.trim() === "") errors.dTime = "Required";

      return errors;
    };
  } else {
    initialValues = {
      phone: phoneNumber,
      specialRequest: "",
      dTime: dTimeInitialValue,
      offCode: initialCouponCode,
      receive: isConfirmSms,
    };

    validate = (values) => {
      const { phone, dTime } = values;
      let errors = {};

      const isPhoneValid = validatePhoneNumber(phone);
      if (isPhoneValid) errors.phone = isPhoneValid;
      if (!dTime || dTime.trim() === "") errors.dTime = "Required";

      return errors;
    };
  }

  const formik = useFormik({
    initialValues: initialValues,
    validate: validate,
  });

  return formik;
};

export const recoveryPasswordFormikSetup = () => {
  const initialValues = {
    password: "",
    confirmPassword: "",
  };

  const initialErrors = {
    password: "Required",
  };

  const validate = ({ password, confirmPassword }) => {
    let errors = {};
    if (!password || password.trim() === "") errors.password = "Required";
    if (!confirmPassword || confirmPassword.trim() === "")
      errors.confirmPassword = "Required";
    else if (confirmPassword !== password)
      errors.confirmPassword = "Password and password confirm does not match";

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validate,
    initialErrors,
  });

  return formik;
};

export const orderReviewRatingFormikSetup = (rawQuestions) => {
  let initialValues = {};
  let initialErrors = {};

  for (let idx = 0; idx < rawQuestions.length; idx++) {
    const { idQ, isComplate, grade, note } = rawQuestions[idx];
    initialValues[idQ] = isComplate ? grade : 0;
    initialValues.note = note;
    if (isComplate) initialValues.isComplete = isComplate;
    else initialErrors[idQ] = "Required";
  }

  const validate = (values) => {
    let errors = {};

    for (const name in values) {
      if (Object.hasOwnProperty.call(values, name) && name !== "note") {
        if (!values[name] || values[name] <= 0) errors[name] = "Required";
        else if (values[name] > 5) errors[name] = "Should be less than 5";
      }
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    initialErrors,
    validate,
  });

  return formik;
};

export const loginFormikSetup = (requiredMessage, notValidEmailMessage) => {
  const initialValues = {
    email: "",
    password: "",
  };

  const validate = (values) => {
    const { email, password } = values;
    let errors = {};

    const isEmailValid = validateEmail(
      email,
      requiredMessage,
      notValidEmailMessage
    );
    if (isEmailValid) errors.email = isEmailValid;

    if (!password || password.trim() === "") errors.password = requiredMessage;
    // else if (password.length < 8)
    //   errors.password = "Must be at least 8 characters";

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validate,
    isInitialValid: false,
  });

  return formik;
};

export const signUpFormikSetup = (
  requiredMessage,
  notValidEmailMessage,
  phoneMinCharMessage,
  notValidPhoneMessage
) => {
  const initialValues = {
    name: "",
    email: "",
    phone: "",
    password: "",
  };
  const validate = (values) => {
    const { name, email, phone, password } = values;
    let errors = {};

    if (!name || name.trim() === "") errors.name = requiredMessage;

    //* validate email
    const isEmailValid = validateEmail(
      email,
      requiredMessage,
      notValidEmailMessage
    );
    if (isEmailValid) errors.email = isEmailValid;

    //* validate phone number
    const isPhoneValid = validatePhoneNumber(
      phone,
      requiredMessage,
      phoneMinCharMessage,
      notValidPhoneMessage
    );
    if (isPhoneValid) errors.phone = isPhoneValid;

    if (!password || password.trim() === "") errors.password = requiredMessage;
    // else if (password.length < 8)
    //   errors.password = "Must be at least 8 characters";

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validate,
    isInitialValid: false,
  });

  return formik;
};
