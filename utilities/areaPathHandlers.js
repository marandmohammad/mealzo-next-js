import jsCookie from "js-cookie";
import postCodeToLatLong from "./postCodeToLatLong";
import axiosConfig from "../config/axios";

/*
 * Constant Data
 */
export const saleMethods = {
  delivery: {
    code: 10012,
    title: "Delivery",
    shpDetailProperty: "saleMethodDelivery",
    iconAddress: "/Images/sale-method-icons/delivery-icon.png",
    wantedTimeLabel: "Delivery time",
    restaurantCardDifferentSaleMethodTitle: "Receive now!",
  },
  collection: {
    code: 10011,
    title: "Collection",
    shpDetailProperty: "saleMethodCollection",
    iconAddress: "/Images/sale-method-icons/collection-icon.png",
    wantedTimeLabel: "Collection time",
    restaurantCardDifferentSaleMethodTitle: "Collect now!",
  },
  // dine_in: {
  //   code: 10014,
  //   title: "Dine In",
  //   shpDetailProperty: "saleMethodEatin",
  //   iconAddress: "/Images/sale-method-icons/dine-in-icon.png",
  //   wantedTimeLabel: "Dine-In time"
  // },
};

//! Define Query Keywords
export const queryKeyWords = {
  postCodeQueryKeyWord: "area",
  categoryQueryKeyWord: "cuisine",
  sortQueryKeyWord: "so",
  offerQueryKeyWord: "offers",
  saleMethodQueryKeyWord: "sm",
  searchQueryKeyWord: "q",
};

export function queryParamEncode(param, paramKeyWord = null) {
  switch (paramKeyWord) {
    case queryKeyWords.postCodeQueryKeyWord:
      param = param.replace(/ /g, "_");
      break;
    case queryKeyWords.categoryQueryKeyWord:
      param = param.replace(/ /g, "-");
      param = param.replace(/&/g, "and");
      param = param.replace(/\?/g, "+");
      break;
    case queryKeyWords.sortQueryKeyWord:
      param = param.replace(/ /g, "_");
      param = param.replace(/%/g, "-");
      param = param.replace(/\?/g, "+");
      break;
    case queryKeyWords.offerQueryKeyWord:
    case queryKeyWords.saleMethodQueryKeyWord:
    default:
      param = param.replace(/ /g, "_");
      param = param.replace(/%/g, "-");
      param = param.replace(/&/g, "and");
      param = param.replace(/\?/g, "+");
      break;
  }

  return param;
}

export function queryParamDecode(param, paramKeyWord = null) {
  switch (paramKeyWord) {
    case queryKeyWords.postCodeQueryKeyWord:
      param = param.replace(/_/g, " ");
      break;
    case queryKeyWords.categoryQueryKeyWord:
      param = param.replace(/-/g, " ");
      param = param.replace(/and/g, "&");
      param = param.replace(/\+/g, "?");
      break;
    case queryKeyWords.sortQueryKeyWord:
      param = param.replace(/_/g, " ");
      param = param.replace(/-/g, "%");
      param = param.replace(/\+/g, "?");
      break;
    case queryKeyWords.offerQueryKeyWord:
    case queryKeyWords.saleMethodQueryKeyWord:
    default:
      param = param.replace(/_/g, " ");
      param = param.replace(/-/g, "%");
      param = param.replace(/and/g, "&");
      param = param.replace(/\+/g, "?");
      break;
  }

  return param;
}

/*
 * Change Handlers
 */
export const areaCategoryChangeHandler = async (
  router,
  categoryId,
  categoryTitle,
  categories,
  setCategories,
  paramKeyWord,
  setShops,
  shopLoading,
  setShopLoading
) => {
  let cuisineParams = [];
  const encodedCategoryTitle = queryParamEncode(categoryTitle, paramKeyWord);

  if (typeof router.query.cuisine !== "undefined") {
    const { cuisine } = router.query;
    const index = cuisine.indexOf(encodedCategoryTitle);

    if (typeof cuisine === "object") {
      if (index !== -1) {
        cuisineParams = [...cuisine];
        cuisineParams = cuisineParams.filter(
          (cus) => cus !== encodedCategoryTitle
        );
      } else cuisineParams = [...cuisine, encodedCategoryTitle];
    } else if (typeof cuisine === "string") {
      if (index !== -1) cuisineParams = [];
      else cuisineParams = [cuisine, encodedCategoryTitle];
    }
  } else cuisineParams.push(encodedCategoryTitle);

  const newQuery = {
    ...router.query,
    cuisine: cuisineParams,
  };

  await setCategories({ ...categories, [categoryId]: !categories[categoryId] });
  router.push(
    {
      pathname: router.pathname,
      query: newQuery,
    },
    undefined,
    { shallow: true }
  );

  await setShopLoading(true);
  const newShops = await changesRequest(newQuery);
  await setShops(newShops);
  await setShopLoading(false);
};

export const areaFilterChangeHandler = async (
  router,
  queryKeyWord,
  value,
  setState,
  data,
  setShops,
  shopLoading,
  setShopLoading
) => {
  const { title } = await data.filter(({ id }) => id === value)[0];

  const newQuery = {
    ...router.query,
    [queryKeyWord]: queryParamEncode(title, queryKeyWord),
  };

  await setState(value);
  router.push(
    {
      pathname: router.pathname,
      query: newQuery,
    },
    undefined,
    { shallow: true }
  );

  await setShopLoading(true);
  const newShops = await changesRequest(newQuery);
  await setShops(newShops);
  await setShopLoading(false);
};

export const areaFilterAndSearchReset = async (
  router,
  queryKeyWord,
  setState,
  setShops,
  shopLoading,
  setShopLoading,
  categoryData = null
) => {
  const routerQuery = {
    ...router.query,
  };
  delete routerQuery[queryKeyWord];

  router.push(
    {
      pathname: router.pathname,
      query: routerQuery,
    },
    undefined,
    { shallow: true }
  );

  if (queryKeyWord !== "cuisine") setState("");
  else {
    const categoryResetState = {};
    await categoryData.forEach(({ id }) => (categoryResetState[id] = false));
    await setState(categoryResetState);
  }

  await setShopLoading(true);
  const newShops = await changesRequest(routerQuery);
  await setShops(newShops);
  await setShopLoading(false);
};

export const saleMethodAndSearchChangeHandler = async (
  router,
  queryKeyWord,
  value,
  setState,
  setShops,
  shopLoading,
  setShopLoading
) => {
  const newQuery = await {
    ...router.query,
    [queryKeyWord]: queryParamEncode(value),
  };
  await setState(value);
  router.push(
    {
      pathname: router.pathname,
      query: newQuery,
    },
    undefined,
    { shallow: true }
  );

  await setShopLoading(true);
  const newShops = await changesRequest(newQuery);
  await setShops(newShops);
  await setShopLoading(false);
};

export const addressChangeHandler = async (
  router,
  queryKeyWord,
  value,
  setShops,
  setShopLoading
) => {
  const postCode =
    typeof value.DeliveryPostCode !== "undefined"
      ? value.DeliveryPostCode
      : value.deliveryPostcode;

  const newQuery = await {
    ...router.query,
    [queryKeyWord]: queryParamEncode(postCode),
  };

  router.push(
    {
      pathname: router.pathname,
      query: newQuery,
    },
    undefined,
    { shallow: true }
  );

  await setShopLoading(true);
  const newShops = await changesRequest(newQuery);
  await setShops(newShops);
  await setShopLoading(false);
};

/*
 * Initial Data Handlers
 */
export const getCategoriesInitialState = (
  queryParams,
  categoriesData,
  paramKeyWord
) => {
  const categoryInitialState = {};

  switch (typeof queryParams.cuisine) {
    case "object":
      categoriesData.forEach(({ id, title }) => {
        queryParams.cuisine.forEach((cus) => {
          if (
            title.toLowerCase() ===
            queryParamDecode(cus, paramKeyWord).toLowerCase()
          ) {
            categoryInitialState[id] = true;
          } else {
            if (typeof categoryInitialState[id] === "undefined")
              categoryInitialState[id] = false;
          }
        });
      });
      break;

    case "string":
      categoriesData.forEach(({ id, title }) => {
        if (
          title.toLowerCase() ===
          queryParamDecode(queryParams.cuisine, paramKeyWord).toLowerCase()
        )
          categoryInitialState[id] = true;
        else categoryInitialState[id] = false;
      });
      break;

    default:
      categoriesData.forEach(({ id }) => {
        categoryInitialState[id] = false;
      });
      break;
  }

  return categoryInitialState;
};

export const getFilterInitialState = (
  queryParams,
  queryKeyWord,
  filterData,
  paramKeyWord
) => {
  let filterInitialState = null;

  if (typeof queryParams[queryKeyWord] === "string") {
    filterData.forEach(({ id, title }) => {
      if (
        title.toLowerCase() ===
        queryParamDecode(queryParams[queryKeyWord], paramKeyWord).toLowerCase()
      ) {
        filterInitialState = id;
        return filterInitialState;
      }
    });
  }

  return filterInitialState;
};

export const organizeAreaInitialData = (pageData) => {
  const categories = [...pageData.countOfSubjectList];
  categories.sort((a, b) => a.countOfSubject > b.countOfSubject); //categoriesData

  const sort = pageData.countOfOfferList.filter(
    ({ filterGroup }) => filterGroup.toLowerCase() === "sort"
  );
  const offers = pageData.countOfOfferList.filter(
    ({ filterGroup }) => filterGroup.toLowerCase() === "offers"
  );
  const promotions = pageData.promotionList.filter(
    ({ groupName }) => groupName.toLowerCase() === "web"
  );
  const shops = pageData.shopList;

  return [categories, sort, offers, promotions, shops];
};

export const getPrevQueryParamsInString = (query, postCodeQueryKeyWord) => {
  let prevQueryParamsString = "";
  const queryParams = { ...query };
  delete queryParams[postCodeQueryKeyWord];

  const prevQueryParamsKeys = Object.keys(queryParams);
  if (prevQueryParamsKeys.length > 1) {
    prevQueryParamsKeys.forEach((key) => {
      prevQueryParamsString += `&${key}=${queryParams[key]}`;
    });
  }

  return prevQueryParamsString;
};

/*
 * Utilities
 */
export const objectKeyValueToUrlQueryParam = (object) => {
  const objectKeys = Object.keys(object);

  if (objectKeys.length <= 0) return "";

  let queryString = "";
  objectKeys.forEach((key) => {
    queryString += `&${key}=${object[key]}`;
  });

  return queryString;
};

export const changesRequest = async (routeQuery) => {
  let initialRequestParameters = {};

  // Get Decoded Post Code
  const postCode = queryParamDecode(
    routeQuery[queryKeyWords.postCodeQueryKeyWord],
    queryKeyWords.postCodeQueryKeyWord
  );

  // Set post code
  initialRequestParameters["PostCode"] = postCode;
  const locationDetailCookie = jsCookie.get("LocationDetail");

  // Validate and set location details
  if (typeof locationDetailCookie !== "undefined") {
    if (postCode !== "all") {
      const { Lat, Lon } = JSON.parse(locationDetailCookie);

      initialRequestParameters["La"] = Lat;
      initialRequestParameters["Lo"] = Lon;
    }
  } else {
    if (postCode !== "all") {
      const [Lat, Lon] = await postCodeToLatLong(postCode);
      const locationDetail = {
        DeliveryPostCode: postCode,
        Lat: Lat,
        Lon: Lon,
      };

      jsCookie.set("LocationDetail", JSON.stringify(locationDetail));
      initialRequestParameters["La"] = Lat;
      initialRequestParameters["Lo"] = Lon;
    }
  }

  // Set sale method
  initialRequestParameters["SaleMethod"] =
    saleMethods[routeQuery[queryKeyWords.saleMethodQueryKeyWord]].code;

  // Validate sort
  if (typeof routeQuery[queryKeyWords.sortQueryKeyWord] === "string")
    initialRequestParameters["Sort"] = queryParamDecode(
      routeQuery[queryKeyWords.sortQueryKeyWord]
    );

  // Validate offer
  if (typeof routeQuery[queryKeyWords.offerQueryKeyWord] === "string")
    initialRequestParameters["Offer"] = queryParamDecode(
      routeQuery[queryKeyWords.offerQueryKeyWord]
    );

  // Validate and set searchText
  if (
    typeof routeQuery[queryKeyWords.searchQueryKeyWord] === "string" &&
    routeQuery[queryKeyWords.searchQueryKeyWord].trim() !== ""
  )
    initialRequestParameters["SearchText"] = queryParamDecode(
      routeQuery[queryKeyWords.searchQueryKeyWord]
    );

  // Validate and set category list ( Cuisines )
  if (typeof routeQuery[queryKeyWords.categoryQueryKeyWord] === "object")
    initialRequestParameters["SubjectList"] = [
      ...routeQuery[queryKeyWords.categoryQueryKeyWord],
    ]
      .map((cuisine) =>
        queryParamDecode(cuisine, queryKeyWords.categoryQueryKeyWord)
      )
      .join(";");
  else if (typeof routeQuery[queryKeyWords.categoryQueryKeyWord] === "string")
    initialRequestParameters["SubjectList"] = queryParamDecode(
      routeQuery[queryKeyWords.categoryQueryKeyWord],
      queryKeyWords.categoryQueryKeyWord
    );

  // Map Object To String
  const requestQueryParameter = objectKeyValueToUrlQueryParam(
    initialRequestParameters
  );

  const newShops = (
    await axiosConfig.get(`/AreaShops?${requestQueryParameter}`)
  ).data.shopList;

  return newShops;
};

export const areaHeadPropertiesHandler = (
  postCode,
  cuisineCount,
  pageData,
  initialRequestParameters
) => {
  let title = "Order from the best takeaways & restaurants near you";
  let description = "";
  let pageKeywords = "";

  if (postCode === "all" && cuisineCount > 0) {
    const firstCuisine =
      cuisineCount === 1
        ? initialRequestParameters["SubjectList"]
        : initialRequestParameters["SubjectList"].splice(";")[0];
    title = `${firstCuisine} Takeaways & Restaurants Near Me | Mealzo`;
    description = `Find the best ${firstCuisine} Takeaways & Restaurants near you on Mealzo. Order from Mealzo App or Website & Enjoy up to 50% exclusive discounts.`;
  }
  if (postCode !== "all" && cuisineCount === 0) {
    if (typeof pageData.postCodeComplet === "undefined") {
      title = "The Best Takeaways & Restaurants near me | Order from Mealzo";
      description =
        "Find the best Takeaways & Restaurants near me with Mealzo. Order from Mealzo App or Website & Enjoy up to 50% exclusive discounts.";
    } else {
      title = `The Best Takeaways & Restaurants in ${pageData.postCodeComplet} | Order from Mealzo`;
      description = `Find the best Takeaways & Restaurants in ${pageData.postCodeComplet} with Mealzo. Order from Mealzo App or Website & Enjoy up to 50% exclusive discounts.`;
    }
  } else if (postCode !== "all" && cuisineCount > 0) {
    const firstCuisine =
      cuisineCount === 1
        ? initialRequestParameters["SubjectList"]
        : initialRequestParameters["SubjectList"].splice(";")[0];
    title = `${firstCuisine} Delivery & Takeaway `;

    description = `"Find the Best ${firstCuisine} Delivery and Takeaway `;
    if (typeof pageData.postCodeComplet === "undefined") {
      title += "near me | Order On Mealzo";
      description +=
        "near me with Mealzo. Order from Mealzo App or Website & Enjoy up to 50% exclusive discounts.";
    } else {
      title = `in ${pageData.postCodeComplet} | Order On Mealzo`;
      description = `in ${pageData.postCodeComplet} with Mealzo. Order from Mealzo App or Website & Enjoy up to 50% exclusive discounts.`;
    }
  }

  if (typeof pageData.subjectKeyWord !== "undefined" && cuisineCount > 0)
    pageKeywords = pageData.subjectKeyWord;

  return [title, description, pageKeywords];
};
