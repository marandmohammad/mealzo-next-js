import axios from "axios";

export default async function postCodeToLatLong(postCode) {
  const response = await axios.get(
    `http://dev.virtualearth.net/REST/v1/Locations?postalCode=${postCode}&key=Atvez4o6Knc2bEEkeI2Vdxbysbk8JC-5yLFdohTMrKAg4HwVtaSYgz8rowEpjEW-`
  );

  let lat = await response.data.resourceSets[0].resources[0].point
    .coordinates[0];
  let long = await response.data.resourceSets[0].resources[0].point
    .coordinates[1];

  return [lat, long];
}
