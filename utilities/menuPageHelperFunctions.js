import { useFormik } from "formik";
import axiosConfig, { localeHeaderConfig } from "../config/axios";
import { saleMethodChangeStatusCodes } from "./appWideHelperFunctions";

export const calcTotalPrice = (subMenu, item, formik) => {
  const values = formik.values;
  let totalCount = values["counter"];
  if (totalCount === 0) return 0;

  const { product, optionId } = item;
  const { amount } = product;

  let productTotal = optionId
    ? product.options.filter(({ optionId }) => optionId === item.optionId)[0]
        .optionAmount
    : amount;
  let subMenuTotal = 0;

  if (subMenu.length > 0) {
    const names = Object.keys(values);
    let totalInputPrice = 0;
    let totalCrustPrice = 0;

    names.forEach((name) => {
      if (name === "crust") {
        const currentCrust = subMenu.filter(({ isCrust }) => isCrust !== 0)[0];
        currentCrust.subMenuItems.forEach(({ id, amount }) => {
          if (id === parseInt(values[name])) totalCrustPrice += amount;
        });
      }

      if (name !== "crust" && name !== "counter") {
        const [nameSubMenuId, nameSubMenuItemId] = name
          .split("-")
          .map((el) => parseInt(el));

        const currentSubMenu = subMenu.filter(
          ({ subMenuId }) => subMenuId === nameSubMenuId
        )[0];

        currentSubMenu.subMenuItems.forEach(({ id, amount }) => {
          if (id === nameSubMenuItemId)
            totalInputPrice += values[name] * amount;
        });
      }
    });
    subMenuTotal = totalInputPrice + totalCrustPrice;
  }
  return (productTotal + subMenuTotal) * totalCount;
};

export const setupInitialFormikConfig = (subMenu) => {
  // Initial Values
  let initialValues = {};
  subMenu.forEach(
    ({ isCrust, subMenuId, subMenuItems, minSelect, maxSelect }) => {
      if (isCrust !== 0) {
        initialValues["crust"] = null;
      } else if (minSelect === 1 && maxSelect === 1) {
        initialValues[`${subMenuId}`] = "";
      } else {
        subMenuItems.forEach(({ id }) => {
          initialValues[`${subMenuId}-${id}`] = 0;
        });
      }
    }
  );
  initialValues["counter"] = 1;

  // Initial Errors
  let initialErrors = {};
  subMenu.forEach(
    ({ isCrust, subMenuId, subMenuItems, minSelect, maxSelect }) => {
      if (isCrust !== 0) {
        initialErrors["crust"] = "Required";
      } else if (minSelect === 1 && maxSelect === 1) {
        initialErrors[`${subMenuId}`] = "Required";
      } else {
        subMenuItems.forEach(({ id }) => {
          initialErrors[subMenuId] = "Required";
        });
      }
    }
  );

  // Validation Functionality
  const validate = (values) => {
    let errors = {};
    const names = Object.keys(values);

    names.forEach((name) => {
      // if (name === "crust") {
      //   if (!values[name]) errors[name] = "Required";
      // } else
      if (name === "counter") {
        if (!values[name] || values[name] < 1) errors[name] = "Required";
      }

      if (name !== "crust" && name !== "counter") {
        const [nameSubMenuId, nameSubMenuItemId] = name
          .split("-")
          .map((el) => parseInt(el));
        const { minSelect, maxSelect } = subMenu.find(
          ({ subMenuId }) => subMenuId === nameSubMenuId
        );
        let itemsCount = 0;

        names.forEach((lowerName) => {
          if (
            lowerName !== "crust" &&
            lowerName !== "counter" &&
            !(minSelect === 0 && maxSelect === 0)
          ) {
            const [lowerSMI, lowerSMII] = lowerName
              .split("-")
              .map((el) => parseInt(el));
            if (nameSubMenuId === lowerSMI) itemsCount += values[lowerName];
          }
        });

        if (!(minSelect === 0 && maxSelect === 0)) {
          if (minSelect === 1 && maxSelect === 1) {
            if (!values[nameSubMenuId] || values[nameSubMenuId].trim() === "")
              errors[nameSubMenuId] = "Required";
          } else if (minSelect !== 0 && maxSelect === 0) {
            if (itemsCount < minSelect)
              errors[nameSubMenuId] = "Must be in range";
          } else if (itemsCount < minSelect || itemsCount > maxSelect)
            errors[nameSubMenuId] = "Must be in range";
        }
      }
    });

    return errors;
  };

  const formik = useFormik({
    initialValues: initialValues,
    initialErrors: initialErrors,
    validate: validate,
    validateOnMount: true,
  });

  return formik;
};

export const detectInBasketProducts = (products, basket) => {
  const newProducts = [...products];

  if (!basket || basket.orderDetail.length <= 0) {
    products.forEach((category, categoryIdx) => {
      return category.products.forEach((product, productIdx) => {
        const { id, options } = product;

        if (options.length > 0 && options[0] !== null) {
          options.forEach((option, optionIdx) => {
            newProducts[categoryIdx].products[productIdx].options[
              optionIdx
            ].inBasket = false;
          });
        } else {
          newProducts[categoryIdx].products[productIdx].inBasket = false;
        }
      });
    });

    return newProducts;
  }

  const { orderDetail } = basket;

  products.forEach((category, categoryIdx) => {
    return category.products.forEach((product, productIdx) => {
      const { id, options } = product;
      let upperIsSetFlag = false;

      if (options.length > 0 && options[0] !== null) {
        options.forEach((option, optionIdx) => {
          let isSetFlag = false;
          orderDetail.forEach((basketItem) => {
            if (basketItem.optioId !== 0) {
              if (
                basketItem.proudctId === id &&
                basketItem.optioId === option.optionId
              ) {
                newProducts[categoryIdx].products[productIdx].options[
                  optionIdx
                ].inBasket = true;
                newProducts[categoryIdx].products[productIdx].options[
                  optionIdx
                ].basketCount = basketItem.count;
                isSetFlag = true;
              } else if (!isSetFlag) {
                newProducts[categoryIdx].products[productIdx].options[
                  optionIdx
                ].inBasket = false;
                newProducts[categoryIdx].products[productIdx].options[
                  optionIdx
                ].basketCount = 0;
              }
            } else if (!isSetFlag) {
              newProducts[categoryIdx].products[productIdx].options[
                optionIdx
              ].inBasket = false;
              newProducts[categoryIdx].products[productIdx].options[
                optionIdx
              ].basketCount = 0;
            }
          });
        });
      } else {
        orderDetail.forEach((basketItem) => {
          if (basketItem.proudctId === id && !(basketItem.optioId !== 0)) {
            newProducts[categoryIdx].products[productIdx].inBasket = true;
            newProducts[categoryIdx].products[productIdx].basketCount =
              basketItem.count;
            upperIsSetFlag = true;
          } else if (!upperIsSetFlag) {
            newProducts[categoryIdx].products[productIdx].inBasket = false;
            newProducts[categoryIdx].products[productIdx].basketCount = 0;
          }
        });
      }
    });
  });

  return newProducts;
};

export const addPopularItemCategoryToProduct = (pageData) => {
  const popularItems = [];
  let numberOfPush = 0;

  //* Filter Data
  pageData.forEach(({ products }) => {
    if (numberOfPush < 8)
      products.forEach((el) => {
        const { isPopular, options } = el;
        if (!isPopular) return;

        if (options.length > 0 && options[0] !== null) {
          const rawOption = [...options];
          let replaceOptionArray = [];

          if (rawOption.length === 1) {
            replaceOptionArray = [rawOption[0]];
          } else {
            const orderedOptions = rawOption.sort(
              (a, b) => a.optionAmount < b.optionAmount
            );
            replaceOptionArray = [orderedOptions[0]];
          }

          popularItems.push({ ...el, options: replaceOptionArray });
          numberOfPush++;
        } else {
          popularItems.push(el);
          numberOfPush++;
        }
      });
    else return;
  });

  //* Create Category For Popular Items
  const popularItemsCategory = [
    {
      categoryId: "popularItems",
      categoryTitle: "Popular Items",
      products: popularItems,
    },
  ];

  //* Add Popular Items To The Products
  const newPageData =
    popularItems.length > 0 ? popularItemsCategory.concat(pageData) : pageData;

  return newPageData;
};

export const getShopMenuByIdentity = async (mealzoIdentity, saleMethod, locale) => {
  const pageData = await (
    await axiosConfig.get(`/Menu/GetByIdentity/${mealzoIdentity}/${saleMethod}`, localeHeaderConfig(locale))
  ).data;
  return pageData;
};

export const bundleGetMenuWithPopularItemsCategory = async (
  mealzoIdentity,
  saleMethod,
  locale,
) => {
  // Fetch Products
  const pageData = await getShopMenuByIdentity(mealzoIdentity, saleMethod, locale);

  // Get Popular Items
  const newPageData = addPopularItemCategoryToProduct(pageData);

  return newPageData;
};

export const changeSaleMethodStatusHandler = async (
  callbackObject,
  shopId,
  saleMethod,
  postCode = "all",
  orderId = 0
) => {
  const { OK_SHOP, CLOSE_SHOP, NO_DELIVERY_SHOP, PRE_ORDER_SHOP } =
    saleMethodChangeStatusCodes;
  const requestData = {
    shopId,
    orderId,
    saleMethod,
    postCode,
  };
  try {
    const result = await axiosConfig.patch(
      "/Basket/ChangeSaleMethod",
      requestData
    );

    if (
      typeof result.data.status.statusCode !== "undefined" &&
      result.data.status.statusCode === PRE_ORDER_SHOP
    ) {
      callbackObject[PRE_ORDER_SHOP](result.data);
    } else if (result.status === OK_SHOP) {
      callbackObject[OK_SHOP](result.data);
    } else {
      alert("Unsupported response status");
    }

    // switch (result.data.statusCode) {
    //   case OK_SHOP:
    //     callbackObject[OK_SHOP](result.data);
    //     break;
    //   case PRE_ORDER_SHOP:
    //     callbackObject[PRE_ORDER_SHOP](result.data);
    //     break;
    //   default:
    //     alert("Unsupported response status");
    // }
  } catch (e) {
    const { response, message } = e;
    switch (response.status) {
      case CLOSE_SHOP:
        callbackObject[CLOSE_SHOP](response.data);
        break;
      case NO_DELIVERY_SHOP:
        callbackObject[NO_DELIVERY_SHOP](response.data);
        break;
      default:
        alert(message);
    }
  }
};
