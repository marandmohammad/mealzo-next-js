export default {
  RecommendOptions: [
    {
      title: "Find your local flavours",
      subtitle:
        "Choose from local to national favourites. No matter what food you are in the mood for, we’ve got you covered.",
      imageUrl: "/Images/yourlocal.svg",
      buttonText: "Explore",
      href: "/local-flavours",
    },
    {
      title: "Special Perks",
      subtitle:
        "Explore a world of exclusive offers and discounts near you. Saving you money on every order.",
      imageUrl: "/Images/SpecialPerks.svg",
      buttonText: "Read more",
      href: "/Special-Perks",
    },
    {
      title: "Get the App",
      subtitle:
        "Download our free IOS and Android App and enjoy a seamless ordering journey.",
      imageUrl: "/Images/GettheApp1.svg",
      buttonText: "Download the App",
      href: '/Apps',
    },
  ],
};
