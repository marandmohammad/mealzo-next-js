const BecamePartnerData = [
  {
    title: "Branded Business Website and App",
    text: "A business website and an app for both iOS and Android tailored to suit your business needs and getting your message across. Optimised to promote your brand effectively by utilising search engine algorithms to make your name stand out online.",
    iconUrl: "/Images/become-partner/branded-business.png",
  },
  {
    title: "Mealzo Order Pad",
    text: "Order and payment processing made easy with a slim-line easy-to-use portable device. Manage orders by using the full HD touch screen to keep track of current orders, delivery drivers and much more. Takes care of all receipt and order printing while being integrated with your own website.",
    iconUrl: "/Images/become-partner/order-pad.png",
  },
  {
    title: "Money Management App",
    text: "An app designed to keep track of all your orders and transactions connected to your 3D secure payment gateway providing you with accurate updates and seamless withdrawals. You will be able to use our analysis tools to identify your best-selling products and offer personalized promotions increasing your revenue.",
    iconUrl: "/Images/become-partner/money-management.png",
  },
  {
    title: "QR Table Ordering",
    text: "Allows customers to view menus on their iOS and android device using QR technology. This reduces the time taken up through the ordering process, and lets customers pay directly and securely using their mobile device.",
    iconUrl: "/Images/become-partner/qr.png",
  },
  {
    title: "Social Media Management Team",
    text: "Our team of marketing experts will launch social media campaigns aimed at promoting your brand. At Mealzo, we are dedicated to making your brand stand out online and attracting new customers in creative ways using online trends and ad campaigns.",
    iconUrl: "/Images/become-partner/social-media.png",
  },
  {
    title: "Promotional Package for Business",
    text: "Mealzo promotional flyers (worth £1500) Allergy Awareness chart menu holders Push & Pull door signs Open & Closed signs.",
    iconUrl: "/Images/become-partner/promotional-package.png",
  },
  {
    title: "SEO to market your business name effectively",
    text: "We use enhanced search engine techniques to ensure your brand is given the exposure it deserves. By utilising key words associated with your business, we can blend these words into your online presence and give you an edge over your competition.",
    iconUrl: "/Images/become-partner/search-engine-optimization.png",
  },
  {
    title: "24/7 Technical Support",
    text: "Enjoy access to our free 24/7 customer service team including IT engineers and marketing consultants with live chat also available. Our friendly team of experts are available for any assistance our partners may need.",
    iconUrl: "/Images/become-partner/technical-support.png",
  },
  {
    title: "Secure Payment Gateways",
    text: "Online food ordering has never been so easy and secure, we offer a variety of payment methods from accepting all major cards, to having Apple Pay and Google Pay on our portal.",
    iconUrl: "/Images/become-partner/payment.png",
  },
];

export default BecamePartnerData;
