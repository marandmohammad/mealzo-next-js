import axios from "axios";

import {
  getUserAccessToken,
  getLocaleFromCookie,
} from "../utilities/appWideHelperFunctions";

const axiosConfig = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
  headers: {
    "content-type": "application/json; charset=utf-8",
    "Cache-Control": "no-cache",
    Pragma: "no-cache",
    Expires: "0",
  },
});

axiosConfig.interceptors.request.use(
  async (config) => {
    const locale = getLocaleFromCookie();
    if (locale) config.headers["language"] = locale;

    const token = getUserAccessToken();
    if (token) config.headers["Authorization"] = "Bearer " + token;

    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default axiosConfig;

export const localeHeaderConfig = (locale) => ({
  headers: {
    language: locale,
  },
});
